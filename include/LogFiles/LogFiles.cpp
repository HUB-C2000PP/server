//
// Copyright Kuzminov Sergey (kuzminov_sergey@mail.ru) 2021
//
// Distributed under the Unlicense License. (See accompanying
// file LICENSE_1_0.txt or copy at http://unlicense.org/)
//

#include "LogFiles.h"
#include <time.h>
#include <stdio.h>
#include <stdlib.h>

//---------------------------------------------------------------------------------------
//Для корректного завершения работы консольной программы.
//---------------------------------------------------------------------------------------
#ifdef BUILD_FOR_CONSOLE
#if defined(Q_CC_MSVC)
#include <qt_windows.h>
static BOOL WINAPI consoleCtrlHandlerRoutine(__in  DWORD dwCtrlType){Q_UNUSED(dwCtrlType); QCoreApplication::quit(); return TRUE; }
void installCtrlCHandler(){SetConsoleCtrlHandler(&consoleCtrlHandlerRoutine, TRUE);}
#else
#include <csignal>
static void sigIntHandler(int sig){QCoreApplication::quit(); Q_UNUSED(sig)}
void installCtrlCHandler(){
    signal(SIGINT,  sigIntHandler);
    signal(SIGTERM, sigIntHandler);
#if !defined(Q_OS_WIN)
    signal(SIGKILL, sigIntHandler);
#endif
}
#endif
#endif //BUILD_FOR_CONSOLE
//---------------------------------------------------------------------------------------
#ifndef QT_NO_DEBUG_OUTPUT
logfile logfiles::fileDebug;
#endif

#ifndef QT_NO_WARNING_OUTPUT
logfile logfiles::fileWarning;
#endif

#ifndef QT_NO_INFO_OUTPUT
logfile logfiles::fileInfo;
#endif

logfile logfiles::fileCritical;
logfile logfiles::fileFatal;

//---------------------------------------------------------------------------------------
logfile::logfile()
{
}
//---------------------------------------------------------------------------------------
logfile::~logfile()
{
    if (pfile != nullptr) {
        fclose(pfile);
        pfile=nullptr;
    }
}
//---------------------------------------------------------------------------------------
void logfile::open(QString fileName)
{
    baFileName = fileName.toUtf8();

    if (pfile == nullptr)
    {
#ifdef Q_CC_MSVC
        fopen_s( &pfile, baFileName.constData(), "a" );
#else
        pfile = fopen( baFileName.constData(), "a" );
#endif
    }
}
//---------------------------------------------------------------------------------------
void logfile::outlog(const QString &msg)
{
    if (pfile != nullptr)
    {
        time_t rawtime;
        time ( &rawtime );

#ifdef Q_CC_MSVC
        struct tm t;
        localtime_s(&t, &rawtime);
        fprintf(pfile, "[%.2d.%.2d.%.2d, %.2d:%.2d:%.2d] %s\n", t.tm_mday, t.tm_mon+1, t.tm_year-100, t.tm_hour, t.tm_min, t.tm_sec, msg.toUtf8().constData());
#else
        struct tm *t = localtime(&rawtime);
        fprintf(pfile, "[%.2d.%.2d.%.2d, %.2d:%.2d:%.2d] %s\n", t->tm_mday, t->tm_mon+1, t->tm_year-100, t->tm_hour, t->tm_min, t->tm_sec, msg.toUtf8().constData());
#endif
        fflush(pfile);

        if (ftell(pfile) > 1024*1024)
        {
            rename(baFileName.constData() , (baFileName + QByteArray(".bak")).constData());
            fclose(pfile); pfile=nullptr;
            open();
        }
    }
}

//---------------------------------------------------------------------------------------
void logfiles::myMessageOutput(QtMsgType type, const QMessageLogContext &context, const QString &msg)
{
    Q_UNUSED(context)

    if ( type == QtCriticalMsg ) {
        fprintf(stderr, "Critical: %s\n", msg.toLocal8Bit().constData());
        fileCritical.outlog(msg);

#ifndef BUILD_FOR_CONSOLE
        {
            QApplication *qapp = qobject_cast<QApplication *>(qApp);
            if (qapp) QMessageBox::critical(qapp->activeWindow(), QStringLiteral(u"Ошибка в %1").arg(QCoreApplication::applicationName()), msg);
        }
#endif
    }

    else if ( type == QtFatalMsg) {
        fprintf(stderr, "Fatal: %s\n", msg.toLocal8Bit().constData());
        fileFatal.outlog(msg);

#ifndef BUILD_FOR_CONSOLE
        {
            QApplication *qapp = qobject_cast<QApplication *>(qApp);
            if (qapp) QMessageBox::critical(qapp->activeWindow(), QStringLiteral(u"Фатальная ошибка в %1").arg(QCoreApplication::applicationName()), msg);
        }
#endif
        QCoreApplication::exit(-1);
    }

#ifndef QT_NO_INFO_OUTPUT
    else if ( type == QtInfoMsg ) {
        fprintf(stderr, "Info: %s\n", msg.toLocal8Bit().constData());
        fileInfo.outlog(msg);
    }
#endif

#ifndef QT_NO_DEBUG_OUTPUT
    else if ( type == QtDebugMsg ) {
        fprintf(stderr, "Debug: %s\n", msg.toLocal8Bit().constData());
        fileDebug.outlog(msg);
    }
#endif

#ifndef QT_NO_WARNING_OUTPUT
    else if ( type == QtWarningMsg ) {
        fprintf(stderr, "Warning: %s\n", msg.toLocal8Bit().constData());
        fileWarning.outlog(msg);
    }
#endif
}

//---------------------------------------------------------------------------------------
void logfiles::init ()
{

#ifdef BUILD_FOR_CONSOLE
    //Для корректного завершения работы консольной программы.
    installCtrlCHandler();
#endif

    QString dirName = LogFile::dirName();
    LogFile::mkpath(dirName);

#ifndef QT_NO_DEBUG_OUTPUT
    fileDebug.open( QStringLiteral("%1/Debug.log").arg(dirName) );
#endif

#ifndef QT_NO_WARNING_OUTPUT
    fileWarning.open( QStringLiteral("%1/Warning.log").arg(dirName) );
#endif

#ifndef QT_NO_INFO_OUTPUT
    fileInfo.open( QStringLiteral("%1/Info.log"    ).arg(dirName) );
#endif

    fileCritical.open( QStringLiteral("%1/Critical.log").arg(dirName) );
    fileFatal.   open( QStringLiteral("%1/Fatal.log"   ).arg(dirName) );

    qInstallMessageHandler(myMessageOutput);
}

//---------------------------------------------------------------------------------------
// Отдельный простой класс для для работы с log-файлами.
//---------------------------------------------------------------------------------------
LogFile::~LogFile()
{
    if (strList!=nullptr){
        delete strList;
        strList = nullptr;
    }
}
//---------------------------------------------------------------------------------------
QString LogFile::dirName(void)
{
    return QStringLiteral("%1/log/%2").arg(mainPath, QCoreApplication::applicationName());
}
//---------------------------------------------------------------------------------------
void LogFile::mkpath(QString dirName)
{
    QDir dir;
    if (!dir.exists(dirName)) dir.mkpath(dirName);
}
//---------------------------------------------------------------------------------------
void LogFile::setFileName(const QString &str, bool logRead)
{
    if ( !file.isOpen() )
    {
        bLogRead = logRead;

        QFileInfo fileinfo( QStringLiteral("%1/%2").arg( dirName(), str) );
        mkpath( fileinfo.absolutePath() );

        file.setFileName( fileinfo.absoluteFilePath() );
        openFile();

        if (bLogRead) {
            strList = new QStringList();

            if (file.isOpen())
            {
                file.seek(0);

                int n=0;
                while(!file.atEnd() && (++n<100000)){
                    strList->append( QString::fromUtf8( file.readLine().simplified() ) );
                }
            }
        }
    }
}
//---------------------------------------------------------------------------------------
void LogFile::openFile(void)
{
    if ( !file.isOpen() ){
        if (bLogRead) file.open(QFile::ReadWrite | QIODevice::Append | QIODevice::Text);
        else file.open(QFile::WriteOnly | QIODevice::Append | QIODevice::Text);
    }
}
//---------------------------------------------------------------------------------------
void LogFile::writeLog(QString str, bool bDateAndTime)
{
    if (file.isOpen())
    {
        str=str.simplified();
        if (bDateAndTime){
            str = QStringLiteral("[%1, %2] %3").arg(
                QDate::currentDate().toString(QStringLiteral("dd.MM.yy")),
                QTime::currentTime().toString(),
                str);
        }

        if (bLogRead) strList->append(str);

        str.append(QLatin1Char('\n'));
        file.write( str.toUtf8() );

        file.flush();

        if ( file.size()> maxSize )
        {
            QString fileName = file.fileName();
            QString backName = fileName + QStringLiteral(".bak");
            QFile::remove( backName );
            file.rename( backName );
            file.close();
            setFileName( QFileInfo(fileName).fileName() );
            openFile();
            if (bLogRead) strList->clear();
        }
    }
}
//---------------------------------------------------------------------------------------
void LogFile::clear(void)
{
    if (file.isOpen()) file.resize(0);

    QFile::remove( file.fileName() + QStringLiteral(".bak") );

    if (bLogRead) strList->clear();
}
