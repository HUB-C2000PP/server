//
// Copyright Kuzminov Sergey (kuzminov_sergey@mail.ru) 2021
//
// Distributed under the Unlicense License. (See accompanying
// file LICENSE_1_0.txt or copy at http://unlicense.org/)
//

#ifndef LOGFILES_H
#define LOGFILES_H

#include <QtCore>

extern QString mainPath;
extern const QDataStream::Version dataStreamVersion;

#ifndef BUILD_FOR_CONSOLE
#include <QtWidgets/QMessageBox>
#include <QtWidgets/QApplication>
#endif

//---------------------------------------------------------------------------------------
// Вспомогательный класс для записи в файл. В данном случае нельзя использовать QFile,
// т.к. этот и аналогичные классы в процессе своей работы могут сгенерировать qWarning,
// что в данном случае может привести к зацикливанию.
//---------------------------------------------------------------------------------------
class logfile
{
private:
    FILE *pfile = nullptr;
    QByteArray baFileName;

public:
    logfile();
    ~logfile();
    void open(QString fileName = QString());
    void outlog(const QString &msg);
};

//---------------------------------------------------------------------------------------
// Для переопределения встроенных в Qt методов:
// qDebug(). qWarning(), qInfo(), qCritical(), qFatal().
//---------------------------------------------------------------------------------------
class logfiles
{
public:
    static void init ();
    static void myMessageOutput(QtMsgType type, const QMessageLogContext &context, const QString &msg);

private:
#ifndef QT_NO_DEBUG_OUTPUT
    static logfile fileDebug;
#endif

#ifndef QT_NO_WARNING_OUTPUT
    static logfile fileWarning;
#endif

#ifndef QT_NO_INFO_OUTPUT
    static logfile fileInfo;
#endif

    static logfile fileCritical;
    static logfile fileFatal;
};

//---------------------------------------------------------------------------------------
// Отдельный простой класс для для работы с log-файлами.
//---------------------------------------------------------------------------------------
class LogFile
{
private:
    QFile file;
    inline void openFile(void);

    int maxSize;
    QStringList *strList = nullptr;
    bool bLogRead = false;

public:
    LogFile(int maxSize=1024*1024) {this->maxSize=maxSize;};
    ~LogFile();
    static QString dirName(void);
    static void mkpath(QString dirName);

    void setFileName(const QString &str, bool logRead=false);
    void writeLog(QString str, bool bDateAndTime = true);
    void clear(void);

    int countMsg(void) {return bLogRead ? strList->count() : 0;}
    QString msg(int n) {return bLogRead ? strList->value(n) : QString();}
};
//---------------------------------------------------------------------------------------

#endif // LOGFILES_H
