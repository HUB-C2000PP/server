//
// Copyright Kuzminov Sergey (kuzminov_sergey@mail.ru) 2021
//
// Distributed under the Unlicense License. (See accompanying
// file LICENSE_1_0.txt or copy at http://unlicense.org/)
//

#include "StateC2000PP.h"

StateC2000PP::StateC2000PP(QObject *parent) : QObject(parent)
{

}
//------------------------------------------------------------------------------
bool StateC2000PP::setVersion(quint16 val)
{
    if (deviceVer!=val)
    {
        deviceVer=val;
        emit versionChanged(val);

        // Если 0, то произошел разрыв связи с С2000ПП, обнулим все переменные.
        if (deviceVer==0) {
            shStates.clear();
            partStates.clear();
            rlStates.clear();

            emit shStateChanged(0, 0);
            emit partStateChanged(0, 0);
            emit rlStateChanged(0, 0);
        }

        return true;
    }

    return false;
}
//------------------------------------------------------------------------------
void StateC2000PP::calcPartState(quint8 nPart, const ConfigC2000PP *configC2000PP)
{
    if (configC2000PP!=nullptr)
    {
        quint8 partState = 0;
        quint8 priority  = 0xff;

        const auto shInPart = configC2000PP->getShInPart(nPart);
        for(const quint16 sh : shInPart)
        {
            quint8 shState   = shStates.value(sh);
            quint8 cPriority = EventsStatic::getEventPriority(shState);
            if (cPriority < priority)
            {
                partState = shState;
                priority  = cPriority;
            }
        }

        if (partStates.value(nPart)!=partState)
        {
            partStates[nPart] = partState;
            emit partStateChanged(nPart, partState);
        }
    }
}
//------------------------------------------------------------------------------
bool StateC2000PP::setShState(quint16 nSh, quint8 state, const ConfigC2000PP *configC2000PP)
{
    if (nSh==0){
        if (!shStates.isEmpty()){
            shStates.clear();
            partStates.clear();
            emit partStateChanged(0, 0);
            emit shStateChanged(0, 0);
            return true;
        }
    }
    else if (shStates.value(nSh) != state){
        shStates[nSh] = state;
        emit shStateChanged(nSh, state);

        if (configC2000PP!=nullptr){
            quint8 part = configC2000PP->getShPart(nSh);
            if (part!=0xff)
                calcPartState(part, configC2000PP);
        }

        return true;
    }

    return false;
}
//------------------------------------------------------------------------------
void StateC2000PP::setShState(QMap <quint16, quint8> mapShStates, const ConfigC2000PP *configC2000PP)
{
    QSet <quint8> parts;

    if (shStates.isEmpty() || mapShStates.isEmpty()){
        shStates = mapShStates;
        emit shStateChanged(0, 0);

        if (configC2000PP!=nullptr){
            if (mapShStates.isEmpty()) {
                partStates.clear();
                emit partStateChanged(0, 0);
            }
            else
            {
                auto itEnd = mapShStates.constEnd();
                for (auto it = mapShStates.constBegin(); it != itEnd; ++it)
                {
                    quint8 part = configC2000PP->getShPart( it.key() );
                    if (part!=0xff)
                        parts.insert(part);
                }
            }
        }
    }
    else
    {
        auto itEnd = mapShStates.constEnd();
        for (auto it = mapShStates.constBegin(); it != itEnd; ++it)
        {
            if (shStates.value(it.key())!=it.value()){
                shStates[it.key()] = it.value();
                emit shStateChanged(it.key(), it.value());

                if (configC2000PP!=nullptr){
                    quint8 part = configC2000PP->getShPart( it.key() );
                    if (part!=0xff)
                        parts.insert(part);
                }
            }
        }
    }

    if (!parts.isEmpty()) {
        QList <quint8> listParts = parts.values();
        std::sort(listParts.begin(), listParts.end());

        for(const quint8 part : qAsConst(listParts))
            calcPartState(part, configC2000PP);
    }
}
//------------------------------------------------------------------------------
bool StateC2000PP::setRlState(quint8 nRl, bool state)
{
    if ( (!rlStates.contains(nRl)) || (rlStates.value(nRl) != state) ){
        rlStates[nRl] = state;
        emit rlStateChanged(nRl, state);
        return true;
    }

    return false;
}
//------------------------------------------------------------------------------
void StateC2000PP::setRlState(QMap <quint8, bool> mapRlStates)
{
    if (rlStates.isEmpty() || mapRlStates.isEmpty()){
        rlStates = mapRlStates;
        emit rlStateChanged(0, 0);
    }
    else
    {
        auto itEnd = mapRlStates.constEnd();
        for (auto it = mapRlStates.constBegin(); it != itEnd; ++it)
        {
            if ( (!rlStates.contains(it.key())) || (rlStates.value(it.key())!=it.value()) ){
                rlStates[it.key()] = it.value();
                emit rlStateChanged(it.key(), it.value());
            }
        }
    }
}
