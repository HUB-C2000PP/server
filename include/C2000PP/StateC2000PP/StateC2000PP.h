//
// Copyright Kuzminov Sergey (kuzminov_sergey@mail.ru) 2021
//
// Distributed under the Unlicense License. (See accompanying
// file LICENSE_1_0.txt or copy at http://unlicense.org/)
//

#ifndef STATEC2000PP_H
#define STATEC2000PP_H

#include <QObject>
#include <QMap>
#include "include/C2000PP/ConfigC2000PP/ConfigC2000PP.h"
#include "include/C2000PP/EventsStatic/EventsStatic.h"

class StateC2000PP : public QObject
{
    Q_OBJECT
public:
    StateC2000PP(QObject *parent = nullptr);

    bool                    setVersion(quint16 val);
    quint16                 getVersion(void) const  {return deviceVer;}

    bool                    setShState(quint16 nSh, quint8 state, const ConfigC2000PP *configC2000PP=nullptr);
    void                    setShState(QMap <quint16, quint8> mapShStates, const ConfigC2000PP *configC2000PP=nullptr);

    quint8                  getShState(quint16 nSh) const {return shStates.value(nSh);}
    QMap <quint16, quint8>  getShState(void) const        {return shStates;}

    quint8                  getPartState(quint8 nPart) const {return partStates.value(nPart);}
    QMap <quint8, quint8>   getPartState(void) const         {return partStates;}

    bool                    setRlState(quint8 nRl, bool state);
    void                    setRlState(QMap <quint8, bool> mapRlStates);

    bool                    isRlContains(quint8 nRl) const {return rlStates.contains(nRl);}
    bool                    getRlState  (quint8 nRl) const {return rlStates.value(nRl);}
    QMap <quint8, bool>     getRlState  (void)       const {return rlStates;}

private:
    QMap <quint16, quint8> shStates;  // Состояния шлейфов сигнализации
    QMap <quint8,  quint8> partStates;// Состояния разделов
    QMap <quint8,  bool>   rlStates;  // Состояния релейных выходов

    quint16 deviceVer = 0;            // Версия прибора С2000-ПП

    void calcPartState(quint8 nPart, const ConfigC2000PP *configC2000PP);

signals:
    void versionChanged(quint16 ver);
    void shStateChanged(quint16 nSh, quint8 val);
    void partStateChanged(quint8 nPart, quint8 val);
    void rlStateChanged(quint8  nRl, bool state);

    void shOneCounter(quint16 nSh, quint64 counter);
    void shOneADC(quint16 nSh, qint16 adc);
    void shMultiADC(QMap<quint16, qint16> adc);
};

#endif // STATEC2000PP_H
