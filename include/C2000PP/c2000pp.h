//
// Copyright Kuzminov Sergey (kuzminov_sergey@mail.ru) 2021
//
// Distributed under the Unlicense License. (See accompanying
// file LICENSE_1_0.txt or copy at http://unlicense.org/)
//

#ifndef C2000PP_H
#define C2000PP_H

#include <QTimer>
#include <QDebug>
#include <QtCore>
#include <QModbusReply>
#include <QModbusDataUnit>
#include <QObject>

#include "include/Define.h"
#include "include/LogFiles/LogFiles.h"
#include "include/C2000PP/EventsStatic/EventsStatic.h"
#include "include/C2000PP/ConfigC2000PP/ConfigC2000PP.h"
#include "include/C2000PP/EventsC2000PP/EventC2000PP.h"

//-----------------------------------------------------------------------------
// Номера регистров для обмена данными с прибором С2000-ПП.
// C2KPP_название_L2, где L2 - это длинна данных для этого регистра.
//-----------------------------------------------------------------------------
#define C2KPP_BI_GetMasterSlave_L1  8 // Для ПП 2.0 выяснить режим работы master/slave

#define C2KPP_GetMaxValue_L7     46144 // Максимальные значения параметров.
#define C2KPP_GetDeviceType_L2   46152 // Тип прибора и версия прошивки прибора.
#define C2KPP_GetEventCount_L1   46162 // Количество непрочитанных событий.
#define C2KPP_GetEvent_Md2       46264 // Самое старое событие. Длинна запроса зависит от прибора. Двухэтапный запрос.

#define C2KPP_SetEventRead       46163 // Установить признак "событие прочитано"
#define C2KPP_SetDateTime        46165 // Установить дату и время.
#define C2KPP_GetDateTime        46166 // Такого номера регистра нет, есть 46165. Добавил +1, чтобы отличать в очереди запросов.
#define C2KPP_GetDateTime_L3     46165 // Ответ на запрос даты и времени или на установку времени.

#define C2KPP_SetShforGetADC     46179 // Установить номер зоны для запроса АЦП (температуры/влажности/CO)
#define C2KPP_SetShforGetVoltage 46181 // Установить номер зоны для запроса напряжения/тока.
#define C2KPP_RequestADC_L1      46328 // Запросить числовое значение АЦП (температуры/влажности/CO) и напряжения/тока

#define C2KPP_SetShforGetCounter 46180 // Установить номер зоны для запроса счетчика
#define C2KPP_RequestCounter_L3  46332 // Запросить числовое значение счетчика

//-----------------------------------------------------------------------------
// Начало диапазонов регистров С2000-ПП.
//-----------------------------------------------------------------------------
#define C2KPP_FirstRl   10000 // Номер регистра первого реле
#define C2KPP_FirstADC  30000 // Номер регистра первого шлейфа для запроса температуры и влажности в режиме master
#define C2KPP_FirstSh   40000 // Номер регистра первого шлейфа
#define C2KPP_FirstPart 44096 // Номер регистра первого раздела

//-----------------------------------------------------------------------------
// Вспомогательные структуры для контейнеров
//-----------------------------------------------------------------------------
struct struct88{
    quint8 a = 0;
    quint8 b = 0;
    bool operator==(const struct88& x) const { return (a==x.a) && (b==x.b); }
};

//--------------------
struct struct168{
    quint16 a = 0;
    quint8  b = 0;
    bool operator==(const struct168& x) const { return (a==x.a) && (b==x.b); }
};

//-----------------------------------------------------------------------------
// Класс для работы с прибором С2000-ПП производства ЗАО НВП "Болид".
//-----------------------------------------------------------------------------
class C2000PP : public QObject
{
    Q_OBJECT

public:
    explicit C2000PP(QObject *parent = nullptr);
    ~C2000PP() override;

    void loadConfig(void);
    void loadConfig(const ConfigC2000PP &configC2000PP) { this->configC2000PP=configC2000PP; loadConfig();}
    void loadFile(const QString &fileName) {configC2000PP.loadFile(fileName); loadConfig();}
    void loadFileDescription(const QString &fileName) {configC2000PP.loadDescriptions(fileName);}

    void   setAddressPP(quint8 address);
    quint8 getAddressPP(void) {return addressPP;}

    bool isConnected(void) {return m_deviceVer!=0;}
    bool getPortState(void) {return lastState==QModbusDevice::ConnectedState;}

    void controlSh(quint16 nSh, quint8 state);
    void controlPart(quint8 nPart, quint8 state);
    void controlRl(quint8  nRl, bool on);

    ConfigC2000PP* getConfigC2000PP(void) {return &configC2000PP;}
    bool isCorrectConfig(void) {return configC2000PP.isCorrectConfig();}

    void readConfigFromDev(bool start);

    quint16 getMaxRl  (void) {return s_maxRl;}
    quint16 getMaxSh  (void) {return s_maxSh;}
    quint16 getMaxPart(void) {return s_maxPart;}

private:
    QSettings *settings;
    static const QString keySetAddressPP;

    bool bMasterFail = false;   // Потеряна связь с пультом С2000М
    quint8 addressPP = 0;       // Адрес прибора С2000-ПП
    ConfigC2000PP configC2000PP;// Конфигурация прибора С2000-ПП
    QModbusDevice::State lastState = QModbusDevice::UnconnectedState;    

    quint8 countControlRl   = 0;
    quint8 countControlPart = 0;
    quint8 countControlSh   = 0;
    quint8 countRequestSh   = 0;
    quint8 countRequestRl   = 0;

    // Максимальные значения переменных, поддерживаемые прибором.
    quint16 s_maxRl            = 0; // Максимальное количество реле.
    quint16 s_maxSh            = 0; // Максимальное количество шлейфов.
    quint16 s_maxPart          = 0; // Максимальное количество разделов.
    //quint16 s_maxShState       = 0; // Максимальное количество состояний шлейфов.
    //quint16 s_maxPartStateSh   = 0; // Максимальное количество состояний разделов.
    //quint16 s_maxEvents        = 0; // Максимальное количество событий.
    quint16 s_halfMaxLenEvents = 0; // Половина максимальной длины описания события.

    quint16 twoStepRequestEvent = 0;      // Номер запрошенного события в двухэтапном запросе.
    quint16 s_EventCount        = 0;      // Количество непрочитанных событий в буфере прибора.
    quint16 s_OldEventCount     = 0xffff; // Количество "старых" событий - это количество событий на момент начала работы с прибором.

    // Очереди и их приоритеты (приоритеты в скобках)
    // (1) Незаконченный двухэтапный запрос
    enum {step2_No, step2_event, step2_ADC, step2_Counter} twoStepRequest = step2_No;
    QQueue <quint16>   queueService;    // (2)  Очередь приоритетных служебных запросов (тип, версия, время,...)
    qint16 queueConfig = -1;            // (3)  Чтение конфигурации из прибора С2000-ПП
    QQueue <struct88>  queueControlRl;  // (4)  Очередь управления Рл. <номер Рл, программа>
    QQueue <struct88>  queueControlPart;// (5)  Очередь управления разделом. <номер раздела, программа>
    QQueue <struct168> queueControlSh;  // (6)  Очередь управления ШС. <номер ШС, программа>
    QQueue <struct168> queueRequestSh;  // (7)  Очередь запроса состояний ШС. <номер ШС, количество ШС>
    QQueue <struct88>  queueRequestRl;  // (8)  Очередь запроса состояний РЛ. <номер Рл, количество Рл>
                                        // (9)  Запрос события
                                        // (10) Запрос количества непрочитанных событий
    QQueue <struct168> queueRequestADC; // (12) Очередь запроса числовых значений ШС. Одноэтапный запрос. <номер ШС, количество ШС>
    QQueue <quint16>   queueRequestADC6;// (12) Очередь запроса числовых значений ШС. Двухэтапный запрос.
    QQueue <quint16>   queueRequestADC7;// (11) Очередь запроса счетсика импульсов. Двухэтапный запрос.
    QQueue <quint16>   queueRequestADC8;// (11) Очередь запроса напряжения и тока РИП. Двухэтапный запрос.

    quint8 confRlMax = 0;                      // Максимальный номер реле в конфигурации прибора    
    QMap <quint16, quint8> countBadRequestSh;  // Подсчет неудачных попыток запросов для шлейфов. Для реле это не требуется, т.к. всегда есть ответ.
    QMap <quint16, quint8> countBadRequestADC; // Подсчет неудачных попыток запросов температуры/влажности/CO
    QMap <quint16, quint8> countBadControlSh;  // Подсчет неудачных попыток управления для шлейфов
    QMap <quint8,  quint8> countBadControlPart;// Подсчет неудачных попыток управления для раздела
    QMap <quint8,  quint8> countBadControlRl;  // Подсчет неудачных попыток управления для реле
    QMap <quint8, QList<quint16>> listShOnDev; // Списки шлейфов конкретного прибора
    QMap <quint16, quint8> continuousAreaSh;   // Непрерывные области сконфигурированных шлейфов для групповых заросов
    QMap <quint16, quint8> continuousAreaADC;  // Непрерывные области запросов АЦП температура/влажность/CO для групповых заросов
    QList<quint16>         listShType6;        // Список шлейфов для запроса АЦП температура/влажность/CO
    QList<quint16>         listShType7;        // Список шлейфов для запроса счетчика импульсов
    QList<quint16>         listShType8;        // Список шлейфов для запроса напряжения и тока РИП

    bool bRequestVoltage = false;              // true - запрашивали напряжение/ток, false - запрашивали счетчик. Чтобы попеременно их запрашивать в одну секунду.
    quint16 zoneInTwoStepRequestADC = 0;       // Номер запрошенной зоны в двухэтапном запросе АЦП/напряжения/тока.
    quint16 zoneInTwoStepRequestCNT = 0;       // Номер запрошенной зоны в двухэтапном запросе счетчика.
    quint8 countBadGetADC = 0;                 // Подсчет неудачных запросов при двухэтапном запросе АЦП
    QElapsedTimer timerTwoStepADC;

    bool adcRequestType = false;               // Тип запроса АЦП. Если false - двухэтапный запрос, если true - одноэтапный.
    void testAdcRequestType(void);             // Проверить какой тип запроса АЦП одноэтапный/двухэтапный

    quint8  adcRequestCounter = 0;             // Подсчет количества запросов АЦП за 1 секунду.
    quint16 satisticsTxOneSec = 0;             // Количество переданных пакетов за 1 секунду
    quint16 satisticsTxOneMin = 0;             // Количество переданных пакетов за 1 минуту
    quint16 satisticsRxOneSec = 0;             // Количество принятых пакетов за 1 секунду
    quint16 satisticsRxOneMin = 0;             // Количество принятых пакетов за 1 минуту
    int  timer1s=0;                            // ID таймера на 1 секунду
    int  timer1m=0;                            // ID таймера на 1 минуту
    int  timer1h=0;                            // ID таймера на 1 час
    void timerEvent (QTimerEvent * event) override;   // Событие от таймеров
    QMap <quint16, quint8> getStateShTimeer; // Опрашивать состояния ШС через определенное время для не генерирующих собятия ШС.
    QMap <quint8, quint8> getStateRlTimeer;  // Опрашивать состояния реле через определенное время для не генерирующих собятия реле.

    quint8 lastHour = 0; // Устранение ошибки в С2000-ПП. В событии для реле не хватает байта часа.

    // Номер последнего считанного события. Если считано событие с таким же номером
    // (два события подряд с одним и тем же номером), то не удалять его, иначе появится событие с нулевым номером.
    quint16 lastNumEvent=0xffff;

    void setTime(void);

    void readCoilStatus       (int reg, quint16 len);
    void readDiscreteInputs   (int reg, quint16 len);
    void readHoldingRegisters (int reg, quint16 len);
    void readInputRegisters   (int reg, quint16 len);
    void forceSingleCoil      (int reg, quint16 data);
    void presetHoldinRegisters(int reg, quint16 data);

    void restart(void);
    void stop();
    void nextRequest(void);
    void noop(quint8 count);

    void requestAllRl(void);
    void requestRl(quint8 nRl);
    void requestSh(quint16 nSh);
    void requestSh(quint16 nSh, quint8 count);
    void requestADC(quint16 nSh);
    void requestADC(quint16 nSh, quint8 count);
    void clearADCforDev(quint8 dev);
    void clearADCforDev(QSet<quint8> devs);
    void addADCforDev(quint8 dev);
    void addADCforDev(QSet<quint8> devs);
    void requestShDev(quint8 dev=0);
    void requestOnAtTimeShDev(quint8 dev=0);
    void efficientReqSh(quint8 dev=0);

    //----------------------------------------------------------------------
    // Взаимодействие с Modbus
    //----------------------------------------------------------------------
public slots:
    void readyRead(void);
    void readyWrite(void);
    void stateChanged (QModbusDevice::State state);
    void extErrorOccurred(QModbusDevice::Error errCode, QString strErr);

signals:    
    void sendReadRequest(QModbusDataUnit modbusDataUnit);
    void sendWriteRequest(QModbusDataUnit modbusDataUnit);
    void signalNewConfigC2000PP(void);

    //----------------------------------------------------------------------
    // Доступ к данным, полученным от прибора С2000-ПП
    //----------------------------------------------------------------------
public:
    quint16 getVersion(void) const {return m_deviceVer;}

signals:
    void eventReceived(EventC2000PP event);
    void versionChanged(quint16 ver);
    void shStateChanged(quint16 nSh, quint8 state);
    void rlStateChanged(quint8  nRl, bool   state);
    void satisticsRxTxOneSec(quint16 t, quint16 r);
    void satisticsRxTxOneMin(quint16 t, quint16 r);
    void satisticsModbusState(bool enable);
    void satisticsModbusEroor(const QString &err);
    void shOneCounter(quint16 nSh, quint64 counter);
    void shOneADC(quint16 nSh, qint16 adc);
    void shMultiADC(QMap<quint16, qint16> adc);
    void configReading(qint8 percent);

    void errorRele(bool err);

private:
    void readyReadCoilsRegisters  (const QModbusDataUnit &unit);
    void readyReadDiscreteInputs  (const QModbusDataUnit &unit);
    void readyReadInputRegisters  (const QModbusDataUnit &unit);
    void readyReadHoldingRegisters(const QModbusDataUnit &unit);
    void readyReadError           (const QModbusPdu::ExceptionCode exceptionCode, const int startAddress);

    void readyWriteCoilsRegisters  (const QModbusDataUnit &unit);
    void readyWriteHoldingRegisters(const QModbusDataUnit &unit);
    void readyWriteError           (const QModbusPdu::ExceptionCode exceptionCode, const int startAddress);

    // Запись в переменные с префиксом m_ внутри объекта только через функции set!
    // Это нужно чтобы корректно отправлялись уведомлении об изменении переменных.

    bool m_Master = true;
    void setMaster(bool master);

    quint16 m_deviceVer = 0;
    void setVersion(quint16 ver);

    QMap <quint16, quint8> m_ShState;
    bool setShState(quint16 nSh, quint8 state);

    QMap <quint8, bool> m_RlState;
    bool setRlState(quint8 nRl, bool state);
};
#endif // C2000PP_H
