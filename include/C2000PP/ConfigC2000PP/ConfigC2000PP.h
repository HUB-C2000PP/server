//
// Copyright Kuzminov Sergey (kuzminov_sergey@mail.ru) 2021
//
// Distributed under the Unlicense License. (See accompanying
// file LICENSE_1_0.txt or copy at http://unlicense.org/)
//

#ifndef CONFIGC2000PP_H
#define CONFIGC2000PP_H

#include <QtCore>
#include "include/Define.h"

class ConfigC2000PP
{
public:
    ConfigC2000PP();
    void clear(void);
    QString errorString(void) const {return strLastError;}
    bool loadFile(const QString &fileName);
    bool saveFile(const QString &fileName);

    //-------------------------------
    bool calcCorrect(void);
    void setConfigFromVector(int startAddress, const QVector<quint16> &d);
    //void setMaster(bool master);

    //-------------------------------
    quint8 getShDev (quint16 n) const;
    quint8 getShNum (quint16 n) const;
    quint8 getShPart(quint16 n) const;
    quint8 getShType(quint16 n) const;

    quint8 getRlDev (quint8 n) const;
    quint8 getRlNum (quint8 n) const;

    quint16 getPart (quint8 n) const;

    int     getPortSpeed(void) const;
    quint8  getPortParity(void) const;
    quint8  getAddress(void) const;
    bool    isControlAllow(void) const; // Если true, то можно управлять разделами/шлейфами/реле, но это актуально только для версии 1.32 и выше!
    bool    isAdcOnSlaveAllow(void) const; // Разрешен ли запрос АЦП в режиме Slave

    //-------------------------------
    QBitArray getMapSh(void)   const {return mapSh;}
    QBitArray getMapRl(void)   const {return mapRl;}
    QBitArray getMapPart(void) const {return mapPart;}

    //-------------------------------
    bool isCorrectConfig(void)    const {return !data.isEmpty();}
    bool isCorrectSh  (quint16 n) const {return ((n!=0) && (n<=mapSh.count()))   ? mapSh.testBit(n-1)   : false;}
    bool isCorrectRl  (quint8  n) const {return ((n!=0) && (n<=mapRl.count()))   ? mapRl.testBit(n-1)   : false;}
    bool isCorrectPart(quint8  n) const {return ((n!=0) && (n<=mapPart.count())) ? mapPart.testBit(n-1) : false;}

    //-------------------------------
    QString getDescriptionSh  (quint16 n) const {return descriptionSh.value(n);}
    QString getDescriptionRl  (quint8  n) const {return descriptionRl.value(n);}
    QString getDescriptionPart(quint8  n) const {return descriptionPart.value(n);}

    QVector<QString> getDescriptionsSh  (void) const {return descriptionSh;}
    QVector<QString> getDescriptionsRl  (void) const {return descriptionRl;}
    QVector<QString> getDescriptionsPart(void) const {return descriptionPart;}

    void setDescriptionSh  (quint16 n, const QString &descr) {if (n<maxSh) descriptionSh[n] = descr;}
    void setDescriptionRl  (quint8  n, const QString &descr) {if (n<maxRl) descriptionRl[n] = descr;}
    void setDescriptionPart(quint8  n, const QString &descr) {if (n<maxPart) descriptionPart[n] = descr;}

    void setDescriptions(const QVector<QString> &descrSh, const QVector<QString> &descrRl, const QVector<QString> &descrPart);
    bool saveDescriptions(const QString &fileName);
    void loadDescriptions(const QString &fileName);

    QSet <quint16> getShInPart(quint8 part) const {return mapPartSh.value(part);}

    bool containsKey(quint8 n) {return keysType.contains(n);}
    bool isKeyTM(quint8 n)     {return keysType.value(n);}
    QByteArray getKey(quint8 n){return keys.value(n);}

    //-------------------------------
    //Констаныты
    static const quint16 maxSh   = 512;
    static const quint8  maxRl   = 255;
    static const quint8  maxPart = 64;

    static const quint8  constDevType = 36;
    static const quint16 constLengthConfig = 4223;

private:
    static const QByteArray constDeviceType;
    static const QByteArray constDeviceVersion;
    static const QByteArray constNewConfiguration;
    static const QByteArray constLengthConfiguration;
    static const QByteArray constLine;
    static const QByteArray constLineCount;

    QString strLastError;
    bool makeDir(QString path);

    quint16 deviceVersion = 0;
    quint16 newConfiguration = 0;
    QVector<quint8> data;

    QBitArray mapSh;
    QBitArray mapRl;
    QBitArray mapPart;

    QMap <quint8, bool> keysType;
    QMap <quint8, QByteArray> keys;

    // Вспомогательые данные, не относящиеся к конфигурационному файлу.
    QVector<QString> descriptionSh;
    QVector<QString> descriptionRl;
    QVector<QString> descriptionPart;    
    QMap <quint8, QSet<quint16>> mapPartSh;

    friend QDataStream & operator<< (QDataStream& stream, const ConfigC2000PP& d);
    friend QDataStream & operator>> (QDataStream& stream,       ConfigC2000PP& d);
};

#endif // CONFIGC2000PP_H
