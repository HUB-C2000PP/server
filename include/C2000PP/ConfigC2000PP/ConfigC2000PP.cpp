//
// Copyright Kuzminov Sergey (kuzminov_sergey@mail.ru) 2021
//
// Distributed under the Unlicense License. (See accompanying
// file LICENSE_1_0.txt or copy at http://unlicense.org/)
//

#include "ConfigC2000PP.h"

const QByteArray ConfigC2000PP::constDeviceType          = "DeviceType=";
const QByteArray ConfigC2000PP::constDeviceVersion       = "DeviceVersion=";
const QByteArray ConfigC2000PP::constNewConfiguration    = "NewConfiguration=";
const QByteArray ConfigC2000PP::constLengthConfiguration = "LengthConfiguration=";
const QByteArray ConfigC2000PP::constLine                = "Line";
const QByteArray ConfigC2000PP::constLineCount           = "LineCount=";

//------------------------------------------------------------------------------
ConfigC2000PP::ConfigC2000PP()
{
    descriptionSh.resize(maxSh);
    descriptionRl.resize(maxRl);
    descriptionPart.resize(maxPart);
}
//------------------------------------------------------------------------------
void ConfigC2000PP::clear(void)
{
    mapSh.clear();
    mapRl.clear();
    mapPart.clear();
    mapPartSh.clear();

    data.clear();
    deviceVersion = 0;
    newConfiguration = 0;
}
//------------------------------------------------------------------------------
bool ConfigC2000PP::loadFile(const QString &fileName)
{
    clear();

    QFile file(fileName);
    if (file.open(QIODevice::ReadOnly | QIODevice::Text))
    {
        quint8 deviceType = 0;

        while (!file.atEnd())
        {
            QByteArray line = file.readLine().simplified();

            if (line.startsWith(constLineCount)) // Не удалять это условие, иначе будет неправильно работать
            {
                //qDebug() << "LineCount" << line;
            }
            else if (line.startsWith(constLine))
            {
                QList<QByteArray> list = line.split(L'=');
                if (list.count()>1)
                {
                    QByteArray num = list.value(0);
                    quint16 row = num.right(num.count()-4).toUShort();

                    if (row>0)
                    {
                        --row;
                        quint8 col = 0;
                        list = list.value(1).simplified().split(L' ');

                        for(const QByteArray &val : qAsConst(list))
                        {
                            quint16 index = 32*row + col++;
                            if (index < data.count())
                                data[index] = static_cast<quint8>( val.toUShort() );
                        }
                    }
                }
            }
            /*
            else if (line.startsWith(L'['))
            {
                //qDebug() << "Type" << line;
            }
            */
            else if (line.startsWith(constDeviceType))
            {
                QList<QByteArray> list = line.split(L'=');
                if (list.count()>1) deviceType = static_cast<quint8>( list.value(1).toUShort() );
            }
            else if (line.startsWith(constDeviceVersion))
            {
                QList<QByteArray> list = line.split(L'=');
                if (list.count()>1) deviceVersion = list.value(1).toUShort();
            }
            else if (line.startsWith(constNewConfiguration))
            {
                QList<QByteArray> list = line.split(L'=');
                if (list.count()>1) newConfiguration = list.value(1).toUShort();
            }
            else if (line.startsWith(constLengthConfiguration))
            {
                QList<QByteArray> list = line.split(L'=');
                if (list.count()>1)
                {
                    quint16 lengthConfiguration = list.value(1).toUShort();
                    if (lengthConfiguration<constLengthConfig) break;

                    data.resize(lengthConfiguration+1);
                }
            }
        }

        file.close();

        if (deviceType!=constDevType) strLastError = QStringLiteral(u"Не верный тип прибора.");
        else if (!calcCorrect()) strLastError = QStringLiteral(u"Не верная длина конфигурации.");
        else {
            strLastError.clear();
            return true;
        }

        clear();
    }
    else strLastError = file.errorString();

    return false;
}
//------------------------------------------------------------------------------
bool ConfigC2000PP::makeDir(QString path)
{
    QDir dir(path);
    if(!dir.exists()){
        if (!dir.mkpath(dir.path())){
            strLastError = QStringLiteral(u"Не удалось создать каталог:\n%1").arg(dir.path());
            return false;
        }
    }

    return true;
}
//------------------------------------------------------------------------------
bool ConfigC2000PP::saveFile(const QString &fileName)
{
    if (!data.isEmpty())
    {
        QFileInfo fileInfo(fileName);
        if (!makeDir(fileInfo.path())) return false;

        const QByteArray endl("\r\n");
        QSaveFile file(fileName);
        if (file.open(QIODevice::WriteOnly | QIODevice::Text | QIODevice::Truncate))
        {
            quint16 lengthConfiguration = static_cast<quint16>(data.count());
            quint16 lineCount = (lengthConfiguration)/32;
            if (lengthConfiguration>0) --lengthConfiguration;

            file.write( QByteArray("[")+ QByteArray::number(constDevType) + QByteArray("]")  + endl +
                        constDeviceType            + QByteArray::number(constDevType)        + endl +
                        constDeviceVersion         + QByteArray::number(deviceVersion)       + endl +
                        constNewConfiguration      + QByteArray::number(newConfiguration)    + endl +
                        constLengthConfiguration   + QByteArray::number(lengthConfiguration) + endl);

            for (quint16 row=0; row<lineCount; row++)
            {
                QByteArray line( constLine + QByteArray::number(row + 1) + QByteArray("=") );

                for (quint8 col=0; col<32; col++) line += QByteArray::number(data.value(32*row + col)) + QByteArray("  ");

                file.write(line + endl);
            }

            file.write( constLineCount + QByteArray::number(lineCount) + endl);

            bool ok = file.commit();
            if (ok) strLastError.clear();
            else strLastError = file.errorString();

            return ok;
        }
        else strLastError = file.errorString();
    }
    else strLastError = QStringLiteral(u"Пустая конфигурация");

    return false;
}
//------------------------------------------------------------------------------
bool ConfigC2000PP::calcCorrect(void)
{
    mapSh.clear();
    mapRl.clear();
    mapPart.clear();
    mapPartSh.clear();

    keys.clear();
    keysType.clear();

    if (data.count()>constLengthConfig)
    {
        mapSh.resize(maxSh);

        for (quint16 nSh = 1; nSh <= maxSh; nSh++)
        {
            if (getShDev(nSh)!=0xff)
                if (getShNum(nSh)!=0xff)
                {
                    quint8 part = getShPart(nSh);
                    if ( (part!=0) && (part!=0xff) )
                    {
                        quint8 d = getShType(nSh);
                        if ( (d!=0) && (d!=0xff) ) {
                            mapSh.setBit(nSh-1);

                            QSet<quint16> setSh;
                            if (mapPartSh.contains(part)){
                                setSh = mapPartSh.value(part);
                                setSh.insert(nSh);
                            }
                            else setSh.insert(nSh);
                            mapPartSh[part] = setSh;
                        }
                    }
                }
        }

        //------------------------
        mapRl.resize(maxRl);
        for (quint16 nRl = 1; nRl <= maxRl; nRl++)
        {
            quint8 d = getRlDev(static_cast<quint8>(nRl));
            if ( (d!=0) && (d!=0xff) )
            {
                d = getRlNum(static_cast<quint8>(nRl));
                if ( (d!=0) && (d!=0xff) )
                    mapRl.setBit(nRl-1);
            }
        }

        //------------------------
        mapPart.resize(maxPart);
        for (quint16 nPart = 1; nPart <= maxPart; nPart++)
        {
            quint16 d = getPart(static_cast<quint8>(nPart));
            if ( (d!=0) && (d!=0xffff) ) mapPart.setBit(nPart-1);
        }

        //------------------------
        for (quint8 n=1; n<=64; n++)
        {
            int j = 3583+n*8;
            if (static_cast<quint8>(data.value(j)) != 0xff)
            {
                QByteArray key;
                for (quint8 i=0; i<8; i++){
                    key.append( data.value(j-i) );
                }

                if ( static_cast<quint8>(key[7]) != 0xf0 ) {
                    key = key.toHex();
                    keysType[n] = true;
                }
                else{
                    key = key.mid(1,6).toHex();
                    int k = key.lastIndexOf('f');
                    if (k!=-1) key = key.right(key.count() - k - 1);

                    keysType[k] = false;
                }

                keys[n] = key;
            }
        }

        return true;
    }

    return false;
}
//------------------------------------------------------------------------------
int ConfigC2000PP::getPortSpeed(void) const
{
    int portSpeed = 0;

    if (data.count()>8)
    {
        switch (data.value(8)){
        case 0: portSpeed = 1200; break;
        case 1: portSpeed = 2400; break;
        case 2: portSpeed = 9600; break;
        case 3: portSpeed = 19200; break;
        case 4: portSpeed = 38400; break;
        case 5: portSpeed = 57600; break;
        case 6: portSpeed = 115200; break;
        }
    }

    return portSpeed;
}
//------------------------------------------------------------------------------
quint8 ConfigC2000PP::getPortParity(void) const
{
    if (data.count()>9) return data.value(9);
    return 0;
}
//------------------------------------------------------------------------------
bool ConfigC2000PP::isControlAllow(void) const
{
    if (data.count()>10) return data.value(10)==128;
    return false;
}
//------------------------------------------------------------------------------
bool ConfigC2000PP::isAdcOnSlaveAllow(void) const
{
    if (data.count()>44) return data.value(44) & 2;
    return false;
}
//------------------------------------------------------------------------------
quint8 ConfigC2000PP::getAddress(void) const
{
    if (data.count()>11) return data.value(11);
    return 0;
}
//------------------------------------------------------------------------------
quint8 ConfigC2000PP::getShDev(quint16 n) const
{
    if ( (n!=0) && (n<=maxSh) ){
        int num = 507+n*5;
        if (num < data.count())
            return data.value(num);
    }

    return 0xff;
}
//------------------------------------------------------------------------------
quint8 ConfigC2000PP::getShNum(quint16 n) const
{
    if ( (n!=0) && (n<=maxSh) ){
        int num = 508+n*5;
        if (num < data.count())
            return data.value(num);
    }

    return 0xff;
}
//------------------------------------------------------------------------------
quint8 ConfigC2000PP::getShPart(quint16 n) const
{
    if ((n!=0) && (n<=maxSh)){
        int num = 509+n*5;
        if (num < data.count())
            return data.value(num);
    }

    return 0xff;
}
//------------------------------------------------------------------------------
quint8 ConfigC2000PP::getShType(quint16 n) const
{
    if ( (n!=0) && (n<=maxSh) ){
        int num = 511+n*5;
        if (num < data.count())
            return data.value(num);
    }

    return 0xff;
}
//------------------------------------------------------------------------------
quint8 ConfigC2000PP::getRlDev(quint8 n) const
{
    if (n!=0){ // && (n<=maxRl))
        int num = 3070+n*2;
        if (num < data.count())
            return data.value(num);
    }

    return 0xff;
}
//------------------------------------------------------------------------------
quint8 ConfigC2000PP::getRlNum(quint8 n) const
{
    if (n!=0){ // && (n<=maxRl))
        int num = 3071+n*2;
        if (num < data.count())
            return data.value(num);
    }

    return 0xff;
}
//------------------------------------------------------------------------------
quint16 ConfigC2000PP::getPart(quint8 n) const
{
    if ( (n!=0) && (n<=maxPart) ){
        int num = 4095+n*2;
        if (num < data.count())
            return static_cast<quint16>((data.value(num)<<8) | data.value(num-1));
    }

    return 0xffff;
}
//------------------------------------------------------------------------------
void ConfigC2000PP::setDescriptions(const QVector<QString> &descrSh,
                                    const QVector<QString> &descrRl,
                                    const QVector<QString> &descrPart)
{
    descriptionSh   = descrSh;
    descriptionRl   = descrRl;
    descriptionPart = descrPart;

    descriptionSh.resize(maxSh);
    descriptionRl.resize(maxRl);
    descriptionPart.resize(maxPart);
}
//------------------------------------------------------------------------------
bool ConfigC2000PP::saveDescriptions(const QString &fileName)
{
    QFileInfo fileInfo(fileName);
    if (!makeDir(fileInfo.path())) return false;

    QSaveFile file(fileName);
    if (file.open(QIODevice::WriteOnly))
    {
        QByteArray data;
        QDataStream ds(&data, QIODevice::WriteOnly);
        ds.setVersion(dataStreamVersion);
        ds << descriptionSh << descriptionRl << descriptionPart;

        file.write(data);

        bool ok = file.commit();
        if (ok) strLastError.clear();
        else strLastError = file.errorString();

        return ok;
    }
    else strLastError = file.errorString();

    return false;
}
//------------------------------------------------------------------------------
void ConfigC2000PP::loadDescriptions(const QString &fileName)
{
    QFile file(fileName);
    if (file.open(QIODevice::ReadOnly)) {
        QByteArray data = file.readAll();
        file.close();
        QDataStream ds(&data, QIODevice::ReadOnly);
        ds.setVersion(dataStreamVersion);
        ds >> descriptionSh >> descriptionRl >> descriptionPart;
    }

    descriptionSh.resize(maxSh);
    descriptionRl.resize(maxRl);
    descriptionPart.resize(maxPart);
}
//------------------------------------------------------------------------------
void ConfigC2000PP::setConfigFromVector(int startAddress, const QVector<quint16> &d)
{
    if (data.count()<=constLengthConfig) data.resize(constLengthConfig+1);

    for (const quint16 val : d)
    {
        if (startAddress<0x800){ // Шлейф
            int num = startAddress>>2;
            quint8 numType = static_cast<quint8>(startAddress) & 3;

            if (numType==0) data[512+num*5] = static_cast<quint8>(val); // адрес прибора
            else if (numType==1) data[513+num*5] = static_cast<quint8>(val); // номер ШС в приборе
            else if (numType==2) // номер раздела MODBUS
            {
                num*=5;
                num+=514;
                data[num] = static_cast<quint8>(val);
                data[++num] = static_cast<quint8>(val>>8);
            }
            else /*if (numType==3)*/ data[516+num*5] = static_cast<quint8>(val); // тип зоны
        }
        else if (startAddress<0xA00){ // Реле
            int num = startAddress-0x800;
            data[3072+num] = static_cast<quint8>(val);
        }
        //else if (startAddress<0xA80){ // Раздел. Ошибка в документации на ПП 2.0
        else if (startAddress<0xA40){ // Раздел
            int num = (startAddress-0xA00)<<1;
            data[4096+num] = static_cast<quint8>(val);
            data[4097+num] = static_cast<quint8>(val>>8);
        }

        ++startAddress;
    }
}
//--------------------------------------------------------------------------
QDataStream & operator<< (QDataStream& stream, const ConfigC2000PP& d)
{
    stream << d.deviceVersion << d.newConfiguration << d.data;
    return stream;
}

//--------------------------------------------------------------------------
QDataStream & operator>> (QDataStream& stream, ConfigC2000PP& d)
{
    d.clear();
    stream >> d.deviceVersion >> d.newConfiguration >> d.data;
    if (!d.calcCorrect()) d.clear();

    return stream;
}


