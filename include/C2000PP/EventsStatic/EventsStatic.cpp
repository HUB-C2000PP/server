//
// Copyright Kuzminov Sergey (kuzminov_sergey@mail.ru) 2021
//
// Distributed under the Unlicense License. (See accompanying
// file LICENSE_1_0.txt or copy at http://unlicense.org/)
//

#include "EventsStatic.h"

const QString EventsStatic::dateFormat = QStringLiteral("dd.MM.yyyy");

const QMap<quint8, QString> EventsStatic::eventsName={
    {1, QStringLiteral(u"Восстановление сети 220 В")},
    {2, QStringLiteral(u"Авария сети 220 В")},
    {3, QStringLiteral(u"Тревога проникновения")},
    {4, QStringLiteral(u"Помеха")},
    {5, QStringLiteral(u"Реакция оператора")},
    {6, QStringLiteral(u"Помеха устранена")},
    {7, QStringLiteral(u"Ручное включение исполнительного устройства")},
    {8, QStringLiteral(u"Ручное выключение исполнительного устройства")},
    {9, QStringLiteral(u"Активировано устройство дистанционного пуска")},
    {10, QStringLiteral(u"Восстановление устройства дистанционного пуска")},
    {13, QStringLiteral(u"Отказ от прохода")},
    {14, QStringLiteral(u"Подбор кода")},
    {15, QStringLiteral(u"Дверь открыта")},
    {17, QStringLiteral(u"Неудачное взятие")},
    {18, QStringLiteral(u"Предъявлен код принуждения")},
    {19, QStringLiteral(u"Тест извещателя")},
    {20, QStringLiteral(u"Включение режима тестирования")},
    {21, QStringLiteral(u"Выключение режима тестирования")},
    {22, QStringLiteral(u"Восстановление контроля ШС")},
    {23, QStringLiteral(u"Задержка взятия")},
    {24, QStringLiteral(u"Взятие ШС на охрану")},
    {25, QStringLiteral(u"Доступ закрыт")},
    {26, QStringLiteral(u"Доступ отклонён")},
    {27, QStringLiteral(u"Дверь взломана")},
    {28, QStringLiteral(u"Доступ предоставлен")},
    {29, QStringLiteral(u"Доступ запрещён")},
    {30, QStringLiteral(u"Доступ восстановлен")},
    {31, QStringLiteral(u"Дверь закрыта")},
    {32, QStringLiteral(u"Проход")},
    {33, QStringLiteral(u"Дверь заблокирована")},
    {34, QStringLiteral(u"Идентификация")},
    {35, QStringLiteral(u"Восстановление технологического ШС")},
    {36, QStringLiteral(u"Нарушение технологического ШС")},
    {37, QStringLiteral(u"Пожар")},
    {38, QStringLiteral(u"Нарушение 2 технологического ШС")},
    {39, QStringLiteral(u"Оборудование в норме")},
    {40, QStringLiteral(u"Пожар 2")},
    {41, QStringLiteral(u"Неисправность оборудования")},
    {42, QStringLiteral(u"Нестандартное оборудование")},
    {44, QStringLiteral(u"Внимание! Опасность пожара")},
    {45, QStringLiteral(u"Обрыв шлейфа")},
    {46, QStringLiteral(u"Обрыв ДПЛС")},
    {47, QStringLiteral(u"Восстановление ДПЛС")},
    {58, QStringLiteral(u"Тихая тревога")},
    {67, QStringLiteral(u"Изменение даты")},
    {69, QStringLiteral(u"Журнал событий заполнен")},
    {70, QStringLiteral(u"Журнал событий переполнен")},
    {71, QStringLiteral(u"Понижение уровня")},
    {72, QStringLiteral(u"Уровень в норме")},
    {73, QStringLiteral(u"Изменение времени")},
    {74, QStringLiteral(u"Повышение уровня")},
    {75, QStringLiteral(u"Аварийное повышение уровня")},
    {76, QStringLiteral(u"Повышение температуры")},
    {77, QStringLiteral(u"Аварийное понижение уровня")},
    {78, QStringLiteral(u"Температура в норме")},
    {79, QStringLiteral(u"Тревога затопления")},
    {80, QStringLiteral(u"Восстановление датчика затопления")},
    {82, QStringLiteral(u"Неисправность термометра")},
    {83, QStringLiteral(u"Восстановление термометра")},
    {84, QStringLiteral(u"Начало локального программирования")},
    {90, QStringLiteral(u"Неисправность канала связи")},
    {91, QStringLiteral(u"Восстановление канала связи")},
    {94, QStringLiteral(u"Нагрев калорифера")},
    {95, QStringLiteral(u"Угроза охлаждения")},
    {96, QStringLiteral(u"Угроза замерзания")},
    {97, QStringLiteral(u"Перегрев обратной воды")},
    {98, QStringLiteral(u"Загрязнение воздушного фильтра")},
    {99, QStringLiteral(u"Отказ вентилятора")},
    {100, QStringLiteral(u"Лето-День")},
    {101, QStringLiteral(u"Лето-Ночь")},
    {102, QStringLiteral(u"Зима-День")},
    {103, QStringLiteral(u"Зима-Ночь")},
    {109, QStringLiteral(u"Снятие ШС с охраны")},
    {110, QStringLiteral(u"Сброс тревоги")},
    {111, QStringLiteral(u"Вход включен")},
    {112, QStringLiteral(u"Вход отключен")},
    {113, QStringLiteral(u"Выход включен")},
    {114, QStringLiteral(u"Выход отключен")},
    {117, QStringLiteral(u"Восстановление снятого ШС")},
    {118, QStringLiteral(u"Тревога входной зоны")},
    {119, QStringLiteral(u"Нарушение снятого ШС")},
    {121, QStringLiteral(u"Обрыв цепи выхода (реле)")},
    {122, QStringLiteral(u"Короткое замыкание цепи выхода (реле)")},
    {123, QStringLiteral(u"Восстановление цепи выхода (реле)")},
    {126, QStringLiteral(u"Потеря связи с выходом (реле)")},
    {127, QStringLiteral(u"Восстановление связи с выходом (реле)")},
    {128, QStringLiteral(u"Изменение состояния выхода (вкл./выкл. реле)")},
    {130, QStringLiteral(u"Включение насоса")},
    {131, QStringLiteral(u"Выключение насоса")},
    {135, QStringLiteral(u"Ошибка при автоматическом тестировании")},
    {136, QStringLiteral(u"Восстановление напряжения питания")},
    {137, QStringLiteral(u"Пуск (реле)")},
    {138, QStringLiteral(u"Неудачный пуск (реле)")},
    {139, QStringLiteral(u"Неудачный пуск пожаротушения")},
    {140, QStringLiteral(u"Запуск внутреннего теста")},
    {141, QStringLiteral(u"Задержка пуска пожаротушения")},
    {142, QStringLiteral(u"Автоматика пожаротушения выключена")},
    {143, QStringLiteral(u"Отмена пуска пожаротушения")},
    {144, QStringLiteral(u"Тушение")},
    {145, QStringLiteral(u"Аварийный пуск пожаротушения")},
    {146, QStringLiteral(u"Пуск пожаротушения")},
    {147, QStringLiteral(u"Блокировка пуска пожаротушения")},
    {148, QStringLiteral(u"Автоматика пожаротушения включена")},
    {149, QStringLiteral(u"Взлом корпуса")},
    {150, QStringLiteral(u"Пуск речевого оповещения")},
    {151, QStringLiteral(u"Останов речевого оповещения")},
    {152, QStringLiteral(u"Восстановление корпуса")},
    {153, QStringLiteral(u"Исполнительное устройство включено")},
    {154, QStringLiteral(u"Исполнительное устройство в исходном состоянии")},
    {155, QStringLiteral(u"Отказ исполнительного устройства")},
    {156, QStringLiteral(u"Ошибка исполнительного устройства")},
    {157, QStringLiteral(u"Сброс задержки пуска пожаротушения")},
    {158, QStringLiteral(u"Восстановление внутренней зоны")},
    {159, QStringLiteral(u"Задержка пуска речевого оповещения")},
    {160, QStringLiteral(u"Сброс задержки пуска речевого оповещения")},
    {161, QStringLiteral(u"Останов задержки пуска пожаротушения")},
    {165, QStringLiteral(u"Ошибка параметров ШС")},
    {172, QStringLiteral(u"Включение принтера")},
    {173, QStringLiteral(u"Выключение принтера")},
    {186, QStringLiteral(u"Требуется замена батареи")},
    {187, QStringLiteral(u"Потеря связи с ШС")},
    {188, QStringLiteral(u"Восстановление связи с ШС")},
    {189, QStringLiteral(u"Потеря связи по ветви ДПЛС1")},
    {190, QStringLiteral(u"Потеря связи по ветви ДПЛС2")},
    {191, QStringLiteral(u"Восстановление связи по ветви ДПЛС1")},
    {192, QStringLiteral(u"Отключение выходного напряжения")},
    {193, QStringLiteral(u"Включение выходного напряжения")},
    {194, QStringLiteral(u"Перегрузка источника питания")},
    {195, QStringLiteral(u"Перегрузка источника питания устранена")},
    {196, QStringLiteral(u"Неисправность зарядного устройства")},
    {197, QStringLiteral(u"Восстановление зарядного устройства")},
    {198, QStringLiteral(u"Неисправность источника питания")},
    {199, QStringLiteral(u"Восстановление источника питания")},
    {200, QStringLiteral(u"Батарея в норме")},
    {201, QStringLiteral(u"Восстановление связи по ветви ДПЛС2")},
    {202, QStringLiteral(u"Неисправность батареи")},
    {203, QStringLiteral(u"Перезапуск прибора")},
    {204, QStringLiteral(u"Требуется обслуживание")},
    {205, QStringLiteral(u"Ошибка теста батареи")},
    {206, QStringLiteral(u"Понижение температуры")},
    {211, QStringLiteral(u"Батарея разряжена")},
    {212, QStringLiteral(u"Резервная батарея разряжена")},
    {213, QStringLiteral(u"Резервная батарея в норме")},
    {214, QStringLiteral(u"Короткое замыкание ШС")},
    {215, QStringLiteral(u"Короткое замыкание ДПЛС")},
    {216, QStringLiteral(u"Сработка датчика")},
    {217, QStringLiteral(u"Отключение ветви интерфейса RS-485")},
    {218, QStringLiteral(u"Восстановление ветви интерфейса RS-485")},
    {219, QStringLiteral(u"Доступ открыт для свободного прохода")},
    {220, QStringLiteral(u"Срабатывание датчика вых. огнетуш. в-ва")},
    {221, QStringLiteral(u"Отказ датчика выхода огнетушащего в-ва")},
    {222, QStringLiteral(u"Авария ДПЛС (повышенное напряжение)")},
    {223, QStringLiteral(u"Отметка наряда")},
    {224, QStringLiteral(u"Некорректный ответ устройства в ДПЛС")},
    {225, QStringLiteral(u"Неустойчивый ответ устройства в ДПЛС")},
    {226, QStringLiteral(u"Автоматика выхода (реле) включена")},
    {227, QStringLiteral(u"Автоматика выхода (реле) отключена")},
    {228, QStringLiteral(u"Блокировка выхода (реле)")},
    {229, QStringLiteral(u"Задержка пуска (реле)")},
    {230, QStringLiteral(u"Останов задержки пуска (реле)")},
    {231, QStringLiteral(u"Отмена пуска (реле)")},
    {232, QStringLiteral(u"Увеличение задержки пуска (реле)")},
    {233, QStringLiteral(u"Сброс задержки пуска (реле)")},
    {234, QStringLiteral(u"Дистанционный контроль не пройден")},
    {237, QStringLiteral(u"Раздел снят под принуждением")},
    {241, QStringLiteral(u"Раздел взят")},
    {242, QStringLiteral(u"Раздел снят")},
    {243, QStringLiteral(u"Удалённый запрос на взятие")},
    {244, QStringLiteral(u"Удалённый запрос на снятие")},
    {245, QStringLiteral(u"Удалённый запрос доступа")},
    {249, QStringLiteral(u"Завершение локального программирования")},
    {250, QStringLiteral(u"Потеря связи с прибором")},
    {251, QStringLiteral(u"Восстановление связи с прибором")},
    {252, QStringLiteral(u"Подмена прибора")},
    {253, QStringLiteral(u"Включение пульта С2000М")},
    {254, QStringLiteral(u"Отметка даты")},
    {255, QStringLiteral(u"Отметка времени")}
};

const QMap<quint8, QString> EventsStatic::eventsNameEx ={
    {1, QStringLiteral(u"Норма сети 220 В")},
    {7, QStringLiteral(u"Исполнительное устройство включено вручную")},
    {8, QStringLiteral(u"Исполнительное устройство выключено вручную")},
    {9, QStringLiteral(u"Устройство дистанционного пуска активировано")},
    {10, QStringLiteral(u"Устройство дистанционного пуска в норме")},
    {23, QStringLiteral(u"Идёт взятие")},
    {24, QStringLiteral(u"Взят")},
    {35, QStringLiteral(u"Технологический ШС в норме")},
    {36, QStringLiteral(u"Технологический ШС нарушен")},
    {47, QStringLiteral(u"ДПЛС в норме")},
    {80, QStringLiteral(u"Датчик затопления в норме")},
    {83, QStringLiteral(u"Термометр в норме")},
    {91, QStringLiteral(u"Канал связи в норме")},
    {109, QStringLiteral(u"Снят")},
    {111, QStringLiteral(u"Включен контроль состояния входа (ШС)")},
    {112, QStringLiteral(u"Отключен контроль состояния входа (ШС)")},
    {113, QStringLiteral(u"Включен контроль состояния выхода (реле)")},
    {114, QStringLiteral(u"Отключен контроль состояния выхода (реле)")},
    {117, QStringLiteral(u"Снят и в норме")},
    {119, QStringLiteral(u"Снят и нарушен")},
    {123, QStringLiteral(u"Выход (реле) в норме")},
    {127, QStringLiteral(u"Связь с реле восстановлена")},
    {130, QStringLiteral(u"Насос включен")},
    {131, QStringLiteral(u"Насос выключен")},
    {136, QStringLiteral(u"Напряжение питания в норме")},
    {152, QStringLiteral(u"Корпус закрыт")},
    {158, QStringLiteral(u"Внутренняя зона в норме")},
    {188, QStringLiteral(u"Связь с ШС в норме")},
    {191, QStringLiteral(u"ДПЛС1 в норме")},
    {192, QStringLiteral(u"Выходное напряжение отключено")},
    {193, QStringLiteral(u"Выходное напряжение включено")},
    {195, QStringLiteral(u"Токопотребление в норме")},
    {197, QStringLiteral(u"Зарядное устройство в норме")},
    {199, QStringLiteral(u"Источник питания в норме")},
    {201, QStringLiteral(u"ДПЛС2 в норме")},
    {206, QStringLiteral(u"Температура ниже заданного значения")},
    {218, QStringLiteral(u"RS-485 в норме")},
    {250, QStringLiteral(u"Нет связи с прибором")},
    {251, QStringLiteral(u"Связь с прибором восстановлена")}
};

const QMap<quint8, quint8> EventsStatic::eventsGroup = {
    {1, 3},	{2, 3},	{3, 2},	{4, 3},	{5, 6},	{6, 3},	{7, 9},	{8, 9},	{9, 7},	{10, 7},
    {13, 8},	{14, 8},	{15, 8},	{17, 4},	{18, 2},	{19, 6},	{20, 6},	{21, 6},	{22, 4},	{23, 5},
    {24, 5},	{25, 8},	{26, 8},	{27, 8},	{28, 8},	{29, 8},	{30, 8},	{31, 8},	{32, 8},	{33, 8},
    {34, 4},	{35, 7},	{36, 7},	{37, 1},	{38, 7},	{39, 3},	{40, 1},	{41, 3},	{42, 3},	{44, 1},
    {45, 3},	{46, 3},	{47, 3},	{58, 2},	{67, 6},	{69, 6},	{70, 6},	{71, 6},	{72, 6},	{73, 6},
    {74, 6},	{75, 3},	{76, 6},	{77, 3},	{78, 6},	{79, 2},	{80, 5},	{82, 3},	{83, 3},	{84, 6},
    {90, 3},	{91, 3},	{94, 6},	{95, 6},	{96, 6},	{97, 6},	{98, 6},	{99, 6},	{100, 6},	{101, 6},
    {102, 6},	{103, 6},	{109, 5},	{110, 4},	{111, 4},	{112, 3},	{113, 4},	{114, 3},	{117, 7},	{118, 2},
    {119, 7},	{121, 3},	{122, 3},	{123, 3},	{126, 3},	{127, 3},	{128, 9},	{130, 9},	{131, 9},	{135, 3},
    {136, 6},	{137, 9},	{138, 3},	{139, 1},	{140, 6},	{141, 1},	{142, 4},	{143, 1},	{144, 1},	{145, 1},
    {146, 1},	{147, 1},	{148, 4},	{149, 3},	{150, 1},	{151, 1},	{152, 3},	{153, 9},	{154, 9},	{155, 3},
    {156, 3},	{157, 1},	{158, 6},	{159, 1},	{160, 1},	{161, 1},	{165, 3},	{172, 6},	{173, 6},	{186, 3},
    {187, 3},	{188, 3},	{189, 3},	{190, 3},	{191, 3},	{192, 3},	{193, 4},	{194, 3},	{195, 3},	{196, 3},
    {197, 3},	{198, 3},	{199, 3},	{200, 3},	{201, 3},	{202, 3},	{203, 3},	{204, 3},	{205, 3},	{206, 6},
    {211, 3},	{212, 3},	{213, 3},	{214, 3},	{215, 3},	{216, 6},	{217, 3},	{218, 3},	{219, 8},	{220, 6},
    {221, 6},	{222, 3},	{223, 6},	{224, 3},	{225, 3},	{226, 9},	{227, 9},	{228, 9},	{229, 9},	{230, 9},
    {231, 9},	{232, 9},	{233, 9},	{234, 3},	{237, 2},	{241, 4},	{242, 4},	{243, 4},	{244, 4},	{245, 8},
    {249, 6},	{250, 3},	{251, 3},	{252, 3},	{253, 6},	{254, 6},	{255, 6}
};

const QMap<quint8, quint8> EventsStatic::eventsPriority = {
    {1, 83},	{2, 42},	{3, 13},	{4, 22},	{5, 254},	{6, 88},	{7, 254},	{8, 254},	{9, 62},	{10, 74},
    {13, 254},	{14, 254},	{15, 254},	{17, 37},	{18, 254},	{19, 254},	{20, 254},	{21, 254},	{22, 92},	{23, 54},
    {24, 67},	{25, 254},	{26, 254},	{27, 254},	{28, 254},	{29, 254},	{30, 254},	{31, 254},	{32, 254},	{33, 254},
    {34, 254},	{35, 73},	{36, 72},	{37, 10},	{38, 58},	{39, 76},	{40, 9},	{41, 26},	{42, 254},	{44, 11},
    {45, 23},	{46, 36},	{47, 86},	{58, 12},	{67, 254},	{69, 254},	{70, 254},	{71, 64},	{72, 70},	{73, 254},
    {74, 59},	{75, 29},	{76, 60},	{77, 30},	{78, 71},	{79, 15},	{80, 66},	{82, 27},	{83, 77},	{84, 254},
    {90, 33},	{91, 91},	{94, 254},	{95, 254},	{96, 254},	{97, 254},	{98, 254},	{99, 254},	{100, 254},	{101, 254},
    {102, 254},	{103, 254},	{109, 52},	{110, 254},	{111, 254},	{112, 252},	{113, 254},	{114, 253},	{117, 53},	{118, 14},
    {119, 51},	{121, 31},	{122, 32},	{123, 78},	{126, 254},	{127, 254},	{128, 254},	{130, 55},	{131, 68},	{135, 254},
    {136, 254},	{137, 254},	{138, 254},	{139, 5},	{140, 254},	{141, 4},	{142, 50},	{143, 16},	{144, 1},	{145, 2},
    {146, 3},	{147, 49},	{148, 69},	{149, 48},	{150, 7},	{151, 17},	{152, 87},	{153, 61},	{154, 63},	{155, 20},
    {156, 21},	{157, 254},	{158, 75},	{159, 8},	{160, 254},	{161, 6},	{165, 25},	{172, 254},	{173, 254},	{186, 45},
    {187, 19},	{188, 89},	{189, 254},	{190, 254},	{191, 254},	{192, 38},	{193, 79},	{194, 39},	{195, 80},	{196, 40},
    {197, 81},	{198, 41},	{199, 82},	{200, 84},	{201, 254},	{202, 43},	{203, 254},	{204, 28},	{205, 44},	{206, 65},
    {211, 46},	{212, 47},	{213, 85},	{214, 24},	{215, 34},	{216, 254},	{217, 254},	{218, 254},	{219, 254},	{220, 56},
    {221, 57},	{222, 35},	{223, 254},	{224, 254},	{225, 254},	{226, 254},	{227, 254},	{228, 254},	{229, 254},	{230, 254},
    {231, 254},	{232, 254},	{233, 254},	{234, 254},	{237, 254},	{241, 254},	{242, 254},	{243, 254},	{244, 254},	{245, 254},
    {249, 254},	{250, 18},	{251, 90},	{252, 254},	{253, 254},	{254, 254},	{255, 254}
};

const QList<quint8> EventsStatic::eventsAlarm = {3,18,27,33,37,40,44,45,58,79,137,138,139,141,144,145,146,147,149,161,214,229,230,250,252};

//-----------------------------------------------------------------------------
QString EventsStatic::getStrEvent(quint8 e)
{
    static quint8 lastEvent=0;
    static QString lastStr;

    if (lastEvent==e) return lastStr;

    lastEvent = e;
    lastStr = eventsName.value(e);

    return lastStr;
}
//-----------------------------------------------------------------------------
QString EventsStatic::getSrtEventEx(quint8 e)
{
    static quint8 lastEvent=0;
    static QString lastStr;

    if (lastEvent==e) return lastStr;

    lastEvent=e;
    lastStr = eventsNameEx.value(e);

    return lastStr;
}
//-----------------------------------------------------------------------------
QString EventsStatic::getSrtState(quint8 e)
{
    static quint8 lastEvent=0;
    static QString lastStr;

    if (lastEvent==e) return lastStr;

    lastEvent=e;
    lastStr = getSrtEventEx(e);
    if (lastStr.isEmpty()) lastStr = getStrEvent(e);

    return lastStr;
}
//-----------------------------------------------------------------------------
quint8 EventsStatic::getEventGroup(quint8 e)
{
    static quint8 lastEvent=0;
    static quint8 lastGroup=0;

    if (lastEvent==e) return lastGroup;

    lastEvent=e;
    lastGroup = eventsGroup.value(e);

    return lastGroup;
}
//-----------------------------------------------------------------------------
quint8 EventsStatic::getEventPriority(quint8 e)
{
    static quint8 lastEvent = 0;
    static quint8 lastPrior = 0;

    if (lastEvent==e) return lastPrior;

    lastEvent = e;
    lastPrior = eventsPriority.value(e);

    return lastPrior;
}

//-----------------------------------------------------------------------------
QString EventsStatic::getEventMarker(quint8 e)
{
    QString ret;

    if      (e==26) ret = QStringLiteral(u"⛔"); // Доступ отклонён
    else if (e==27) ret = QStringLiteral(u"🥊"); // Дверь взломана
    else if (e==29) ret = QStringLiteral(u"🚫"); // Доступ запрещён
    else if (e==33) ret = QStringLiteral(u"📛"); // Дверь заблокирована
    else{
        switch (getEventGroup(e)){
        case 1: ret = QStringLiteral(u"🔥"); break; // Пожары
        case 2: ret = QStringLiteral(u"🚨"); break; // Тревоги
        case 3: ret = QStringLiteral(u"⚠"); break; // Неисправности
        case 4: ret = QStringLiteral(u"✔"); break; // Взятие/снятие
        case 5: ret = QStringLiteral(u"🔹"); break; // Взятие ШС
        case 6: ret = QStringLiteral(u"⚙️"); break; // Служебные
        case 7: ret = QStringLiteral(u"🚧"); break; // Технологические
        case 8: ret = QStringLiteral(u"🔑"); break; // Доступ
        case 9: ret = QStringLiteral(u"💡"); break; // Реле
        default: ret = QStringLiteral(u"❓"); break; // Без группы (неизвестные)
        }
    }

    return ret;
}
//-----------------------------------------------------------------------------
QString EventsStatic::getEventDescription(const EventC2000PP &event, const ConfigC2000PP *configC2000PP)
{
    QString ret;
    if (event.isSh()) ret = configC2000PP->getDescriptionSh( event.getSh()-1 );
    else if (event.isRl()) ret = configC2000PP->getDescriptionRl( event.getRl()-1 );

    if (event.isPart()) {
        QString partStr = configC2000PP->getDescriptionPart( event.getPart()-1 );
        if (!partStr.isEmpty()) ret = ret.isEmpty() ? partStr : partStr + QStringLiteral(", ") + ret;
    }

    if (event.isRlState())
        ret += event.getRlState()==1 ? QStringLiteral(u"<Вкл.>") : QStringLiteral(u"<Выкл.>");

    return ret;
}
//-----------------------------------------------------------------------------
QString EventsStatic::getStrPart(const EventC2000PP &event, const ConfigC2000PP *configC2000PP)
{
    QString ret;

    if (event.isPart()){
        quint8 partModbus = event.getPart();
        quint16 part = configC2000PP->getPart(partModbus);

        if ((part==0xffff) || (part==partModbus)) ret += QString::number( partModbus );
        else ret += QStringLiteral(u"%1 (%2)").arg(partModbus).arg(part);
    }

    return ret;
}
//-----------------------------------------------------------------------------
QString EventsStatic::getStrAgregat(const EventC2000PP &event, const ConfigC2000PP *configC2000PP)
{
    QString ret;

    if (event.isSh()) {
        quint16 sh = event.getSh();
        quint8 dev = configC2000PP->getShDev(sh);
        quint8 num = configC2000PP->getShNum(sh);
        if (dev==0) ret = QStringLiteral(u"ШС %1 (ПП)").arg(sh);
        else if (num==0) ret = QStringLiteral(u"ШС %1 (Пр %2)").arg(sh).arg(dev);
        else ret = QStringLiteral(u"ШС %1 (%2/%3)").arg(sh).arg(dev).arg(num);
    }
    else if (event.isRl()) {
        quint8 rl = event.getRl();
        ret = QStringLiteral(u"Рл %1 (%2/%3)").arg(rl).arg(configC2000PP->getRlDev(rl)).arg(configC2000PP->getRlNum(rl));
    }

    return ret;
}
//-----------------------------------------------------------------------------
QString EventsStatic::getStrAgregat(const EventC2000PP &event)
{
    QString ret;

    if (event.isSh()) {
        ret = QStringLiteral(u"ШС %1").arg( event.getSh() );
    }
    else if (event.isRl()) {
        ret = QStringLiteral(u"Рл %1").arg( event.getRl() );
    }

    return ret;
}
