//
// Copyright Kuzminov Sergey (kuzminov_sergey@mail.ru) 2021
//
// Distributed under the Unlicense License. (See accompanying
// file LICENSE_1_0.txt or copy at http://unlicense.org/)
//

#ifndef EVENTSSTATIC_H
#define EVENTSSTATIC_H

#include <QtCore>
#include "../EventsC2000PP/EventC2000PP.h"
#include "../ConfigC2000PP/ConfigC2000PP.h"

//---------------------------------
enum controlPP {
    SHPP_NO          = 0,   // ничиго не делать
    SHPP_ARM         = 24,  // взять ШС
    SHPP_DISARM      = 109, // снять ШС
    SHPP_CONTROL_ON  = 111, // включить контроль ШС
    SHPP_CONTROL_OFF = 112, // выключить контроль ШС
    SHPP_DISABLE_AUTO= 142, // отключить автоматику
    SHPP_ENABLE_AUTO = 148, // включить автоматику
    SHPP_RESET_START = 143, // сброс пуска АСПТ
    SHPP_START       = 146  // дистанционный пуск
};

//---------------------------------
//Группы событий
//---------------------------------
enum groupEvents
{
    EG_FIRES    = 1,
    EG_ALARMS   = 2,
    EG_FAULT    = 3,
    EG_PARM     = 4,
    EG_SARM     = 5,
    EG_SERVICE  = 6,
    EG_TECHNO   = 7,
    EG_ACCESS   = 8,
    EG_RELAY    = 9
};

class EventsStatic
{
public:
    static const QString dateFormat;

    static const QMap<quint8, QString> eventsName;     // События
    static const QMap<quint8, QString> eventsNameEx;   // Названия состояний, заменяющие названия в eventsName
    static const QMap<quint8, quint8>  eventsGroup;    // Группы событий
    static const QMap<quint8, quint8>  eventsPriority; // Приоритет событий
    static const QList<quint8>         eventsAlarm;    // Список тревожных событий

    static QString getStrEvent(quint8 e);     // Перевод кода события в текстове описание
    static QString getSrtState(quint8 e);     // Перевод кода события в состояние агрегата
    static quint8  getEventGroup(quint8 e);   // Перевод кода события в группу события
    static quint8  getEventPriority(quint8 e);// Перевод кода события в приоритет события

    static QString getEventMarker(quint8 e);
    static QString getEventDescription(const EventC2000PP &event, const ConfigC2000PP *configC2000PP);
    static QString getStrPart         (const EventC2000PP &event, const ConfigC2000PP *configC2000PP);
    static QString getStrAgregat      (const EventC2000PP &event, const ConfigC2000PP *configC2000PP);
    static QString getStrAgregat      (const EventC2000PP &event);

private:
    // Вспомогательные функции.
    static QString getSrtEventEx(quint8 e);
};

#endif // EVENTSSTATIC_H
