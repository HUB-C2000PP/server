//
// Copyright Kuzminov Sergey (kuzminov_sergey@mail.ru) 2021
//
// Distributed under the Unlicense License. (See accompanying
// file LICENSE_1_0.txt or copy at http://unlicense.org/)
//

#ifndef EVENTPP_H
#define EVENTPP_H

#include <QtCore>

class EventC2000PP
{
public:
    EventC2000PP();

private:
    struct MainData
    {
        quint32 datePP = 0; // Время и дата прибора С2000-ПП
        quint32 datePC = 0; // Время и дата компьютера на момент получения события
        quint16 user   = 0; // Номер пользователя в С2000-ПП или в компьютере (на перспективу), зависит от флага evntUserPC.
        quint16 data1  = 0; // Данные: номер шлейфа или немер реле, зависит от флага evntRl
        quint8  data2  = 0; // Данные: номер раздела или состояние реле, зависит от флага evntRl
        quint8  event  = 0; // Номер события из таблицы событий.
        quint8  flag   = 0; // Флаги
        quint8  reserv = 0; // Зарезервировано
    } mainData;

    enum evntFlags{
        evntBadDate = 1,
        evntBadTime = 2,
        evntRl      = 4,
        evntOld     = 8,
        evntUserPC  = 16
    };

public:
    void setSh  (quint16 nSh)   {mainData.data1 = nSh;   mainData.flag&=evntRl^0xff;}
    void setPart(quint8  nPart) {mainData.data2 = nPart; mainData.flag&=evntRl^0xff;}

    void setRl  (quint8  nRl)   {mainData.data1 = nRl;   mainData.flag|=evntRl;}
    void setRlSt(quint8  state) {mainData.data2 = state; mainData.flag|=evntRl;}

    void setDatePP(QDateTime dateTime, bool badDate, bool badTime);
    void setDatePC(QDateTime dateTime);

    void setUser(quint16 user) {mainData.user=user;}
    quint16 getUser(void) const {return mainData.user;}

    void setEvent(quint8 event, bool old) {mainData.event=event; if (old) mainData.flag|=evntOld; /*else flag&=evntOldEvnt^0xff;*/}
    quint8 getEvent(void) const {return mainData.event;}
    bool isOldEvent(void) const {return mainData.flag & evntOld;}

    bool isBadDate(void) const {return mainData.flag & evntBadDate;}
    bool isBadTime(void) const {return mainData.flag & evntBadTime;}

    bool    isSh (void) const {return (mainData.flag & evntRl) ? false : mainData.data1!=0;}
    quint16 getSh(void) const {return mainData.data1;}

    bool   isRl (void) const {return (mainData.flag & evntRl) ? mainData.data1!=0 : false;}
    quint8 getRl(void) const {return static_cast<quint8>(mainData.data1);}

    bool isRlState   (void) const {return (mainData.flag & evntRl);}
    quint8 getRlState(void) const {return mainData.data2;}

    bool isPart(void) const {return (mainData.flag & evntRl) ? false : mainData.data2!=0;}
    quint8 getPart(void) const {return mainData.data2;}

    QDateTime getDatePP(void) const;
    QDateTime getDatePC(void) const;

    inline static quint8 size(void){ return sizeof(MainData); }
    void setData(QByteArray data);
    QByteArray data(void);
};

#endif // EVENTPP_H
