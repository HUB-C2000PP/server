//
// Copyright Kuzminov Sergey (kuzminov_sergey@mail.ru) 2021
//
// Distributed under the Unlicense License. (See accompanying
// file LICENSE_1_0.txt or copy at http://unlicense.org/)
//

#include "EventC2000PP.h"

//-----------------------------------------------------------------------------
EventC2000PP::EventC2000PP()
{

}
//-----------------------------------------------------------------------------
void EventC2000PP::setDatePP(QDateTime dateTime, bool badDate, bool badTime)
{
    mainData.datePP=static_cast<quint32>(dateTime.toSecsSinceEpoch());

    if (badDate) mainData.flag|=evntBadDate;// else flag&=evntBadDate^0xff;
    if (badTime) mainData.flag|=evntBadTime;// else flag&=evntBadTime^0xff;
}
//-----------------------------------------------------------------------------
void EventC2000PP::setDatePC(QDateTime dateTime)
{
    mainData.datePC = static_cast<quint32>(dateTime.toSecsSinceEpoch());
}
//-----------------------------------------------------------------------------
QDateTime EventC2000PP::getDatePP(void) const
{
    return QDateTime::fromSecsSinceEpoch(mainData.datePP);
}
//-----------------------------------------------------------------------------
QDateTime EventC2000PP::getDatePC(void) const
{
    return QDateTime::fromSecsSinceEpoch(mainData.datePC);
}
//-----------------------------------------------------------------------------
void EventC2000PP::setData(QByteArray data)
{
    if (data.count()<size()){
        QByteArray ba(size() - data.count(), 0);
        data.append(ba);
    }

    mainData = *(reinterpret_cast<MainData*>( data.data() ));
}
//-----------------------------------------------------------------------------
QByteArray EventC2000PP::data(void)
{
    QByteArray ret;
    ret.reserve(size());

    ret.append( reinterpret_cast<char*>(&mainData), sizeof(mainData));

    return ret;
}
