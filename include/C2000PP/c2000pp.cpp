//
// Copyright Kuzminov Sergey (kuzminov_sergey@mail.ru) 2021
//
// Distributed under the Unlicense License. (See accompanying
// file LICENSE_1_0.txt or copy at http://unlicense.org/)
//

#include "c2000pp.h"

const QString C2000PP::keySetAddressPP = QStringLiteral("ppAddress");

//------------------------------------------------------------------------------
C2000PP::C2000PP(QObject *parent) : QObject(parent)
{
    settings = new QSettings(mainPath+constFileNameAddressPP, QSettings::IniFormat, this);
}
//------------------------------------------------------------------------------
C2000PP::~C2000PP()
{
}
//------------------------------------------------------------------------------
void C2000PP::controlSh(quint16 nSh, quint8 state)
{
    if ( configC2000PP.isCorrectSh(nSh) && (nSh<=s_maxSh)) {
        struct168 data;
        data.a = nSh;
        data.b = state;
        if (!queueControlSh.contains(data)) queueControlSh.enqueue(data);
    }
}
//------------------------------------------------------------------------------
void C2000PP::controlPart(quint8 nPart, quint8 state)
{
    if ( configC2000PP.isCorrectPart(nPart) && (nPart<=s_maxPart)) {
        struct88 data;
        data.a = nPart;
        data.b = state;
        if (!queueControlPart.contains(data)) queueControlPart.enqueue(data);
    }
}
//------------------------------------------------------------------------------
void C2000PP::controlRl(quint8  nRl, bool on)
{
    if ( configC2000PP.isCorrectRl(nRl) ){ //&& (nRl<=s_maxRl)) {
        struct88 data;
        data.a = nRl;
        data.b = on ? 1 : 0;
        if (!queueControlRl.contains(data)) queueControlRl.enqueue(data);
    }
}
//-----------------------------------------------------------------------------
void C2000PP::setAddressPP(quint8 address)
{
    if (addressPP!=address) {addressPP = address; settings->setValue(keySetAddressPP, addressPP);}
}
//-----------------------------------------------------------------------------
// Загрузить конфигурацию прибора С2000-ПП
//-----------------------------------------------------------------------------
void C2000PP::loadConfig(void)
{
    addressPP = static_cast<quint8>( settings->value(keySetAddressPP, addressPP).toUInt() );
    listShOnDev.clear();

    QBitArray mapSh    = configC2000PP.getMapSh();
    quint16 firstSh    = 0xffff;
    quint16 firstADC   = 0xffff;
    quint8  lastDevADC = 0xff;

    int count = mapSh.count();
    for (quint16 i=1; i<=count; ++i)
    {
        if (mapSh.testBit(i-1))
        {
            quint8 typeSh = configC2000PP.getShType(i);

            // Сформировать списки шлейфов для запроса АЦП разными методами
            if      (typeSh==6) listShType6.append(i);
            else if (typeSh==7) listShType7.append(i);
            else if (typeSh==8) listShType8.append(i);

            //--------------------------------------------------
            // Определить непрерывные области сконфигурированных
            // шлейфов, но не более 10 шлейфов для групповых запросов
            //--------------------------------------------------
            if (firstSh == 0xffff){
                firstSh = i;
                continuousAreaSh[firstSh] = 1;
            }
            else {
                if (++continuousAreaSh[firstSh]>=10) firstSh = 0xffff;
            }

            //--------------------------------------------------
            // То же самое, но для запроса АЦП
            //--------------------------------------------------
            if (typeSh==6)
            {
                quint8 dev = configC2000PP.getShDev(i);

                if ( (firstADC == 0xffff) || (lastDevADC != dev) ){
                    firstADC = i;
                    lastDevADC = dev;
                    continuousAreaADC[firstADC] = 1;
                }
                else if (++continuousAreaADC[firstADC]>=10) firstADC = 0xffff;
            }
            else firstADC = 0xffff;
        }
        else {
            firstSh  = 0xffff;
            firstADC = 0xffff;
        }
    }

    // Определить максимальный номер реле в конфигурации для ограничения запроса состояний реле
    QBitArray mapRl = configC2000PP.getMapRl();
    count = mapRl.count();
    for (quint16 i=0; i<count; ++i){
        if (mapRl.testBit(i)) confRlMax = static_cast<quint8>(i+1);
    }

    testAdcRequestType();
}
//------------------------------------------------------------------------------
void C2000PP::timerEvent(QTimerEvent * event)
{
    if (event->timerId() == timer1s)
    {
        emit satisticsRxTxOneSec(satisticsTxOneSec, satisticsRxOneSec);
        satisticsTxOneSec=0;
        satisticsRxOneSec=0;

        adcRequestCounter = 0;

        // Для ШС, от которых не приходят события
        auto mapSh = getStateShTimeer;
        auto itEndSh = mapSh.constEnd();
        for (auto it = mapSh.constBegin(); it != itEndSh; ++it)
        {
            if ((it.value()==4) || (it.value()==0)) requestSh(it.key());

            if (it.value()==0) getStateShTimeer.remove(it.key());
            else --getStateShTimeer[it.key()];
        }

        // Для реле, от которых не приходят события
        auto mapRl = getStateRlTimeer;
        auto itEndRl = mapRl.constEnd();
        for (auto it = mapRl.constBegin(); it != itEndRl; ++it)
        {
            if ((it.value()==4) || (it.value()==0)) requestRl(it.key());

            if (it.value()==0) getStateRlTimeer.remove(it.key());
            else --getStateRlTimeer[it.key()];
        }
    }
    if (event->timerId() == timer1m)
    {
        emit satisticsRxTxOneMin(satisticsTxOneMin, satisticsRxOneMin);
        satisticsTxOneMin=0;
        satisticsRxOneMin=0;
    }
    else if (event->timerId() == timer1h)
    {
        if (m_Master) queueService.enqueue(C2KPP_SetDateTime); // Задать время в ПП

        efficientReqSh(0); // Эффективный перезапрос состояний ШС для работающих пиборов.
        requestAllRl();    // Переапрос состояний всех Рл.

        if (adcRequestType) // Если одноэтапный тип запроса АЦП
        {
            // Сгруппировать разбитые запросы АЦП.
            auto itEnd = continuousAreaADC.constEnd();
            for(auto it = continuousAreaADC.constBegin(); it != itEnd; ++it)
            {
                quint8 cnt = it.value();
                if (cnt>1)
                {
                    quint16 nSh = it.key();

                    bool ok = true;
                    struct168 data;
                    data.b=1;

                    for (quint8 i=0; i<cnt; ++i){
                        data.a = nSh+i;
                        if (!queueRequestADC.contains(data)){
                            ok = false;
                            break;
                        }
                    }

                    if (ok){
                        for (quint8 i=0; i<cnt; ++i){
                            data.a = nSh+i;
                            queueRequestADC.removeAll(data);
                        }

                        requestADC(nSh, cnt);
                    }
                }
            }
        }
    }
}
//-----------------------------------------------------------------------------
// Получена информация об изменении состояни канала связи
//-----------------------------------------------------------------------------
void C2000PP::stateChanged(QModbusDevice::State state)
{
    if (state==QModbusDevice::ConnectedState)
    {
        if (lastState!=state)
        {
#ifdef MODBUS_DEBUG
            qInfo() << "# Измененилось состояни канала связи" << state;
#endif
            lastState = state;

            restart();

            if (timer1s==0) timer1s = startTimer(1000);
            if (timer1m==0) timer1m = startTimer(1000*60);
            if (timer1h==0) timer1h = startTimer(1000*60*60);

            satisticsTxOneSec=0;
            satisticsRxOneSec=0;

            satisticsTxOneMin=0;
            satisticsRxOneMin=0;

            emit satisticsModbusState(true);
        }
    }
    else if (state==QModbusDevice::ClosingState || state==QModbusDevice::UnconnectedState)
    {
        if (lastState==QModbusDevice::ConnectedState)
        {
#ifdef MODBUS_DEBUG
            qInfo() << "# Измененилось состояни канала связи" << state;
#endif
            lastState = state;
            if (timer1h!=0) {
                killTimer(timer1h);
                timer1h=0;
            }

            if (timer1m!=0) {
                killTimer(timer1m);
                timer1m=0;
            }

            if (timer1s!=0) {
                killTimer(timer1s);
                timer1s=0;
            }

            stop();

            emit satisticsModbusState(false);
        }
    }
}
//-----------------------------------------------------------------------------
// Получена информация об ошибках канала связи
//-----------------------------------------------------------------------------
void C2000PP::extErrorOccurred(QModbusDevice::Error errCode, QString strErr)
{
    if (!strErr.isEmpty()) emit satisticsModbusEroor(strErr);

#ifdef MODBUS_DEBUG
    qInfo() << "# Ошибка канала связи" << errCode << strErr;
#else
    Q_UNUSED(errCode)
#endif
}
//------------------------------------------------------------------------------
void C2000PP::stop()
{
    if (isConnected()) // Если подключено, то обнулить переменные
    {
        bMasterFail = false;

        countControlRl   = 0;
        countControlPart = 0;
        countControlSh   = 0;
        countRequestSh   = 0;
        countRequestRl   = 0;

        s_maxRl          = 0;
        s_maxSh          = 0;
        s_maxPart        = 0;
        //s_maxShState     = 0;
        //s_maxPartStateSh = 0;
        //s_maxEvents      = 0;
        s_halfMaxLenEvents=0;

        s_EventCount     = 0;
        s_OldEventCount  = 0xffff;

        twoStepRequest   = step2_No;

        queueConfig = -1;
        queueService.clear();
        //queueConrtolRl.clear(); // Эту очередь не обнулять!
        //queueConrtolSh.clear(); // Эту очередь не обнулять!
        queueRequestSh.clear();
        queueRequestRl.clear();
        queueRequestADC.clear();

        countBadRequestSh.clear();
        countBadRequestADC.clear();
        countBadControlSh.clear();
        countBadControlRl.clear();
        getStateShTimeer.clear();
        getStateRlTimeer.clear();

        lastNumEvent=0xffff;

        m_ShState.clear();
        m_RlState.clear();
        setVersion(0);
        setMaster(true);

        adcRequestType = false;
    }
}
//------------------------------------------------------------------------------
void C2000PP::restart(void)
{    
    stop();

    if (lastState==QModbusDevice::ConnectedState){
#ifdef MODBUS_DEBUG
        qInfo("Restart!================================");
        qInfo("Запросить тип и версию прибора.");
#endif

        readHoldingRegisters(C2KPP_GetDeviceType_L2, 2); // Запросить тип и версию прибора.
    }
}
//------------------------------------------------------------------------------
// Запросить состояния шлейфов прибора по одному. Нужно для пропавших приборов.
// Если dev!=0, то запросим состояния только для конкретного прибора.
//------------------------------------------------------------------------------
void C2000PP::requestOnAtTimeShDev(quint8 dev)
{
    QList<quint16> listSh;

    if (listShOnDev.contains(dev)) listSh = listShOnDev.value(dev);
    else
    {
        QBitArray mapSh = configC2000PP.getMapSh();
        quint16   count = static_cast<quint16>( mapSh.count() );

        for (quint16 i=1; i<=count; i++){
            if ( mapSh.testBit(i-1) && ( (dev==0) || (dev==configC2000PP.getShDev(i)) ) )
                listSh.append(i);
        }

        listShOnDev[dev] = listSh;
    }

    if (!listSh.isEmpty()){
        for (quint16 nSh : qAsConst(listSh))
            requestSh(nSh);
    }
}
//------------------------------------------------------------------------------
// Запросить состояния шлейфов прибора. Соседние шлейфы сгруппированы.
// Если dev!=0, то запросим состояния только для конкретного прибора.
//------------------------------------------------------------------------------
void C2000PP::requestShDev(quint8 dev)
{
    auto itEnd = continuousAreaSh.constEnd();
    for (auto it = continuousAreaSh.constBegin(); it != itEnd; ++it)
    {
        quint16 nSh = it.key();
        if ( (dev==0) || (dev==configC2000PP.getShDev(nSh)) )
            requestSh(nSh, it.value());
    }
}
//------------------------------------------------------------------------------
// Эффективный перезапрос состояний ШС для работающих пиборов.
// Добавить в очередь запросы состояний всех шлейфов (ШС). Сгруппировать ШС для
// ускорения процесса запроса. В группу входят соседние ШС, которые отвечали на
// предыдущие запросы и не более 10 ШС в запросе.
// Если dev!=0, то запросим состояния только для конкретного прибора.
//------------------------------------------------------------------------------
void C2000PP::efficientReqSh(quint8 dev)
{
    quint16 firstSh = 0xffff;
    QMap <quint16, quint8> continuousArea;
    QBitArray mapSh = configC2000PP.getMapSh();

    quint16 count =static_cast<quint16>( mapSh.count() );
    for (quint16 i=1; i<=count; i++)
    {
        if ( mapSh.testBit(i-1) && ((dev==0) || (dev==configC2000PP.getShDev(i))) )
        {
            //if (m_ShState.contains(i))
            if (m_ShState.value(i)!=0)
            {
                if (firstSh == 0xffff)
                {
                    firstSh = i;
                    continuousArea[firstSh] = 1;
                }
                else {
                    if (++continuousArea[firstSh]>=10) firstSh = 0xffff;
                }
            }
            else {
                firstSh = 0xffff;
                continuousArea[i] = 1;
            }
        }
        else firstSh = 0xffff;
    }

    // Очередь запроса состояний ШС. <номер ШС, количество ШС>
    auto itEnd = continuousArea.constEnd();
    for (auto it = continuousArea.constBegin(); it != itEnd; ++it)
        requestSh(it.key(), it.value());
}
//------------------------------------------------------------------------------
// Добавить в очередь запрос состояний всех Рл.
//------------------------------------------------------------------------------
void C2000PP::requestAllRl(void)
{
    if (confRlMax>0){
        struct88 data;
        data.a = 1;
        data.b = confRlMax;
        if (!queueRequestRl.contains(data)) queueRequestRl.enqueue(data);
    }
}
//------------------------------------------------------------------------------
// Добавить в очередь запрос состояния Рл.
//------------------------------------------------------------------------------
void C2000PP::requestRl(quint8 nRl)
{
    if ( configC2000PP.isCorrectRl(nRl) && (nRl<=s_maxRl)){
        struct88 data;
        data.a = nRl;
        data.b = 1;
        if (!queueRequestRl.contains(data)) queueRequestRl.enqueue(data);
    }
}
//------------------------------------------------------------------------------
// Добавить в очередь запрос состояния ШС.
//------------------------------------------------------------------------------
void C2000PP::requestSh(quint16 nSh)
{
    if ( configC2000PP.isCorrectSh(nSh) && (nSh<=s_maxSh)){
        struct168 data;
        data.a = nSh;
        data.b = 1;
        if (!queueRequestSh.contains(data)) queueRequestSh.enqueue(data);
    }
}
//------------------------------------------------------------------------------
// Добавить в очередь запрос состояний нескольких ШС.
//------------------------------------------------------------------------------
void C2000PP::requestSh(quint16 nSh, quint8 count)
{
    if ( (nSh!=0) && ((nSh-1+count)<=s_maxSh) ){
        struct168 data;
        data.a = nSh;
        data.b = count;
        if (!queueRequestSh.contains(data)) queueRequestSh.enqueue(data);
    }
}
//------------------------------------------------------------------------------
// Добавить в очередь запрос температуры/влажности/CO.
//------------------------------------------------------------------------------
void C2000PP::requestADC(quint16 nSh)
{
    if ( configC2000PP.isCorrectSh(nSh) && (nSh<=s_maxSh)){
        struct168 data;
        data.a = nSh;
        data.b = 1;
        if (!queueRequestADC.contains(data)) queueRequestADC.enqueue(data);
    }
}
//------------------------------------------------------------------------------
// Добавить в очередь запрос температуры/влажности/CO.
//------------------------------------------------------------------------------
void C2000PP::requestADC(quint16 nSh, quint8 count)
{
    if ( (nSh!=0) && ((nSh-1+count)<=s_maxSh) ){
        struct168 data;
        data.a = nSh;
        data.b = count;
        if (!queueRequestADC.contains(data)) queueRequestADC.enqueue(data);
    }
}
//------------------------------------------------------------------------------
// Очистить очередь АЦП шлейфов для данного прибора
//------------------------------------------------------------------------------
void C2000PP::clearADCforDev(quint8 dev)
{
    if (dev==0) queueRequestADC.clear();
    else {
        QQueue <struct168> newQueueRequestADC;
        for (struct168 data : qAsConst(queueRequestADC)){
            if ( dev!=configC2000PP.getShDev(data.a) )
                newQueueRequestADC.enqueue(data);
        }

        queueRequestADC = newQueueRequestADC;
    }
}
//------------------------------------------------------------------------------
void C2000PP::clearADCforDev(QSet<quint8> devs)
{
    if (devs.contains(0)) queueRequestADC.clear();
    else {
        QQueue <struct168> newQueueRequestADC;
        for (struct168 data : qAsConst(queueRequestADC)){
            if ( !devs.contains( configC2000PP.getShDev(data.a) ) )
                newQueueRequestADC.enqueue(data);
        }
        queueRequestADC = newQueueRequestADC;
    }
}
//------------------------------------------------------------------------------
// Заполнить очередь АЦП шлейфов для данного прибора
//------------------------------------------------------------------------------
void C2000PP::addADCforDev(quint8 dev)
{
    auto itEnd = continuousAreaADC.constEnd();
    for(auto it = continuousAreaADC.constBegin(); it != itEnd; ++it)
    {
        quint16 nSh = it.key();
        if ( (dev==0) || (dev==configC2000PP.getShDev(nSh)) ){
            struct168 data;
            data.a = nSh;
            data.b = it.value();
            if (!queueRequestADC.contains(data)) queueRequestADC.enqueue(data);
        }
    }
}
//------------------------------------------------------------------------------
void C2000PP::addADCforDev(QSet<quint8> devs)
{
    auto itEnd = continuousAreaADC.constEnd();
    for(auto it = continuousAreaADC.constBegin(); it != itEnd; ++it)
    {
        quint16 nSh = it.key();
        if ( (devs.contains(0)) || (devs.contains( configC2000PP.getShDev(nSh)) )){
            struct168 data;
            data.a = nSh;
            data.b = it.value();
            if (!queueRequestADC.contains(data)) queueRequestADC.enqueue(data);
        }
    }
}
//-----------------------------------------------------------------------------
// Чтение из нескольких регистров флагов (1 - Read Coil Status)
//-----------------------------------------------------------------------------
void C2000PP::readCoilStatus(int reg, quint16 len)
{
#ifdef MODBUS_DEBUG
    qInfo() << "<[01]" << reg << len;
#endif

    QModbusDataUnit modbusDataUnit = QModbusDataUnit(QModbusDataUnit::Coils, reg, len);
    emit sendReadRequest(modbusDataUnit);
    ++satisticsTxOneSec;
    ++satisticsTxOneMin;
}
//-----------------------------------------------------------------------------
// Чтение из нескольких регистров входов (2 - DiscreteInputs)
//-----------------------------------------------------------------------------
void C2000PP::readDiscreteInputs(int reg, quint16 len)
{
#ifdef MODBUS_DEBUG
    qInfo() << "<[02]" << reg << len;
#endif

    QModbusDataUnit modbusDataUnit = QModbusDataUnit(QModbusDataUnit::DiscreteInputs, reg, len);
    emit sendReadRequest(modbusDataUnit);
    ++satisticsTxOneSec;
    ++satisticsTxOneMin;
}
//-----------------------------------------------------------------------------
// Чтение из нескольких регистров хранения (3 - Read Holding Registers)
//-----------------------------------------------------------------------------
void C2000PP::readHoldingRegisters(int reg, quint16 len)
{
#ifdef MODBUS_DEBUG
    if (reg!=C2KPP_GetEventCount_L1)
        qInfo() << "<[03]" << reg << len;
#endif

    QModbusDataUnit modbusDataUnit = QModbusDataUnit(QModbusDataUnit::HoldingRegisters, reg, len);
    emit sendReadRequest(modbusDataUnit);
    ++satisticsTxOneSec;
    ++satisticsTxOneMin;
}
//-----------------------------------------------------------------------------
// Чтение значений из нескольких входных регистров (4 - Read Input Registers)
//-----------------------------------------------------------------------------
void C2000PP::readInputRegisters(int reg, quint16 len)
{
#ifdef MODBUS_DEBUG
    qInfo() << "<[04]" << reg << len;
#endif

    QModbusDataUnit modbusDataUnit = QModbusDataUnit(QModbusDataUnit::InputRegisters, reg, len);
    emit sendReadRequest(modbusDataUnit);
    ++satisticsTxOneSec;
    ++satisticsTxOneMin;
}
//-----------------------------------------------------------------------------
// Запись значения одного флага (5 - Force Single Coil)
//-----------------------------------------------------------------------------
void C2000PP::forceSingleCoil(int reg, quint16 data)
{
    QVector<quint16> buff;
    buff.append(data);

#ifdef MODBUS_DEBUG
    qInfo() << "<[05]" << reg << data;
#endif

    QModbusDataUnit modbusDataUnit = QModbusDataUnit(QModbusDataUnit::Coils, reg, buff);
    emit sendWriteRequest(modbusDataUnit);
    ++satisticsTxOneSec;
    ++satisticsTxOneMin;
}
//-----------------------------------------------------------------------------
// Запись в один регистр хранения (6 - Preset Single Registers)
//-----------------------------------------------------------------------------
void C2000PP::presetHoldinRegisters(int reg, quint16 data)
{
    QVector<quint16> buff;
    buff.append(data);

#ifdef MODBUS_DEBUG
    qInfo() << "<[06]" << reg << data;
#endif

    QModbusDataUnit modbusDataUnit = QModbusDataUnit(QModbusDataUnit::HoldingRegisters, reg, buff);
    emit sendWriteRequest(modbusDataUnit);
    ++satisticsTxOneSec;
    ++satisticsTxOneMin;
}
//-----------------------------------------------------------------------------
// Задать время в приборе С2000-ПП (16 - Preset Multiple Registers)
//-----------------------------------------------------------------------------
void C2000PP::setTime(void)
{
    QVector<quint16> buff;

    QDateTime dateTime = QDateTime::currentDateTime();
    buff.append( static_cast<quint16>( (dateTime.time().hour()  << 8) | dateTime.time().minute() ) );
    buff.append( static_cast<quint16>( (dateTime.time().second()<< 8) | dateTime.date().day() ) );
    buff.append( static_cast<quint16>( (dateTime.date().month() << 8) | static_cast<quint8>(dateTime.date().year()-2000) ) );

#ifdef MODBUS_DEBUG
    qInfo() << "<[16]" << C2KPP_SetDateTime << buff << "Установить время в приборе.";
#endif

    QModbusDataUnit modbusDataUnit = QModbusDataUnit(QModbusDataUnit::HoldingRegisters, C2KPP_SetDateTime, buff);
    emit sendWriteRequest(modbusDataUnit);
    ++satisticsTxOneSec;
    ++satisticsTxOneMin;
}
//------------------------------------------------------------------------------
// Приняты данные либо ошибка.
//------------------------------------------------------------------------------
void C2000PP::readyRead(void)
{
    auto reply = qobject_cast<QModbusReply *>(sender());
    if (reply)
    {
        // --------------------------------------------------
        // Ошибок нет
        // --------------------------------------------------
        if (reply->error() == QModbusDevice::NoError)
        {
            ++satisticsRxOneSec;
            ++satisticsRxOneMin;

            const QModbusDataUnit unit = reply->result();

#ifdef MODBUS_DEBUG
            if (unit.startAddress()!=C2KPP_GetEventCount_L1)
            {
                if (unit.registerType()==QModbusDataUnit::Coils){
                    qInfo() << ">[01]" << unit.startAddress() << unit.values();
                }
                else if (unit.registerType()==QModbusDataUnit::DiscreteInputs){
                    qInfo() << ">[02]" << unit.startAddress() << unit.values();
                }
                else if (unit.registerType()==QModbusDataUnit::HoldingRegisters){
                    qInfo() << ">[03]" << unit.startAddress() << unit.values();
                }
                else if (unit.registerType()==QModbusDataUnit::InputRegisters){
                    qInfo() << ">[04]" << unit.startAddress() << unit.values();
                }
            }
#endif

            if (unit.registerType()==QModbusDataUnit::Coils){
                readyReadCoilsRegisters(unit);
            }
            else if (unit.registerType()==QModbusDataUnit::DiscreteInputs){
                readyReadDiscreteInputs(unit);
            }
            else if (unit.registerType()==QModbusDataUnit::HoldingRegisters){
                readyReadHoldingRegisters(unit);
            }
            else if (unit.registerType()==QModbusDataUnit::InputRegisters){
                readyReadInputRegisters(unit);
            }
            else nextRequest();
        }
        // --------------------------------------------------
        // Ошибка протокола (ошибку передал сам С2000-ПП)
        // --------------------------------------------------
        else if (reply->error() == QModbusDevice::ProtocolError)
        {
            ++satisticsRxOneSec;
            ++satisticsRxOneMin;

            //const QModbusPdu::FunctionCode  functionCode = reply->rawResult().functionCode();
            const QModbusPdu::ExceptionCode exceptionCode = reply->rawResult().exceptionCode();
            const int startAddress = reply->property("startAddress").toInt();

#ifdef MODBUS_DEBUG
            qInfo() << QStringLiteral("### Read response error: %1 (exception: 0x%2), %3").
                       arg(reply->errorString()).
                       arg(exceptionCode, -1, 16).
                       arg(startAddress);
#endif

            readyReadError(exceptionCode, startAddress);
        }
        // --------------------------------------------------
        // Остальные ошибки (таймаут и т.д.)
        // --------------------------------------------------
        else
        {
#ifdef MODBUS_DEBUG
            const int startAddress = reply->property("startAddress").toInt();
            qInfo() << QStringLiteral("### Read response error: %1 (code: 0x%2), %3").
                       arg(reply->errorString()).
                       arg(reply->error(), -1, 16).
                       arg(startAddress)
                    << reply->rawResult().functionCode();
#endif

            restart();
        }

        reply->deleteLater();
    }
}
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
void C2000PP::readyReadCoilsRegisters(const QModbusDataUnit &unit)
{
    if (!isConnected()) return;

    const int startAddress = unit.startAddress();

    // --------------------------------------------------
    // Получено состояние реле
    // --------------------------------------------------
    if ( (startAddress >= C2KPP_FirstRl) && (startAddress < C2KPP_FirstRl + s_maxRl) )
    {
#ifdef MODBUS_DEBUG
        qInfo() << "Получено состояние реле";
#endif
        int count = static_cast<int>(unit.valueCount());
        for (int i = 0; i < count; ++i)
        {
            quint8 nRl = static_cast<quint8>(startAddress + 1 - C2KPP_FirstRl + i);

            if (configC2000PP.isCorrectRl(nRl))
            {
                quint8 state = static_cast<quint8>(unit.value(i));
                setRlState(nRl, state);

#ifdef MODBUS_DEBUG
                qInfo() << QStringLiteral(u"Рл %1: %2").arg(nRl).arg(state);
#endif
            }
        }
    }

    nextRequest();
}
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
void C2000PP::readyReadDiscreteInputs(const QModbusDataUnit &unit)
{
    if (!isConnected()) return;

    const int startAddress = unit.startAddress();
    // --------------------------------------------------
    // Режим работы по интерфейсу Орион
    // --------------------------------------------------
    if (startAddress == C2KPP_BI_GetMasterSlave_L1)
    {
        if (unit.value(0)==0) setMaster(false);
        else {
            setMaster(true);
            queueService.enqueue(C2KPP_SetDateTime); // Задать время в ПП
        }
    }

    nextRequest();
}
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
void C2000PP::readyReadInputRegisters(const QModbusDataUnit &unit)
{    
    if (!isConnected()) return;

    const int startAddress = unit.startAddress();

    // ------------------------------------------------------------
    // Чтение конфигурации прибора.
    // ------------------------------------------------------------
    if (queueConfig>=0)
    {
        configC2000PP.setConfigFromVector(startAddress, unit.values());

        queueConfig+=64;
        //emit configReading(queueConfig*100/2688); // Ошибка в документации на ПП 2.0
        emit configReading(queueConfig*100/2623);
        //if (queueConfig>2687) { // Ошибка в документации на ПП 2.0
        if (queueConfig>2623) {
            queueConfig=-1;

            configC2000PP.saveFile(mainPath+constFileNameConfigPP);
            configC2000PP.calcCorrect();
            loadConfig();

            emit signalNewConfigC2000PP();
        }
    }

    nextRequest();
}
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
void C2000PP::readyReadHoldingRegisters(const QModbusDataUnit &unit)
{
    const int startAddress = unit.startAddress();

    if (!isConnected())
    {
        //--------------------------------------------------
        // Тип прибора и версия прошивки.
        //--------------------------------------------------
        if (startAddress == C2KPP_GetDeviceType_L2)
        {
            const quint16 devType = unit.value(0);
            const quint16 devVer  = unit.value(1);

            if ( (devVer>0) && (devType==36) )
            {
#ifdef MODBUS_DEBUG
                qInfo("Соединение установлено, начнём опришивать прибор.");
                qInfo("Запросить максимальные значения переменных.");
#endif
                setVersion(devVer);
                readHoldingRegisters(C2KPP_GetMaxValue_L7, 7); // Запросить максимальные значения переменных
                return;
            }
        }

        restart();
    }
    else
    {
        // --------------------------------------------------
        // Получено состояние ШС
        // --------------------------------------------------
        if ( (startAddress >= C2KPP_FirstSh) && (startAddress < C2KPP_FirstSh + s_maxSh) )
        {
#ifdef MODBUS_DEBUG
            qInfo() << "Получено состояние ШС";
#endif
            QSet <quint8> setClearDevADC;
            QSet <quint8> setAddDevADC;

            int count = static_cast<int>(unit.valueCount());
            for (int i = 0; i < count; ++i)
            {
                quint16 nSh = static_cast<quint16>(startAddress + 1 - C2KPP_FirstSh + i);
                if (configC2000PP.isCorrectSh(nSh))
                {
                    quint16 state = unit.value(i);
                    quint8 state1 = static_cast<quint8>(state>>8);
                    quint8 state2 = static_cast<quint8>(state);
                    quint8 lastState = m_ShState.value(nSh);

                    if (state1==0) state1 = state2;

                    bool ok = false;
                    if (bMasterFail){
                        quint8 dev = configC2000PP.getShDev(nSh);
                        if (dev==0) ok = setShState(nSh, state1);
                    }
                    else{
                        ok = setShState(nSh, state1);
                    }

                    if (ok)
                    {
                        if (state1==250) // Нет связи с прибором
                        {
                            quint8 dev = configC2000PP.getShDev(nSh);

                            // Очистить очередь АЦП для данного прибора.
                            // Если этого не сделать, то ПП 1.32 перезагружается при таких запросах.
                            setClearDevADC.insert( dev );

                            if ( (dev==0) && (!m_Master) ) // Потеряна связь с пультом С2000М
                            {
                                // Это в двух местах для надежности и скорости реакции.
                                setShState(0, 0);
                                setShState(nSh, state1);
                                bMasterFail = true;
                            }
                        }
                        else if (lastState==250) // Не было связи с прибором и появилась
                        {
                            quint8 dev = configC2000PP.getShDev(nSh);

                            setAddDevADC.insert( dev );

                            if ( (dev==0) && (!m_Master) ) // Востановлена связь с пультом С2000М
                            {
                                // Это в двух местах для надежности и скорости реакции.
                                bMasterFail = false;
                            }
                        }
                    }

#ifdef MODBUS_DEBUG
                    qInfo() << QStringLiteral(u"ШС %1: %2, %3").arg(nSh).arg(state1).arg(state2);
#endif
                }
            }

            if (!setClearDevADC.isEmpty()) clearADCforDev( setClearDevADC );
            if (!setAddDevADC.isEmpty())   addADCforDev( setAddDevADC );

            if (!queueRequestSh.isEmpty()){
                struct168 dataSh = queueRequestSh.dequeue();
                if (dataSh.b==1) countBadRequestSh.remove(dataSh.a); // Если одиночный шлейф, а не группа шлейфов.
            }
        }
        // --------------------------------------------------
        // Получено самое старое событие в кольцевом буфере
        // --------------------------------------------------
        else if (startAddress == C2KPP_GetEvent_Md2)
        {
            //quint8 lenght = static_cast<quint8>(unit.value(1)>>8);
            quint8 event  = static_cast<quint8>(unit.value(1));

            if (event!=0)
            {
                quint16 numEvent = unit.value(0); // Номер события в буфере прибора
                if (lastNumEvent!=numEvent)
                {
                    lastNumEvent = numEvent;

                    // Следующим шагом нужно удалить это событие из кольцевого буфера прибора.
                    twoStepRequest      = step2_event;
                    twoStepRequestEvent = numEvent;

                    EventC2000PP eventPP;
                    eventPP.setDatePC(QDateTime::currentDateTime());
                    eventPP.setEvent(event, s_OldEventCount!=0);

                    quint16 nUser   = 0;
                    quint8  nPart   = 0;
                    quint16 nSh     = 0;
                    quint8  nRl     = 0;
                    quint8  rlState = 0;
#ifdef MODBUS_DEBUG
                    quint16 idPart  = 0;
#endif

                    const int count = static_cast<int>(unit.valueCount());
                    for (int i=2; i+1 <= count;)
                    {
                        quint8 cod = static_cast<quint8>(unit.value(i)>>8);
                        quint8 len = static_cast<quint8>(unit.value(i));

                        switch (cod){
                        case 1: nUser   = unit.value(i+1);                      eventPP.setUser(nUser);   break;
                        case 2: nPart   = static_cast<quint8>(unit.value(i+1)); eventPP.setPart(nPart);   break;
                        case 3: nSh     = unit.value(i+1);                      eventPP.setSh(nSh);       break;
                        case 5: nRl     = static_cast<quint8>(unit.value(i+1)); eventPP.setRl(nRl);       break;
                        case 7: rlState = static_cast<quint8>(unit.value(i+1)); eventPP.setRlSt(rlState); break;
                        case 11:
                            if (i+3 <= count) {
                                quint8 hour   = static_cast<quint8>(unit.value(i+1)>>8);
                                bool badData = hour & 0x80;
                                bool badTime = hour & 0x40;
                                hour &= 0x3F;

                                quint8 minute = static_cast<quint8>(unit.value(i+1));
                                quint8 second = static_cast<quint8>(unit.value(i+2)>>8);
                                quint8 day    = static_cast<quint8>(unit.value(i+2));
                                quint8 month  = static_cast<quint8>(unit.value(i+3)>>8);
                                quint8 year   = static_cast<quint8>(unit.value(i+3));

                                // Устранение ошибки в С2000-ПП. В событии для реле не хватает байта часа.
                                if (m_deviceVer<=132)
                                {
                                    if (eventPP.isRl()){
                                        year    = month;
                                        month   = day;
                                        day     = second;
                                        second  = minute;
                                        minute  = hour;
                                        hour    = lastHour;
                                        badTime = true;
                                    }
                                    else lastHour = hour;
                                }

                                QDateTime dateTime;
                                dateTime.setDate( QDate(2000 + year, month, day) );
                                dateTime.setTime( QTime(hour, minute, second) );
                                eventPP.setDatePP(dateTime, badData, badTime);

#ifdef MODBUS_DEBUG
                                qInfo("------------------------------");
                                qInfo() << "dateTime1" << dateTime;
                                qInfo() << "dateTime2" << hour <<':'<< minute <<':'<< second <<' '<< day <<'.'<< month <<'.'<< year << badData << badTime;
#endif

                                if (len==0) i+=2;
                            }
                            break;
#ifdef MODBUS_DEBUG
                        case 24:idPart = unit.value(i+1); break;
#endif
                        }

                        if (len==0) i+=2; else i+=1+(len>>1);
                    }

#ifdef MODBUS_DEBUG
                    qInfo() << "Разбор события:" << unit.value(0);
                    //qInfo() << "lenght " << lenght;
                    qInfo() << "event  " << event;
                    qInfo() << "nUser  " << nUser;
                    qInfo() << "nPart  " << nPart;
                    qInfo() << "idPart " << idPart;
                    qInfo() << "nSh    " << nSh;
                    qInfo() << "nRele  " << nRl;
                    qInfo() << "stateRl" << rlState;
                    //qInfo() << s_OldEventCount;
                    qInfo("------------------------------");
#endif

                    // Реакция на события
                    if (!eventPP.isOldEvent()) // Если событие не старое
                    {
                        if (nSh!=0) // Если событие от ШС
                        {
                            if (event==250) // Если потеряна связь с прибором
                            {
                                quint8 dev = configC2000PP.getShDev(nSh);
                                if (dev!=0xff){

                                    requestOnAtTimeShDev(dev); // Запросим состояния шлейфов прибора по одному.

                                    // Это не требуется. Это реализовано не через события, а через запрос состояния ШС.
                                    //clearADCforDev(dev); // Очистим очередь запросов АЦП для данного прибора

                                    if (dev==0)
                                    {
                                        if (!m_Master) // Потеряна связь с пультом С2000М
                                        {
                                            requestAllRl(); // Переапрос состояний всех Рл.

                                            // Это в двух местах для надежности и скорости реакции.
                                            setShState(0, 0);
                                            bMasterFail = true;
                                        }
                                    }

                                    //*** Обнулить показания АЦП всех ШС прибора?
                                }
                            }
                            else if (event==251 || event==252) // Если восстановлена связь с прибором или подмена прибора
                            {
                                quint8 dev = configC2000PP.getShDev(nSh);
                                if (dev!=0xff){

                                    requestShDev(dev); // Запросим состояния шлейфов прибора групповыми запросам.

                                    if (dev!=0)
                                    {
                                        // Очистим очередь запросов АЦП для данного прибора, чтобы потом добавить их снова.
                                        // Нужно из-зи того, что в очереди могут быть разбитые групповые запросы,
                                        // а при добавлении новых это не обнаружится.
                                        clearADCforDev(dev);

                                        // Запрос температуры/влажности/CO для всех ШС прибора.
                                        addADCforDev(dev);
                                    }
                                    else{
                                        if (!m_Master) // Восстановлена связь с пультом С2000М
                                        {
                                            requestAllRl();      // Перезапрос состояний всех Рл.

                                            // Это в двух местах для надежности и скорости реакции.
                                            bMasterFail = false;
                                        }
                                    }
                                }
                            }
                            else {
                                requestSh(nSh); // запросим состояние одного ШС.

                                if (event==187){ // Потеря связи с ШС
                                    emit shOneADC(nSh, static_cast<qint16>(0xFFFF));
                                }
                                else if (event==188){ // Восстановление связи с ШС
                                    if (configC2000PP.getShType(nSh)==6) // Если тип ШС == 6, то
                                        requestADC(nSh); // поставим в очередь запрос АЦП ШС.
                                }

                                // Т.к. от данного ШС приходят события, то не требуется запрашивать состояние по таймеру.
                                if (getStateShTimeer.contains(nSh)) getStateShTimeer.remove(nSh);
                            }
                        }
                        else if (nRl!=0) // Если событие от Рл,
                        {
                            requestRl(nRl); // то запросим состояние Рл.

                            // Т.к. от данного реле приходят события, то не требуется запрашивать состояние по таймеру.
                            if (getStateRlTimeer.contains(nRl)) getStateRlTimeer.remove(nRl);
                        }
                    }

                    emit eventReceived(eventPP);
                }
            }
            else {
#ifdef MODBUS_DEBUG
                // Нулевое событие возникает если стереть одно событие два раза!
                qInfo() << "### Нулевое событие." << unit.values();
#endif
                noop(1);
            }
        }
        // --------------------------------------------------
        // Количество непрочитанных событий в буфере прибора.
        // --------------------------------------------------
        else if (startAddress == C2KPP_GetEventCount_L1)
        {
            s_EventCount = unit.value(0);       // Количество непрочитанных событий в буфере прибора.
            if (s_OldEventCount==0xffff)        // Если это начало опроса, то
                s_OldEventCount = s_EventCount; // зададим количество "старых" событий.

            if (s_EventCount!=0) {
#ifdef MODBUS_DEBUG
                qInfo() << "Количество непрочитанных событий" << s_EventCount;
#endif
                queueService.removeAll(C2KPP_GetEventCount_L1);
            }
        }
        // --------------------------------------------------
        // Получено АЦП/напряжение/ток в двухэтапном запросе.
        // --------------------------------------------------
        else if (startAddress == C2KPP_RequestADC_L1)
        {
            quint16 adc = unit.value(0);
            if (static_cast<quint8>(adc)==1) adc&=0xff00;

            emit shOneADC(zoneInTwoStepRequestADC, adc);
#ifdef MODBUS_DEBUG
            qInfo() << "Получено АЦП/напряжение/ток" << zoneInTwoStepRequestADC << adc;
#endif
            zoneInTwoStepRequestADC=0;
        }
        // --------------------------------------------------
        // Получено значение счетчика в двухэтапном запросе.
        // --------------------------------------------------
        else if (startAddress == C2KPP_RequestCounter_L3)
        {
            if (unit.valueCount()==3){
                quint64 counter = (quint64(unit.value(0))<<32) | (quint64(unit.value(1))<<16) | unit.value(2);
                emit shOneCounter(zoneInTwoStepRequestCNT, counter);

#ifdef MODBUS_DEBUG
                qInfo() << "Получено значение счетчика" << zoneInTwoStepRequestCNT << counter;
#endif
            }

            zoneInTwoStepRequestCNT=0;
        }
        // --------------------------------------------------
        // Получено числовое значение температуры/влажности/СО
        // одноэтапным запросом
        // --------------------------------------------------
        else if ( (startAddress >= C2KPP_FirstADC) && (startAddress < C2KPP_FirstADC + s_maxSh) )
        {
            if (queueRequestADC.count()>1){
                queueRequestADC.enqueue( queueRequestADC.dequeue() );
            }

            const int count = static_cast<int>(unit.valueCount());
            if (count==1) {
                quint16 nSh = static_cast<quint16>(startAddress + 1 - C2KPP_FirstADC);
                qint16  adc = static_cast<qint16> (unit.value(0));
                countBadRequestADC.remove(nSh);
                emit shOneADC(nSh, adc);
            }
            else if (count>1) {
                QMap<quint16, qint16> mapADC;

                for (int i = 0; i < count; ++i) {
                    quint16 nSh = static_cast<quint16>(startAddress + 1 - C2KPP_FirstADC + i);
                    qint16  adc = static_cast<qint16>(unit.value(i));
                    mapADC[nSh] = adc;
                }

                emit shMultiADC(mapADC);
            }

#ifdef MODBUS_DEBUG
            for (int i = 0; i < count; ++i)
            {
                quint16 nSh = static_cast<quint16>(startAddress + 1 - C2KPP_FirstADC + i);
                qint16 d = static_cast<qint16>(unit.value(i));
                float val = static_cast<float>(d/256.0);
                quint8 h = static_cast<quint8>(d>>8);
                quint8 l = static_cast<quint8>(d);
                qInfo() << "Т/В/СО" << nSh << "(" << h << l <<")"<< val;
            }
#endif
        }
        // --------------------------------------------------
        // Максимальные значения переменных, поддерживаемые прибором.
        // --------------------------------------------------
        else if (startAddress == C2KPP_GetMaxValue_L7)
        {
            s_maxRl          = unit.value(0);   // Максимальное количество реле.
            s_maxSh          = unit.value(1);   // Максимальное количество шлейфов.
            s_maxPart        = unit.value(2);   // Максимальное количество разделов.
            //s_maxShState     = unit.value(3);   // Максимальное количество состояний шлейфов.
            //s_maxPartStateSh = unit.value(4);   // Максимальное количество состояний разделов.
            //s_maxEvents      = unit.value(5);   // Максимальное количество событий.
            s_halfMaxLenEvents=unit.value(6)>>1;// Половина максимальной длины описания события.

            // --------------------------------------------------
            // Первоначальное заполнение очередей.
            // --------------------------------------------------
            if (m_deviceVer<200)
                // Установить время и так узнать в каком режиме (master/slave) прибор
                queueService.enqueue(C2KPP_SetDateTime);
            else{
                // Для ПП 2.0 выяснить режим работы master/slave
                queueService.enqueue(C2KPP_BI_GetMasterSlave_L1);
            }

            queueService.enqueue(C2KPP_GetEventCount_L1); // Запросить количство не считанных событий

            // Очередь запроса состояний ШС. <номер ШС, количество ШС>
            auto itEnd = continuousAreaSh.constEnd();
            for (auto it = continuousAreaSh.constBegin(); it != itEnd; ++it)
                requestSh(it.key(), it.value());

            requestAllRl(); // Поставить в очередь запрос состояний всех реле

            // Очередь одноэтапных запросовы АЦП.
            itEnd = continuousAreaADC.constEnd();
            for(auto it = continuousAreaADC.constBegin(); it != itEnd; ++it)
                requestADC(it.key(), it.value());

            // Очередь двухэтапных запросов АЦП.
            queueRequestADC6.append( listShType6 );
            queueRequestADC7.append( listShType7 );
            queueRequestADC8.append( listShType8 );
        }

#ifdef MODBUS_DEBUG
        // --------------------------------------------------
        // Ответ на запрос даты и времени
        // --------------------------------------------------
        else if (startAddress == C2KPP_GetDateTime_L3)
        {
            quint8 hour  = static_cast<quint8>(unit.value(0)>>8);
            bool badData = hour & 0x80;
            bool badTime = hour & 0x40;
            hour &= 0x3F;

            quint8 minute = static_cast<quint8>(unit.value(0));
            quint8 second = static_cast<quint8>(unit.value(1)>>8);
            quint8 day    = static_cast<quint8>(unit.value(1));
            quint8 month  = static_cast<quint8>(unit.value(2)>>8);
            quint8 year   = static_cast<quint8>(unit.value(2));

            qInfo() << hour <<':'<< minute <<':'<< second <<' '<< day <<'.'<< month <<'.'<< year << badData << badTime;

            QDateTime dateTime;
            dateTime.setDate( QDate(2000 + year, month, day) );
            dateTime.setTime( QTime(hour, minute, second) );
            qInfo() <<"GetDateTime" << dateTime << dateTime.toSecsSinceEpoch();
        }
#endif
        nextRequest();
    }
}
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
void C2000PP::readyReadError(const QModbusPdu::ExceptionCode exceptionCode, const int startAddress)
{
    if ((exceptionCode==6 || exceptionCode==15) && queueService.count()<5){
        noop(3); // Сделаем паузу
    }

    // ------------------------------------------------------------
    // Ошибка чтения конфигурации прибора.
    // ------------------------------------------------------------
    if (queueConfig>=0){
        queueConfig = -1;
        configC2000PP.loadFile(mainPath+constFileNameConfigPP);
        emit configReading(-1);
    }
    // --------------------------------------------------
    // Ошибка на запрос состояния ШС. Разобъем
    // запрашиваемый блок шлейфов или исключим этот шлейф
    // из запроса если запрос выполняется неудачно 15 раз
    // --------------------------------------------------
    else if ( (startAddress >= C2KPP_FirstSh) && (startAddress < C2KPP_FirstSh + s_maxSh) )
    {
        if (!queueRequestSh.isEmpty())
        {
            struct168 dataSh = queueRequestSh.dequeue();
            quint16 nSh = dataSh.a;
            quint8  cnt = dataSh.b;

            if (cnt>1){ // Разбить блок шлейфов на одиночные шлейфы
                for (quint16 i = 0; i<cnt; i++ ) requestSh(nSh+i);
            }
            else // Если это запрос одиночного шлейфа, запрашивать его не больше 15 раз.
            {
                // Если прибор занят или данные пока не получены
                if ( (exceptionCode==6) || (exceptionCode==15) )
                {
                    if (countBadRequestSh.contains(nSh)) ++countBadRequestSh[nSh];
                    else countBadRequestSh[nSh] = 1;

                    if (countBadRequestSh.value(nSh)<15) requestSh(nSh);
                    else {
                        countBadRequestSh.remove(nSh);
                        if (!bMasterFail) setShState(nSh, 0);
                    }
                }
                else {
                    countBadRequestSh.remove(nSh);
                    if (!bMasterFail) setShState(nSh, 0);
                }
            }
        }
    }
    // --------------------------------------------------
    // Ошибка на запрос температуры/влажности/CO одноэтапным
    // запросом. Разобъем запрашиваемый блок шлейфов или
    // исключим этот шлейф из запроса если запрос
    // выполняется неудачно 15 раз
    // --------------------------------------------------
    else if ( (startAddress >= C2KPP_FirstADC) && (startAddress < C2KPP_FirstADC + s_maxSh) )
    {
        if (!queueRequestADC.isEmpty())
        {
            struct168 dataSh = queueRequestADC.dequeue();
            quint16 nSh = dataSh.a;
            quint8  cnt = dataSh.b;

            if (cnt>1){ // Разбить блок шлейфов на одиночные шлейфы
                for (quint16 i = 0; i<cnt; i++ ) requestADC(nSh+i);
            }
            else // Если это запрос одиночного шлейфа, запрашивать его не больше 15 раз.
            {
                // Если прибор занят или данные пока не получены
                if ( (exceptionCode==6) || (exceptionCode==15) )
                {
                    if (countBadRequestADC.contains(nSh)) ++countBadRequestADC[nSh];
                    else countBadRequestADC[nSh] = 1;

                    if (countBadRequestADC.value(nSh)<15) requestADC(nSh);
                    else countBadRequestADC.remove(nSh);
                }
                else countBadRequestADC.remove(nSh);
            }
        }
    }
    // --------------------------------------------------
    // Ошибка на запрос АЦП/напряжения/тока. Данные еще
    // не получены ПП от прибора, запросим повторно.
    // --------------------------------------------------
    else if ( startAddress == C2KPP_RequestADC_L1)
    {
        // Следующим шагом нужно запросить значение.
        if (++countBadGetADC>20){
            twoStepRequest =step2_No;
            zoneInTwoStepRequestADC = 0;
            noop(1); // На всякий случай
        }
        else twoStepRequest = step2_ADC;
    }
    // --------------------------------------------------
    // Ошибка на запрос счетчика. Данные еще не получены
    // ПП от прибора, запросим повторно.
    // --------------------------------------------------
    else if ( startAddress == C2KPP_RequestCounter_L3)
    {
        // Следующим шагом нужно запросить значение.
        if (++countBadGetADC>20){
            zoneInTwoStepRequestCNT = 0;
            twoStepRequest =step2_No;
            noop(1); // На всякий случай
        }
        else twoStepRequest = step2_Counter;
    }

    nextRequest();
}
//------------------------------------------------------------------------------
// Принято подтверждение записи, либо ошибка.
//------------------------------------------------------------------------------
void C2000PP::readyWrite(void)
{
    auto reply = qobject_cast<QModbusReply *>(sender());
    if (reply)
    {
        // --------------------------------------------------
        // Ошибок нет
        // --------------------------------------------------
        if (reply->error() == QModbusDevice::NoError)
        {
            ++satisticsRxOneSec;
            ++satisticsRxOneMin;

            const QModbusDataUnit unit = reply->result();

#ifdef MODBUS_DEBUG
            if (unit.registerType()==QModbusDataUnit::Coils){
                if (unit.valueCount()==1)
                    qInfo() << ">[05]" << unit.startAddress() << unit.values();
                else
                    qInfo() << ">[15]" << unit.startAddress() << unit.values();
            }
            else if (unit.registerType()==QModbusDataUnit::HoldingRegisters){
                if (unit.valueCount()==1)
                    qInfo() << ">[06]" << unit.startAddress() << unit.values();
                else
                    qInfo() << ">[16]" << unit.startAddress() << unit.values();
            }
#endif

            if (unit.registerType()==QModbusDataUnit::Coils){
                readyWriteCoilsRegisters(unit);
            }
            else if (unit.registerType()==QModbusDataUnit::HoldingRegisters){
                readyWriteHoldingRegisters(unit);
            }
            else nextRequest();
        }
        // --------------------------------------------------
        // Ошибка протокола (ошибку передал сам С2000-ПП)
        // --------------------------------------------------
        else if (reply->error() == QModbusDevice::ProtocolError)
        {
            ++satisticsRxOneSec;
            ++satisticsRxOneMin;

            //const QModbusPdu::FunctionCode  functionCode = reply->rawResult().functionCode();
            const QModbusPdu::ExceptionCode exceptionCode = reply->rawResult().exceptionCode();
            const int startAddress = reply->property("startAddress").toInt();

#ifdef MODBUS_DEBUG
            qInfo() << QStringLiteral("### Write response error: %1 (exception: 0x%2), %3").
                       arg(reply->errorString()).
                       arg(reply->rawResult().exceptionCode(), -1, 16).
                       arg(startAddress);
#endif

            readyWriteError(exceptionCode, startAddress);
        }
        // --------------------------------------------------
        // Остальные ошибки (таймаут и т.д.)
        // --------------------------------------------------
        else
        {
#ifdef MODBUS_DEBUG
            qInfo() << QStringLiteral("### Write response error: %1 (code: 0x%2)").
                       arg(reply->errorString()).
                       arg(reply->error(), -1, 16);
#endif

            restart();
        }

        reply->deleteLater();
    }
}
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
void C2000PP::readyWriteCoilsRegisters(const QModbusDataUnit &unit)
{
    if (!isConnected()) return;

    const int startAddress = unit.startAddress();

    // --------------------------------------------------
    // Получено подтверждение управления реле
    // --------------------------------------------------
    if ( (startAddress >= C2KPP_FirstRl) && (startAddress < C2KPP_FirstRl + s_maxRl) )
    {
        quint8 nRl = static_cast<quint8>(startAddress - C2KPP_FirstRl +1);
        getStateRlTimeer[nRl] = 5; // Запрос состояния реле по таймеру. Для реле, не генерирующих события.

#ifdef MODBUS_DEBUG
        qInfo() << "Получено подтверждение управления реле" << startAddress << nRl << unit.value(0);
#endif

        if (!queueControlRl.isEmpty())
        {
            struct88 dataRl = queueControlRl.dequeue();
            countBadControlRl.remove(dataRl.a);
        }

        if (!m_Master) noop(3); // Сделаем паузу
    }

    nextRequest();
}
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
void C2000PP::readyWriteHoldingRegisters(const QModbusDataUnit &unit)
{
    if (!isConnected()) return;

    const int startAddress = unit.startAddress();

    // --------------------------------------------------
    // Установлен признак событие прочитано
    // --------------------------------------------------
    if (startAddress == C2KPP_SetEventRead)
    {
#ifdef MODBUS_DEBUG
        qInfo()  << "Установлен признак прочитано событие" << unit.value(0);
#endif
        --s_EventCount;
        if (s_OldEventCount>0) --s_OldEventCount;
    }
    // --------------------------------------------------
    // Получено подтверждение изменения состояния раздела
    // --------------------------------------------------
    else if ( (startAddress >= C2KPP_FirstPart) && (startAddress < C2KPP_FirstPart + s_maxPart) )
    {
        quint8 nPart = static_cast<quint8>(startAddress - C2KPP_FirstPart +1);
        const QSet <quint16> shs = configC2000PP.getShInPart(nPart);
        for(quint16 nSh : shs)
            getStateShTimeer[nSh] = 5; // Запрос состояния ШС по таймеру. Для ШС, не генерирующих события.

#ifdef MODBUS_DEBUG
        qInfo() << "Получено подтверждение изменения состояния раздела" << startAddress << unit.value(1) << unit.value(0);
#endif

        if (!queueControlPart.isEmpty())
        {
            struct88 dataPart = queueControlPart.dequeue();
            countBadControlPart.remove(dataPart.a);
        }

        if (!m_Master) noop(3); // Сделаем паузу
    }
    // --------------------------------------------------
    // Получено подтверждение изменения состояния ШС
    // --------------------------------------------------
    else if ( (startAddress >= C2KPP_FirstSh) && (startAddress < C2KPP_FirstSh + s_maxSh) )
    {
        quint16 nSh = static_cast<quint16>(startAddress - C2KPP_FirstSh +1);
        getStateShTimeer[nSh] = 5; // Запрос состояния ШС по таймеру. Для ШС, не генерирующих события.

#ifdef MODBUS_DEBUG
        qInfo() << "Получено подтверждение изменения состояния ШС" << startAddress << unit.value(1) << unit.value(0);
#endif

        if (!queueControlSh.isEmpty())
        {
            struct168 dataSh = queueControlSh.dequeue();
            countBadControlSh.remove(dataSh.a);
        }

        if (!m_Master) noop(3); // Сделаем паузу
    }
#ifdef MODBUS_DEBUG
    // --------------------------------------------------
    // Дата и время
    // --------------------------------------------------
    else if (startAddress == C2KPP_SetDateTime)
    {
        quint8 hour   = static_cast<quint8>(unit.value(0)>>8);
        bool badData = hour & 0x80;
        bool badTime = hour & 0x40;
        hour &= 0x3F;

        quint8 minute = static_cast<quint8>(unit.value(0));
        quint8 second = static_cast<quint8>(unit.value(1)>>8);
        quint8 day    = static_cast<quint8>(unit.value(1));
        quint8 month  = static_cast<quint8>(unit.value(2)>>8);
        quint8 year   = static_cast<quint8>(unit.value(2));


        qInfo() << hour <<':'<< minute <<':'<< second <<' '<< day <<'.'<< month <<'.'<< year << badData << badTime;

        QDateTime dateTime;
        dateTime.setDate( QDate(2000 + year, month, day) );
        dateTime.setTime( QTime(hour, minute, second) );

        qInfo() << "SetDateTime" << dateTime << dateTime.toSecsSinceEpoch();
    }
#endif
    // --------------------------------------------------
    // Получено подтверждение установки номера ШС для
    // запроса температуры/влажности/CO
    // --------------------------------------------------
    else if (startAddress == C2KPP_SetShforGetADC)
    {
        countBadGetADC = 0;
        if (queueRequestADC6.count()>1){
            queueRequestADC6.enqueue( queueRequestADC6.dequeue() );
        }

        timerTwoStepADC.start();
        zoneInTwoStepRequestADC = static_cast<quint16>(unit.value(0));

#ifdef MODBUS_DEBUG
        qInfo() << "Подтверждение установки номера ШС для запроса АЦП" << zoneInTwoStepRequestADC;
#endif
    }
    // --------------------------------------------------
    // Получено подтверждение установки номера ШС для
    // запроса счетчика
    // --------------------------------------------------
    else if (startAddress == C2KPP_SetShforGetCounter)
    {
        countBadGetADC = 0;
        if (queueRequestADC7.count()>1){
            queueRequestADC7.enqueue( queueRequestADC7.dequeue() );
        }

        timerTwoStepADC.start();
        zoneInTwoStepRequestCNT = static_cast<quint16>(unit.value(0));

#ifdef MODBUS_DEBUG
        qInfo() << "Подтверждение установки номера ШС для запроса счетчика" << zoneInTwoStepRequestCNT;
#endif
    }
    // --------------------------------------------------
    // Получено подтверждение установки номера ШС для
    // запроса напряжения/тока
    // --------------------------------------------------
    else if (startAddress == C2KPP_SetShforGetVoltage)
    {
        countBadGetADC = 0;
        if (queueRequestADC8.count()>1){
            queueRequestADC8.enqueue( queueRequestADC8.dequeue() );
        }

        timerTwoStepADC.start();
        zoneInTwoStepRequestADC = static_cast<quint16>(unit.value(0));

#ifdef MODBUS_DEBUG
        qInfo() << "Подтверждение установки номера ШС для запроса напряжения/тока" << zoneInTwoStepRequestADC;
#endif
    }

    nextRequest();
}
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
void C2000PP::readyWriteError(const QModbusPdu::ExceptionCode exceptionCode, const int startAddress)
{
    if ((exceptionCode==6 || exceptionCode==15) && queueService.count()<5){
        noop(3); // Сделаем паузу
    }

    if (startAddress==C2KPP_SetDateTime)
    {
        if (exceptionCode==1)
        {
            setMaster(false); // Таким методом можно определить только для 1.32. В версии 2.0 так не определяется.
        }
    }
    // --------------------------------------------------
    // Ошибка на управление разделом. Удалим команду из
    // очереди если запрос выполняется неудачно 15 раз.
    // --------------------------------------------------
    else if ( (startAddress >= C2KPP_FirstPart) && (startAddress < C2KPP_FirstPart + s_maxPart) )
    {
        if (!queueControlPart.isEmpty())
        {
            struct88 dataPart = queueControlPart.dequeue();
            quint8 nPart = dataPart.a;

            // Если прибор занят или данные пока не получены
            if ( (exceptionCode==6) || (exceptionCode==15) )
            {
                quint8 count = 1;
                if (countBadControlPart.contains(nPart))
                    count = ++countBadControlPart[nPart];
                else countBadControlPart[nPart] = 1;

                if (count<15) controlPart(nPart, dataPart.b);
                else countBadControlPart.remove(nPart);
            }
            else countBadControlPart.remove(nPart);
        }
    }
    // --------------------------------------------------
    // Ошибка на управление ШС. Удалим команду из очереди
    // если запрос выполняется неудачно 15 раз.
    // --------------------------------------------------
    else if ( (startAddress >= C2KPP_FirstSh) && (startAddress < C2KPP_FirstSh + s_maxSh) )
    {
        if (!queueControlSh.isEmpty())
        {
            struct168 dataSh = queueControlSh.dequeue();
            quint16 nSh = dataSh.a;

            // Если прибор занят или данные пока не получены
            if ( (exceptionCode==6) || (exceptionCode==15) )
            {
                quint8 count = 1;
                if (countBadControlSh.contains(nSh))
                    count = ++countBadControlSh[nSh];
                else countBadControlSh[nSh] = 1;

                if (count<15) controlSh(nSh, dataSh.b);
                else countBadControlSh.remove(nSh);
            }
            else countBadControlSh.remove(nSh);
        }
    }
    // --------------------------------------------------
    // Ошибка на управление реле. Удалим команду из очереди
    // если запрос выполняется неудачно 5 раз.
    // --------------------------------------------------
    else if ( (startAddress >= C2KPP_FirstRl) && (startAddress < C2KPP_FirstRl + s_maxRl) )
    {
        if (!queueControlRl.isEmpty())
        {
            struct88 dataRl = queueControlRl.dequeue();
            quint8 nRl = dataRl.a;

            // Если прибор занят или данные пока не получены
            if ( (exceptionCode==6) || (exceptionCode==15) )
            {
                quint8 count = 1;
                if (countBadControlRl.contains(nRl))
                    count = ++countBadControlRl[nRl];
                else countBadControlRl[nRl] = 1;

                if (count<15) controlRl(nRl, dataRl.b);
                else countBadControlRl.remove(nRl);
            }
            else countBadControlRl.remove(nRl);
        }
    }
    // --------------------------------------------------
    // Ошибка установки номера ШС для запроса АЦП
    // --------------------------------------------------
    else if ( startAddress == C2KPP_SetShforGetADC)
    {
        // Все ошибки, кроме если прибор занят или данные пока не получены
        if ( !((exceptionCode==6) || (exceptionCode==15)) )
            if (!queueRequestADC6.isEmpty()) queueRequestADC6.dequeue();
    }
    // --------------------------------------------------
    // Ошибка установки номера ШС для запроса счетчика
    // --------------------------------------------------
    else if ( startAddress == C2KPP_SetShforGetCounter)
    {
        // Все ошибки, кроме если прибор занят или данные пока не получены
        if ( !((exceptionCode==6) || (exceptionCode==15)) )
            if (!queueRequestADC7.isEmpty()) queueRequestADC7.dequeue();
    }
    // --------------------------------------------------
    // Ошибка установки номера ШС для запроса напряжения/тока
    // --------------------------------------------------
    else if ( startAddress == C2KPP_SetShforGetVoltage)
    {
        // Все ошибки, кроме если прибор занят или данные пока не получены
        if ( !((exceptionCode==6) || (exceptionCode==15)) )
            if (!queueRequestADC8.isEmpty()) queueRequestADC8.dequeue();
    }

    nextRequest();
}
//------------------------------------------------------------------------------
// Следующий запрос
//------------------------------------------------------------------------------
void C2000PP::nextRequest(void)
{
    if (!isConnected()) return;

    // ------------------------------------------------------------
    // (Приоритет) Очередь
    // ------------------------------------------------------------
    int requestMissedADC = 0;
    bool requestOk = false;
    while (!requestOk)
    {
        // ------------------------------------------------------------
        // (1) Незаконченный двухэтапный запрос.
        // ------------------------------------------------------------
        if (twoStepRequest != step2_No)
        {
            if (twoStepRequest == step2_event)
            {
#ifdef MODBUS_DEBUG
                qInfo() << "Сотрем событие " << twoStepRequestEvent;
#endif
                presetHoldinRegisters(C2KPP_SetEventRead, twoStepRequestEvent);
                requestOk = true;

                // Если устанавливали зону для запроса АЦП и счетчика, то нужно установить повторно, т.к. у них общий буфер.
                zoneInTwoStepRequestADC = 0;
                zoneInTwoStepRequestCNT = 0;
            }
            else if (twoStepRequest == step2_ADC)
            {
#ifdef MODBUS_DEBUG
                qInfo() << "Запрос АЦП/напряжения/тока";
#endif
                readHoldingRegisters(C2KPP_RequestADC_L1, 1);
                requestOk = true;
            }
            else if (twoStepRequest == step2_Counter)
            {
#ifdef MODBUS_DEBUG
                qInfo() << "Запрос счетчика";
#endif
                readHoldingRegisters(C2KPP_RequestCounter_L3, 3);
                requestOk = true;
            }

            twoStepRequest = step2_No;
        }
        // ------------------------------------------------------------
        // (2) Самая приоритетная очередь.
        // ------------------------------------------------------------
        else if (!queueService.isEmpty())
        {
            quint16 req = queueService.dequeue();

            if (req == C2KPP_BI_GetMasterSlave_L1)
            {
                if (m_deviceVer>=200){
#ifdef MODBUS_DEBUG
                    qInfo("Для ПП 2.0 выяснить режим работы master/slave.");
#endif
                    readDiscreteInputs(C2KPP_BI_GetMasterSlave_L1, 1);
                    requestOk = true;
                }
            }
            else if (req == C2KPP_GetEventCount_L1)
            {
#ifdef MODBUS_DEBUG
                qInfo("Приоритетный запрос количества непрочитанных событий.");
#endif
                readHoldingRegisters(C2KPP_GetEventCount_L1, 1); // Запросить количество непрочитанных событий.
                requestOk = true;
            }
            else if (req == C2KPP_GetDeviceType_L2)
            {
#ifdef MODBUS_DEBUG
                qInfo("Приоритетный запрос типа и версии прибора.");
#endif
                readHoldingRegisters(C2KPP_GetDeviceType_L2, 2); // Запросить тип и версию прибора.
                requestOk = true;
            }
            else if (req == C2KPP_GetDateTime)
            {
#ifdef MODBUS_DEBUG
                qInfo("Приоритетный запрос даты и времени.");
#endif
                readHoldingRegisters(C2KPP_GetDateTime_L3, 3); // Запросить дату и время
                requestOk = true;
            }
            else if (req == C2KPP_SetDateTime)
            {
                if (m_Master) // Только в режиме Master
                {
                    setTime();  // Установить время в приборе.
                    requestOk = true;
                }
            }
        }
        // ------------------------------------------------------------
        // (3) Чтение конфигурации прибора.
        // ------------------------------------------------------------
        else if (queueConfig>=0){
#ifdef MODBUS_DEBUG
            qInfo("Чтение конфигурации прибора");
#endif
            if (queueConfig==0) emit configReading(0);
            readInputRegisters(queueConfig, 64);
            requestOk = true;
        }
        // ------------------------------------------------------------
        // (4) Очередь управления Рл. <номер Рл, программа>.
        // ------------------------------------------------------------
        else if ( (countControlRl<2) && !queueControlRl.isEmpty())
        {
            ++countControlRl;

            struct88 dataRl = queueControlRl.first();

#ifdef MODBUS_DEBUG
            qInfo() << "Управление Рл" << dataRl.a << dataRl.b;
#endif

            forceSingleCoil(C2KPP_FirstRl - 1 + dataRl.a, dataRl.b==0 ? 0 : 0xffff);
            requestOk = true;
        }
        // ------------------------------------------------------------
        // (5) Очередь управления разделом. <номер раздела, программа>.
        // ------------------------------------------------------------
        else if ( (countControlPart<2) && !queueControlPart.isEmpty())
        {
            countControlRl = 0;
            ++countControlPart;

            struct88 dataPart = queueControlPart.first();

#ifdef MODBUS_DEBUG
            qInfo() << "Управление разделом" << dataPart.a << dataPart.b;
#endif

            presetHoldinRegisters(C2KPP_FirstPart - 1 + dataPart.a, dataPart.b);
            requestOk = true;
        }
        // ------------------------------------------------------------
        // (6) Очередь управления ШС. <номер ШС, программа>.
        // ------------------------------------------------------------
        else if ( (countControlSh<2) && !queueControlSh.isEmpty())
        {
            countControlRl   = 0;
            countControlPart = 0;
            ++countControlSh;

            struct168 dataSh = queueControlSh.first();

#ifdef MODBUS_DEBUG
            qInfo() << "Управление ШС" << dataSh.a << dataSh.b;
#endif

            presetHoldinRegisters(C2KPP_FirstSh - 1 + dataSh.a, dataSh.b);
            requestOk = true;
        }
        // ------------------------------------------------------------
        // (7) Очередь запроса состояний ШС. <номер ШС, количество ШС>.
        // ------------------------------------------------------------
        else if ( (countRequestSh<2) && !queueRequestSh.isEmpty())
        {
            countControlRl   = 0;
            countControlPart = 0;
            countControlSh   = 0;
            ++countRequestSh;

            struct168 dataSh = queueRequestSh.first();

#ifdef MODBUS_DEBUG
            qInfo() << "Запрос состояний ШС" << dataSh.a << dataSh.b;
#endif

            readHoldingRegisters(C2KPP_FirstSh - 1 + dataSh.a, dataSh.b);
            requestOk = true;
        }
        // ------------------------------------------------------------
        // (8) Очередь запроса состояний РЛ. <номер Рл, количество Рл>.
        // ------------------------------------------------------------
        else if ( (countRequestRl<2) && !queueRequestRl.isEmpty())
        {
            countControlRl   = 0;
            countControlPart = 0;
            countControlSh   = 0;
            countRequestSh   = 0;
            ++countRequestRl;

            struct88 dataRl = queueRequestRl.dequeue();

#ifdef MODBUS_DEBUG
            qInfo() << "Запрос состояний Рл" << dataRl.a << dataRl.b;
#endif

            readCoilStatus(C2KPP_FirstRl - 1 + dataRl.a, dataRl.b);
            requestOk = true;
        }
        else
        {
            countControlRl   = 0;
            countControlPart = 0;
            countControlSh   = 0;
            countRequestSh   = 0;
            countRequestRl   = 0;

            // ------------------------------------------------------------
            // (9) Запрос события.
            // ------------------------------------------------------------
            if (s_EventCount>0) // Если непрочитанных событий >0
            {
#ifdef MODBUS_DEBUG
                qInfo() << "Запрос события";
#endif
                readHoldingRegisters(C2KPP_GetEvent_Md2, s_halfMaxLenEvents);
                requestOk = true;
            }
            // ------------------------------------------------------------
            // (10) Запрос количества непрочитанных событий либо АЦП.
            // ------------------------------------------------------------
            else
            {
                // Определим что будем запрашивать
                quint8 typeRequest = 0;

                // Если не завершен двухэтапный запрос АЦП
                if (zoneInTwoStepRequestADC!=0){
                    if (timerTwoStepADC.hasExpired(m_Master ? 320 : 450)){ // если пора запрашивать значение
                        twoStepRequest = step2_ADC;
                        continue;
                    }
                }
                // Если не завершен двухэтапный запрос счетчика
                else if (zoneInTwoStepRequestCNT!=0){
                    if (timerTwoStepADC.hasExpired(m_Master ? 320 : 450)){ // если пора запрашивать значение
                        twoStepRequest = step2_Counter;
                        continue;
                    }
                }
                else
                {
                    bool b7 = !queueRequestADC7.isEmpty();
                    bool b8 = !queueRequestADC8.isEmpty();

                    // Если в эту секунду еще не было запросов АЦП и есть шлейфы тип 7 (счетчик) и 8 (напряжение/ток).
                    if ( (adcRequestCounter==0) && (b7 || b8) )
                    {
                        if      (b7 && !b8) typeRequest = 1;
                        else if (!b7 && b8) typeRequest = 2;
                        else if (bRequestVoltage) {
                            bRequestVoltage = false;
                            typeRequest = 1;
                        }
                        else {
                            bRequestVoltage = true;
                            typeRequest = 2;
                        }
                    }
                    // Если в эту секунду было меньше трех запросов АЦП.
                    else if (adcRequestCounter<3)
                    {
                        // Если одноэтапный тип запроса АЦП
                        if (adcRequestType){
                            if (!queueRequestADC.isEmpty()) // Если очередь запросов не пуста
                                typeRequest = 4;
                        }
                        // Если двухэтапный тип запроса АЦП
                        else{
                            if (!queueRequestADC6.isEmpty()) // Если очередь запросов не пуста
                                typeRequest = 3;
                        }
                    }
                }

                switch (typeRequest)
                {
                case 0:
                    //--------------------------------------------------
                    //  Запрос количества непрочитанных событий.
                    //--------------------------------------------------
#ifdef MODBUS_DEBUG
                    qInfo("Запрос количества непрочитанных событий.");
#endif
                    readHoldingRegisters(C2KPP_GetEventCount_L1, 1);
                    requestOk = true;

                    break;
                case 1:
                    //--------------------------------------------------
                    // Запрос счетчика
                    //--------------------------------------------------
#ifdef MODBUS_DEBUG
                    qInfo("Установить номер ШС для запроса счетчика");
#endif
                    ++adcRequestCounter;
                    presetHoldinRegisters(C2KPP_SetShforGetCounter, queueRequestADC7.first());
                    requestOk = true;

                    break;

                case 2:
                    //--------------------------------------------------
                    //  Запрос напряжения/тока
                    //--------------------------------------------------
#ifdef MODBUS_DEBUG
                    qInfo("Установить номер ШС для запроса напряжения/тока");
#endif
                    ++adcRequestCounter;
                    presetHoldinRegisters(C2KPP_SetShforGetVoltage, queueRequestADC8.first());
                    requestOk = true;

                    break;

                case 3:
                    //--------------------------------------------------
                    //  Запрос АЦП двухэтапной командой
                    //--------------------------------------------------
#ifdef MODBUS_DEBUG
                    qInfo("Установить номер ШС для запроса АЦП.");
#endif
                    ++adcRequestCounter;
                    presetHoldinRegisters(C2KPP_SetShforGetADC, queueRequestADC6.first());
                    requestOk = true;

                    break;

                case 4:
                    //--------------------------------------------------
                    //  Запрос АЦП одноэтапной командой
                    //--------------------------------------------------
                    struct168 data = queueRequestADC.first();
                    quint16 nSh = data.a;
                    quint8  cnt = data.b;
                    bool ok=true;
                    for(quint8 i=0; i<cnt; ++i){
                        quint8 state = m_ShState.value(nSh++);
                        if (state==0 ||       // Cостояние неизвестно
                                state==187 || // Потеря связи с ШС
                                state==250){  // Потеря связи с прибором
                            ok = false;
                            break;
                        }
                    }

                    if (ok){
#ifdef MODBUS_DEBUG
                        qInfo("Запрос числовых значений.");
#endif
                        ++adcRequestCounter;
                        readHoldingRegisters(C2KPP_FirstADC - 1 + data.a, data.b);
                        requestOk = true;
                    }
                    else{
                        if (cnt>1){ // Разобъем групповой запрос на одиночные
                            queueRequestADC.dequeue();

                            nSh = data.a;
                            for(quint8 i=0; i<cnt; ++i){
                                quint8 state = m_ShState.value(nSh);
                                if ( !(state==187 ||  // Потеря связи с ШС
                                       state==250) ){ // Потеря связи с прибором
                                    requestADC(nSh);
                                }

                                ++nSh;
                            }
                        }
                        else if (queueRequestADC.count()>1){ // Переместим запрос в конец очереди
                            auto d = queueRequestADC.dequeue();
                            queueRequestADC.enqueue(d);
                            ++requestMissedADC;
                            if (requestMissedADC>=queueRequestADC.count()) // Чтобы бесконечно не пропускали
                                adcRequestCounter = 3;
                        }
                        else adcRequestCounter = 3;
                    }

                    break;
                }
            }
        }
    }
}
//------------------------------------------------------------------------------
// Добавить приоритетные запросы для отсрочки выполнения других заросов
//------------------------------------------------------------------------------
void C2000PP::noop(quint8 count)
{
    if ((count<1) || (queueService.count()>4)) return;
    queueService.enqueue(C2KPP_GetDeviceType_L2); // Запросить тип и версию прибора.

    //*** Бесполезный запрос, ответ я никак не применяю.
    if (count<2) return;
    queueService.enqueue(C2KPP_GetDateTime);      // Запросить дату и время

    if (count<3) return;
    if (queueService.count()<count)
    {
        count-=2;

        for (quint8 i=0; i<count; ++i)
            queueService.enqueue(C2KPP_GetEventCount_L1); // Запросить количство не считанных событий
    }
}
//------------------------------------------------------------------------------
// Загрузить конфигурацию из прибора С2000-ПП
//------------------------------------------------------------------------------
void C2000PP::readConfigFromDev(bool start)
{
    if (start){
        if ((queueConfig<0) && (m_deviceVer>=200)) queueConfig=0;
    }
    else{
        queueConfig=-1;
        emit configReading(-2);
    }
}
//------------------------------------------------------------------------------
// Проверить какой тип запроса АЦП одноэтапный/двухэтапный
//------------------------------------------------------------------------------
void C2000PP::testAdcRequestType(void)
{
    adcRequestType = (m_deviceVer>=200 && configC2000PP.isAdcOnSlaveAllow()) || (m_deviceVer<200 && m_Master);
}
//------------------------------------------------------------------------------
// Задать режим работы С2000-ПП мастер/слейв
//------------------------------------------------------------------------------
void C2000PP::setMaster(bool master)
{
    if (m_Master!=master){
        m_Master = master;

#ifdef MODBUS_DEBUG
        qInfo() << "Прибор в режиме master" << m_Master;
#endif

        testAdcRequestType();
    }
}
//------------------------------------------------------------------------------
// Задать версию С2000-ПП
//------------------------------------------------------------------------------
void C2000PP::setVersion(quint16 ver)
{
    if (m_deviceVer!=ver) {
        m_deviceVer = ver;
        testAdcRequestType();
        emit versionChanged(ver);
    }
}
//------------------------------------------------------------------------------
// Установить состояние ШС
//------------------------------------------------------------------------------
bool C2000PP::setShState(quint16 nSh, quint8 state)
{
    if (nSh==0){
        if (!m_ShState.isEmpty()){
            m_ShState.clear();
            emit shStateChanged(nSh, state);
            return true;
        }
    }
    else if (m_ShState.value(nSh)!=state)
    {
        if (state==0) {
            m_ShState.remove(nSh);
        }
        else {
            m_ShState[nSh]=state;
        }

        emit shStateChanged(nSh, state);
        return true;
    }

    return false;
}
//------------------------------------------------------------------------------
// Установить состояние Рл
//------------------------------------------------------------------------------
bool C2000PP::setRlState(quint8 nRl, bool state)
{
    if ((!m_RlState.contains(nRl)||(m_RlState.value(nRl)!=state)))
    {
        m_RlState[nRl]=state;
        emit rlStateChanged(nRl, state);
        return true;
    }

    return false;
}
