/** @file
 * @brief Реализация алгоритма 28147-89
 *
 * @copyright InfoTeCS. All rights reserved.
 */

#include "GOST89.h"

// Конвертирует массив байт в int32
inline unsigned int uint8ToUint32(unsigned char* input)
{
    unsigned int r = ( (input[3]) | (input[2]<<8) | (input[1]<<16) | (input[0]<<24));
    return r;
}
//------------------------------------------------------------------------------
// Конвертирует int32 в массив байт
inline void uint32ToUint8(unsigned int input, unsigned char* output)
{
    int i;
    for(i = 0; i < 4; ++i)
    {
        output[3-i] = ( ( input >> (8*i) ) & 0x000000ff );
    }
}
//------------------------------------------------------------------------------
inline unsigned int funcT(unsigned int a)
{
    // Таблица подстановки id-tc26-gost-28147-param-Z OID: 1.2.643.7.1.2.5.1.1
    static unsigned char p[8][16] =
    {
        {0xc, 0x4, 0x6, 0x2, 0xa, 0x5, 0xb, 0x9, 0xe, 0x8, 0xd, 0x7, 0x0, 0x3, 0xf, 0x1},
        {0x6, 0x8, 0x2, 0x3, 0x9, 0xa, 0x5, 0xc, 0x1, 0xe, 0x4, 0x7, 0xb, 0xd, 0x0, 0xf},
        {0xb, 0x3, 0x5, 0x8, 0x2, 0xf, 0xa, 0xd, 0xe, 0x1, 0x7, 0x4, 0xc, 0x9, 0x6, 0x0},
        {0xc, 0x8, 0x2, 0x1, 0xd, 0x4, 0xf, 0x6, 0x7, 0x0, 0xa, 0x5, 0x3, 0xe, 0x9, 0xb},
        {0x7, 0xf, 0x5, 0xa, 0x8, 0x1, 0x6, 0xd, 0x0, 0x9, 0x3, 0xe, 0xb, 0x4, 0x2, 0xc},
        {0x5, 0xd, 0xf, 0x6, 0x9, 0x2, 0xc, 0xa, 0xb, 0x7, 0x8, 0x1, 0x4, 0x3, 0xe, 0x0},
        {0x8, 0xe, 0x2, 0x5, 0x6, 0x9, 0x1, 0xc, 0xf, 0x4, 0xb, 0x0, 0xd, 0xa, 0x3, 0x7},
        {0x1, 0x7, 0xe, 0xd, 0x0, 0x5, 0x8, 0x3, 0x4, 0xf, 0xa, 0x6, 0x9, 0xc, 0xb, 0x2}
    };

    unsigned int res = 0;

    res ^=   p[ 0 ][     a & 0x0000000f ];
    res ^= ( p[ 1 ][ ( ( a & 0x000000f0 ) >>  4 ) ] << 4 );
    res ^= ( p[ 2 ][ ( ( a & 0x00000f00 ) >>  8 ) ] << 8 );
    res ^= ( p[ 3 ][ ( ( a & 0x0000f000 ) >> 12 ) ] << 12 );
    res ^= ( p[ 4 ][ ( ( a & 0x000f0000 ) >> 16 ) ] << 16 );
    res ^= ( p[ 5 ][ ( ( a & 0x00f00000 ) >> 20 ) ] << 20 );
    res ^= ( p[ 6 ][ ( ( a & 0x0f000000 ) >> 24 ) ] << 24 );
    res ^= ( p[ 7 ][ ( ( a & 0xf0000000 ) >> 28 ) ] << 28 );

    return res;
}
//------------------------------------------------------------------------------
unsigned int funcG(unsigned int a, unsigned int k)
{
    unsigned int c = a + k;
    unsigned int tmp = funcT(c);
    unsigned int r = (tmp<<11) | (tmp >> 21);

    return r;
}
//------------------------------------------------------------------------------
inline void Round(unsigned int* a1, unsigned int* a0, unsigned int k)
{
    unsigned int a;
    unsigned int tmp;

    a = *a0;
    tmp = funcG(*a0, k);

    *a0 = *a1 ^ tmp;
    *a1 = a;
}
//------------------------------------------------------------------------------
inline void RoundShtrih(unsigned int* a1, unsigned int* a0, unsigned int k)
{
    *a1 ^= funcG(*a0, k);
}
//------------------------------------------------------------------------------
int CryptBlock_89(unsigned char* input, unsigned char* output, unsigned char* key, bool bCrypt)
{
    if( !input || !output || !key ) return -1;

    static unsigned char kEncRoundKey[32] = { 0, 4, 8, 12, 16, 20, 24, 28, 0, 4, 8, 12, 16, 20, 24, 28, 0, 4, 8, 12, 16, 20, 24, 28, 28, 24, 20, 16, 12, 8, 4, 0 };
    static unsigned char kDecRoundKey[32] = { 0, 4, 8, 12, 16, 20, 24, 28, 28, 24, 20, 16, 12, 8, 4, 0, 28, 24, 20, 16, 12, 8, 4, 0, 28, 24, 20, 16, 12, 8, 4, 0 };

    unsigned char* keyIndex = (bCrypt) ? kEncRoundKey : kDecRoundKey;

    unsigned int a1 = uint8ToUint32(input);
    unsigned int a0 = uint8ToUint32(input + 4);
    int i;

    for(i = 0; i < 31; ++i)
    {
        Round(&a1, &a0, uint8ToUint32(key + keyIndex[i]));
    }

    RoundShtrih(&a1, &a0, uint8ToUint32(key + keyIndex[31]));

    uint32ToUint8(a1, output);
    uint32ToUint8(a0, output+4);

    return 0;
}
//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
QByteArray ExpandKey_89(QByteArray key)
{
    if (key.count() < 32)
        key = QCryptographicHash::hash(key, QCryptographicHash::Sha3_256);

    return key;
}
//------------------------------------------------------------------------------
QByteArray EncryptCFB_89(QByteArray data, QByteArray key, int init)
{
    static const quint8 sz = 8;

    QByteArray dataInit = QByteArray::fromRawData((char*)&init, sizeof(init));
    dataInit = QCryptographicHash::hash(dataInit, QCryptographicHash::Md5);
    dataInit.resize(sz);

    key = ExpandKey_89(key);

    QByteArray ret;

    int count = data.count() / sz;
    for (int i = 0; i < count; ++i)
    {
        QByteArray tmp(sz, 0);

        CryptBlock_89((unsigned char*)dataInit.data(), (unsigned char*)tmp.data(), (unsigned char*)key.data(), true);

        dataInit = data.mid(i*sz, sz);

        for (quint8 j=0; j < sz; ++j)
            dataInit[j] = dataInit[j] ^ tmp[j];

        ret += dataInit;
    }

    count = data.count() % sz;
    if (count>0)
    {
        QByteArray tmp(sz, 0);

        CryptBlock_89((unsigned char*)dataInit.data(), (unsigned char*)tmp.data(), (unsigned char*)key.data(), true);

        dataInit = data.right(count);

        for (quint8 j=0; j < count; ++j)
            dataInit[j] = dataInit[j] ^ tmp[j];

        ret += dataInit;
    }

    return ret;
}
//------------------------------------------------------------------------------
QByteArray DecryptCFB_89(QByteArray data, QByteArray key, int init)
{
    static const quint8 sz = 8;

    QByteArray dataInit = QByteArray::fromRawData((char*)&init, sizeof(init));
    dataInit = QCryptographicHash::hash(dataInit, QCryptographicHash::Md5);
    dataInit.resize(sz);

    key = ExpandKey_89(key);

    QByteArray ret;

    int count = data.count() / sz;
    for (int i = 0; i < count; ++i)
    {
        QByteArray tmp(sz, 0);

        CryptBlock_89((unsigned char*)dataInit.data(), (unsigned char*)tmp.data(), (unsigned char*)key.data(), true);

        dataInit = data.mid(i*sz, sz);

        for (quint8 j=0; j < sz; ++j)
            tmp[j] = dataInit[j] ^ tmp[j];

        ret += tmp;
    }

    count = data.count() % sz;
    if (count>0)
    {
        QByteArray tmp(sz, 0);

        CryptBlock_89((unsigned char*)dataInit.data(), (unsigned char*)tmp.data(), (unsigned char*)key.data(), true);

        dataInit = data.right(count);

        for (quint8 j=0; j < count; ++j)
            dataInit[j] = dataInit[j] ^ tmp[j];

        ret += dataInit;
    }

    return ret;
}
