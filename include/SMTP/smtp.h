//
// Copyright Kuzminov Sergey (kuzminov_sergey@mail.ru) 2021
//
// Distributed under the Unlicense License. (See accompanying
// file LICENSE_1_0.txt or copy at http://unlicense.org/)
//

#ifndef SMTP_H
#define SMTP_H

#include "simpleSmtp.h"

//------------------------------------------------------------------------------
class Smtp : public QObject
{
    Q_OBJECT

private:
    QString to;
    QString from;
    QString pass;
    QString subject;
    QString host;
    quint16 port=0;

    QString bodyDelay;
    quint8 delay=0;
    int timer=0;
    void timerEvent(QTimerEvent *event);

    QMap<QString, QString> mapBody;
    QMap<QString, int> mapTimer;

public:
    explicit Smtp(QObject *parent = nullptr);

public slots:
    void send(QString to, QString from, QString pass, QString subject, QString body, QString host, quint16 port = 465);

    void setAddress(const QString &to, const QString &from, const QString &pass, const QString &subject, const QString &host, quint16 port = 465);
    void send(const QString &body);
    void sendTo(const QString &body, const QString &to);

    void setDelay (quint8 delay);
    quint8 getDelay(void) {return delay;}
    void sendDelay(const QString &body);
    void sendDelayTo(const QString &body, const QString &to);

signals:
    void status(QString msg);
};
#endif //SMTP_H
