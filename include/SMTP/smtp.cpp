//
// Copyright Kuzminov Sergey (kuzminov_sergey@mail.ru) 2021
//
// Distributed under the Unlicense License. (See accompanying
// file LICENSE_1_0.txt or copy at http://unlicense.org/)
//

#include "smtp.h"

//------------------------------------------------------------------------------
Smtp::Smtp(QObject *parent) : QObject(parent)
{
}
//------------------------------------------------------------------------------
void Smtp::send(QString to, QString from, QString pass, QString subject, QString body, QString host, quint16 port)
{
    SimpleSmtp *simpleSmtp = new SimpleSmtp(this);
    QObject::connect(simpleSmtp, &SimpleSmtp::status, this, &Smtp::status);
    QObject::connect(simpleSmtp, &SimpleSmtp::finished, [=]{simpleSmtp->deleteLater();});
    simpleSmtp->send(to, from, pass, subject, body, host, port);
}
//------------------------------------------------------------------------------
void Smtp::setAddress(const QString &to, const QString &from, const QString &pass, const QString & subject, const QString &host, quint16 port)
{
    this->to      = to;
    this->from    = from;
    this->pass    = pass;
    this->subject = subject;
    this->host    = host;
    this->port    = port;
}
//------------------------------------------------------------------------------
void Smtp::send(const QString &body)
{
    if (!to.isEmpty())
        send(to, from, pass, subject, body, host, port);
}
//------------------------------------------------------------------------------
void Smtp::sendTo(const QString &body, const QString &to)
{
    if (!to.isEmpty())
        send(to, from, pass, subject, body, host, port);
}
//------------------------------------------------------------------------------
void Smtp::setDelay(quint8 delay)
{
    this->delay=delay;
}
//------------------------------------------------------------------------------
void Smtp::sendDelay(const QString &body)
{
    if (bodyDelay.isEmpty()) bodyDelay = body;
    else bodyDelay += QStringLiteral("\n") + body;

    if (!timer) timer = startTimer(delay*1000);
}
//------------------------------------------------------------------------------
void Smtp::sendDelayTo(const QString &body, const QString &to)
{
    if (mapBody.contains(to))
        mapBody[to] += QStringLiteral("\n") + body;
    else mapBody[to] = body;

    if (!mapTimer.contains(to)) mapTimer[to] = startTimer(delay*1000);
}
//------------------------------------------------------------------------------
void Smtp::timerEvent(QTimerEvent *event)
{
    if (timer == event->timerId())
    {
        killTimer(timer);
        timer = 0;

        send(bodyDelay);
        bodyDelay.clear();
    }
    else
    {
        auto timers = mapTimer;
        auto itEnd = timers.constEnd();
        for(auto it = timers.constBegin(); it!=itEnd; it++)
        {
            if (event->timerId()==it.value())
            {
                killTimer(it.value());
                mapTimer.remove(it.key());

                auto bodys = mapBody;
                auto itEnd2 = bodys.constEnd();
                for(auto it2 = bodys.constBegin(); it2!=itEnd2; it2++){
                    if (it2.key()==it.key())
                    {
                        sendTo(it2.value(), it2.key());
                        mapBody.remove(it2.key());
                        break;
                    }
                }

                break;
            }
        }
    }
}
