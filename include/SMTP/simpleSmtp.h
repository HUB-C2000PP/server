//
// Copyright Kuzminov Sergey (kuzminov_sergey@mail.ru) 2021
//
// Distributed under the Unlicense License. (See accompanying
// file LICENSE_1_0.txt or copy at http://unlicense.org/)
//

#ifndef SIMPLESMTP_H
#define SIMPLESMTP_H

#include <QObject>
#include <QString>
#include <QByteArray>
#include <QTimerEvent>
#include <QtNetwork/QAbstractSocket>
#include <QtNetwork/QSslSocket>

/*
 http://opds.sut.ru/old/electronic_manuals/mail/3_SMTP.htm
 https://en.wikipedia.org/wiki/MIME
*/

//------------------------------------------------------------------------------
class SimpleSmtp : public QObject
{
    Q_OBJECT

private:
    QSslSocket socket;

    QString mailTo;

    QByteArray to;
    QByteArray from;
    QByteArray pass;
    QByteArray message;

    int state;
    int timer;
    void timerEvent(QTimerEvent *event) override;

    enum states{Init, Auth, Mail, Rcpt, Data, Body, Quit, Close};

    static const QString log_Error;    
    static const QString log_MsgSent;
    static const QString log_MsgNotSent;
    static const QString log_MsgTimeExpired;

public:
    explicit SimpleSmtp(QObject *parent = nullptr);
    void send(QString to, QString from, QString pass, QString subject, QString body, QString host, quint16 port = 465);

signals:
    void status(QString msg);
    void finished(void);

private slots:
    //void connected();
    //void disconnected();
    void readyRead();
    void socketError(QAbstractSocket::SocketError err);
};

#endif //SIMPLESMTP_H
