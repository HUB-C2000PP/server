//
// Copyright Kuzminov Sergey (kuzminov_sergey@mail.ru) 2021
//
// Distributed under the Unlicense License. (See accompanying
// file LICENSE_1_0.txt or copy at http://unlicense.org/)
//

#include "simpleSmtp.h"

const QString SimpleSmtp::log_Error          = QStringLiteral(u"Ошибка (%1): %2.");
const QString SimpleSmtp::log_MsgSent        = QStringLiteral(u"Отправлено: ");
const QString SimpleSmtp::log_MsgNotSent     = QStringLiteral(u"Не отправлено: ");
const QString SimpleSmtp::log_MsgTimeExpired = QStringLiteral(u"Истекло время ожидания. ");

//------------------------------------------------------------------------------
SimpleSmtp::SimpleSmtp(QObject *parent) : QObject(parent), timer(0)
{
    //QObject::connect(&socket, &QSslSocket::connected,    this, &Smtp::connected);
    //QObject::connect(&socket, &QSslSocket::disconnected, this, &Smtp::disconnected);
    QObject::connect(&socket, &QSslSocket::readyRead,    this, &SimpleSmtp::readyRead);
}
//------------------------------------------------------------------------------
void SimpleSmtp::send(QString to, QString from, QString pass, QString subject, QString body, QString host, quint16 port)
{
    mailTo     = to;
    this->to   = to.toUtf8();
    this->from = from.toUtf8();
    this->pass = pass.toUtf8();

    state = Init;

    body.replace( QStringLiteral( "\r\n" ),      QStringLiteral( "\n" ) );
    body.replace( QStringLiteral( "\n" ),        QStringLiteral( "\r\n" ) );
    body.replace( QStringLiteral( "\r\n.\r\n" ), QStringLiteral( "\r\n..\r\n" ) );

    message  = "To: "   + this->to   + "\r\n";
    message += "From: " + this->from + "\r\n";
    message += "Subject: =?utf-8?B?" + subject.toUtf8().toBase64() + "?=\r\n";
    message += "MIME-Version: 1.0\r\n";
    message += "Content-Type: text/plain; charset=UTF-8\r\n";
    message += "Content-Transfer-Encoding: 8bit\r\n\r\n";
    message += body.toUtf8();

#ifdef Embedded
    socket.setPeerVerifyMode(QSslSocket::VerifyNone);
#endif

    socket.connectToHostEncrypted(host, port);
    if (socket.waitForConnected(30000))
    {
        timer = startTimer(30000);

#if QT_VERSION > QT_VERSION_CHECK(5, 14, 0)
        QObject::connect(&socket, &QSslSocket::errorOccurred, this, &SimpleSmtp::socketError, Qt::UniqueConnection);
#else
        QObject::connect(&socket, static_cast<void (QAbstractSocket::*)(QAbstractSocket::SocketError)>(&QSslSocket::error), this, &SimpleSmtp::socketError, Qt::UniqueConnection);
#endif
    }
    else
    {
        emit status( socket.errorString() + QStringLiteral(" (%1:%2)").arg(host).arg(port));
        emit finished();
    }
}
//------------------------------------------------------------------------------
void SimpleSmtp::timerEvent(QTimerEvent *event)
{
    if (timer == event->timerId())
    {
        killTimer(timer);
        timer = 0;

        emit status( log_MsgTimeExpired + mailTo );
        emit finished();
    }
}
/*
//------------------------------------------------------------------------------
void Smtp::connected()
{
    emit status( QStringLiteral("Connected") );
}
//------------------------------------------------------------------------------
void Smtp::disconnected()
{
    emit status( QStringLiteral("Disconneted") );
}
*/
//------------------------------------------------------------------------------
void SimpleSmtp::socketError(QAbstractSocket::SocketError err)
{
    emit status( log_Error.arg(err).arg(socket.errorString()) );
}
//------------------------------------------------------------------------------
void SimpleSmtp::readyRead()
{
    QString line;
    QString response;
    do
    {
        line = QString::fromUtf8( socket.readLine() );
        response += line;
    }
    while ( socket.canReadLine() && line[3] != QLatin1Char(' ') );

    //qDebug() << state << response;

    quint16 responseCode = line.left(3).toUShort();

    if ( state == Init && responseCode == 220 )
    {
        socket.write("EHLO localhost\r\n");
        state = Auth;
    }
    else if ( state == Auth && responseCode == 250 )
    {
        socket.write( "AUTH PLAIN " + QByteArray('\0'+ from + '\0' + pass).toBase64() + "\r\n" );
        state = Mail;
    }
    else if ( state == Mail && responseCode == 235 )
    {
        socket.write( "MAIL FROM:<" + from + ">\r\n" );
        state = Rcpt;
    }
    else if ( state == Rcpt && responseCode == 250 )
    {
        socket.write( "RCPT TO:<" + to + ">\r\n" );
        state = Data;
    }
    else if ( state == Data && responseCode == 250 )
    {
        socket.write( "DATA\r\n" );
        state = Body;
    }
    else if ( state == Body && responseCode == 354 )
    {
        socket.write( message + "\r\n.\r\n" );
        state = Quit;
    }
    else if ( state == Quit && responseCode == 250 )
    {
        state = Close;

        if (timer) {
            killTimer(timer);
            timer = 0;
        }

        socket.close();

        emit status( log_MsgSent + mailTo );
        emit finished();
    }
    else if ( state == Close )
    {
        // Не удалять это условие!
        // На случай, если будет получен еще один пакет.
    }
    else
    {
        state = Close;

        if (timer) {
            killTimer(timer);
            timer = 0;
        }

        socket.close();

        // Здесь можно обработать другие responseCode и выдать на русском языке
        emit status( response.simplified() );
        emit finished();
    }
}
