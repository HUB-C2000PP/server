//
// Copyright Kuzminov Sergey (kuzminov_sergey@mail.ru) 2021
//
// Distributed under the Unlicense License. (See accompanying
// file LICENSE_1_0.txt or copy at http://unlicense.org/)
//

#ifndef BROADCASTLISTENER_H
#define BROADCASTLISTENER_H

#include <QObject>
#include <QVariant>
#include <QUdpSocket>
#include <QDataStream>

extern const QDataStream::Version dataStreamVersion;

class BroadcastListener : public QObject
{
    Q_OBJECT
public:
    explicit BroadcastListener(QObject *parent = nullptr);
    void setMagicByte(quint64 magic = 0xA5C3F09618814224 ){magicByte=magic;}
    void setListenPort(quint16 port) {listenPort=port;}
    void setAnswerPort(quint16 port) {answerPort=port;}
    void startListen(void);
    void stopListen(void);

    void setExtData(QMap<QByteArray, QVariant> data) {extData=data;}

private:
    QUdpSocket *udpSocket=nullptr;
    quint16 listenPort=0;
    quint16 answerPort=0;
    quint64 magicByte=0;

    QMap<QByteArray, QVariant> extData;

signals:

};

#endif // BROADCASTLISTENER_H
