//
// Copyright Kuzminov Sergey (kuzminov_sergey@mail.ru) 2021
//
// Distributed under the Unlicense License. (See accompanying
// file LICENSE_1_0.txt or copy at http://unlicense.org/)
//

#include "broadcastlistener.h"

//------------------------------------------------------------------------------
BroadcastListener::BroadcastListener(QObject *parent) : QObject(parent)
{
    setMagicByte();
}
//------------------------------------------------------------------------------
void BroadcastListener::startListen(void)
{
    if ( (listenPort!=0) && (udpSocket==nullptr) )
    {
        udpSocket = new QUdpSocket(this);

        QObject::connect(udpSocket, &QUdpSocket::readyRead, this, [=]()
        {
            while (udpSocket->hasPendingDatagrams())
            {
                QByteArray datagram;
                datagram.resize( static_cast<int>(udpSocket->pendingDatagramSize()) );

                udpSocket->readDatagram(datagram.data(), datagram.size());

                if (answerPort!=0)
                {
                    QDataStream ds(&datagram, QIODevice::ReadOnly);
                    ds.setVersion(dataStreamVersion);
                    quint64 magic;
                    ds >> magic;

                    if (magic==magicByte)
                    {
                        qint64 msecsSinceEpoch;
                        ds >> msecsSinceEpoch;

                        QByteArray data;
                        QDataStream dsRet(&data, QIODevice::WriteOnly);
                        dsRet.setVersion(dataStreamVersion);
                        dsRet << (magic>>1) << msecsSinceEpoch << QSysInfo::machineUniqueId() << extData;

                        udpSocket->writeDatagram(data, QHostAddress::Broadcast, answerPort);
                    }
                }
            }
        });

        udpSocket->bind(listenPort, QAbstractSocket::ShareAddress);
    }
}
//------------------------------------------------------------------------------
void BroadcastListener::stopListen(void)
{
    if (udpSocket!=nullptr){
        udpSocket->deleteLater();
        udpSocket = nullptr;
    }
}
