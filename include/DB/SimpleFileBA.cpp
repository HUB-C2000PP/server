//
// Copyright Kuzminov Sergey (kuzminov_sergey@mail.ru) 2021
//
// Distributed under the Unlicense License. (See accompanying
// file LICENSE_1_0.txt or copy at http://unlicense.org/)
//

#include "SimpleFileBA.h"

//--------------------------------------------------------------------------------
SimpleFileBA::SimpleFileBA (QObject *parent) : QObject(parent)
{
}
//--------------------------------------------------------------------------------
bool SimpleFileBA::write(const QByteArray &data)
{
    QFileInfo info(filename);
    QString path = info.dir().path();
    QDir dir;
    if ( !dir.exists( path ) ) dir.mkpath( path );

    bool ret = false;
    QSaveFile file(filename);
    if (file.open(QIODevice::WriteOnly)) {
        file.write(data);
        ret = file.commit();
    }

    if (file.error() != QFileDevice::NoError)
        qCritical( "[SimpleFileBA::write] %s: %s", qUtf8Printable(file.errorString()), qUtf8Printable(file.fileName()) );

    return ret;
}
//--------------------------------------------------------------------------------
QByteArray SimpleFileBA::read(bool printErr)
{
    QByteArray ret;

    QFile file(filename);
    if (file.open(QIODevice::ReadOnly)) {
        ret = file.readAll();
        file.close();
    } else
        if (printErr)
            qCritical( "[SimpleFileBA::read] %s: %s", qUtf8Printable(file.errorString()), qUtf8Printable(file.fileName()) );

    return ret;
}
