//
// Copyright Kuzminov Sergey (kuzminov_sergey@mail.ru) 2021
//
// Distributed under the Unlicense License. (See accompanying
// file LICENSE_1_0.txt or copy at http://unlicense.org/)
//

#include "SimpleFileDB.h"

//--------------------------------------------------------------------------------
SimpleFileDB::SimpleFileDB (QObject *parent) : QObject(parent)
{
}
//--------------------------------------------------------------------------------
SimpleFileDB::~SimpleFileDB()
{
    if (file.isOpen()) file.close();
}
//--------------------------------------------------------------------------------
bool SimpleFileDB::openFile(const QString &fileName, quint8 eSize)
{
    bool ret = false;
    if (file.isOpen()) ret = true;
    else
    {
        this->eSize = eSize;

        QFileInfo info(fileName);
        QString path = info.dir().path();
        QDir dir;
        if ( !dir.exists( path ) ) dir.mkpath( path );

        file.setFileName(fileName);
        if ( file.open(QIODevice::ReadWrite) )
        {
            fSize = file.size();

            if (fSize==0) {
                if ( file.write(reinterpret_cast<char*>(&eSize), sizeof(eSize)) == sizeof(eSize) )
                    ret = true;
            }
            else {
                quint8 feSize;
                file.read(reinterpret_cast<char*>(&feSize), sizeof(feSize));

                if (feSize==eSize) ret = true;
                else {
                    file.close();
                    qFatal( "[SimpleFileDB::openFile]: Не совпадает размер записи файла %s", qUtf8Printable(fileName) );
                }
            }
        }
    }

    return ret;
}
//--------------------------------------------------------------------------------
qint32 SimpleFileDB::count(void)
{
    return static_cast<qint32>(fSize > static_cast<qint8>(sizeof(eSize)) ? (fSize-static_cast<qint8>(sizeof(eSize)))/eSize : 0);
}
//--------------------------------------------------------------------------------
void SimpleFileDB::write(qint32 n, QByteArray d)
{
    qint64 pos = n * eSize + static_cast<qint64>(sizeof(eSize));
    if (file.seek(pos))
    {
        if (d.count()<eSize) {
            QByteArray ba(eSize - d.count(), 0);
            d.append(ba);
        }
        else if (d.count()>eSize) d.truncate(eSize);

        lastNum = n;
        lastData = d;

        file.write(d);
        file.flush();

        if ( fSize < (pos + eSize) ) fSize = pos + eSize;
    }
}
//--------------------------------------------------------------------------------
qint32 SimpleFileDB::append(const QByteArray &d)
{
    qint32 cnt = count();
    write(cnt, d);
    return cnt;
}
//--------------------------------------------------------------------------------
void SimpleFileDB::read(qint32 n)
{
    qint64 pos = n * eSize + static_cast<qint64>(sizeof(eSize));
    if (file.seek(pos))
    {
        lastData = file.read(eSize);
        lastNum = n;
    }
    else {
        lastNum = -1;
        lastData.clear();
    }
}
//--------------------------------------------------------------------------------
QByteArray SimpleFileDB::value(qint32 n)
{
    if (lastNum != n) read(n);
    return lastData;
}
