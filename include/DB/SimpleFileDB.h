//
// Copyright Kuzminov Sergey (kuzminov_sergey@mail.ru) 2021
//
// Distributed under the Unlicense License. (See accompanying
// file LICENSE_1_0.txt or copy at http://unlicense.org/)
//

#ifndef SIMPLEFILEDB_H
#define SIMPLEFILEDB_H

#include <QtCore/QDir>
#include <QtCore/QFile>
#include <QtCore/QFileInfo>

class SimpleFileDB: public QObject
{
    Q_OBJECT

protected:
    QFile   file;
    qint64  fSize=0;
    quint8  eSize=0;

    qint32     lastNum=-1;
    QByteArray lastData;

    void read(qint32 n);

public:
    explicit SimpleFileDB(QObject *parent = nullptr);
    ~SimpleFileDB();
    bool openFile(const QString &fileName, quint8 eSize);

    qint32      count(void);
    void        write(qint32 n, QByteArray d);
    qint32      append(const QByteArray &d);
    QByteArray  value(qint32 n);
};

#endif // SIMPLEFILEDB_H
