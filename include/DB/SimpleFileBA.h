//
// Copyright Kuzminov Sergey (kuzminov_sergey@mail.ru) 2021
//
// Distributed under the Unlicense License. (See accompanying
// file LICENSE_1_0.txt or copy at http://unlicense.org/)
//

#ifndef SIMPLEFILEBA_H
#define SIMPLEFILEBA_H

#include <QtCore/QDir>
#include <QtCore/QFile>
#include <QtCore/QFileInfo>
#include <QtCore/QSaveFile>

class SimpleFileBA: public QObject
{
    Q_OBJECT

protected:
    QString filename;

public:
    explicit    SimpleFileBA(QObject *parent = nullptr);
    void        setFileName(const QString &filename) {this->filename = filename;}

    QByteArray  read(bool printErr = false);
    bool        write(const QByteArray &data);
};

#endif //SIMPLEFILEBA_H
