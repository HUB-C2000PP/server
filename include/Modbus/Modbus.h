//
// Copyright Kuzminov Sergey (kuzminov_sergey@mail.ru) 2021
//
// Distributed under the Unlicense License. (See accompanying
// file LICENSE_1_0.txt or copy at http://unlicense.org/)
//

#ifndef MODBUS_H
#define MODBUS_H

#include "include/Define.h"
#include "include/ModbusC2000E/ModbusRtuC2000EthernetMaster.h"
#include "include/LogFiles/LogFiles.h"
#include "include/C2000PP/c2000pp.h"

#include <QDebug>
#include <QtCore>
#include <QUrl>
#include <QSerialPort>
#include <QModbusClient>
#include <QModbusDataUnit>
#include <QModbusTcpClient>

#if QT_VERSION < QT_VERSION_CHECK(6, 0, 0)
#include <QModbusRtuSerialMaster>
#else
#include <QModbusRtuSerialClient>
#endif

class Modbus : public QObject
{
    Q_OBJECT    
public:
    explicit Modbus(QObject *parent = nullptr);
    ~Modbus();

    void registration(C2000PP *obj);
    void loadConfig(void);

    void setSerial(QString name, quint32 baudRate, quint8 parity, quint8 stopBits);

    void setC2000E(QUrl url) {setC2000E(url.host(), static_cast<quint16>( url.port() ) );}
    void setC2000E(QString host, quint16 port);

    void setTCP(QUrl url) {setTCP(url.host(), static_cast<quint16>( url.port() ) );}
    void setTCP(QString host, quint16 port);

    void setEnable(bool enable = true);

    bool    isEnable             (void) {return serialPortOn;}
    quint8  getSerialPortType    (void) {return serialPortType;}
    QString getSerialPortName    (void) {return serialPortName;}
    quint32 getSerialPortBaudRate(void) {return serialPortBaudRate;}
    quint8  getSerialPortParity  (void) {return serialPortParity;}
    quint8  getSerialPortStopBits(void) {return serialPortStopBits;}
    QString getSerialPortHost    (void) {return serialPortHost;}
    quint16 getSerialPortIPort   (void) {return serialPortIPort;}

    QString getLastErrorString(void) {return lastErrorString;}

    enum enumPortType {
        portType_Serial=0,
        portType_C2000E=1,
        portType_TCPRTU=0
    };

private:
    QSettings *settings;
    static const QString keySetPortOn;
    static const QString keySetPortType;
    static const QString keySetPortName;
    static const QString keySetPortBaud;
    static const QString keySetPortParity;
    static const QString keySetPortStopBits;
    static const QString keySetPortHost;
    static const QString keySetPortIPort;

    bool    serialPortOn       = false;
    quint8  serialPortType     = 0;
    QString serialPortName;
    quint32 serialPortBaudRate = 9600;
    quint8  serialPortParity   = 0;
    quint8  serialPortStopBits = 1;
    QString serialPortHost     = QStringLiteral("192.168.127.254");
    quint16 serialPortIPort    = 40000;
    // ----------------------------------------

    QModbusClient *modbusClient = nullptr;

    QList < QPointer < C2000PP > > serverObj;

    bool bRestart = false;
    QModbusDevice::Error lastErrorCode = QModbusDevice::NoError;
    QString lastErrorString;

    void deleteModbus(void);
    void initModbus();

private slots:
    void restart(void);
    void sendReadRequest(QModbusDataUnit modbusDataUnit);
    void sendWriteRequest(QModbusDataUnit modbusDataUnit);
    void versionChanged(quint16 ver);

public slots:
    void reconnect(void);

signals:
    void extErrorOccurred(QModbusDevice::Error errCode, QString strErr);    
};

#endif // MODBUS_H
