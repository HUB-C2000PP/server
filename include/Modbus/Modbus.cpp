//
// Copyright Kuzminov Sergey (kuzminov_sergey@mail.ru) 2021
//
// Distributed under the Unlicense License. (See accompanying
// file LICENSE_1_0.txt or copy at http://unlicense.org/)
//

#include "Modbus.h"

const QString Modbus::keySetPortOn      = QStringLiteral("Port/On");
const QString Modbus::keySetPortType    = QStringLiteral("Port/Type");
const QString Modbus::keySetPortName    = QStringLiteral("Port/Name");
const QString Modbus::keySetPortBaud    = QStringLiteral("Port/BaudRate");
const QString Modbus::keySetPortParity  = QStringLiteral("Port/Parity");
const QString Modbus::keySetPortStopBits= QStringLiteral("Port/StopBits");
const QString Modbus::keySetPortHost    = QStringLiteral("Port/Host");
const QString Modbus::keySetPortIPort   = QStringLiteral("Port/IPort");

//------------------------------------------------------------------------------
Modbus::Modbus(QObject *parent) : QObject(parent)
{    
    //----------------------------------------
    // Файл параметров порта ".ini"
    //----------------------------------------
    settings = new QSettings(mainPath+constFileNamePort, QSettings::IniFormat, this);
}
//------------------------------------------------------------------------------
Modbus::~Modbus()
{
    deleteModbus();
}
//------------------------------------------------------------------------------
void Modbus::loadConfig(void)
{
    serialPortOn        =                       settings->value(keySetPortOn,         serialPortOn        ).toBool();
    serialPortType      = static_cast<quint8>(  settings->value(keySetPortType,       serialPortType      ).toUInt() );

    serialPortName      =                       settings->value(keySetPortName,       serialPortName      ).toString();
    serialPortBaudRate  =                       settings->value(keySetPortBaud,       serialPortBaudRate  ).toUInt();
    serialPortParity    = static_cast<quint8>(  settings->value(keySetPortParity,     serialPortParity    ).toUInt() );
    serialPortStopBits  = static_cast<quint8>(  settings->value(keySetPortStopBits,   serialPortStopBits  ).toUInt() );

    serialPortHost      =                       settings->value(keySetPortHost,       serialPortHost      ).toString();
    serialPortIPort     = static_cast<quint16>(  settings->value(keySetPortIPort,      serialPortIPort     ).toUInt() );

    if (serialPortType==portType_Serial){
        setSerial(serialPortName, serialPortBaudRate, serialPortParity, serialPortStopBits);
        if (serialPortOn) setEnable();
    }
    else if (serialPortType==portType_C2000E){
        setC2000E(serialPortHost, serialPortIPort);
        if (serialPortOn) setEnable();
    }
    else if (serialPortType==portType_TCPRTU){
        setTCP(serialPortHost, serialPortIPort);
        if (serialPortOn) setEnable();
    }
}
//------------------------------------------------------------------------------
void Modbus::deleteModbus(void)
{
    if (modbusClient) {
        modbusClient->disconnectDevice();
        delete modbusClient;
        modbusClient = nullptr;
    }
}
//------------------------------------------------------------------------------
void Modbus::setSerial(QString name, quint32 baudRate, quint8 parity, quint8 stopBits)
{    
    if (serialPortType     != portType_Serial) { serialPortType =portType_Serial; settings->setValue(keySetPortType,    serialPortType); }
    if (serialPortName     != name)            { serialPortName =name;            settings->setValue(keySetPortName,    serialPortName); }
    if (serialPortBaudRate != baudRate)        { serialPortBaudRate=baudRate;     settings->setValue(keySetPortBaud,    serialPortBaudRate); }
    if (serialPortParity   != parity)          { serialPortParity  =parity;       settings->setValue(keySetPortParity,  serialPortParity); }
    if (serialPortStopBits != stopBits)        { serialPortStopBits=stopBits;     settings->setValue(keySetPortStopBits,serialPortStopBits); }

    QSerialPort::BaudRate eBaudRate = static_cast<QSerialPort::BaudRate>(baudRate);
    QSerialPort::Parity   eParity   = static_cast<QSerialPort::Parity>  (parity);
    QSerialPort::StopBits eStopBits = static_cast<QSerialPort::StopBits>(stopBits);

    deleteModbus();
#if QT_VERSION < QT_VERSION_CHECK(6, 0, 0)
    QModbusRtuSerialMaster *modbus = new QModbusRtuSerialMaster(this);
#else
    QModbusRtuSerialClient *modbus = new QModbusRtuSerialClient(this);
#endif
    modbus->setInterFrameDelay(35000);
    modbus->setTimeout(1000);

    modbusClient = modbus;
    modbusClient->setConnectionParameter(QModbusDevice::SerialPortNameParameter, name);
    modbusClient->setConnectionParameter(QModbusDevice::SerialBaudRateParameter, eBaudRate);
    modbusClient->setConnectionParameter(QModbusDevice::SerialDataBitsParameter, QSerialPort::Data8);
    modbusClient->setConnectionParameter(QModbusDevice::SerialParityParameter,   eParity);
    modbusClient->setConnectionParameter(QModbusDevice::SerialStopBitsParameter, eStopBits);

    initModbus();
}
//------------------------------------------------------------------------------
void Modbus::setC2000E(QString host, quint16 port)
{
    if (serialPortType  != portType_C2000E) { serialPortType =portType_C2000E; settings->setValue(keySetPortType,  serialPortType); }
    if (serialPortHost  != host)            { serialPortHost =host;            settings->setValue(keySetPortHost,  serialPortHost); }
    if (serialPortIPort != port)            { serialPortIPort=port;            settings->setValue(keySetPortIPort, serialPortIPort); }

    deleteModbus();
    modbusClient = new ModbusRtuC2000EthernetMaster(this);
    modbusClient->setTimeout(1000);

    modbusClient->setConnectionParameter(QModbusDevice::NetworkAddressParameter, host);
    modbusClient->setConnectionParameter(QModbusDevice::NetworkPortParameter,    port);

    initModbus();
}
//------------------------------------------------------------------------------
// Это на перспеутиву. Не проверено. Пригодится для шлюзов RTU to TCP
void Modbus::setTCP(QString host, quint16 port)
{
    if (serialPortType  != portType_TCPRTU) { serialPortType =portType_TCPRTU; settings->setValue(keySetPortType,  serialPortType); }
    if (serialPortHost  != host)            { serialPortHost =host;            settings->setValue(keySetPortHost,  serialPortHost); }
    if (serialPortIPort != port)            { serialPortIPort=port;            settings->setValue(keySetPortIPort, serialPortIPort); }

    deleteModbus();
    modbusClient = new QModbusTcpClient(this);

    modbusClient->setConnectionParameter(QModbusDevice::NetworkAddressParameter, host);
    modbusClient->setConnectionParameter(QModbusDevice::NetworkPortParameter,    port);

    initModbus();
}
//------------------------------------------------------------------------------
void Modbus::initModbus()
{
    QObject::connect(modbusClient, &QModbusClient::errorOccurred, this, [=](QModbusDevice::Error errCode)
    {
        if (lastErrorCode!=errCode)
        {
            lastErrorCode = errCode;
            if (modbusClient) lastErrorString = modbusClient->errorString();
            emit extErrorOccurred(errCode, lastErrorString);
        }

        if (!bRestart){
            bRestart = true;
            if (modbusClient) modbusClient->disconnectDevice();
            QTimer::singleShot(1000, this, &Modbus::restart);
        }
    });

    QObject::connect(modbusClient, &QModbusClient::stateChanged, this, [=](QModbusDevice::State state)
    {
        if (state==QModbusDevice::ConnectedState){
            lastErrorCode = QModbusDevice::NoError;
            lastErrorString.clear();
        }
    });

    for(const QPointer <C2000PP> &obj : qAsConst(serverObj)){
        if (obj) {
            QObject::connect(modbusClient, &QModbusClient::stateChanged, obj, &C2000PP::stateChanged, Qt::UniqueConnection);
        }
    }
}
//------------------------------------------------------------------------------
void Modbus::setEnable(bool enable)
{
    if (serialPortOn != enable) {
        serialPortOn = enable;
        settings->setValue(keySetPortOn, serialPortOn);
    }

    if (modbusClient) {
        if (enable) modbusClient->connectDevice();
        else modbusClient->disconnectDevice();
    }
}
//------------------------------------------------------------------------------
void Modbus::restart(void)
{
    bRestart = false;
    if (serialPortOn) modbusClient->connectDevice();
}
//------------------------------------------------------------------------------
void Modbus::sendReadRequest(QModbusDataUnit modbusDataUnit)
{
    auto obj = qobject_cast<C2000PP *>(sender());

    if (obj)
    {
        int serverAddress = obj->getAddressPP();

        QModbusReply *reply;
        if (serialPortType == portType_TCPRTU) reply = modbusClient->sendReadRequest(modbusDataUnit, serverAddress);
        else reply = static_cast<ModbusRtuC2000EthernetMaster*>(modbusClient)->sendReadRequest(modbusDataUnit, serverAddress);

        if (reply)
        {
            reply->setProperty("startAddress", modbusDataUnit.startAddress());

            if (reply->isFinished())
                delete reply; // Широковещательные ответы возвращаются немедленно
            else
                connect(reply, &QModbusReply::finished, obj, &C2000PP::readyRead);
        }
    }
}
//------------------------------------------------------------------------------
void Modbus::sendWriteRequest(QModbusDataUnit modbusDataUnit)
{
    auto obj = qobject_cast<C2000PP *>(sender());

    if (obj)
    {
        int serverAddress = obj->getAddressPP();

        QModbusReply *reply;
        if (serialPortType == portType_TCPRTU) reply = modbusClient->sendWriteRequest(modbusDataUnit, serverAddress);
        else reply = static_cast<ModbusRtuC2000EthernetMaster*>(modbusClient)->sendWriteRequest(modbusDataUnit, serverAddress);

        if (reply)
        {
            reply->setProperty("startAddress", modbusDataUnit.startAddress());

            if (reply->isFinished())
                delete reply; // Широковещательные ответы возвращаются немедленно
            else
                connect(reply, &QModbusReply::finished, obj, &C2000PP::readyWrite);
        }
    }
}
//------------------------------------------------------------------------------
void Modbus::versionChanged(quint16 ver)
{
#if QT_VERSION < QT_VERSION_CHECK(6, 0, 0)
    QModbusRtuSerialMaster *modbus = qobject_cast<QModbusRtuSerialMaster*>(modbusClient);
#else
    QModbusRtuSerialClient *modbus = qobject_cast<QModbusRtuSerialClient*>(modbusClient);
#endif

    if(modbus){
        if (ver==0 || ver==200){
            modbus->setInterFrameDelay(35000);
        }
        else modbus->setInterFrameDelay(-1);
    }
}
//------------------------------------------------------------------------------
void Modbus::registration(C2000PP *obj)
{
    if (obj && !serverObj.contains(obj))
    {
        serverObj.append(obj);
        QObject::connect(obj, &C2000PP::sendReadRequest,  this, &Modbus::sendReadRequest);
        QObject::connect(obj, &C2000PP::sendWriteRequest, this, &Modbus::sendWriteRequest);
        QObject::connect(obj, &C2000PP::versionChanged,   this, &Modbus::versionChanged);
        QObject::connect(this,&Modbus::extErrorOccurred,  obj,  &C2000PP::extErrorOccurred);
        if (modbusClient) QObject::connect(modbusClient, &QModbusClient::stateChanged, obj, &C2000PP::stateChanged);
    }
}
//------------------------------------------------------------------------------
void Modbus::reconnect(void)
{
    if (isEnable()){
        setEnable(false);
        setEnable(true);
    }
}
