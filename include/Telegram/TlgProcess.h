//
// Copyright Kuzminov Sergey (kuzminov_sergey@mail.ru) 2021
//
// Distributed under the Unlicense License. (See accompanying
// file LICENSE_1_0.txt or copy at http://unlicense.org/)
//

#ifndef TLGPROCESS_H
#define TLGPROCESS_H

#include <QtCore>
#include <QByteArray>
#include <QSysInfo>
#include <QProcess>
//#include <QDebug>

#include "TlgState.h"
#include "include/LogFiles/LogFiles.h"

//------------------------------------------------------------------------------
class TlgProcess : public QObject
{
    Q_OBJECT

public:
    explicit TlgProcess(QObject *parent = nullptr);
    ~TlgProcess() override {stop();}

    void setPhoneNumber(const QByteArray &phoneNumber);
    void setProxy(const QString &proxy, quint16 proxyPort, const QString &proxyLogin, const QString &proxyPwd);
    void setApiID(int api_id, const QString &api_hash);
    void start(void);
    void stop(void);
    bool isStarted(void) {return process; }

    void telegramLogOut();
    void telegramEnterCode(const QByteArray &code);
    void telegramSendMessage(qint64 chatId, QString msg);
    void telegramEditMessage(qint64 chatId, QString msg);

    static QString fullFileName(void);

    bool isConnected(void) {return bConnected;}

protected:
    void timerEvent(QTimerEvent *event) override;

private:
    QByteArray phoneNumber;
    QString proxy;
    QString proxyLogin;
    QString proxyPwd;
    quint16 proxyPort=0;

    //Original
    int api_id = 94575; // Идентификатор приложения для доступа к Telegram API, который можно получить по адресу https://my.telegram.org
    QString api_hash = QStringLiteral("a3406de8d171bb422bb6ddf3bbd800e2"); // Хэш идентификатора приложения для доступа к Telegram API

    QPointer<QProcess> process;

    void stopLoopTimer();
    void startLoopTimer();
    int timerID_Loop    = 0;
    int timerID_Restart = 0;
    bool bStoping = false;
    bool bLastWork=false;
    bool bConnected = false;

    QByteArrayList commands;

    bool needLogOut=false;
    bool needRestart=false;

    const static int countUPDATECHATTITLE;
    const static int countGOTMESSAGE;
    const static int countSENTMESSAGE;
    const static int countNOTSENTMESSAGE;

    LogFile tlgError;

signals:
    void sentMessage(qint64 chatID, bool ok);
    void authState(TelegramState state);
    void gotMessage(qint64 chatID, QString msg);
    void updateChatTitle(qint64 chatID, QString title);
    void signalError(QString strErr);
};
#endif //TLGPROCESS_H
