//
// Copyright Kuzminov Sergey (kuzminov_sergey@mail.ru) 2021
//
// Distributed under the Unlicense License. (See accompanying
// file LICENSE_1_0.txt or copy at http://unlicense.org/)
//

#ifndef TLGSTATE_H
#define TLGSTATE_H

enum TelegramState
{
    eEmpty,
    eGotAuth,
    eLogOut,
    eClosing,
    eTerminated,
    eAuthCode,
    eWaitReg,
    ePassword,
    eLink,
    ePhone,
    eEncriptionKey,
    eDevInfo,
    eConnected,
    eNotConnected
};

#endif //TLGSTATE_H

