//
// Copyright Kuzminov Sergey (kuzminov_sergey@mail.ru) 2021
//
// Distributed under the Unlicense License. (See accompanying
// file LICENSE_1_0.txt or copy at http://unlicense.org/)
//

#ifndef TLGSTR_H
#define TLGSTR_H

const char * const WORK             = "Telegram.";
const char * const GOTAUTH          = "Got authorization.";
const char * const CLOSING          = "Closing.";
const char * const LOGOUT           = "Logging out.";
const char * const TERMINATED       = "Terminated.";
const char * const PHONE            = "Enter phone number: ";
const char * const AUTHCODE         = "Enter authentication code: ";
const char * const PASSWORD         = "Enter authentication password: ";
const char * const ENCRIPTIONKEY    = "Enter encryption key or DESTROY: ";
const char * const DEVMODEL         = "Enter device model: ";
const char * const SYSVER           = "Enter system version: ";
const char * const VERSION          = "Enter application version: ";
const char * const APIID            = "Enter api id: ";
const char * const WAITREG          = "Wait registration.";

const char * const ERROR            = "Error: ";
const char * const LINK             = "Confirm this login link on another device: ";
const char * const GOTMESSAGE       = "GotMessage: ";
const char * const UPDATECHATTITLE  = "UpdateChatTitle: ";
const char * const SENTMESSAGE      = "SentMessage: ";
const char * const NOTSENTMESSAGE   = "NotSentMessage: ";

const char * const CONNECTED        = "Connected.";
const char * const NOTCONNECTED     = "Noy connected.";

const char * const PROXY            = "Enter proxy: ";

#endif //TLGSTR_H

