//
// Copyright Kuzminov Sergey (kuzminov_sergey@mail.ru) 2021
//
// Distributed under the Unlicense License. (See accompanying
// file LICENSE_1_0.txt or copy at http://unlicense.org/)
//

#include "TlgProcess.h"
#include "TlgStr.h"

extern QString mainPath;

const int TlgProcess::countGOTMESSAGE       = QByteArray(GOTMESSAGE).count();
const int TlgProcess::countUPDATECHATTITLE  = QByteArray(UPDATECHATTITLE).count();
const int TlgProcess::countSENTMESSAGE      = QByteArray(SENTMESSAGE).count();
const int TlgProcess::countNOTSENTMESSAGE   = QByteArray(NOTSENTMESSAGE).count();

//-----------------------------------------------------------------------------------
TlgProcess::TlgProcess(QObject *parent) : QObject(parent)
{
    tlgError.setFileName(QStringLiteral("telegram_err.log"));
}
//-----------------------------------------------------------------------------------
void TlgProcess::setPhoneNumber(const QByteArray &phoneNumber)
{
    if (this->phoneNumber!=phoneNumber){
        if (process) needLogOut=true;
        this->phoneNumber=phoneNumber;
    }
}
//-----------------------------------------------------------------------------------
void TlgProcess::setProxy(const QString &proxy, quint16 proxyPort, const QString &proxyLogin, const QString &proxyPwd)
{
    if ((this->proxy!=proxy) || (this->proxyPort!=proxyPort) || (this->proxyLogin!=proxyLogin) || (this->proxyPwd!=proxyPwd)){
        if (process) needRestart=true;
        this->proxy     = proxy;
        this->proxyPort = proxyPort;
        this->proxyLogin= proxyLogin;
        this->proxyPwd  = proxyPwd;
    }
}
//-----------------------------------------------------------------------------------
void TlgProcess::setApiID(int api_id, const QString &api_hash)
{
    if (api_id!=0 && !api_hash.isEmpty()){
        this->api_id = api_id;
        this->api_hash=api_hash;
    }
}
//-----------------------------------------------------------------------------------
void TlgProcess::start(void)
{    
    if (process && needLogOut) {
        telegramLogOut();

        if (!needRestart) stop();
    }

    if (process && needRestart) stop();

    if (!process)
    {
        bStoping = false;
        bLastWork = false;

        process = new QProcess(this);


        //------------------------------------------------------------
        connect(process, &QProcess::readyReadStandardOutput, this, [=]
        {
            QList<QByteArray> listBa = process->readAllStandardOutput().split('\0');

            for(const QByteArray &ba : qAsConst(listBa))
            {
                if (!ba.isEmpty())
                {
                    //qDebug() << QString::fromUtf8(ba);

                    if (ba.startsWith(WORK)) bLastWork = true;
                    else
                    {
                        bLastWork = false;

                        if (ba.startsWith(GOTMESSAGE)){
                            int index = ba.indexOf(' ', countGOTMESSAGE);

                            if (index>0){
                                bool ok=false;
                                qint64 chatID = ba.mid(countGOTMESSAGE, index - countGOTMESSAGE).toLongLong(&ok);
                                if (ok) emit gotMessage(chatID, QString::fromUtf8(ba.right(ba.size() - index - 1)));
                            }
                        }
                        else if (ba.startsWith(UPDATECHATTITLE)){
                            int index = ba.indexOf(' ', countUPDATECHATTITLE);

                            if (index>0){
                                bool ok=false;
                                qint64 chatID = ba.mid(countUPDATECHATTITLE, index - countUPDATECHATTITLE).toLongLong(&ok);
                                if (ok) emit updateChatTitle(chatID, QString::fromUtf8(ba.right(ba.size() - index - 1)));
                            }
                        }
                        else if (ba.startsWith(SENTMESSAGE))   {
                            qint64 chatID = ba.mid(countSENTMESSAGE, ba.size() - countSENTMESSAGE).toLongLong();
                            emit sentMessage(chatID, true);
                        }
                        else if (ba.startsWith(NOTSENTMESSAGE)){
                            qint64 chatID = ba.mid(countSENTMESSAGE, ba.size() - countSENTMESSAGE).toLongLong();
                            emit sentMessage(chatID, false);
                        }
                        else if (ba.startsWith(GOTAUTH))       { emit authState(eGotAuth); startLoopTimer(); }
                        else if (ba.startsWith(PHONE))         { emit authState(ePhone); process->write(phoneNumber + '\0'); }
                        else if (ba.startsWith(ENCRIPTIONKEY)) { emit authState(eEncriptionKey); process->write(QByteArrayLiteral("\0")); }
                        else if (ba.startsWith(DEVMODEL))      { emit authState(eDevInfo); process->write(QSysInfo::prettyProductName().toLatin1() + '\0');}
                        else if (ba.startsWith(SYSVER))        { process->write(QSysInfo::kernelVersion().toLatin1() + '\0'); }
                        else if (ba.startsWith(VERSION))       { process->write(QCoreApplication::applicationVersion().toLatin1() + '\0');}
                        else if (ba.startsWith(APIID))         {
                            process->write(QStringLiteral("%1 %2\0").arg(api_id).arg(api_hash).toLatin1());
                        }
                        else if (ba.startsWith(AUTHCODE))      { stopLoopTimer(); emit authState(eAuthCode); }
                        else if (ba.startsWith(PASSWORD))      { stopLoopTimer(); emit authState(ePassword); }
                        else if (ba.startsWith(LOGOUT))        { emit authState(eLogOut); }
                        else if (ba.startsWith(CLOSING))       { emit authState(eClosing); }
                        else if (ba.startsWith(TERMINATED))    { emit authState(eTerminated); stop(); }
                        else if (ba.startsWith(WAITREG))       { emit authState(eWaitReg); }
                        else if (ba.startsWith(LINK))          { /*stopLoopTimer();*/ emit authState(eLink);}
                        else if (ba.startsWith(CONNECTED))     { bConnected=true;  emit authState(eConnected); }
                        else if (ba.startsWith(NOTCONNECTED))  { bConnected=false; emit authState(eNotConnected); }
                        else if (ba.startsWith(PROXY))
                        {
                            if (proxy.isEmpty() || (proxyPort==0)) process->write(QStringLiteral("\0").toLatin1());
                            else
                                process->write(QStringLiteral("%1 %2 %3 %4\0").arg(proxy).arg(proxyPort).arg(proxyLogin).arg(proxyPwd).toLatin1());
                        }
                        //else if (ba.startsWith(ERROR))        {};
                    }
                }
            }
        } );

        //------------------------------------------------------------
        connect(process, QOverload<int, QProcess::ExitStatus>::of(&QProcess::finished), this, [=](int exitCode, QProcess::ExitStatus exitStatus)
        {
            Q_UNUSED(exitCode)
            Q_UNUSED(exitStatus)
            //qDebug() << "finished" << exitCode << exitStatus << bStoping << timerID_Restart;

            if (!bStoping) {
                stop();

                if (timerID_Restart==0) timerID_Restart = startTimer(3000);
            }

            emit authState(eEmpty);
        } );
        //------------------------------------------------------------
        connect(process, &QProcess::errorOccurred, this, [=](QProcess::ProcessError error)
        {
            //qDebug() <<"ProcessError"<< error;

            if (error==QProcess::FailedToStart)
                emit signalError( QStringLiteral(u"Не удалось запустить файл:\n%1").arg(fullFileName()) );
        } );

        /*
        connect(process, &QProcess::started, [=]{ qDebug() << "started";  } );        
        connect(process, &QProcess::stateChanged, [=](QProcess::ProcessState newState){ qDebug() << "stateChanged:" << newState; } );
        */

        connect(process, &QProcess::readyReadStandardError, [=]{ tlgError.writeLog( QString::fromUtf8( process->readAllStandardError() )); } );

        process->setWorkingDirectory(mainPath);
        process->start( fullFileName(), QStringList() );
    }
}
//-----------------------------------------------------------------------------------
void TlgProcess::stop(void)
{
    bStoping = true;
    needRestart = false;

    if (timerID_Restart != 0){
        killTimer(timerID_Restart);
        timerID_Restart = 0;
    }

    stopLoopTimer();

    if (process)
    {
        if (process->state()==QProcess::Running) {
            if (bLastWork) process->write(QByteArrayLiteral("q\0"));
            process->waitForFinished(1000);
        }

        process->kill();
        process->waitForFinished(100);
        process->deleteLater();
        process=nullptr;
    }
}
//-----------------------------------------------------------------------------------
QString TlgProcess::fullFileName(void)
{
#ifdef Q_OS_WIN
    return mainPath + QStringLiteral("/telegram.exe");
#else
    return mainPath + QStringLiteral("/telegram");
#endif
}
//------------------------------------------------------------------------------
void TlgProcess::telegramLogOut()
{
    needLogOut = false;
    if (process) commands.append(QByteArrayLiteral("l\0"));
}
//------------------------------------------------------------------------------
void TlgProcess::telegramSendMessage(qint64 chatId, QString msg)
{
    commands.append( QStringLiteral("m %1 %2\0").arg(chatId).arg(msg).toUtf8() );
}
//------------------------------------------------------------------------------
void TlgProcess::telegramEditMessage(qint64 chatId, QString msg)
{
    commands.append( QStringLiteral("e %1 %2\0").arg(chatId).arg(msg).toUtf8() );
}
//------------------------------------------------------------------------------
void TlgProcess::telegramEnterCode(const QByteArray &code)
{
    if (process)
        if (process->state()==QProcess::Running)
        {
            process->write(code + '\0');
            startLoopTimer();
        }
}
//------------------------------------------------------------------------------
void TlgProcess::stopLoopTimer()
{
    if (timerID_Loop != 0) {
        killTimer(timerID_Loop);
        timerID_Loop = 0;
    }
}
//------------------------------------------------------------------------------
void TlgProcess::startLoopTimer()
{
    if (timerID_Loop==0) timerID_Loop = startTimer(200);
}
//------------------------------------------------------------------------------
void TlgProcess::timerEvent(QTimerEvent *event)
{
    if (event->timerId()==timerID_Loop)
    {
        if (process && bLastWork)
            if (process->state()==QProcess::Running)
            {
                if (bConnected && !commands.isEmpty()){
                    process->write(commands.first());
                    commands.removeFirst();
                }
                else process->write(QByteArrayLiteral("u\0"));
            }
    }
    else if (event->timerId()==timerID_Restart)
    {
        killTimer(timerID_Restart);
        timerID_Restart = 0;

        start();
    }
}
