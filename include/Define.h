//
// Copyright Kuzminov Sergey (kuzminov_sergey@mail.ru) 2021
//
// Distributed under the Unlicense License. (See accompanying
// file LICENSE_1_0.txt or copy at http://unlicense.org/)
//

#ifndef DEFINE_H
#define DEFINE_H

#include <QtCore>

extern quint16 applicationVersion;
extern QString mainPath;
extern const QDataStream::Version dataStreamVersion;

//---------------------------------------------------------------------------------------
// Решение проблемы преобразования QList в QSet при сборке одновременно в Qt 5.12 и 5.14
//---------------------------------------------------------------------------------------
template <typename T>
inline QSet<T> QListToQSet(const QList<T>& qlist)
{
#if QT_VERSION > QT_VERSION_CHECK(5, 14, 0)
    return QSet<T> (qlist.constBegin(), qlist.constEnd());
#else
    return qlist.toSet();
#endif
}

//---------------------------------------------------------------------------------------
// (de)serialize enums into QDataStream для Qt < 5.14
//---------------------------------------------------------------------------------------
#if QT_VERSION < QT_VERSION_CHECK(5, 14, 0)
template <typename T>
typename std::enable_if<std::is_enum<T>::value, QDataStream &>::type&
operator<<(QDataStream &s, const T &t)
{ return s << static_cast<typename std::underlying_type<T>::type>(t); }

template <typename T>
typename std::enable_if<std::is_enum<T>::value, QDataStream &>::type&
operator>>(QDataStream &s, T &t)
{ return s >> reinterpret_cast<typename std::underlying_type<T>::type &>(t); }
#endif

//---------------------------------------------------------------------------------------

const QString constFileMail         ( QStringLiteral("/Base/mail.bin")  );
const QString constFileGSM          ( QStringLiteral("/Base/gsm.bin")  );
const QString constFileTlg          ( QStringLiteral("/Base/telegram.bin")  );

const QString constFileNameServer   ( QStringLiteral("/Base/server.ini")  );
const QString constFileNamePort     ( QStringLiteral("/Base/port.ini")  );

const QString constFileNameAddressPP( QStringLiteral("/Base/adress.ini")  );
const QString constFileNameConfigPP ( QStringLiteral("/Base/configPP.cnu")  );
const QString constFileDescriptions ( QStringLiteral("/Base/descriptions.bin")  );
const QString constFileNameBD       ( QStringLiteral("/Events/C2000PP.bin") );



#endif // DEFINE_H
