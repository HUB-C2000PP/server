//
// Copyright Kuzminov Sergey (kuzminov_sergey@mail.ru) 2021
//
// Distributed under the Unlicense License. (See accompanying
// file LICENSE_1_0.txt or copy at http://unlicense.org/)
//

#ifndef LANDEF_H
#define LANDEF_H

#include <QtCore>

const quint16 magicByte = 0xfabf;

enum enumOverhead
{
    eo_empty  = 0,
    eo_login  = 1,
    eo_rand   = 2,
    eo_canals = 3
};

//---------------------------------------------------------------------------------
const QString lanMsgBegin               = QStringLiteral("====================");
const QString lanMsgAttempt             = QStringLiteral(u"Попытка подключиться.");
const QString lanMsgAttemptP            = QStringLiteral(u"Попытка подключиться: %1:%2.");

const QString lanMsgAttemptServer       = QStringLiteral(u"Попытка подключения: %1.");

const QString lanMsgConnected           = QStringLiteral(u"Подключение установлено.");
const QString lanMsgConnectedID         = QStringLiteral(u"Подключение установлено: <%1>.");

const QString lanMsgCorrupted           = QStringLiteral(u"Данные повреждены.");
const QString lanMsgCorruptedID         = QStringLiteral(u"Данные повреждены: <%1>.");

const QString lanMsgDisconnect          = QStringLiteral(u"Отключение.");
const QString lanMsgDisconnectID        = QStringLiteral(u"Отключение:          %1.");

const QString lanMsgStartLocalServer    = QStringLiteral(u"Запущен сервер локальных подключений.");
const QString lanMsgStartTCPServer      = QStringLiteral(u"Запущен сервер TCP:  %1:%2.");

const QString lanMsgErrorLocalServer    = QStringLiteral(u"Не удается создать сервер локальных подключений: %1.");
const QString lanMsgErrorTCPServer      = QStringLiteral(u"Не удается создать TCP сервер: %1:%2, %3.");

const QString lanMsgStopServer          = QStringLiteral(u"Останов сервера.");
const QString lanMsgStopTCPServer       = QStringLiteral(u"Останов TCP сервера: %1:%2.");

const QString lanMsgCheckingLogin       = QStringLiteral(u"Проверка логина [%1].");
const QString lanMsgCorrectLogin        = QStringLiteral(u"Верный логин.");
const QString lanMsgUncorrectLogin      = QStringLiteral(u"Неверный логин.");
const QString lanMsgCorrectPWD          = QStringLiteral(u"Верный пароль.");
const QString lanMsgUncorrectPWD        = QStringLiteral(u"Неверный пароль.");
const QString lanMsgDecryptionError     = QStringLiteral(u"Ошибка расшифровки.");

const QString lanMsgCorrectLoginServ    = QStringLiteral(u"Верный логин:        %1 [%2].");
const QString lanMsgUncorrectLoginServ  = QStringLiteral(u"Неверный логин:      %1 [%2].");
const QString lanMsgCorrectPWDServ      = QStringLiteral(u"Верный пароль:       %1.");
const QString lanMsgUncorrectPWDServ    = QStringLiteral(u"Неверный пароль:     %1.");
const QString lanMsgDecryptionErrorServ = QStringLiteral(u"Ошибка расшифровки:  %1.");
//---------------------------------------------------------------------------------

#endif // LANDEF_H
