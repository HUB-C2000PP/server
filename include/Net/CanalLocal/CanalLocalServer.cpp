//
// Copyright Kuzminov Sergey (kuzminov_sergey@mail.ru) 2021
//
// Distributed under the Unlicense License. (See accompanying
// file LICENSE_1_0.txt or copy at http://unlicense.org/)
//

#include "CanalLocalServer.h"
#include "../LanDef.h"

//---------------------------------------------------------------------------------
CanalLocalServer::CanalLocalServer(QObject *parent) : QObject(parent)
{
    qRegisterMetaType<QLocalSocket::LocalSocketError>("LocalSocketError");

    server.setMaxPendingConnections(16); // Ограничение очереди ожидающих подключение сокетов
    server.setSocketOptions( QLocalServer::WorldAccessOption ); // Без ограничения доступа.

    QObject::connect(&server, &QLocalServer::newConnection, this, &CanalLocalServer::serverNewConnection);
    QObject::connect(this,    &CanalLocalServer::signalReg, this, &CanalLocalServer::slotReg, Qt::BlockingQueuedConnection);
}
//---------------------------------------------------------------------------------
CanalLocalServer::~CanalLocalServer()
{
    QObject::disconnect(this,    nullptr, this, nullptr);
    QObject::disconnect(&server, nullptr, this, nullptr);

    stopServer();

    logFile.writeLog(lanMsgStopServer);
}
//---------------------------------------------------------------------------------
QByteArray CanalLocalServer::makeCurrentPathAddress(QByteArray prefix, QByteArray postfix)
{
    QByteArray hashName = mainPath.
        #ifdef Q_OS_WIN
            toUpper().
        #endif
            toUtf8();

    hashName = QCryptographicHash::hash(hashName, QCryptographicHash::Sha3_256).toHex().prepend(prefix).append(postfix);

#ifdef Q_OS_MACOS
    hashName = hashName.right(32);
#endif

    return hashName;
}
//---------------------------------------------------------------------------------
void CanalLocalServer::setLogName(QString logName)
{
    logFile.setFileName(logName);
    logFile.writeLog(lanMsgBegin);
}
//---------------------------------------------------------------------------------
void CanalLocalServer::setAddress(const QString &serverName)
{
    this->serverName = serverName;
    reStartServer();
}
//---------------------------------------------------------------------------------
void CanalLocalServer::registration(ServerLink* obj, quint8 canal)
{
    if (obj->thread() == this->thread())
        slotReg(obj, canal);
    else
        emit signalReg(obj, canal);
}
//---------------------------------------------------------------------------------
void CanalLocalServer::stopServer(void)
{
    auto socketDescriptors = sockets.keys();
    for (auto socketDescriptor : socketDescriptors)
        deleteSocket( socketDescriptor );

    server.close();

    // Эта функция предназначена для восстановления после сбоя.
    // В Windows эта функция ничего не делает, на Unix она удаляет файл сокета.
#ifdef Q_OS_UNIX
    server.removeServer(serverName);
#endif
}
//---------------------------------------------------------------------------------
void CanalLocalServer::reStartServer(void)
{
    stopServer();

    if ( server.listen(serverName) )
    {
        logFile.writeLog(lanMsgStartLocalServer);
        lastError.clear();
    }
    else
    {
        if ( lastError != server.errorString() ) {
            lastError = server.errorString();
            logFile.writeLog( lanMsgErrorLocalServer.arg(lastError) );
        }

        QTimer::singleShot(1000, this, &CanalLocalServer::reStartServer);
    }
}
//---------------------------------------------------------------------------------
void CanalLocalServer::deleteSocket(qintptr socketDescriptor)
{
    if ( sockets.contains(socketDescriptor) )
    {
        logFile.writeLog( lanMsgDisconnectID.arg( socketDescriptor ) );

        QPointer < QLocalSocket > socket = sockets.value(socketDescriptor);
        if (socket) {
            QObject::disconnect(socket, nullptr, this, nullptr);
            socket->deleteLater();
        }

        sockets.   remove(socketDescriptor);
        byteCount. remove(socketDescriptor);
        lastCanals.remove(socketDescriptor);

        auto itEnd = canalObj.constEnd();
        for (auto itObj = canalObj.constBegin(); itObj != itEnd; ++itObj)
            if (itObj.value()) emit itObj.value()->delConnection(socketDescriptor);
    }
}
//---------------------------------------------------------------------------------
void CanalLocalServer::serverNewConnection(void)
{
    while (server.hasPendingConnections())
    {
        QLocalSocket *socket = server.nextPendingConnection();

        qintptr socketDescriptor = socket->socketDescriptor();
        sockets[ socketDescriptor ] = socket;

        QObject::connect(socket, &QLocalSocket::disconnected, this, &CanalLocalServer::socketDisconnected);
        QObject::connect(socket, &QLocalSocket::readyRead,    this, &CanalLocalServer::socketReadyRead);
#if QT_VERSION < QT_VERSION_CHECK(5, 15, 0)
        QObject::connect(socket, qOverload<QLocalSocket::LocalSocketError>(&QLocalSocket::error), this, &CanalLocalServer::socketError );
#else
        QObject::connect(socket, &QLocalSocket::errorOccurred, this, &CanalLocalServer::socketError );
#endif

        logFile.writeLog( lanMsgConnectedID.arg( socketDescriptor ) );

        syncID(socketDescriptor);
    }
}
//---------------------------------------------------------------------------------
void CanalLocalServer::syncID(qintptr socketDescriptor)
{    
    QByteArray data;
    QDataStream ds(&data, QIODevice::WriteOnly);
    ds.setVersion(dataStreamVersion);
    ds << static_cast<quint8>(eo_canals) << QListToQSet(canalObj.keys());
    canalSendData(0, socketDescriptor, data);
}
//---------------------------------------------------------------------------------
void CanalLocalServer::socketDisconnected(void)
{
    QLocalSocket* socket = qobject_cast<QLocalSocket*>(sender());
    if (socket) deleteSocket( socket->socketDescriptor() );
}
//---------------------------------------------------------------------------------
void CanalLocalServer::socketError(QLocalSocket::LocalSocketError)
{
    QLocalSocket* socket = qobject_cast<QLocalSocket*>(sender());
    if (socket) deleteSocket( socket->socketDescriptor() );
}

//---------------------------------------------------------------------------------
void CanalLocalServer::canalSendData(quint8 canal, qintptr socketDescriptor, QByteArray data)
{
    if ( sockets.contains(socketDescriptor) )
    {
        QPointer < QLocalSocket > socket = sockets.value(socketDescriptor);
        if (socket){
            if (socket->state() == QLocalSocket::ConnectedState)
            {
                int count = data.count() + static_cast<int>( sizeof(canal) );

                QByteArray buff;
                buff.append( reinterpret_cast<const char*>(&magicByte), sizeof(magicByte) );
                buff.append( reinterpret_cast<const char*>(&count),     sizeof(count) );
                buff.append( reinterpret_cast<const char*>(&canal),     sizeof(canal) );
                buff.append( data );

                socket->write( buff );
            }
        }
    }
}
//---------------------------------------------------------------------------------
void CanalLocalServer::sendData(qintptr socketDescriptor, const QByteArray &data)
{
    ServerLink* obj = qobject_cast<ServerLink*>(sender());
    quint8 canal = canalObj.key(obj);
    if ( canal!=0 ) canalSendData(canal, socketDescriptor, data);
}
//---------------------------------------------------------------------------------
void CanalLocalServer::socketReadyRead(void)
{
    QLocalSocket* socket = qobject_cast<QLocalSocket*>(sender());
    if (socket){
        qintptr socketDescriptor = socket->socketDescriptor();

        while (socket->bytesAvailable() )
        {
            qint32 bCount = byteCount.value( socketDescriptor, -1 );

            if ( bCount < 0 ) {
                if (socket->bytesAvailable() < static_cast<qint64>(sizeof(magicByte)+ sizeof(bCount)) ) break;
                else
                {
                    quint16 magByte;
                    socket->read( reinterpret_cast<char*>(&magByte), sizeof(magByte) ) ;

                    if (magByte == magicByte) {
                        socket->read( reinterpret_cast<char*>(&bCount), sizeof(bCount) ) ;
                        byteCount[socketDescriptor] = bCount;
                    }
                    else {
                        logFile.writeLog( lanMsgCorruptedID.arg( socketDescriptor ) );
                        deleteSocket( socketDescriptor );
                        break;
                    }
                }
            }

            if (bCount >= 0) {
                if (socket->bytesAvailable() < bCount) break;
                else
                {
                    quint8 canal;
                    socket->read( reinterpret_cast<char*>(&canal), sizeof(canal) ) ;

                    QByteArray data = socket->read( bCount - static_cast<int>(sizeof(canal)) ) ;
                    byteCount[socketDescriptor] = -1;

                    if (canal!=0)
                    {
                        QPointer <ServerLink> obj = canalObj.value(canal);
                        if ( obj ) emit obj->readyRead( socketDescriptor, data );
                    }
                    else
                    {
                        QDataStream dataStream(&data, QIODevice::ReadOnly);
                        dataStream.setVersion(dataStreamVersion);
                        quint8 cmd = 0;
                        dataStream >> cmd;

                        if (cmd == eo_canals)
                        {
                            QSet<quint8> canals;
                            dataStream >> canals;

                            QSet<quint8> newCanals = canals - lastCanals.value(socketDescriptor);

                            for(const auto newCanal : qAsConst(newCanals))
                                if (canalObj.contains(newCanal))
                                {
                                    QPointer < ServerLink > obj = canalObj.value(newCanal);
                                    if (obj) emit obj->newConnection(socketDescriptor);
                                }

                            lastCanals[socketDescriptor] = canals;
                        }
                    }
                }
            }
        }
    }
}
//---------------------------------------------------------------------------------
void CanalLocalServer::slotReg(ServerLink* obj, quint8 canal)
{
    if ( (canal!=0) && (canalObj.key(obj)==0) )
    {
        canalObj.insert(canal, obj);

        if (obj->thread() == this->thread())
            QObject::connect(obj,  &ServerLink::sendData,  this, &CanalLocalServer::sendData);
        else QObject::connect(obj,  &ServerLink::sendData,  this, &CanalLocalServer::sendData, Qt::BlockingQueuedConnection);

        auto itEnd = sockets.constEnd();
        for (auto itSocket = sockets.constBegin(); itSocket != itEnd; ++itSocket)
            if (itSocket.value())
                if (itSocket.value()->state() == QLocalSocket::ConnectedState){
                    qintptr socketDescriptor = itSocket.value()->socketDescriptor();

                    syncID(socketDescriptor);

                    QSet<quint8> lastCanal = lastCanals.value(socketDescriptor);
                    if (lastCanal.contains(canal)) emit obj->newConnection( socketDescriptor );
                }
    }
}
