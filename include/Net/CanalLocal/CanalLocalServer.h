//
// Copyright Kuzminov Sergey (kuzminov_sergey@mail.ru) 2021
//
// Distributed under the Unlicense License. (See accompanying
// file LICENSE_1_0.txt or copy at http://unlicense.org/)
//

#ifndef CANALLOCALSERVER_H
#define CANALLOCALSERVER_H

#include <QtCore>
#include <QtNetwork>

#include "../CanalLink/ServerLink.h"
#include "include/LogFiles/LogFiles.h"
#include "include/Define.h"

//---------------------------------------------------------------------------------
class CanalLocalServer : public QObject
{
    Q_OBJECT

private:
    QString      serverName;
    QLocalServer server;
    QString      lastError;

    LogFile      logFile;

    QMap <qintptr, int> byteCount;
    QMap <qintptr, QSet<quint8> > lastCanals;
    QMap <qintptr, QPointer < QLocalSocket > > sockets;

    QMap <quint8, QPointer < ServerLink > > canalObj;

    void syncID       (qintptr socketDescriptor);
    void canalSendData(quint8 canal, qintptr socketDescriptor, QByteArray data);

public:
    explicit CanalLocalServer(QObject *parent = nullptr);
    ~CanalLocalServer();

    static QByteArray       makeCurrentPathAddress(QByteArray prefix, QByteArray postfix);

    void setLogName         (QString logName);    
    void setAddress         (const QString &serverName);
    void setAddress         (const QByteArray &serverName) {setAddress( QString::fromUtf8(serverName) );}
    void registration       (ServerLink* obj, quint8 canal);

private slots:
    void reStartServer      (void);
    void stopServer         (void);
    void serverNewConnection(void);
    void socketReadyRead    (void);
    void socketDisconnected (void);
    void socketError        (QLocalSocket::LocalSocketError);
    void deleteSocket       (qintptr socketDescriptor);
    void sendData           (qintptr socketDescriptor, const QByteArray &data);
    void slotReg            (ServerLink* obj, quint8 canal);

signals:
    void signalReg          (ServerLink* obj, quint8 canal);
};
//---------------------------------------------------------------------------------
#endif // CANALLOCALSERVER_H
