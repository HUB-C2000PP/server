//
// Copyright Kuzminov Sergey (kuzminov_sergey@mail.ru) 2021
//
// Distributed under the Unlicense License. (See accompanying
// file LICENSE_1_0.txt or copy at http://unlicense.org/)
//

#ifndef CANALSTCPSERVER_H
#define CANALSTCPSERVER_H

#include <QtCore>
#include <QtNetwork>

#include "../CanalLink/ServerLink.h"
#include "include/LogFiles/LogFiles.h"
#include "include/GOST/GOST14.h"
#include "include/Define.h"

//---------------------------------------------------------------------------------
class CanalSTcpServer : public QObject
{
    Q_OBJECT

private:
    QHostAddress host;
    quint16      port = 0;
    QTcpServer   server;
    QString      lastError;

    LogFile      logFile;

    QMap <qintptr, int> byteCount;
    QMap <qintptr, QSet<quint8> > lastCanals;
    QMap <qintptr, QPointer < QTcpSocket > > sockets;

    QMap <quint8, QPointer < ServerLink > > canalObj;

    void syncID       (qintptr socketDescriptor);
    void canalSendData(quint8 canal, qintptr socketDescriptor, QByteArray data);

    int  timer = 0;
    int  iKeepAliveTime = 15000;
    bool bKeepAliveStep = false;
    QMap <qintptr, bool> bKeepAliveTaken;
    QMap <qintptr, bool> bKeepAliveTrans;

    void timerEvent(QTimerEvent*) override;
    void startKeepAlive(void);
    void stopKeepAlive (void);

    QString strNameSocket(qintptr socketDescriptor);

    //-----------
    QHash <QString, QByteArray> logins1;
    QHash <QString, QByteArray> logins2;

    QHash <qintptr, QByteArray> pwds1;
    QHash <qintptr, QByteArray> pwds2;

    QHash <qintptr, QByteArray> socketRnd;
    QSet  <qintptr>             socketAcces;

    QByteArray Encrypt(QByteArray data, qintptr socketDescriptor, bool calcCRC=true);
    QByteArray Decrypt(QByteArray data, qintptr socketDescriptor, bool &ok, bool calcCRC=true);
    QByteArray Decrypt(const QByteArray &data, qintptr socketDescriptor) {bool ok; return Decrypt(data, socketDescriptor, ok, false);}
    QByteArray Checksum(QByteArray data, QByteArray pwd, int init);
    //-----------

public:
    explicit CanalSTcpServer(QObject *parent = nullptr);
    ~CanalSTcpServer() override;

    void setLogName         (QString logName);
    void registration       (ServerLink* obj, quint8 canal);

public slots:
    void setAddress         (const QHostAddress host, quint16 port);
    void addLogin           (QString login, QString pwd1, QString pwd2);

private slots:
    void reStartServer      (void);
    void stopServer         (void);
    void serverNewConnection(void);
    void socketReadyRead    (void);
    void socketDisconnected (void);
    void socketError        (QAbstractSocket::SocketError);
    void deleteSocket       (qintptr socketDescriptor);
    void sendData           (qintptr socketDescriptor, const QByteArray &data);
    void slotReg            (ServerLink* obj, quint8 canal);

signals:
    void signalReg          (ServerLink* obj, quint8 canal);
};
//---------------------------------------------------------------------------------
#endif // CANALTCPSERVER_H
