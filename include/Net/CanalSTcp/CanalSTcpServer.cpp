//
// Copyright Kuzminov Sergey (kuzminov_sergey@mail.ru) 2021
//
// Distributed under the Unlicense License. (See accompanying
// file LICENSE_1_0.txt or copy at http://unlicense.org/)
//

#include "CanalSTcpServer.h"
#include "../LanDef.h"

//---------------------------------------------------------------------------------
CanalSTcpServer::CanalSTcpServer(QObject *parent) : QObject(parent)
{
    qRegisterMetaType<QAbstractSocket::SocketError>("AbstractSocketError");

    server.setProxy( QNetworkProxy::NoProxy );
    server.setMaxPendingConnections(16); // Ограничение очереди ожидающих подключение сокетов

    QObject::connect(&server, &QTcpServer::newConnection,  this, &CanalSTcpServer::serverNewConnection);
    QObject::connect(this,    &CanalSTcpServer::signalReg, this, &CanalSTcpServer::slotReg, Qt::BlockingQueuedConnection);
}
//---------------------------------------------------------------------------------
CanalSTcpServer::~CanalSTcpServer()
{
    QObject::disconnect(this,    nullptr, this, nullptr);
    QObject::disconnect(&server, nullptr, this, nullptr);

    stopServer();

    logFile.writeLog( lanMsgStopTCPServer.arg(host.toString()).arg(port) );
}
//---------------------------------------------------------------------------------
void CanalSTcpServer::setLogName(QString logName)
{
    logFile.setFileName(logName);
    logFile.writeLog(lanMsgBegin);
}
//---------------------------------------------------------------------------------
void CanalSTcpServer::setAddress(const QHostAddress host, quint16 port)
{
    if ( (this->host != host) || (this->port != port) ){
        this->host = host;
        this->port = port;

        reStartServer();
    }
}
//---------------------------------------------------------------------------------
void CanalSTcpServer::addLogin(QString login, QString pwd1, QString pwd2)
{
    // Должно быть именно 2 разных пароля. Эти пароли нельзя автоматически (по фиксированному алгоритму) получать из 1 пароля,
    // иначе будет возможность подобрать пароль, перехватив данные "рукопожатия". Хотя длинный пароль это сильно затруднит.

    logins1[login] = ExpandKey_14(pwd1.toUtf8());
    logins2[login] = ExpandKey_14(pwd2.toUtf8());
}
//---------------------------------------------------------------------------------
void CanalSTcpServer::registration(ServerLink* obj, quint8 canal)
{
    if (obj->thread() == this->thread())
        slotReg(obj, canal);
    else
        emit signalReg(obj, canal);
}
//---------------------------------------------------------------------------------
void CanalSTcpServer::stopServer(void)
{
    stopKeepAlive();

    const auto socketDescriptors = sockets.keys();
    for (const auto socketDescriptor : socketDescriptors)
        deleteSocket( socketDescriptor );

    server.close();
}
//---------------------------------------------------------------------------------
void CanalSTcpServer::reStartServer(void)
{
    stopServer();

    if ( server.listen(host, port) )
    {
        logFile.writeLog( lanMsgStartTCPServer.arg(host.toString()).arg(port) );
        lastError.clear();
        startKeepAlive();
    }
    else
    {
        if ( lastError != server.errorString() ) {
            lastError = server.errorString();
            logFile.writeLog( lanMsgErrorTCPServer.arg(host.toString()).arg(port).arg(lastError) );
        }

        QTimer::singleShot(1000, this, &CanalSTcpServer::reStartServer);
    }
}
//---------------------------------------------------------------------------------
void CanalSTcpServer::deleteSocket(qintptr socketDescriptor)
{
    if ( sockets.contains(socketDescriptor) )
    {
        logFile.writeLog( lanMsgDisconnectID.arg( strNameSocket(socketDescriptor) ) );

        QPointer < QTcpSocket > socket = sockets.value(socketDescriptor);
        if (socket) {
            QObject::disconnect(socket, nullptr, this, nullptr);
            socket->deleteLater();
        }

        pwds1.      remove(socketDescriptor);
        pwds2.      remove(socketDescriptor);
        socketRnd.  remove(socketDescriptor);
        socketAcces.remove(socketDescriptor);

        sockets.   remove(socketDescriptor);
        byteCount. remove(socketDescriptor);
        lastCanals.remove(socketDescriptor);

        bKeepAliveTaken.remove(socketDescriptor);
        bKeepAliveTrans.remove(socketDescriptor);

        auto itEnd = canalObj.constEnd();
        for (auto itObj = canalObj.constBegin(); itObj != itEnd; ++itObj)
            if (itObj.value()) emit itObj.value()->delConnection(socketDescriptor);
    }
}
//---------------------------------------------------------------------------------
void CanalSTcpServer::serverNewConnection(void)
{
    while (server.hasPendingConnections())
    {
        QTcpSocket *socket = server.nextPendingConnection();

        qintptr socketDescriptor = socket->socketDescriptor();
        sockets[ socketDescriptor ] = socket;

        QObject::connect(socket, &QTcpSocket::disconnected, this, &CanalSTcpServer::socketDisconnected);
        QObject::connect(socket, &QTcpSocket::readyRead,    this, &CanalSTcpServer::socketReadyRead);
#if QT_VERSION < QT_VERSION_CHECK(5, 15, 0)
        QObject::connect(socket, qOverload<QAbstractSocket::SocketError>(&QTcpSocket::error), this, &CanalSTcpServer::socketError);
#else
        QObject::connect(socket, &QTcpSocket::errorOccurred, this, &CanalSTcpServer::socketError);
#endif

        logFile.writeLog( lanMsgAttemptServer.arg( strNameSocket(socketDescriptor) ) );
    }
}
//---------------------------------------------------------------------------------
void CanalSTcpServer::syncID(qintptr socketDescriptor)
{
    QByteArray data;
    QDataStream ds(&data, QIODevice::WriteOnly);
    ds.setVersion(dataStreamVersion);
    ds << static_cast<quint8>(eo_canals) << QListToQSet(canalObj.keys()) << iKeepAliveTime;
    canalSendData(0, socketDescriptor, data);
}
//---------------------------------------------------------------------------------
void CanalSTcpServer::socketDisconnected(void)
{
    QTcpSocket* socket = qobject_cast<QTcpSocket*>(sender());
    if (socket) deleteSocket( socket->socketDescriptor() );
}
//---------------------------------------------------------------------------------
void CanalSTcpServer::socketError(QAbstractSocket::SocketError)
{
    QTcpSocket* socket = qobject_cast<QTcpSocket*>(sender());
    if (socket) deleteSocket( socket->socketDescriptor() );
}
//---------------------------------------------------------------------------------
void CanalSTcpServer::canalSendData(quint8 canal, qintptr socketDescriptor, QByteArray data)
{
    if ( sockets.contains(socketDescriptor) )
    {
        QPointer < QTcpSocket > socket = sockets.value(socketDescriptor);
        if (socket){
            if (socket->state() == QTcpSocket::ConnectedState)
            {
                int count = data.count() + static_cast<int>( sizeof(canal) );

                QByteArray buff;
                buff.append( reinterpret_cast<const char*>(&magicByte), sizeof(magicByte) );
                buff.append( reinterpret_cast<const char*>(&count),     sizeof(count) );
                buff.append( reinterpret_cast<const char*>(&canal),     sizeof(canal) );
                buff.append( data );

                socket->write( buff );

                bKeepAliveTrans[socketDescriptor] = true;
            }
        }
    }
}
//---------------------------------------------------------------------------------
void CanalSTcpServer::sendData(qintptr socketDescriptor, const QByteArray &data)
{
    if ( socketAcces.contains(socketDescriptor) )
    {
        ServerLink* obj = qobject_cast<ServerLink*>(sender());
        quint8 canal = canalObj.key(obj);
        if ( canal!=0 ) canalSendData(canal, socketDescriptor, Encrypt(data, socketDescriptor) );
    }
}
//---------------------------------------------------------------------------------
void CanalSTcpServer::socketReadyRead(void)
{
    QTcpSocket* socket = qobject_cast<QTcpSocket*>(sender());
    if (socket){
        qintptr socketDescriptor = socket->socketDescriptor();

        bKeepAliveTaken[socketDescriptor] = true;

        while (socket->bytesAvailable() )
        {
            qint32 bCount = byteCount.value( socketDescriptor, -1 );

            if ( bCount < 0 ) {
                if (socket->bytesAvailable() < static_cast<qint64>(sizeof(magicByte)+ sizeof(bCount)) ) break;
                else
                {
                    quint16 magByte;
                    socket->read( reinterpret_cast<char*>(&magByte), sizeof(magByte) ) ;

                    if (magByte == magicByte) {
                        socket->read( reinterpret_cast<char*>(&bCount), sizeof(bCount) ) ;
                        byteCount[socketDescriptor] = bCount;
                    }
                    else {
                        logFile.writeLog( lanMsgCorruptedID.arg( strNameSocket(socketDescriptor) ) );
                        deleteSocket( socketDescriptor );
                        break;
                    }
                }
            }

            if (bCount >= 0) {
                if (socket->bytesAvailable() < bCount) break;
                else
                {
                    quint8 canal;
                    socket->read( reinterpret_cast<char*>(&canal), sizeof(canal) ) ;

                    QByteArray data = socket->read( bCount - static_cast<qint32>(sizeof(canal)) ) ;
                    byteCount[socketDescriptor] = -1;

                    if (canal!=0)
                    {
                        if ( socketAcces.contains(socketDescriptor) )
                        {
                            QPointer <ServerLink> obj = canalObj.value(canal);
                            if ( obj )
                            {
                                bool ok;
                                QByteArray ba = Decrypt(data, socketDescriptor, ok);
                                if (ok) emit obj->readyRead(socketDescriptor, ba);
                                else {
                                    logFile.writeLog( lanMsgDecryptionErrorServ.arg( strNameSocket(socketDescriptor) ) );
                                    deleteSocket(socketDescriptor);
                                }
                            }
                        }
                    }
                    else
                    {
                        QDataStream dataStream(&data, QIODevice::ReadOnly);
                        dataStream.setVersion(dataStreamVersion);
                        quint8 cmd = 0;
                        dataStream >> cmd;

                        if (cmd == eo_canals)
                        {
                            QSet<quint8> canals;
                            dataStream >> canals;

                            QSet<quint8> newCanals = canals - lastCanals.value(socketDescriptor);

                            for(const quint8 newCanal : qAsConst(newCanals))
                                if (canalObj.contains(newCanal))
                                {
                                    QPointer < ServerLink > obj = canalObj.value(newCanal);
                                    if (obj) emit obj->newConnection(socketDescriptor);
                                }

                            lastCanals[socketDescriptor] = canals;
                        }
                        else if (cmd == eo_login){
                            QString login;
                            dataStream >> login;

                            if (logins1.contains(login))
                            {
                                logFile.writeLog( lanMsgCorrectLoginServ.arg(strNameSocket(socketDescriptor), login) );

                                pwds1[socketDescriptor] = logins1.value(login);
                                pwds2[socketDescriptor] = logins2.value(login);

                                int init = static_cast<int>(QRandomGenerator::global()->generate());
                                QByteArray baRand;
                                baRand.append( reinterpret_cast<char*>(&init), sizeof(init) );
                                socketRnd[socketDescriptor] = baRand;

                                QByteArray dt;
                                QDataStream ds(&dt, QIODevice::WriteOnly);
                                ds.setVersion(dataStreamVersion);
                                ds << static_cast<quint8>(eo_login) << static_cast<bool>(true)
                                   << Encrypt(baRand, socketDescriptor, false);
                                canalSendData(0, socketDescriptor, dt);
                            }
                            else
                            {
                                logFile.writeLog( lanMsgUncorrectLoginServ.arg(strNameSocket(socketDescriptor), login) );

                                pwds1.      remove(socketDescriptor);
                                pwds2.      remove(socketDescriptor);
                                socketRnd.  remove(socketDescriptor);
                                socketAcces.remove(socketDescriptor);

                                QByteArray dt;
                                QDataStream ds(&dt, QIODevice::WriteOnly);
                                ds.setVersion(dataStreamVersion);
                                ds << static_cast<quint8>(eo_login) << static_cast<bool>(false);
                                canalSendData(0, socketDescriptor, dt);
                            }
                        }
                        else if (cmd == eo_rand){
                            QByteArray baRand;
                            dataStream >> baRand;

                            baRand = Decrypt(baRand, socketDescriptor);

                            if (baRand == QCryptographicHash::hash(socketRnd.value(socketDescriptor), QCryptographicHash::Sha3_512))
                            {
                                logFile.writeLog( lanMsgCorrectPWDServ.arg( strNameSocket(socketDescriptor) ) );

                                socketAcces.insert(socketDescriptor);

                                QByteArray dt;
                                QDataStream ds(&dt, QIODevice::WriteOnly);
                                ds.setVersion(dataStreamVersion);
                                ds << static_cast<quint8>(eo_rand) << static_cast<bool>(true);
                                canalSendData(0, socketDescriptor, dt);

                                syncID(socketDescriptor);
                            }
                            else{
                                logFile.writeLog( lanMsgUncorrectPWDServ.arg( strNameSocket(socketDescriptor) ) );

                                pwds1.      remove(socketDescriptor);
                                pwds2.      remove(socketDescriptor);
                                socketRnd.  remove(socketDescriptor);
                                socketAcces.remove(socketDescriptor);

                                QByteArray dt;
                                QDataStream ds(&dt, QIODevice::WriteOnly);
                                ds.setVersion(dataStreamVersion);
                                ds << static_cast<quint8>(eo_rand) << static_cast<bool>(false);
                                canalSendData(0, socketDescriptor, dt);
                            }
                        }
                    }
                }
            }
        }
    }
}
//---------------------------------------------------------------------------------------
void CanalSTcpServer::slotReg(ServerLink* obj, quint8 canal)
{
    if ( (canal!=0) && (canalObj.key(obj)==0) )
    {
        canalObj.insert(canal, obj);

        if (obj->thread() == this->thread())
            QObject::connect(obj,  &ServerLink::sendData,  this, &CanalSTcpServer::sendData);
        else QObject::connect(obj,  &ServerLink::sendData,  this, &CanalSTcpServer::sendData, Qt::BlockingQueuedConnection);

        auto itEnd = sockets.constEnd();
        for (auto itSocket = sockets.constBegin(); itSocket != itEnd; ++itSocket)
            if (itSocket.value())
                if (itSocket.value()->state() == QTcpSocket::ConnectedState)
                {
                    syncID(itSocket.key());

                    QSet<quint8> lastCanal = lastCanals.value(itSocket.key());
                    if (lastCanal.contains(canal)) emit obj->newConnection( itSocket.key() );
                }
    }
}
//---------------------------------------------------------------------------------------
void CanalSTcpServer::timerEvent(QTimerEvent*)
{
    if (bKeepAliveStep){
        const auto socketDescriptors = sockets.keys();
        for (const auto socketDescriptor : socketDescriptors)
        {
            if ( bKeepAliveTaken.value(socketDescriptor, false) )
                bKeepAliveTaken[socketDescriptor] = false;
            else deleteSocket(socketDescriptor);
        }
    }

    auto itEnd = sockets.constEnd();
    for (auto itSocket = sockets.constBegin(); itSocket != itEnd; ++itSocket)
    {
        if ( bKeepAliveTrans.value(itSocket.key(), false) )
            bKeepAliveTrans[itSocket.key()] = false;
        else
        {
            QByteArray data;
            QDataStream ds(&data, QIODevice::WriteOnly);
            ds.setVersion(dataStreamVersion);
            ds << static_cast<quint8>(eo_empty);
            canalSendData(0, itSocket.key(), data);
        }
    }

    bKeepAliveStep = !bKeepAliveStep;
}
//---------------------------------------------------------------------------------------
void CanalSTcpServer::startKeepAlive(void)
{
    stopKeepAlive();

    bKeepAliveStep = false;
    bKeepAliveTaken.clear();
    bKeepAliveTrans.clear();

    timer = startTimer(iKeepAliveTime/2, Qt::VeryCoarseTimer);
}
//---------------------------------------------------------------------------------------
void CanalSTcpServer::stopKeepAlive (void)
{
    if (timer!=0) {
        killTimer(timer);
        timer=0;
    }
}
//---------------------------------------------------------------------------------------
QString CanalSTcpServer::strNameSocket(qintptr socketDescriptor)
{
    QPointer < QTcpSocket > socket = sockets.value(socketDescriptor);

    QString socketAddres;
    quint16 socketPort = 0;

    if (socket) {
        socketAddres = socket->peerAddress().toString();
        socketPort   = socket->peerPort();
    }

    return QStringLiteral("%1:%2 <%3>").arg(socketAddres).arg(socketPort).arg(socketDescriptor);
}
//---------------------------------------------------------------------------------------
QByteArray CanalSTcpServer::Encrypt(QByteArray data, qintptr socketDescriptor, bool calcCRC)
{
    QByteArray ret;

    QByteArray pwd1 = pwds1.value(socketDescriptor);
    QByteArray pwd2 = pwds2.value(socketDescriptor);

    int init = static_cast<int>(QRandomGenerator::global()->generate());
    ret.append( reinterpret_cast<char*>(&init), sizeof(init) );
    ret.append( EncryptCFB_14(data, pwd1, init) );
    if (calcCRC) ret.append( Checksum(data, pwd2, init) ); // Контрольная сумма на другом пароле!

    return ret;
}
//---------------------------------------------------------------------------------------
QByteArray CanalSTcpServer::Decrypt(QByteArray data, qintptr socketDescriptor, bool &ok, bool calcCRC)
{
    ok = false;
    QByteArray ret;

    QByteArray pwd1 = pwds1.value(socketDescriptor);
    QByteArray pwd2 = pwds2.value(socketDescriptor);

    if (data.count()>5)
    {
        int init = *(reinterpret_cast<int*>(data.data()));
        data = data.right( data.count() - 4);

        if (calcCRC) {
            QByteArray chk = data.right(2);
            data.chop(2);

            ret  = DecryptCFB_14(data, pwd2, init);

            if ( Checksum(ret, pwd1, init) == chk) ok = true;
        }
        else
            ret  = DecryptCFB_14(data, pwd2, init);
    }

    return ret;
}
//---------------------------------------------------------------------------------------
QByteArray CanalSTcpServer::Checksum(QByteArray data, QByteArray pwd, int init)
{
#if QT_VERSION < QT_VERSION_CHECK(6, 0, 0)
    quint16 crc = qChecksum(data.data(), static_cast<uint>(data.count()));
#else
    quint16 crc = qChecksum(data);
#endif

    QByteArray ba;
    ba.append( reinterpret_cast<char*>(&crc), sizeof(crc) );

    return EncryptCFB_14(ba, pwd, init);
}
