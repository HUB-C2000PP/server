﻿//
// Copyright Kuzminov Sergey (kuzminov_sergey@mail.ru) 2021
//
// Distributed under the Unlicense License. (See accompanying
// file LICENSE_1_0.txt or copy at http://unlicense.org/)
//

#ifndef SERVERLINK_H
#define SERVERLINK_H

#include <QtCore/QObject>

class ServerLink : public QObject
{
    Q_OBJECT

public:
    explicit ServerLink(QObject *parent = nullptr) : QObject(parent){}

signals:
    void newConnection(qintptr socketDescriptor);
    void delConnection(qintptr socketDescriptor);
    void readyRead    (qintptr socketDescriptor, QByteArray data);
    void sendData     (qintptr socketDescriptor, QByteArray data);
};

#endif //SERVERLINK_H
