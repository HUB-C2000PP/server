//
// Copyright Kuzminov Sergey (kuzminov_sergey@mail.ru) 2021
//
// Distributed under the Unlicense License. (See accompanying
// file LICENSE_1_0.txt or copy at http://unlicense.org/)
//

#ifndef LANDEFMONITORING_H
#define LANDEFMONITORING_H

enum enumNetMon
{
    eNetMon_empty         = 0,

    eNetMon_statSendData  = 1,

    eNetMon_CfgPP_Get     = 2,
    eNetMon_CfgPP_Ret     = 3,

    eNetMon_Descr_Get     = 4,
    eNetMon_Descr_Ret     = 5,

    eNetMon_getVersion    = 6,
    eNetMon_retVersion    = 7,

    eNetMon_getCountEvent = 8,
    eNetMon_retCountEvent = 9,

    eNetMon_getEvents     = 10,
    eNetMon_retEvents     = 11,
    eNetMon_newEvent      = 12,

    eNetMon_getShState    = 13,
    eNetMon_retShState    = 14,
    eNetMon_newShState    = 15,

    eNetMon_getRlState    = 16,
    eNetMon_retRlState    = 17,
    eNetMon_newRlState    = 18,

    eNetMon_shOneCounter  = 19,
    eNetMon_shOneAdc      = 20,
    eNetMon_shMultiAdc    = 21,

    eNetMon_controlSh     = 22,
    eNetMon_controlPart   = 23,
    eNetMon_controlRl     = 24
};

enum enumNetMonState
{
    eNetMonState_TxRx1s   = 0,
    eNetMonState_TxRx1m   = 1,
    eNetMonState_enable   = 2,
    eNetMonState_error    = 3
};

#endif // LANDEFMONITORING_H
