//
// Copyright Kuzminov Sergey (kuzminov_sergey@mail.ru) 2021
//
// Distributed under the Unlicense License. (See accompanying
// file LICENSE_1_0.txt or copy at http://unlicense.org/)
//

#include "HubMonitoring.h"

//------------------------------------------------------------------------------
HubMonitoring::HubMonitoring(QObject *parent) : QObject(parent)
{
    // ----------------------------------------
    // Обмен данными через локалсокет
    // ----------------------------------------
    connect(&serverLinkLocal, &ServerLink::newConnection, this, &HubMonitoring::newLocalConnection);
    connect(&serverLinkLocal, &ServerLink::delConnection, this, &HubMonitoring::delLocalConnection);
    connect(&serverLinkLocal, &ServerLink::readyRead,     this, &HubMonitoring::readyReadLocal);

    // ----------------------------------------
    // Обмен данными через TCP
    // ----------------------------------------
    connect(&serverLinkTcp, &ServerLink::newConnection, this, &HubMonitoring::newTcpConnection);
    connect(&serverLinkTcp, &ServerLink::delConnection, this, &HubMonitoring::delTcpConnection);
    connect(&serverLinkTcp, &ServerLink::readyRead,     this, &HubMonitoring::readyReadTcp);
}
//------------------------------------------------------------------------------
// Новое входящее подключение от локального сокета или от TCP-сокета
//------------------------------------------------------------------------------
void HubMonitoring::newConnection(bool local, qintptr socketDescriptor)
{
    if (local) {
        localSocketsDescriptor.insert(socketDescriptor);
    }
    else {
        tcpSocketsDescriptor.insert(socketDescriptor);
    }
}
//------------------------------------------------------------------------------
// Соединение разорвано с локальным сокетом или с TCP-сокетом
//------------------------------------------------------------------------------
void HubMonitoring::delConnection(bool local, qintptr socketDescriptor)
{
    if (local) {
        localSocketsDescriptor.remove(socketDescriptor);
    }
    else {
        tcpSocketsDescriptor.remove(socketDescriptor);
    }
}
//------------------------------------------------------------------------------
// Отправить данные всем подключенным клиентам
//------------------------------------------------------------------------------
void HubMonitoring::sendAllSockets(QByteArray data)
{
    for(const auto socketDescriptor : qAsConst(localSocketsDescriptor))
        emit serverLinkLocal.sendData(socketDescriptor, data);

    for(const auto socketDescriptor : qAsConst(tcpSocketsDescriptor))
        emit serverLinkTcp.sendData(socketDescriptor, data);
}
//------------------------------------------------------------------------------
// Получены данные от локального сокета или от TCP-сокета
//------------------------------------------------------------------------------
void HubMonitoring::readyRead(bool local, qintptr socketDescriptor, QByteArray data)
{
    QDataStream dataStreamIn(&data, QIODevice::ReadOnly);
    dataStreamIn.setVersion(dataStreamVersion);
    quint8 command;
    dataStreamIn >> command;

    QByteArray dataOut;
    QDataStream dataStreamOut(&dataOut, QIODevice::WriteOnly);
    dataStreamOut.setVersion(dataStreamVersion);

    switch (command){
    case eNetMon_CfgPP_Get:
        // ----------------------------------------
        // Ответ на запрос конфигурации С2000-ПП
        // ----------------------------------------
        if (mC2000PP->isCorrectConfig())
            dataStreamOut << static_cast<quint8>(eNetMon_CfgPP_Ret) << *(mC2000PP->getConfigC2000PP());
        break;
    case eNetMon_Descr_Get:
        // ----------------------------------------
        // Ответ на запрос описаний Шс/Рл/Разделов
        // ----------------------------------------
    {
        auto configC2000PP = mC2000PP->getConfigC2000PP();
        dataStreamOut << static_cast<quint8>(eNetMon_Descr_Ret)
                      << configC2000PP->getDescriptionsSh()
                      << configC2000PP->getDescriptionsRl()
                      << configC2000PP->getDescriptionsPart();
    }
        break;
    case eNetMon_getVersion:
        // ----------------------------------------
        // Ответ на запрос версии прибора
        // ----------------------------------------
        dataStreamOut << static_cast<quint8>(eNetMon_retVersion) << mStateC2000PP->getVersion()
                      << mC2000PP->getPortState()
                      << mModbus->getLastErrorString();
        break;
    case eNetMon_getCountEvent:
        // ----------------------------------------
        // Ответ на запрос количества событий
        // ----------------------------------------
        dataStreamOut << static_cast<quint8>(eNetMon_retCountEvent) << mSimpleFileDB->count();
        break;
    case eNetMon_getEvents:
        // ----------------------------------------
        // Ответ на запрос события
        // ----------------------------------------
    {
        qint32 n;
        quint8 count;
        dataStreamIn >> n >> count;
        dataStreamOut << static_cast<quint8>(eNetMon_retEvents);

        qint32 k = n + count;
        for (qint32 i=n; i<k; i++)
            if (mSimpleFileDB->count()>i)
                dataStreamOut << i << mSimpleFileDB->value(i);

        break;
    }
    case eNetMon_getShState:
        // ----------------------------------------
        // Ответ на запрос состояния всех ШС
        // ----------------------------------------
        dataStreamOut << static_cast<quint8>(eNetMon_retShState) << mStateC2000PP->getShState();
        break;
    case eNetMon_getRlState:
        // ----------------------------------------
        // Ответ на запрос состояния всех реле
        // ----------------------------------------
        dataStreamOut << static_cast<quint8>(eNetMon_retRlState) << mStateC2000PP->getRlState();
        break;
    case eNetMon_controlSh:
    {
        // ----------------------------------------
        // Взятие/снятие ШС
        // ----------------------------------------
        controlPP control;
        QSet<quint16> setSh;
        dataStreamIn >> control >> setSh;

        // Дополнительная проверка. В дальнейшем разграничить права пользователям.
        if ( (control==SHPP_ARM) || (control==SHPP_DISARM) )
        {
            QList<quint16> listSh = setSh.values();
            std::sort(listSh.begin(), listSh.end());

            for(const quint16 sh : qAsConst(listSh))
                mC2000PP->controlSh(sh, control);
        }

        break;
    }
    case eNetMon_controlPart:
    {
        // ----------------------------------------
        // Взятие/снятие Раздела
        // ----------------------------------------
        controlPP control;
        QSet<quint8> setPart;
        dataStreamIn >> control >> setPart;

        if ( (control==SHPP_ARM) || (control==SHPP_DISARM) )// Дополнительная проверка. В дальнейшем разграничить права пользователям.
        {
            QSet<quint16> setSh;
            QList<quint8> listPart;
            auto configC2000PP = mC2000PP->getConfigC2000PP();

            for(const quint8 part : qAsConst(setPart)){
                QSet<quint16> shs = configC2000PP->getShInPart(part);

                bool controlTypePart = true;
                quint16 ver = mStateC2000PP->getVersion();

                if (ver==200){
                    if (part==1) controlTypePart = false;
                }
                else if (ver<200){
                    for(const quint16 sh : qAsConst(shs)){
                        // Если есть хотя бы 1 шлейф тип 6,то управлять
                        // пошлейфно, если нет, то управлять разделами.
                        if (configC2000PP->getShType(sh)==6) {
                            controlTypePart = false;
                            break;
                        }
                    }
                }

                if (controlTypePart){
                    if (!shs.isEmpty()) listPart.append(part);
                }
                else setSh += shs;
            }
#ifndef QT_NO_DEBUG_OUTPUT
            qDebug() << "+++controlTypePart" << listPart << setSh;
#endif

            if (!listPart.isEmpty())
            {
                std::sort(listPart.begin(), listPart.end());

                for(const quint8 part : qAsConst(listPart))
                    mC2000PP->controlPart(part, control);
            }

            if (!setSh.isEmpty())
            {
                QList<quint16> listSh = setSh.values();
                std::sort(listSh.begin(), listSh.end());

                for(const quint16 sh : qAsConst(listSh))
                    mC2000PP->controlSh(sh, control);
            }
        }

        break;
    }
    case eNetMon_controlRl:
    {
        // ----------------------------------------
        // Управление реле
        // ----------------------------------------

        bool control;
        QSet<quint8> setRl;
        dataStreamIn >> control >> setRl;

        QList<quint8> listRl = setRl.values();
        std::sort(listRl.begin(), listRl.end());

        for(const quint8 rl : qAsConst(listRl))
            mC2000PP->controlRl(rl, control);

        break;
    }
    }

    if (!dataOut.isEmpty()) {
        if (local)
            emit serverLinkLocal.sendData(socketDescriptor, dataOut);
        else
            emit serverLinkTcp.sendData(socketDescriptor, dataOut);
    }
}
//------------------------------------------------------------------------------
void HubMonitoring::setDataObject(C2000PP *pC2000PP, Modbus *pModbus, SimpleFileDB *pSimpleFileDB, StateC2000PP *pStateC2000PP)
{
    if (mC2000PP==nullptr)
    {
        mC2000PP = pC2000PP;
        mModbus  = pModbus;
        mSimpleFileDB = pSimpleFileDB;
        mStateC2000PP = pStateC2000PP;

        // ----------------------------------------
        // Получно событие от прибора С2000-ПП
        // ----------------------------------------
        QObject::connect(mC2000PP, &C2000PP::eventReceived, this,  [this](EventC2000PP event)
        {
            QByteArray dataEvent = event.data();
            qint32 n = mSimpleFileDB->append( dataEvent );

            //Разослать всем клиентам полученное событие
            QByteArray data;
            QDataStream ds(&data, QIODevice::WriteOnly);
            ds.setVersion(dataStreamVersion);
            ds << static_cast<quint8>(eNetMon_newEvent) << n << dataEvent;
            sendAllSockets(data);
        });

        // ----------------------------------------
        // Получна версия прибора С2000-ПП
        // Если ver==0, то произошел разрыв связи с С2000ПП
        // ----------------------------------------
        QObject::connect(mC2000PP, &C2000PP::versionChanged, this, [this](quint16 ver)
        {
            if (mStateC2000PP->setVersion(ver))
            {
                //Разослать всем клиентам версию прибора
                QByteArray data;
                QDataStream ds(&data, QIODevice::WriteOnly);
                ds.setVersion(dataStreamVersion);
                ds << static_cast<quint8>(eNetMon_retVersion) << ver
                   << mC2000PP->getPortState()
                   << mModbus->getLastErrorString();
                sendAllSockets(data);
            }
        });

        // ----------------------------------------
        // Получено состояние ШС от прибора С2000-ПП
        // ----------------------------------------
        QObject::connect(mC2000PP, &C2000PP::shStateChanged, this, [this](quint16 nSh, quint8 state)
        {
            auto configC2000PP = mC2000PP->getConfigC2000PP();
            if (mStateC2000PP->setShState(nSh, state, configC2000PP))
            {
                //Разослать всем клиентам новое состояние шлейфа
                QByteArray data;
                QDataStream ds(&data, QIODevice::WriteOnly);
                ds.setVersion(dataStreamVersion);
                ds << static_cast<quint8>(eNetMon_newShState) << nSh << state;
                sendAllSockets(data);
            }
        });

        // ----------------------------------------
        // Получено состояние Рл от прибора С2000-ПП
        // ----------------------------------------
        QObject::connect(mC2000PP, &C2000PP::rlStateChanged, this, [this](quint8 nRl, bool state)
        {
            if (mStateC2000PP->setRlState(nRl, state))
            {
                //Разослать всем клиентам новое состояние реле
                QByteArray data;
                QDataStream ds(&data, QIODevice::WriteOnly);
                ds.setVersion(dataStreamVersion);
                ds << static_cast<quint8>(eNetMon_newRlState) << nRl << state;
                sendAllSockets(data);
            }
        });

        // ----------------------------------------
        // Получено значение счетчика
        // ----------------------------------------
        QObject::connect(mC2000PP, &C2000PP::shOneCounter, this, [this](quint16 nSh, quint64 counter)
        {
            //Разослать всем клиентам новое состояние шлейфа
            QByteArray data;
            QDataStream ds(&data, QIODevice::WriteOnly);
            ds.setVersion(dataStreamVersion);
            ds << static_cast<quint8>(eNetMon_shOneCounter) << nSh << counter;
            sendAllSockets(data);
        });

        // ----------------------------------------
        // Получено АЦП (температура/влажность/CO)
        // ----------------------------------------
        QObject::connect(mC2000PP, &C2000PP::shOneADC, this, [this](quint16 nSh, qint16 adc)
        {
            //Разослать всем клиентам новое состояние шлейфа
            QByteArray data;
            QDataStream ds(&data, QIODevice::WriteOnly);
            ds.setVersion(dataStreamVersion);
            ds << static_cast<quint8>(eNetMon_shOneAdc) << nSh << adc;
            sendAllSockets(data);
        });

        // ----------------------------------------
        // Получено АЦП (температура/влажность/CO)
        // ----------------------------------------
        QObject::connect(mC2000PP, &C2000PP::shMultiADC, this, [this](QMap<quint16, qint16> adc)
        {
            //Разослать всем клиентам новое состояние шлейфа
            QByteArray data;
            QDataStream ds(&data, QIODevice::WriteOnly);
            ds.setVersion(dataStreamVersion);
            ds << static_cast<quint8>(eNetMon_shMultiAdc) << adc;
            sendAllSockets(data);
        });

        // ----------------------------------------
        // Статистика отправленных/принятых данных
        // ----------------------------------------
        QObject::connect(mC2000PP, &C2000PP::satisticsRxTxOneSec, this, [this](quint16 t, quint16 r)
        {
            //Разослать всем клиентам статистику отправленных/принятых данных за 1 секунду
            QByteArray data;
            QDataStream ds(&data, QIODevice::WriteOnly);
            ds.setVersion(dataStreamVersion);
            ds << static_cast<quint8>(eNetMon_statSendData)
               << static_cast<quint8>(eNetMonState_TxRx1s) << t << r;
            sendAllSockets(data);
        });

        QObject::connect(mC2000PP, &C2000PP::satisticsRxTxOneMin, this, [this](quint16 t, quint16 r)
        {
            //Разослать всем клиентам статистику отправленных/принятых данных за 1 минуту
            QByteArray data;
            QDataStream ds(&data, QIODevice::WriteOnly);
            ds.setVersion(dataStreamVersion);
            ds << static_cast<quint8>(eNetMon_statSendData)
               << static_cast<quint8>(eNetMonState_TxRx1m) << t << r;
            sendAllSockets(data);
        });

        QObject::connect(mC2000PP, &C2000PP::satisticsModbusState, this, [this](bool enable)
        {
            //Разослать всем клиентам включен/выключен modbus
            QByteArray data;
            QDataStream ds(&data, QIODevice::WriteOnly);
            ds.setVersion(dataStreamVersion);
            ds << static_cast<quint8>(eNetMon_statSendData)
               << static_cast<quint8>(eNetMonState_enable) << enable;
            sendAllSockets(data);
        });

        QObject::connect(mC2000PP, &C2000PP::satisticsModbusEroor, this, [this](const QString &err)
        {
            //Разослать всем клиентам ошибки modbus
            QByteArray data;
            QDataStream ds(&data, QIODevice::WriteOnly);
            ds.setVersion(dataStreamVersion);
            ds << static_cast<quint8>(eNetMon_statSendData)
               << static_cast<quint8>(eNetMonState_error) << err;
            sendAllSockets(data);
        });
    }
}
//------------------------------------------------------------------------------
void HubMonitoring::slotNewConfigC2000PP(void)
{
    //Разослать всем клиентам новую конфигурацию.
    QByteArray data;
    QDataStream ds(&data, QIODevice::WriteOnly);
    ds.setVersion(dataStreamVersion);
    ds << static_cast<quint8>(eNetMon_CfgPP_Ret) << *(mC2000PP->getConfigC2000PP());
    sendAllSockets(data);
}
//------------------------------------------------------------------------------
void HubMonitoring::slotNewDescriptions(void)
{
    //Разослать всем клиентам новые описания ШС/Рл/Разделов.
    QByteArray data;
    QDataStream ds(&data, QIODevice::WriteOnly);
    ds.setVersion(dataStreamVersion);
    auto configC2000PP = mC2000PP->getConfigC2000PP();
    ds << static_cast<quint8>(eNetMon_Descr_Ret)
       << configC2000PP->getDescriptionsSh()
       << configC2000PP->getDescriptionsRl()
       << configC2000PP->getDescriptionsPart();
    sendAllSockets(data);
}
