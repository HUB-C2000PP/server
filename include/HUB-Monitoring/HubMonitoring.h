//
// Copyright Kuzminov Sergey (kuzminov_sergey@mail.ru) 2021
//
// Distributed under the Unlicense License. (See accompanying
// file LICENSE_1_0.txt or copy at http://unlicense.org/)
//

#ifndef HUBMONITORING_H
#define HUBMONITORING_H

#include <QObject>
#include <QSerialPortInfo>

#include "LanDefMonitoring.h"
#include "include/Define.h"
#include "include/Modbus/Modbus.h"
#include "include/Net/CanalLink/ServerLink.h"
#include "include/DB/SimpleFileDB.h"
#include "include/C2000PP/StateC2000PP/StateC2000PP.h"

//-----------------------------------------------------------------------------
//
//-----------------------------------------------------------------------------
class HubMonitoring : public QObject
{
    Q_OBJECT
public:
    explicit HubMonitoring(QObject *parent = nullptr);

    void setDataObject(C2000PP *pC2000PP, Modbus *pModbus, SimpleFileDB *pSimpleFileDB, StateC2000PP *pStateC2000PP);

    ServerLink* objServerLinkLocal(void) {return &serverLinkLocal;}
    ServerLink* objServerLinkTcp(void)   {return &serverLinkTcp;}

private:
    // ----------------------------------------
    // Работа с С2000-ПП
    // ----------------------------------------
    C2000PP *mC2000PP = nullptr; // Логика работы с прибором С2000-ПП
    Modbus  *mModbus = nullptr;

    SimpleFileDB *mSimpleFileDB = nullptr;   // База данных событий
    StateC2000PP *mStateC2000PP = nullptr;   // Состояния шлейфов сигнализации

    // ----------------------------------------
    // Работа с сетью
    // ----------------------------------------
    ServerLink       serverLinkLocal;
    QSet <qintptr>   localSocketsDescriptor;

    ServerLink       serverLinkTcp;
    QSet <qintptr>   tcpSocketsDescriptor;

    void newConnection(bool local, qintptr socketDescriptor);
    void delConnection(bool local, qintptr socketDescriptor);
    void readyRead    (bool local, qintptr socketDescriptor, QByteArray data);
    void sendAllSockets(QByteArray data);

private slots:
    void newLocalConnection(qintptr socketDescriptor)                         { newConnection(true, socketDescriptor); }
    void delLocalConnection(qintptr socketDescriptor)                         { delConnection(true, socketDescriptor); }
    void readyReadLocal    (qintptr socketDescriptor, const QByteArray &data) { readyRead    (true, socketDescriptor, data); }

    void newTcpConnection  (qintptr socketDescriptor)                         { newConnection(false, socketDescriptor); }
    void delTcpConnection  (qintptr socketDescriptor)                         { delConnection(false, socketDescriptor); }
    void readyReadTcp      (qintptr socketDescriptor, const QByteArray &data) { readyRead    (false, socketDescriptor, data); }

public slots:
    void slotNewConfigC2000PP(void);
    void slotNewDescriptions(void);
};

#endif // HUBMONITORING_H
