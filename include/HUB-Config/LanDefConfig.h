//
// Copyright Kuzminov Sergey (kuzminov_sergey@mail.ru) 2021
//
// Distributed under the Unlicense License. (See accompanying
// file LICENSE_1_0.txt or copy at http://unlicense.org/)
//

#ifndef LANDEFCONFIG_H
#define LANDEFCONFIG_H

enum enumNetCfg
{
    eNetCfg_empty           = 0,
    eNetCfg_quit            = 1,
    eNetCfg_setPWD          = 2,

    eNetCfg_getSerial       = 3,
    eNetCfg_retSerial       = 4,
    eNetCfg_setSerial       = 5,

    eNetCfg_CfgPP_Upload    = 6,
    eNetCfg_Descript_Upload = 7,

    eNetCfg_CfgPP_read      = 8,
    eNetCfg_CfgPP_percent   = 9,

    eNetCfg_SysConfig       = 10,

    eNetCfg_Mail            = 11,
    eNetCfg_GSM             = 12,
    eNetCfg_TLG             = 13
};

enum enumSysconfig
{
    eSysCfg_getVer          = 1,
    eSysCfg_retVer          = 2,

    eSysCfg_reboot          = 3,
    eSysCfg_setRoot         = 4,

    eSysCfg_getConfigEth    = 5,
    eSysCfg_retConfigEth    = 6,
    eSysCfg_setConfigEth    = 7,

    eSysCfg_getTzRegion     = 8,
    eSysCfg_retTzRegion     = 9,

    eSysCfg_getTzZone       = 10,
    eSysCfg_retTzZone       = 11,

    eSysCfg_getTime         = 12,
    eSysCfg_retTime         = 13,

    eSysCfg_setConfigTime   = 14
};

enum enumNetCfgMail
{
    eNetMail_setConfig  = 1,
    eNetMail_getConfig  = 2,
    eNetMail_retConfig  = 3,
    eNetMail_test       = 4,

    eNetMail_logClear   = 5,
    eNetMail_logGetCount= 6,
    eNetMail_logRetCount= 7,
    eNetMail_logGetMsg  = 8,
    eNetMail_logRetMsg  = 9,
    eNetMail_logNewMsg  = 10
};

enum enumNetCfgGSM
{
    eNetGSM_setConfig   = 1,
    eNetGSM_getConfig   = 2,
    eNetGSM_retConfig   = 3,
    eNetGSM_getPort     = 4,
    eNetGSM_changePort  = 5,
    eNetGSM_portChanged = 6,
    eNetGSM_sendSMS     = 7,
    eNetGSM_SMSsent     = 8,
    eNetGSM_receivedSMS = 9,
    eNetGSM_sendUSSD    = 10,
    eNetGSM_receivedUSSD= 11,
    eNetGSM_subscribe   = 12,
    eNetGSM_infoLevel   = 13,
    eNetGSM_infoModel   = 14,
    eNetGSM_infoOperator= 15,
    eNetGSM_infoLastErr = 16,

    eNetGSM_logClear    = 17,
    eNetGSM_logGetCount = 18,
    eNetGSM_logRetCount = 19,
    eNetGSM_logGetMsg   = 20,
    eNetGSM_logRetMsg   = 21,
    eNetGSM_logNewMsg   = 22
};

enum enumNetCfgTlg
{
    eNetTLG_setConfig   = 1,
    eNetTLG_getConfig   = 2,
    eNetTLG_retConfig   = 3,
    eNetTLG_connect     = 4,
    eNetTLG_error       = 5,
    eNetTLG_UpdateNick  = 6,
    eNetTLG_AuthState   = 7,
    eNetTLG_EnterCode   = 8,
    eNetTLG_LogOut      = 9,
    eNetTLG_sendMsg     = 10,

    eNetTLG_logClear    = 11,
    eNetTLG_logGetCount = 12,
    eNetTLG_logRetCount = 13,
    eNetTLG_logGetMsg   = 14,
    eNetTLG_logRetMsg   = 15,
    eNetTLG_logNewMsg   = 16
};


#endif // LANDEFCONFIG_H
