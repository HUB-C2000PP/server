//
// Copyright Kuzminov Sergey (kuzminov_sergey@mail.ru) 2021
//
// Distributed under the Unlicense License. (See accompanying
// file LICENSE_1_0.txt or copy at http://unlicense.org/)
//

#ifndef HUBCONFIG_H
#define HUBCONFIG_H

#include <QObject>
#include <QSysInfo>
#include <QSerialPortInfo>

#include <stdio.h>
#include <stdlib.h>

#include "LanDefConfig.h"
#include "include/Define.h"
#include "include/Modbus/Modbus.h"
#include "include/Net/CanalLink/ServerLink.h"
#include "include/DB/SimpleFileBA.h"
#include "include/DB/SimpleFileDB.h"
#include "include/C2000PP/StateC2000PP/StateC2000PP.h"
#include "include/SMTP/smtp.h"
#include "include/GsmSMS/GsmSMS.h"
#include "include/Telegram/TlgProcess.h"
#include "include/Broadcast/broadcastlistener.h"

#ifdef Embedded
#include "include/sysconfig/sysconfig.h"
#include "include/sysconfig/tzdata.h"
#endif

//-----------------------------------------------------------------------------
//
//-----------------------------------------------------------------------------
class HubConfig : public QObject
{
    Q_OBJECT
public:
    explicit HubConfig(QObject *parent = nullptr);
    ~HubConfig();
    void loadNetConfig(void);

    void setDataObject(C2000PP *pC2000PP, Modbus *pModbus, SimpleFileDB *pSimpleFileDB, StateC2000PP *pStateC2000PP);

    ServerLink* objServerLinkLocal(void) {return &serverLinkLocal;}
    ServerLink* objServerLinkTcp(void)   {return &serverLinkTcp;}

private:
    // ----------------------------------------
    // Работа с С2000-ПП
    // ----------------------------------------
    C2000PP *mC2000PP = nullptr;
    Modbus *mModbus = nullptr;

    SimpleFileDB *mSimpleFileDB = nullptr;   // База данных событий
    StateC2000PP *mStateC2000PP = nullptr;   // Состояния шлейфов сигнализации

    // ----------------------------------------
    // Работа с сетью
    // ----------------------------------------
    QSettings *settings=nullptr;
    static const QString keySetConnectionPort;
    static const QString keySetConnectionPWD1;
    static const QString keySetConnectionPWD2;

    const QString defLogin= QStringLiteral("C2000PP");
    const QString defPWD1 = QStringLiteral("юR>ыjЬЛdЭzя~ЯДФY[Й=W");
    const QString defPWD2 = QStringLiteral(".ыфyЪVR+7Ьd]Л@ЮЫWи#Y");

    ServerLink       serverLinkLocal;
    QSet <qintptr>   localSocketsDescriptor;

    ServerLink       serverLinkTcp;
    QSet <qintptr>   tcpSocketsDescriptor;

    void newConnection(bool local, qintptr socketDescriptor);
    void delConnection(bool local, qintptr socketDescriptor);
    void readyRead    (bool local, qintptr socketDescriptor, QByteArray dataIn);
    void sendAllSockets(QByteArray data);

    // ----------------------------------------
    // Работа с Mail
    // ----------------------------------------
    Smtp smtp;
    LogFile logSMTP;

    QMap<QString, QString>       mapMailName;
    QMap<QString, bool>          mapMailEnable;
    QMap<QString, QSet<quint8>>  mapMailFilterA;
    QMap<QString, QSet<quint16>> mapMailFilterZ;

    bool mailEnable = false;
    QString mailSubject = QStringLiteral(u"Название объекта");
    QString mailFrom = QStringLiteral("from@mail.ru");
    QString mailPWD;
    QString mailServer = QStringLiteral("smtp.mail.ru");
    quint16 mailPort=465;
    quint8  mailDelay=5;

    // ----------------------------------------
    // Работа с GSM
    // ----------------------------------------
    GsmSMS gsmSMS;
    LogFile logGSM;

    QMap<QString, QString>       mapGSMName;
    QMap<QString, bool>          mapGSMEnable;
    QMap<QString, QSet<quint8>>  mapGSMFilterA;
    QMap<QString, QSet<quint16>> mapGSMFilterZ;

    bool gsmEnable = false;
    QString serialPortGSM;

    QSet <qintptr> gsmSubscribeLocal;
    QSet <qintptr> gsmSubscribeTcp;

    quint8 gsmInfoLevel = 0;
    QByteArray gsmInfoManufacturer;
    QByteArray gsmInfoModel;
    QByteArray gsmInfoImei;
    quint8 gsmInfoState=0;
    QString gsmInfoName;
    QString gsmInfoerr;

    void sendGsmInfo(enumNetCfgGSM cmd, qintptr socketDescriptor=0, bool local=false);

    // ----------------------------------------
    // Работа с Telegram
    // ----------------------------------------
    TlgProcess telegram;
    void telegramStart(bool start);
    LogFile logTLG_out;
    LogFile logTLG_in;

    QList <qint64> listTlgSelectedID;
    QMap<qint64, QSet<quint8>>  mapTlgFilterE;
    QMap<qint64, QSet<quint8>>  mapTlgFilterG;
    QMap<qint64, QSet<quint16>> mapTlgFilterZ;

    bool tlgEnable = false;
    bool tlgConnect = false;
    bool tlgProxy  = false;
    QByteArray tlgPhone;
    QString tlgObjectName;
    QString tlgProxyName;
    quint16 tlgProxyPort=0;
    QString tlgProxyLogin;
    QString tlgProxyPwd;

    TelegramState tlgState=eEmpty;
    QList<qint64> tlgListID;
    QList<QString> tlgListName;

    QMap<qint64, quint8>       startIdCount;
    QMap<qint64, QList<QDate>> startIdDate;
    QMap<qint64, QStringList>  startIdList1;
    QMap<qint64, QStringList>  startIdList2;
    void telegramSendEvents(qint64 chatID);

    // ----------------------------------------
    // Для поиск широковещательными запросами
    // ----------------------------------------
    BroadcastListener broadcastListener;
    QMap<QByteArray, QVariant> extData;

    // ----------------------------------------
    // Вспомогательные функции
    // ----------------------------------------
    QMap <QString, QString> getMapSerialPortNames();
    QString eventToString(const EventC2000PP &event, bool bMarker=true, bool bDate=true, bool bTime=true, bool bAgregat=true);

private slots:    
    void newLocalConnection(qintptr socketDescriptor)                         { newConnection(true, socketDescriptor); }
    void delLocalConnection(qintptr socketDescriptor)                         { delConnection(true, socketDescriptor); }
    void readyReadLocal    (qintptr socketDescriptor, const QByteArray &data) { readyRead    (true, socketDescriptor, data); }

    void newTcpConnection  (qintptr socketDescriptor)                         { newConnection(false, socketDescriptor); }
    void delTcpConnection  (qintptr socketDescriptor)                         { delConnection(false, socketDescriptor); }
    void readyReadTcp      (qintptr socketDescriptor, const QByteArray &data) { readyRead    (false, socketDescriptor, data); }

    //----------------------------------------
    // GSM
    //----------------------------------------
    void slotGSMLevel(quint8 level);
    void slotSMSsent(QByteArray phone, bool ok);
    void slotSMSreceived(const QByteArray &phone, QDateTime dateTime, const QString &msg);
    void slotUSSDreceived(const QString &msg);
    void slotGSMModel(const QByteArray &manufacturer, const QByteArray &model, const QByteArray &imei);
    void slotGSMOperator(quint8 state, const QString &name);
    void slotGSMLastError(const QString &err);

    //----------------------------------------
    // Telegram
    //----------------------------------------    
    void slotTlgUpdateChatTitle(qint64 id, QString chatTitle);
    void slotTlgSentMessage(qint64 chatID, bool ok);
    void slotTlgGotMessage(qint64 chatID, QString msg);
    void slotTlgAuthState(TelegramState state);
    void slotTlgError(QString errorString);

    //----------------------------------------

signals:
    void setAddress(const QHostAddress host, quint16 port);
    void addLogin(QString login, QString pwd1, QString pwd2);
    void signalNewConfigC2000PP(void);
    void signalNewDescriptions(void);
};

#endif // HUB_H
