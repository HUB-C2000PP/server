//
// Copyright Kuzminov Sergey (kuzminov_sergey@mail.ru) 2021
//
// Distributed under the Unlicense License. (See accompanying
// file LICENSE_1_0.txt or copy at http://unlicense.org/)
//

#include "HubConfig.h"

const QString HubConfig::keySetConnectionPort = QStringLiteral("Connection/Port");
const QString HubConfig::keySetConnectionPWD1 = QStringLiteral("Connection/PWD1");
const QString HubConfig::keySetConnectionPWD2 = QStringLiteral("Connection/PWD2");

//------------------------------------------------------------------------------
HubConfig::HubConfig(QObject *parent) : QObject(parent)
{
    // ----------------------------------------
    // Mail
    // ----------------------------------------
    logSMTP.setFileName(QStringLiteral("smtp.log"), true);
    QObject::connect(&smtp, &Smtp::status, this, [this](QString status)
    {
        int n = logSMTP.countMsg();
        logSMTP.writeLog(status);

        //Разослать всем клиентам
        QByteArray data;
        QDataStream ds(&data, QIODevice::WriteOnly);
        ds.setVersion(dataStreamVersion);
        ds << static_cast<quint8>(eNetCfg_Mail) << static_cast<quint8>(eNetMail_logNewMsg)
           << n << logSMTP.msg(n);
        sendAllSockets(data);
    });

    SimpleFileBA fileBA;
    fileBA.setFileName(mainPath + constFileMail);
    QByteArray data = fileBA.read();

    if (!data.isEmpty()){
        QDataStream ds(&data, QIODevice::ReadOnly);
        ds.setVersion(dataStreamVersion);
        ds >> mapMailName >> mapMailEnable >> mapMailFilterA >> mapMailFilterZ
                >> mailEnable >> mailSubject >> mailFrom >> mailPWD >> mailServer >> mailPort >> mailDelay;

        if (mailEnable){
            smtp.setAddress(QString(), mailFrom, mailPWD, mailSubject, mailServer, mailPort);
            smtp.setDelay(mailDelay);
        }
    }

    // ----------------------------------------
    // GSM
    // ----------------------------------------
    logGSM.setFileName(QStringLiteral("gsm.log"), true);

    fileBA.setFileName(mainPath + constFileGSM);
    data = fileBA.read();

    if (!data.isEmpty()){
        QDataStream ds(&data, QIODevice::ReadOnly);
        ds.setVersion(dataStreamVersion);
        ds >> mapGSMName >> mapGSMEnable >> mapGSMFilterA >> mapGSMFilterZ >> gsmEnable >> serialPortGSM;
    }

    QObject::connect(&gsmSMS, &GsmSMS::signalLevel,        this, &HubConfig::slotGSMLevel);
    QObject::connect(&gsmSMS, &GsmSMS::signalSMSsent,      this, &HubConfig::slotSMSsent);
    QObject::connect(&gsmSMS, &GsmSMS::signalSMSreceived,  this, &HubConfig::slotSMSreceived);
    QObject::connect(&gsmSMS, &GsmSMS::signalUSSDreceived, this, &HubConfig::slotUSSDreceived);
    QObject::connect(&gsmSMS, &GsmSMS::signalModel,        this, &HubConfig::slotGSMModel);
    QObject::connect(&gsmSMS, &GsmSMS::signalOperator,     this, &HubConfig::slotGSMOperator);
    QObject::connect(&gsmSMS, &GsmSMS::signalError,        this, &HubConfig::slotGSMLastError);

    if (gsmEnable){
        if (!serialPortGSM.isEmpty())
        {
            gsmSMS.changePort(serialPortGSM);
            gsmSMS.start();
        }
    }

    // ----------------------------------------
    // Telegram
    // ----------------------------------------
    // Идентификатор приложения для доступа к Telegram API, который можно получить по адресу https://my.telegram.org
    telegram.setApiID(962234, QStringLiteral("787e78c73a860b6863ff719c27ba14bb"));

    logTLG_in.setFileName(QStringLiteral("telegram_in.log"));
    logTLG_out.setFileName(QStringLiteral("telegram_out.log"), true);

    fileBA.setFileName(mainPath + constFileTlg);
    data = fileBA.read();

    if (!data.isEmpty()){
        QDataStream ds(&data, QIODevice::ReadOnly);
        ds.setVersion(dataStreamVersion);
        ds >> listTlgSelectedID >> mapTlgFilterG >> mapTlgFilterE >> mapTlgFilterZ >> tlgEnable
                >> tlgConnect >> tlgPhone >> tlgObjectName >> tlgProxy >> tlgProxyName >> tlgProxyPort >> tlgProxyLogin >> tlgProxyPwd;
    }

    if (tlgObjectName.isEmpty()) tlgObjectName = QStringLiteral(u"Название объекта.");

    connect(&telegram, &TlgProcess::updateChatTitle, this, &HubConfig::slotTlgUpdateChatTitle);
    connect(&telegram, &TlgProcess::sentMessage,     this, &HubConfig::slotTlgSentMessage);
    connect(&telegram, &TlgProcess::gotMessage,      this, &HubConfig::slotTlgGotMessage);
    connect(&telegram, &TlgProcess::authState,       this, &HubConfig::slotTlgAuthState);
    connect(&telegram, &TlgProcess::signalError,     this, &HubConfig::slotTlgError);

    if (tlgEnable) {
        telegram.setPhoneNumber(tlgPhone);
        telegram.setObjectName(tlgObjectName);

        if (tlgProxy) telegram.setProxy(tlgProxyName, tlgProxyPort, tlgProxyLogin, tlgProxyPwd);
        else telegram.setProxy(QString(), 0, QString(), QString());

        if (tlgConnect) telegram.start();
    }

    // ----------------------------------------
    // Обмен данными через локалсокет
    // ----------------------------------------
    connect(&serverLinkLocal, &ServerLink::newConnection, this, &HubConfig::newLocalConnection);
    connect(&serverLinkLocal, &ServerLink::delConnection, this, &HubConfig::delLocalConnection);
    connect(&serverLinkLocal, &ServerLink::readyRead,     this, &HubConfig::readyReadLocal);

    // ----------------------------------------
    // Обмен данными через TCP
    // ----------------------------------------
    connect(&serverLinkTcp, &ServerLink::newConnection, this, &HubConfig::newTcpConnection);
    connect(&serverLinkTcp, &ServerLink::delConnection, this, &HubConfig::delTcpConnection);
    connect(&serverLinkTcp, &ServerLink::readyRead,     this, &HubConfig::readyReadTcp);

    //----------------------------------------
    // Для поиск широковещательными запросами
    //----------------------------------------
    extData[QByteArray("Ver")] = int(applicationVersion);
    broadcastListener.setMagicByte(0xA5C3F09618814202);
    broadcastListener.setAnswerPort(45455);
    broadcastListener.setListenPort(45454);
    broadcastListener.startListen();
}
//------------------------------------------------------------------------------
HubConfig::~HubConfig()
{
}
//------------------------------------------------------------------------------
QString HubConfig::eventToString(const EventC2000PP &event, bool bMarker, bool bDate, bool bTime, bool bAgregat)
{
    ConfigC2000PP *configC2000PP = mC2000PP->getConfigC2000PP();
    quint8 e = event.getEvent();
    QString ret;

    if (bMarker) ret = EventsStatic::getEventMarker(e);

    //----------------------------------------
    if (bDate || bTime)
    {
        if (event.isBadTime() || event.isBadDate()) ret+=QStringLiteral("*");

        QDateTime date = event.getDatePP();
        if (bDate) ret += QStringLiteral("[%1]").arg( date.date().toString( EventsStatic::dateFormat ) );
        if (bTime) ret += QStringLiteral("[%1]").arg( date.time().toString() );
    }

    ret += QStringLiteral(" ") + EventsStatic::getStrEvent(e);

    //----------------------------------------
    QString str = EventsStatic::getEventDescription(event, configC2000PP);
    if (!str.isEmpty()) ret += QStringLiteral(u", ") + str;
    else bAgregat=true;

    //----------------------------------------
    str = EventsStatic::getStrPart(event, configC2000PP);
    if (!str.isEmpty()) ret += QStringLiteral(u", Раздел ") + str;

    //----------------------------------------
    if (bAgregat){
        str = EventsStatic::getStrAgregat(event);
        if (!str.isEmpty()) ret += QStringLiteral(u", ") + str;
    }

    return ret;
}
//------------------------------------------------------------------------------
void HubConfig::setDataObject(C2000PP *pC2000PP, Modbus *pModbus, SimpleFileDB *pSimpleFileDB, StateC2000PP *pStateC2000PP)
{
    if (mC2000PP==nullptr)
    {
        mC2000PP = pC2000PP;
        mModbus  = pModbus;
        mSimpleFileDB = pSimpleFileDB;
        mStateC2000PP = pStateC2000PP;

        // ----------------------------------------
        // Процент чтения конфигурации С2000-ПП
        // ----------------------------------------
        QObject::connect(mC2000PP, &C2000PP::configReading, this,  [this](qint8 percent)
        {
            //Разослать всем клиентам процент чтения конфигурации
            QByteArray data;
            QDataStream ds(&data, QIODevice::WriteOnly);
            ds.setVersion(dataStreamVersion);
            ds << static_cast<quint8>(eNetCfg_CfgPP_percent) << percent;
            sendAllSockets(data);
        });

        // ----------------------------------------
        // Получно событие от прибора С2000-ПП
        // ----------------------------------------
        connect(mC2000PP, &C2000PP::eventReceived, this,  [this](EventC2000PP event)
        {
            quint8 e = event.getEvent();
            bool isAlarm = EventsStatic::eventsAlarm.contains(e);
            if (isAlarm) // Если тревожное событие
            {
                // ----------------------------------------
                // Mail
                // ----------------------------------------
                if (mailEnable)
                {
                    QString strEvent;
                    auto itEnd = mapMailEnable.constEnd();
                    for(auto it = mapMailEnable.constBegin(); it!=itEnd; it++){
                        if (it.value()) // Если почта включена для адресата
                        {
                            const QString &mail = it.key();
                            bool bSend = true;

                            if (mapMailFilterA.contains( mail )){
                                auto filterA = mapMailFilterA.value( mail );
                                if (filterA.contains(e)) bSend = false;
                            }

                            if (bSend) {
                                if (mapMailFilterZ.contains( mail )){
                                    auto filterZ = mapMailFilterZ.value( mail );
                                    if (filterZ.contains( event.getSh() )) bSend = false;
                                }

                                if (bSend)
                                {
                                    if (strEvent.isEmpty()) strEvent = eventToString(event);

                                    if (smtp.getDelay()==0)
                                        smtp.sendTo(strEvent, mail);
                                    else
                                        smtp.sendDelayTo(strEvent, mail);
                                }
                            }
                        }
                    }
                }

                // ----------------------------------------
                // GSM
                // ----------------------------------------
                if (gsmEnable)
                {
                    QString strEvent;
                    auto itEnd = mapGSMEnable.constEnd();
                    for(auto it = mapGSMEnable.constBegin(); it!=itEnd; it++){
                        if (it.value()) // Если включена отправка sms для адресата
                        {
                            const QString &phone = it.key();
                            bool bSend = true;

                            if (mapGSMFilterA.contains( phone )){
                                auto filterA = mapGSMFilterA.value( phone );
                                if (filterA.contains(e)) bSend = false;
                            }

                            if (bSend) {
                                if (mapGSMFilterZ.contains( phone )){
                                    auto filterZ = mapGSMFilterZ.value( phone );
                                    if (filterZ.contains( event.getSh() )) bSend = false;
                                }

                                if (bSend)
                                {
                                    if (strEvent.isEmpty()) strEvent = eventToString(event, false, false, false, false);
                                    gsmSMS.sendMsg(phone.toLatin1(), strEvent);
                                }
                            }
                        }
                    }
                }
            }

            // ----------------------------------------
            // Telegram
            // ----------------------------------------
            if (tlgEnable)
            {
                QString strEvent;
                for(const qint64 chatID : qAsConst(listTlgSelectedID))
                {
                    bool bIsStart = startIdCount.contains(chatID);

                    if (!bIsStart || EventsStatic::eventsAlarm.contains(event.getEvent()))
                    {
                        /*
                        if (bIsStart){
                            startIdCount.remove(chatID);
                            startIdDate. remove(chatID);
                            startIdList1.remove(chatID);
                            startIdList2.remove(chatID);
                        }
                        */

                        bool bSend = true;

                        if (mapTlgFilterG.contains( chatID )){
                            auto filterG = mapTlgFilterG.value( chatID );
                            if (!filterG.contains( EventsStatic::getEventGroup(e) )) bSend = false;
                        }
                        else if (!isAlarm) bSend = false;

                        if (bSend)
                        {
                            if (mapTlgFilterE.contains( chatID )){
                                auto filterE = mapTlgFilterE.value( chatID );
                                if (filterE.contains(e)) bSend = false;
                            }

                            if (bSend)
                            {
                                if (mapTlgFilterZ.contains( chatID )){
                                    auto filterZ = mapTlgFilterZ.value( chatID );
                                    if (filterZ.contains( event.getSh() )) bSend = false;
                                }

                                if (bSend){
                                    if (strEvent.isEmpty()) strEvent = eventToString(event, true, false, false, true);
                                    telegram.telegramSendMessage(chatID, strEvent);
                                }
                            }
                        }
                    }

                    if (bIsStart)
                    {
                        bool bSend = true;
                        if (event.isSh()){
                            if (mapTlgFilterZ.value(chatID).contains( event.getSh() ))
                                bSend=false;
                        }

                        if (bSend)
                        {
                            QList<QDate> listDate = startIdDate.value(chatID);
                            QStringList  strList1 = startIdList1.value(chatID);
                            QStringList  strList2 = startIdList2.value(chatID);

                            listDate.append( event.getDatePP().date() );
                            strList1.append( eventToString(event, true, true, true, true) );
                            strList2.append( eventToString(event, true, false, true, true) );

                            if (listDate.count() > startIdCount.value(chatID)){
                                listDate.removeFirst();
                                strList1.removeFirst();
                                strList2.removeFirst();
                            }

                            startIdDate [chatID] = listDate;
                            startIdList1[chatID] = strList1;
                            startIdList2[chatID] = strList2;

                            telegramSendEvents(chatID);
                        }
                    }
                }
            }
        });

        //*** Добавить уведомления о разрыве связи?
        /*
            // ----------------------------------------
            // Получна версия прибора С2000-ПП
            // Если ver==0, то произошел разрыв связи с С2000ПП
            // ----------------------------------------
            connect(mC2000PP, &C2000PP::versionChanged, this, [this](quint16 ver)
                    {

                    });
*/
    }
}
//------------------------------------------------------------------------------
// Закрузка нонфигурации сервера TCP
//------------------------------------------------------------------------------
void HubConfig::loadNetConfig(void)
{
    settings = new QSettings(mainPath+constFileNameServer, QSettings::IniFormat, this);

    QString pwd1;
    QString pwd2;
    QByteArray pwd = QByteArray::fromBase64( settings->value(keySetConnectionPWD1).toByteArray() );
    if (!pwd.isEmpty()){
        pwd1 = QString::fromUtf8( qUncompress(pwd) );

        pwd = QByteArray::fromBase64( settings->value(keySetConnectionPWD2).toByteArray() );
        if (!pwd.isEmpty())
            pwd2 = QString::fromUtf8( qUncompress(pwd) );
        else {
            pwd1 = defPWD1;
            pwd2 = defPWD2;
        }
    }
    else {
        pwd1 = defPWD1;
        pwd2 = defPWD2;
    }

    emit addLogin(defLogin, pwd1, pwd2);
    quint16 port = static_cast<quint16>(settings->value(keySetConnectionPort, 55321).toUInt());
    emit setAddress(QHostAddress::Any, port);

    extData[QByteArray("Port")] = int(port);
    broadcastListener.setExtData(extData);
}
//------------------------------------------------------------------------------
// Новое входящее подключение от локального сокета или от TCP-сокета
//------------------------------------------------------------------------------
void HubConfig::newConnection(bool local, qintptr socketDescriptor)
{
    if (local) {
        localSocketsDescriptor.insert(socketDescriptor);
    }
    else {
        tcpSocketsDescriptor.insert(socketDescriptor);
    }
}
//------------------------------------------------------------------------------
// Соединение разорвано с локальным сокетом или с TCP-сокетом
//------------------------------------------------------------------------------
void HubConfig::delConnection(bool local, qintptr socketDescriptor)
{
    if (local) {
        localSocketsDescriptor.remove(socketDescriptor);
    }
    else {
        tcpSocketsDescriptor.remove(socketDescriptor);
    }
}
//------------------------------------------------------------------------------
// Отправить данные всем подключенным клиентам
//------------------------------------------------------------------------------
void HubConfig::sendAllSockets(QByteArray data)
{
    for(const auto socketDescriptor : qAsConst(localSocketsDescriptor))
        emit serverLinkLocal.sendData(socketDescriptor, data);

    for(const auto socketDescriptor : qAsConst(tcpSocketsDescriptor))
        emit serverLinkTcp.sendData(socketDescriptor, data);
}
//------------------------------------------------------------------------------
// Получены данные от локального сокета или от TCP-сокета
//------------------------------------------------------------------------------
void HubConfig::readyRead(bool local, qintptr socketDescriptor, QByteArray dataIn)
{
    QDataStream dataStreamIn(&dataIn, QIODevice::ReadOnly);
    dataStreamIn.setVersion(dataStreamVersion);
    quint8 command;
    dataStreamIn >> command;

    QByteArray dataOut;
    QDataStream dataStreamOut(&dataOut, QIODevice::WriteOnly);
    dataStreamOut.setVersion(dataStreamVersion);

    switch (command){
    case eNetCfg_quit:
        // ----------------------------------------
        // Завершить работу приложения но только
        // если данные получены по локальному сокету.
        // ----------------------------------------
        if (local) {
            quint8 command2;
            dataStreamIn >> command2;
            if (command2==eNetCfg_empty){
                dataStreamIn >> command2;
                if (command2==eNetCfg_quit) QCoreApplication::quit();
            }
        }

        break;
    case eNetCfg_setPWD:
    {
        // ----------------------------------------
        // Задать пароль для сетевого подключения
        // ----------------------------------------
        QString pwd1;
        QString pwd2;
        quint16 port;
        dataStreamIn >> pwd1 >> pwd2 >> port;

        if (settings!=nullptr)
        {
            QByteArray pwd = pwd1.toUtf8();
            if (!pwd.isEmpty()) pwd = qCompress(pwd).toBase64();
            settings->setValue(keySetConnectionPWD1, pwd);

            pwd = pwd2.toUtf8();
            if (!pwd.isEmpty()) pwd = qCompress(pwd).toBase64();
            settings->setValue(keySetConnectionPWD2, pwd);

            settings->setValue(keySetConnectionPort, port);
        }

        if (pwd1.isEmpty() || pwd2.isEmpty()) {
            pwd1 = defPWD1;
            pwd2 = defPWD2;
        }

        emit addLogin(defLogin, pwd1, pwd2);
        emit setAddress(QHostAddress::Any, port);

        extData[QByteArray("Port")] = int(port);
        broadcastListener.setExtData(extData);
        break;
    }

    case eNetCfg_getSerial:
    {
        // ----------------------------------------
        // Параметры последовательного порта
        // ----------------------------------------
        dataStreamOut << static_cast<quint8>(eNetCfg_retSerial)
                      << mModbus->isEnable()
                      << mModbus->getSerialPortType()

                      << mModbus->getSerialPortName()
                      << mModbus->getSerialPortBaudRate()
                      << mModbus->getSerialPortParity()
                      << mModbus->getSerialPortStopBits()

                      << getMapSerialPortNames()

                      << mModbus->getSerialPortHost()
                      << mModbus->getSerialPortIPort()

                      << mC2000PP->getAddressPP();
        break;
    }
    case eNetCfg_setSerial:
        // ----------------------------------------
        // Открыть/закрыть порт
        // ----------------------------------------
        bool portCmd;
        dataStreamIn >> portCmd;

        if (portCmd)
        {
            quint8 portType;
            dataStreamIn >> portType;

            // ----------------------------------------
            // Тип порта - последовательный порт
            // ----------------------------------------
            if(portType==Modbus::portType_Serial)
            {
                QString portName;
                quint32 portBaudRate= 0;
                quint8  portParity  = 0;
                quint8  portStopBits= 0;
                quint8  addressPP   = 0;
                dataStreamIn >> portName >> portBaudRate >> portParity >> portStopBits >> addressPP;

                mC2000PP->setAddressPP(addressPP);// Адрес прибора на шине Modbus

                mModbus->setSerial(portName, portBaudRate, portParity, portStopBits);
                mModbus->setEnable();
            }
            // ----------------------------------------
            // Тип порта - сетевой последовательный порт
            // ----------------------------------------
            else
            {
                QString portHost;
                quint16 portPort = 0;
                quint8  addressPP= 0;
                dataStreamIn >> portHost >> portPort >> addressPP;

                mC2000PP->setAddressPP(addressPP);// Адрес прибора на шине Modbus

                if (portType==Modbus::portType_C2000E) {
                    mModbus->setC2000E(portHost, portPort);
                    mModbus->setEnable();
                }
                else if (portType==Modbus::portType_TCPRTU){
                    mModbus->setTCP(portHost, portPort);
                    mModbus->setEnable();
                }
            }
        }
        else
        {
            mModbus->setEnable(false);
        }

        break;
    case eNetCfg_CfgPP_Upload:
    {
        // ----------------------------------------
        // Загрузать конфигурацию С2000-ПП на сервер
        // ----------------------------------------
        ConfigC2000PP configC2000PP;
        dataStreamIn >> configC2000PP;

        bool ok=false;
        bool isEnable = false;
        if (configC2000PP.isCorrectConfig()){
            if ( configC2000PP.saveFile(mainPath+constFileNameConfigPP) ) {
                ok = true;

                isEnable = mModbus->isEnable();
                if (isEnable) mModbus->setEnable(false);

                mC2000PP->loadConfig(configC2000PP);
            }
        }

        if (ok){
            dataStreamOut << static_cast<quint8>(eNetCfg_CfgPP_Upload) << true;
            if (local) emit serverLinkLocal.sendData(socketDescriptor, dataOut);
            else       emit serverLinkTcp.sendData(socketDescriptor, dataOut);
            dataOut.clear();

            emit signalNewConfigC2000PP(); //Разослать всем клиентам новую конфигурацию.

            if (isEnable) mModbus->setEnable();
        }
        else
            dataStreamOut << static_cast<quint8>(eNetCfg_CfgPP_Upload) << false << configC2000PP.errorString();
    }
        break;

    case eNetCfg_Descript_Upload:
    {
        // ----------------------------------------
        // Загрузать описания ШС/Рл/Разделов на сервер
        // ----------------------------------------
        QVector<QString> descriptionSh;
        QVector<QString> descriptionRl;
        QVector<QString> descriptionPart;

        dataStreamIn >> descriptionSh >> descriptionRl >> descriptionPart;
        ConfigC2000PP *configC2000PP = mC2000PP->getConfigC2000PP();
        configC2000PP->setDescriptions(descriptionSh, descriptionRl, descriptionPart);
        configC2000PP->saveDescriptions(mainPath+constFileDescriptions);

        emit signalNewDescriptions();
    }
        break;

    case eNetCfg_CfgPP_read:
    {
        // ----------------------------------------
        // Запросить конфигурацию из С2000-ПП
        // ----------------------------------------
        bool start=false;
        dataStreamIn >> start;
        mC2000PP->readConfigFromDev(start);
    }
        break;

    case eNetCfg_SysConfig:
    {
        //=========================================
        // Sysconfig
        //=========================================
        quint8 command2;
        dataStreamIn >> command2;
        switch (command2) {
        case eSysCfg_getVer:
        {
#ifdef Embedded
            bool bEmbedded = true;
#else
            bool bEmbedded = false;
#endif
            dataStreamOut << static_cast<quint8>(eNetCfg_SysConfig) << static_cast<quint8>(eSysCfg_retVer)
                          << applicationVersion << bEmbedded;

            break;
        }
#ifdef Embedded
        case eSysCfg_reboot:
        {
            int r = system( QStringLiteral("reboot").toLocal8Bit() ); Q_UNUSED(r)
                    break;
        }
        case eSysCfg_setRoot:
        {
            // ----------------------------------------
            // Задать пароль Root
            // ----------------------------------------
            QString pwd;
            dataStreamIn >> pwd;
            QString strPasswd = QStringLiteral("echo -e \"%1\r%1\r\"|passwd").arg(pwd);
            int r = system( strPasswd.toLocal8Bit() ); Q_UNUSED(r)
                    break;
        }
        case eSysCfg_getConfigEth:
        {
            // ----------------------------------------
            // Запрос параметров сети
            // ----------------------------------------
            QString ethAddress;
            QString ethNetmask;
            QString ethGateway;
            QString ethDNS1;
            QString ethDNS2;
            bool    bDHCP;

            ethernet::getConfig(&ethAddress, &ethNetmask, &ethGateway, &bDHCP);
            ethernet::getConfigDNS(&ethDNS1, &ethDNS2);

            dataStreamOut << static_cast<quint8>(eNetCfg_SysConfig) << static_cast<quint8>(eSysCfg_retConfigEth)
                          << bDHCP << ethAddress << ethNetmask << ethGateway << ethDNS1 << ethDNS2;

            break;
        }
        case eSysCfg_setConfigEth:
        {
            // ----------------------------------------
            // Задать параметры сети
            // ----------------------------------------
            QString ethAddress;
            QString ethNetmask;
            QString ethGateway;
            QString ethDNS1;
            QString ethDNS2;
            bool    bDHCP;

            dataStreamIn >> bDHCP >> ethAddress >> ethNetmask >> ethGateway >> ethDNS1 >> ethDNS2;

            QString ethData = ethernet::strConfigEth0(ethAddress, ethNetmask, ethGateway, bDHCP);
            ethernet::setConfig(ethData);
            ethernet::setConfigDNS(ethDNS1, ethDNS2);

            QString sysCommand = QStringLiteral("ifdown -f %1").arg(ethernet::ethNameEth0);
            int r = system( sysCommand.toLocal8Bit() );

            sysCommand = QStringLiteral("ifup %1").arg(ethernet::ethNameEth0);
            r = system( sysCommand.toLocal8Bit() ); Q_UNUSED(r)

                    ethernet::getConfig(&ethAddress, &ethNetmask, &ethGateway, &bDHCP);
            ethernet::getConfigDNS(&ethDNS1, &ethDNS2);

            QByteArray data;
            QDataStream ds(&data, QIODevice::WriteOnly);
            ds.setVersion(dataStreamVersion);
            ds << static_cast<quint8>(eNetCfg_SysConfig) << static_cast<quint8>(eSysCfg_retConfigEth)
               << bDHCP << ethAddress << ethNetmask << ethGateway << ethDNS1 << ethDNS2;
            sendAllSockets(data);

            break;
        }
        case eSysCfg_getTzRegion:
        {
            // ----------------------------------------
            // Запрос параметров региона
            // ----------------------------------------
            QString tzRegion = tzdata::getRegion();
            QStringList tzRegions = tzdata::getRegions();
            dataStreamOut << static_cast<quint8>(eNetCfg_SysConfig) << static_cast<quint8>(eSysCfg_retTzRegion) << tzRegion << tzRegions;
            break;
        }
        case eSysCfg_getTzZone:
        {
            // ----------------------------------------
            // Запрос параметров часового пояса
            // ----------------------------------------
            QString tzRegion;
            dataStreamIn >> tzRegion;
            if (tzRegion.isEmpty()) tzRegion = tzdata::getRegion();

            if (!tzRegion.isEmpty()){
                QString     tzZone  = tzdata::getTZone();
                QStringList tzZones = tzdata::getTZones(tzRegion);
                dataStreamOut << static_cast<quint8>(eNetCfg_SysConfig) << static_cast<quint8>(eSysCfg_retTzZone) << tzZone << tzZones;
            }
            break;
        }

        case eSysCfg_getTime:
            // ----------------------------------------
            // Запрос времени
            // ----------------------------------------
            dataStreamOut << static_cast<quint8>(eNetCfg_SysConfig) << static_cast<quint8>(eSysCfg_retTime) << QDateTime::currentDateTime();
            break;

        case eSysCfg_setConfigTime:
        {
            // ----------------------------------------
            // Задать часовой пояс и часы
            // ----------------------------------------
            QString tzRegion;
            QString tzZone;
            QDateTime dateTime;
            dataStreamIn >> tzRegion >> tzZone >> dateTime;

            if (tzdata::getRegion()!=tzRegion || tzdata::getTZone()!=tzZone)
            {
                tzdata::setTimezone(tzRegion, tzZone); // Задать часовой пояс

                QByteArray data;
                QDataStream ds(&data, QIODevice::WriteOnly);
                ds.setVersion(dataStreamVersion);
                ds << static_cast<quint8>(eNetCfg_SysConfig) << static_cast<quint8>(eSysCfg_retTzRegion) << tzdata::getRegion() << tzdata::getRegions();
                sendAllSockets(data);
            }

            // Задать время
            QString sysCommand = QStringLiteral("date -u ") + dateTime.toString( QStringLiteral("MMddhhmmyyyy.ss") );
            int r = system( sysCommand.toLocal8Bit() );

            // Синхронизация с аппаратными часами
            //command = QStringLiteral("hwclock -w");
            sysCommand = QStringLiteral("chronyc trimrtc");
            r = system( sysCommand.toLocal8Bit()); Q_UNUSED(r)

                    break;
        }
#endif
        }

        break;
        //=========================================
    }

    case eNetCfg_Mail:
    {
        //=========================================
        // Mail
        //=========================================

        quint8 command2;
        dataStreamIn >> command2;
        switch (command2) {
        case eNetMail_setConfig:
        {
            // ----------------------------------------
            // Загрузать конфигурацию Mail на сервер
            // ----------------------------------------
            dataStreamIn >> mapMailName >> mapMailEnable >> mapMailFilterA >> mapMailFilterZ
                    >> mailEnable >> mailSubject >> mailFrom >> mailPWD >> mailServer >> mailPort >> mailDelay;

            if (mailEnable){
                smtp.setAddress(QString(), mailFrom, mailPWD, mailSubject, mailServer, mailPort);
                smtp.setDelay(mailDelay);
            }

            // Сохранить в файл
            QByteArray data;
            QDataStream ds(&data, QIODevice::WriteOnly);
            ds.setVersion(dataStreamVersion);
            ds << mapMailName << mapMailEnable << mapMailFilterA << mapMailFilterZ
               << mailEnable << mailSubject << mailFrom << mailPWD << mailServer << mailPort << mailDelay;

            SimpleFileBA fileBA;
            fileBA.setFileName(mainPath + constFileMail);
            fileBA.write(data);

            // Разослать клиентам
            data.clear();
            QDataStream ds2(&data, QIODevice::WriteOnly);
            ds2.setVersion(dataStreamVersion);
            ds2 << static_cast<quint8>(eNetCfg_Mail) << static_cast<quint8>(eNetMail_retConfig)
                << mapMailName << mapMailEnable << mapMailFilterA << mapMailFilterZ
                << mailEnable << mailSubject << mailFrom << mailPWD << mailServer << mailPort << mailDelay;
            sendAllSockets(data);

            break;
        }

        case eNetMail_getConfig:
        {
            // ----------------------------------------
            // Клиент запрашивает конфигурацию Mail
            // ----------------------------------------
            dataStreamOut << static_cast<quint8>(eNetCfg_Mail) << static_cast<quint8>(eNetMail_retConfig)
                          << mapMailName << mapMailEnable << mapMailFilterA << mapMailFilterZ
                          << mailEnable << mailSubject << mailFrom << mailPWD << mailServer << mailPort << mailDelay;
            break;
        }
        case eNetMail_test:
        {
            // ----------------------------------------
            // Клиент проверяет отправку по Mail
            // ----------------------------------------
            QString testTo;
            QString testSubject;
            QString testFrom;
            QString testPWD;
            QString testServer;
            quint16 testPort;

            dataStreamIn >> testSubject >> testTo >> testFrom >> testPWD >> testServer >> testPort;

            if (!testTo.isEmpty() && !testFrom.isEmpty() && !testPWD.isEmpty() && !testServer.isEmpty() && (testPort!=0))
            {
                smtp.send(testTo, testFrom, testPWD, testSubject,
                          QStringLiteral(u"Проверка отправки почты программой HUB-C2000PP.\n%1, %2, %3, %4")
                          .arg(QSysInfo::prettyProductName(),
                               QSysInfo::kernelVersion(),
                               QSysInfo::currentCpuArchitecture(),
                               QSysInfo::machineHostName()),
                          testServer, testPort);
            }

            break;
        }
        case eNetMail_logClear:
            // ----------------------------------------
            // Mail. Очистить льго событий.
            // ----------------------------------------
        {
            logSMTP.clear();

            QByteArray data;
            QDataStream ds(&data, QIODevice::WriteOnly);
            ds.setVersion(dataStreamVersion);
            ds << static_cast<quint8>(eNetCfg_Mail) << static_cast<quint8>(eNetMail_logRetCount) << logSMTP.countMsg();
            sendAllSockets(data);
            break;
        }
        case eNetMail_logGetCount:
            // ----------------------------------------
            // Ответ на запрос количества событий
            // ----------------------------------------
            dataStreamOut << static_cast<quint8>(eNetCfg_Mail) << static_cast<quint8>(eNetMail_logRetCount)
                          << logSMTP.countMsg();
            break;
        case eNetMail_logGetMsg:
            // ----------------------------------------
            // Ответ на запрос лога отправки mail
            // ----------------------------------------
        {
            qint32 n;
            quint8 count;
            dataStreamIn >> n >> count;
            dataStreamOut << static_cast<quint8>(eNetCfg_Mail) << static_cast<quint8>(eNetMail_logRetMsg);

            qint32 k = n + count;
            for (qint32 i=n; i<k; i++){
                if (logSMTP.countMsg()>i)
                    dataStreamOut << i << logSMTP.msg(i);
                else break;
            }

            break;
        }
        }
        break;
        //=========================================
    }
    case eNetCfg_GSM:
    {
        //=========================================
        // GSM
        //=========================================

        quint8 command2;
        dataStreamIn >> command2;
        switch (command2) {
        case eNetGSM_setConfig:
        {
            // ----------------------------------------
            // Загрузать конфигурацию GSM на сервер
            // ----------------------------------------

            dataStreamIn >> mapGSMName >> mapGSMEnable >> mapGSMFilterA >> mapGSMFilterZ >> gsmEnable >> serialPortGSM;

            // Настроить GSM модем.
            if (gsmEnable){
                if (!serialPortGSM.isEmpty())
                {
                    gsmSMS.changePort(serialPortGSM);
                    gsmSMS.start();
                }
                else gsmSMS.stop();
            }
            else gsmSMS.stop();

            // Сохранить в файл
            QByteArray data;
            QDataStream ds(&data, QIODevice::WriteOnly);
            ds.setVersion(dataStreamVersion);
            ds << mapGSMName << mapGSMEnable << mapGSMFilterA << mapGSMFilterZ << gsmEnable << serialPortGSM;

            SimpleFileBA fileBA;
            fileBA.setFileName(mainPath + constFileGSM);
            fileBA.write(data);

            // Разослать клиентам
            data.clear();
            QDataStream ds2(&data, QIODevice::WriteOnly);
            ds2.setVersion(dataStreamVersion);
            ds2 << static_cast<quint8>(eNetCfg_GSM) << static_cast<quint8>(eNetGSM_retConfig)
                << mapGSMName << mapGSMEnable << mapGSMFilterA << mapGSMFilterZ << gsmEnable << serialPortGSM;
            sendAllSockets(data);

            break;
        }

        case eNetGSM_getConfig:
        {
            // ----------------------------------------
            // Клиент запросил конфигурацию GSM
            // ----------------------------------------
            dataStreamOut << static_cast<quint8>(eNetCfg_GSM) << static_cast<quint8>(eNetGSM_retConfig)
                          << mapGSMName << mapGSMEnable << mapGSMFilterA << mapGSMFilterZ << gsmEnable << serialPortGSM;
            break;
        }
        case eNetGSM_getPort:
        {
            // ----------------------------------------
            // Клиент запросил порт GSM
            // ----------------------------------------
            dataStreamOut << static_cast<quint8>(eNetCfg_GSM) << static_cast<quint8>(eNetGSM_portChanged)
                          << getMapSerialPortNames() << serialPortGSM;
            break;
        }
        case eNetGSM_changePort:
        {
            // ----------------------------------------
            // Клиент изменил порт GSM
            // ----------------------------------------
            dataStreamIn >> serialPortGSM;
            if (!serialPortGSM.isEmpty())
            {
                gsmSMS.changePort(serialPortGSM);
                gsmSMS.start();

                // Разослать клиентам
                QByteArray data;
                QDataStream ds(&data, QIODevice::WriteOnly);
                ds.setVersion(dataStreamVersion);
                ds << static_cast<quint8>(eNetCfg_GSM) << static_cast<quint8>(eNetGSM_portChanged) << getMapSerialPortNames() << serialPortGSM;
                sendAllSockets(data);
            }

            break;
        }
        case eNetGSM_sendSMS:
        {
            // ----------------------------------------
            // Клиент отправляет тестовое SMS
            // ----------------------------------------
            QByteArray phone;
            QString message;
            dataStreamIn >> phone >> message;

            if (!phone.isEmpty() && GsmSMS::testPhoneNumber(QString::fromLatin1(phone))) {
                gsmSMS.sendMsg(phone, message);
            }

            break;
        }
        case eNetGSM_sendUSSD:
        {
            // ----------------------------------------
            // Клиент отправляет USSD запрос
            // ----------------------------------------
            QByteArray baUSSD;
            dataStreamIn >> baUSSD;

            if (!baUSSD.isEmpty() ) gsmSMS.sendUSSD(baUSSD);
            break;
        }
        case eNetGSM_subscribe:
        {
            // ----------------------------------------
            // Клиент подписался/отписался от уведомлений о состоянии GSM модема
            // ----------------------------------------

            bool bSubscribe;
            dataStreamIn >> bSubscribe;

            if (bSubscribe)
            {
                if (local) gsmSubscribeLocal.insert(socketDescriptor);
                else gsmSubscribeTcp.insert(socketDescriptor);

                sendGsmInfo(eNetGSM_infoLevel,     socketDescriptor, local);
                sendGsmInfo(eNetGSM_infoModel,     socketDescriptor, local);
                sendGsmInfo(eNetGSM_infoOperator,  socketDescriptor, local);
                sendGsmInfo(eNetGSM_infoLastErr, socketDescriptor, local);
            }
            else
            {
                if (local) gsmSubscribeLocal.remove(socketDescriptor);
                else gsmSubscribeTcp.remove(socketDescriptor);
            }

            break;
        }
        case eNetGSM_logClear:
            // ----------------------------------------
            // GSM. Очистить льго событий.
            // ----------------------------------------
        {
            logGSM.clear();

            QByteArray data;
            QDataStream ds(&data, QIODevice::WriteOnly);
            ds.setVersion(dataStreamVersion);
            ds << static_cast<quint8>(eNetCfg_GSM) << static_cast<quint8>(eNetGSM_logRetCount) << logGSM.countMsg();
            sendAllSockets(data);
            break;
        }
        case eNetGSM_logGetCount:
            // ----------------------------------------
            // Ответ на запрос количества событий
            // ----------------------------------------
            dataStreamOut << static_cast<quint8>(eNetCfg_GSM) << static_cast<quint8>(eNetGSM_logRetCount)
                          << logGSM.countMsg();
            break;
        case eNetGSM_logGetMsg:
            // ----------------------------------------
            // Ответ на запрос лога отправки mail
            // ----------------------------------------
        {
            qint32 n;
            quint8 count;
            dataStreamIn >> n >> count;
            dataStreamOut << static_cast<quint8>(eNetCfg_GSM) << static_cast<quint8>(eNetGSM_logRetMsg);

            qint32 k = n + count;
            for (qint32 i=n; i<k; i++){
                if (logGSM.countMsg()>i)
                    dataStreamOut << i << logGSM.msg(i);
                else break;
            }

            break;
        }
        }
        break;
        //=========================================
    }
    case eNetCfg_TLG:
    {
        //=========================================
        // Telegram
        //=========================================

        quint8 command2;
        dataStreamIn >> command2;
        switch (command2) {
        case eNetTLG_setConfig:
        {
            // ----------------------------------------
            // Загрузать конфигурацию Telegram на сервер
            // ----------------------------------------

            dataStreamIn >> listTlgSelectedID >> mapTlgFilterG >> mapTlgFilterE >> mapTlgFilterZ
                    >> tlgEnable >> tlgConnect >> tlgPhone >> tlgObjectName >> tlgProxy >> tlgProxyName >> tlgProxyPort >> tlgProxyLogin >> tlgProxyPwd;

            if ( !tlgEnable || ((!tlgConnect) && (tlgPhone.count()>9) && QFile::exists( TlgProcess::fullFileName() )) )
                tlgConnect = tlgEnable;

            if (tlgProxy) telegram.setProxy(tlgProxyName, tlgProxyPort, tlgProxyLogin, tlgProxyPwd);
            else telegram.setProxy(QString(), 0, QString(), QString());

            telegram.setPhoneNumber(tlgPhone);
            telegram.setObjectName(tlgObjectName);
            telegramStart(tlgConnect);

            // Сохранить в файл
            QByteArray data;
            QDataStream ds(&data, QIODevice::WriteOnly);
            ds.setVersion(dataStreamVersion);
            ds << listTlgSelectedID << mapTlgFilterG << mapTlgFilterE << mapTlgFilterZ
               << tlgEnable << tlgConnect << tlgPhone << tlgObjectName << tlgProxy << tlgProxyName << tlgProxyPort << tlgProxyLogin << tlgProxyPwd;

            SimpleFileBA fileBA;
            fileBA.setFileName(mainPath + constFileTlg);
            fileBA.write(data);

            // Разослать клиентам
            data.clear();
            QDataStream ds2(&data, QIODevice::WriteOnly);
            ds2.setVersion(dataStreamVersion);
            ds2 << static_cast<quint8>(eNetCfg_TLG) << static_cast<quint8>(eNetTLG_retConfig)
                << listTlgSelectedID << mapTlgFilterG << mapTlgFilterE << mapTlgFilterZ
                << tlgEnable << tlgConnect << tlgPhone << tlgObjectName << tlgProxy << tlgProxyName << tlgProxyPort
                << tlgState << tlgListID << tlgListName << tlgProxyLogin << tlgProxyPwd;
            sendAllSockets(data);

            break;
        }
        case eNetTLG_getConfig:
        {
            // ----------------------------------------
            // Клиент запросил конфигурацию Telegram
            // ----------------------------------------
            dataStreamOut << static_cast<quint8>(eNetCfg_TLG) << static_cast<quint8>(eNetTLG_retConfig)
                          << listTlgSelectedID << mapTlgFilterG << mapTlgFilterE << mapTlgFilterZ << tlgEnable
                          << tlgConnect << tlgPhone << tlgObjectName << tlgProxy << tlgProxyName << tlgProxyPort
                          << tlgState << tlgListID << tlgListName << tlgProxyLogin << tlgProxyPwd;
            break;
        }
        case eNetTLG_connect:
        {
            // ----------------------------------------
            // Подключиться к Telegram
            // ----------------------------------------
            bool bTlgStart;
            dataStreamIn >> bTlgStart;

            if (bTlgStart) {
                if (QFile::exists( TlgProcess::fullFileName() )){
                    dataStreamIn >> tlgPhone >> tlgObjectName >> tlgProxy >> tlgProxyName >> tlgProxyPort >> tlgProxyLogin >> tlgProxyPwd;

                    if (tlgPhone.count()>9){
                        telegram.setPhoneNumber(tlgPhone);
                        telegram.setObjectName(tlgObjectName);

                        if (tlgProxy) telegram.setProxy(tlgProxyName, tlgProxyPort, tlgProxyLogin, tlgProxyPwd);
                        else telegram.setProxy(QString(), 0, QString(), QString());
                    }
                    else bTlgStart = false;
                }
                else{
                    bTlgStart = false;

                    //Уведомить об ошибке
                    dataStreamOut << static_cast<quint8>(eNetCfg_TLG) << static_cast<quint8>(eNetTLG_error)
                                  << QStringLiteral(u"На сервере не удается найти файл:\n") + TlgProcess::fullFileName();
                }
            }

            telegramStart(bTlgStart);

            break;
        }
        case eNetTLG_EnterCode:
        {
            // ----------------------------------------
            // Отправить Telegram код авторизации
            // ----------------------------------------
            QByteArray code;
            dataStreamIn >> code;
            if (!code.isEmpty()) telegram.telegramEnterCode(code);

            break;
        }
        case eNetTLG_LogOut:
        {
            // ----------------------------------------
            // Разлогиниться в Telegram
            // ----------------------------------------
            telegram.telegramLogOut();

            break;
        }
        case eNetTLG_sendMsg:
        {
            // ----------------------------------------
            // Отправить сообщение в чат Telegram
            // ----------------------------------------
            qint64 id=0;
            QString msg;
            dataStreamIn >> id >> msg;

            if ((id!=0) && !msg.isEmpty()) telegram.telegramSendMessage(id, msg);

            break;
        }
        case eNetTLG_logClear:
            // ----------------------------------------
            // Telegram. Очистить льго событий.
            // ----------------------------------------
        {
            logTLG_out.clear();

            QByteArray data;
            QDataStream ds(&data, QIODevice::WriteOnly);
            ds.setVersion(dataStreamVersion);
            ds << static_cast<quint8>(eNetCfg_TLG) << static_cast<quint8>(eNetTLG_logRetCount) << logTLG_out.countMsg();
            sendAllSockets(data);
            break;
        }
        case eNetTLG_logGetCount:
            // ----------------------------------------
            // Ответ на запрос количества событий
            // ----------------------------------------
            dataStreamOut << static_cast<quint8>(eNetCfg_TLG) << static_cast<quint8>(eNetTLG_logRetCount)
                          << logTLG_out.countMsg();
            break;
        case eNetTLG_logGetMsg:
            // ----------------------------------------
            // Ответ на запрос лога отправки mail
            // ----------------------------------------
        {
            qint32 n;
            quint8 count;
            dataStreamIn >> n >> count;
            dataStreamOut << static_cast<quint8>(eNetCfg_TLG) << static_cast<quint8>(eNetTLG_logRetMsg);

            qint32 k = n + count;
            for (qint32 i=n; i<k; i++){
                if (logTLG_out.countMsg()>i)
                    dataStreamOut << i << logTLG_out.msg(i);
                else break;
            }

            break;
        }
        }
        break;
        //=========================================
    }
    }

    if (!dataOut.isEmpty()) {
        if (local)
            emit serverLinkLocal.sendData(socketDescriptor, dataOut);
        else
            emit serverLinkTcp.sendData(socketDescriptor, dataOut);
    }
}
//------------------------------------------------------------------------------
// Вернуть информацию о всех последовательных портах
//------------------------------------------------------------------------------
QMap <QString, QString> HubConfig::getMapSerialPortNames()
{
    QMap <QString, QString> mapSerialPortNames;
    for (QSerialPortInfo &portInfo : QSerialPortInfo::availablePorts())
        mapSerialPortNames[portInfo.portName()] = portInfo.description();

    return mapSerialPortNames;
}
//------------------------------------------------------------------------------
// GSM. Уровень сигнала
//------------------------------------------------------------------------------
void HubConfig::slotGSMLevel(quint8 level)
{
    gsmInfoLevel = level;

    sendGsmInfo(eNetGSM_infoLevel);
}
//------------------------------------------------------------------------------
// GSM. Отправлено СМС сообщение
//------------------------------------------------------------------------------
void HubConfig::slotSMSsent(QByteArray phone, bool ok)
{
    QString status = (ok ? QStringLiteral("Отправлено: ") : QStringLiteral("Не отправлено: "))
            + QString::fromLatin1( phone );

    int n = logGSM.countMsg();
    logGSM.writeLog(status);

    //Разослать всем клиентам
    QByteArray data;
    QDataStream ds(&data, QIODevice::WriteOnly);
    ds.setVersion(dataStreamVersion);
    ds << static_cast<quint8>(eNetCfg_GSM) << static_cast<quint8>(eNetGSM_logNewMsg)
       << n << logGSM.msg(n);
    sendAllSockets(data);
}
//------------------------------------------------------------------------------
// GSM. Получено СМС сообщение
//------------------------------------------------------------------------------
void HubConfig::slotSMSreceived(const QByteArray &phone, QDateTime dateTime, const QString &msg)
{
    QByteArray data;
    QDataStream ds(&data, QIODevice::WriteOnly);
    ds.setVersion(dataStreamVersion);
    ds << static_cast<quint8>(eNetCfg_GSM)<< static_cast<quint8>(eNetGSM_receivedSMS) << phone << dateTime << msg;
    sendAllSockets(data);
}
//------------------------------------------------------------------------------
// GSM. Получено сообщение USSD
//------------------------------------------------------------------------------
void HubConfig::slotUSSDreceived(const QString &msg)
{
    QByteArray data;
    QDataStream ds(&data, QIODevice::WriteOnly);
    ds.setVersion(dataStreamVersion);
    ds << static_cast<quint8>(eNetCfg_GSM) << static_cast<quint8>(eNetGSM_receivedUSSD) << msg;
    sendAllSockets(data);
}
//------------------------------------------------------------------------------
// GSM. Модель
//------------------------------------------------------------------------------
void HubConfig::slotGSMModel(const QByteArray &manufacturer, const QByteArray &model, const QByteArray &imei)
{
    gsmInfoManufacturer = manufacturer;
    gsmInfoModel = model;
    gsmInfoImei = imei;

    sendGsmInfo(eNetGSM_infoModel);
}
//------------------------------------------------------------------------------
// GSM. Оператор
//------------------------------------------------------------------------------
void HubConfig::slotGSMOperator(quint8 state, const QString &name)
{
    gsmInfoState=state;
    gsmInfoName=name;

    sendGsmInfo(eNetGSM_infoOperator);
}
//------------------------------------------------------------------------------
// GSM. Ошибка
//------------------------------------------------------------------------------
void HubConfig::slotGSMLastError(const QString &err)
{
    gsmInfoerr = err;

    sendGsmInfo(eNetGSM_infoLastErr);
}
//------------------------------------------------------------------------------
// GSM. Отправить информацию о модеме
//------------------------------------------------------------------------------
void HubConfig::sendGsmInfo(enumNetCfgGSM cmd, qintptr socketDescriptor, bool local)
{
    //eNetCfg_empty
    QByteArray data;
    QDataStream ds(&data, QIODevice::WriteOnly);
    ds.setVersion(dataStreamVersion);

    if (cmd==eNetGSM_infoLevel)
        ds << static_cast<quint8>(eNetCfg_GSM)<< static_cast<quint8>(eNetGSM_infoLevel) << gsmInfoLevel;
    else if (cmd==eNetGSM_infoModel)
        ds << static_cast<quint8>(eNetCfg_GSM)<< static_cast<quint8>(eNetGSM_infoModel) << gsmInfoManufacturer << gsmInfoModel << gsmInfoImei;
    else if (cmd==eNetGSM_infoOperator)
        ds << static_cast<quint8>(eNetCfg_GSM)<< static_cast<quint8>(eNetGSM_infoOperator) << gsmInfoState << gsmInfoName;
    else if (cmd==eNetGSM_infoLastErr)
        ds << static_cast<quint8>(eNetCfg_GSM)<< static_cast<quint8>(eNetGSM_infoLastErr) << gsmInfoerr;

    if (data.isEmpty()) return;

    if (socketDescriptor==0)
    {
        for(const auto socketDescript : qAsConst(gsmSubscribeLocal))
            emit serverLinkLocal.sendData(socketDescript, data);

        for(const auto socketDescript : qAsConst(gsmSubscribeTcp))
            emit serverLinkTcp.sendData(socketDescript, data);
    }
    else{
        if (local) emit serverLinkLocal.sendData(socketDescriptor, data);
        else emit serverLinkTcp.sendData(socketDescriptor, data);
    }
}
//------------------------------------------------------------------------------
// Telegram. Подключиться/отключиться
//------------------------------------------------------------------------------
void HubConfig::telegramStart(bool start)
{
    if (start) telegram.start();
    else {
        telegram.stop();
        tlgListID.clear();
        tlgListName.clear();
    }

    // Разослать клиентам
    QByteArray data;
    QDataStream ds(&data, QIODevice::WriteOnly);
    ds.setVersion(dataStreamVersion);
    ds << static_cast<quint8>(eNetCfg_TLG) << static_cast<quint8>(eNetTLG_connect) << start;
    sendAllSockets(data);
}
//------------------------------------------------------------------------------
// Telegram. Получено/обновлено имя чата
//------------------------------------------------------------------------------
void HubConfig::slotTlgUpdateChatTitle(qint64 id, QString chatTitle)
{
    if (chatTitle.isEmpty()) chatTitle = QStringLiteral(u"Удалённый аккаунт");

    if(tlgListID.contains(id)) {
        int n = tlgListID.indexOf(id);
        tlgListName[n] = chatTitle;
    }
    else {
        tlgListID.append(id);
        tlgListName.append(chatTitle);
    }

    QByteArray data;
    QDataStream ds(&data, QIODevice::WriteOnly);
    ds.setVersion(dataStreamVersion);
    ds << static_cast<quint8>(eNetCfg_TLG) << static_cast<quint8>(eNetTLG_UpdateNick) << id << chatTitle;
    sendAllSockets(data);
}
//------------------------------------------------------------------------------
// Telegram. Подтверждение доставки сообщения
//------------------------------------------------------------------------------
void HubConfig::slotTlgSentMessage(qint64 chatID, bool ok)
{
    QString chatTitle;
    if(tlgListID.contains(chatID)) {
        int n = tlgListID.indexOf(chatID);
        chatTitle = tlgListName.value(n);
    }

    QString status = (ok ? QStringLiteral("Отправлено: ") : QStringLiteral("Не отправлено: ")) + chatTitle;

    int n = logTLG_out.countMsg();
    logTLG_out.writeLog(status);

    //Разослать всем клиентам
    QByteArray data;
    QDataStream ds(&data, QIODevice::WriteOnly);
    ds.setVersion(dataStreamVersion);
    ds << static_cast<quint8>(eNetCfg_TLG) << static_cast<quint8>(eNetTLG_logNewMsg)
       << n << logTLG_out.msg(n);
    sendAllSockets(data);
}
//------------------------------------------------------------------------------
// Telegram. Входящее сообщение
//------------------------------------------------------------------------------
void HubConfig::slotTlgGotMessage(qint64 chatID, QString msg)
{
    if (!listTlgSelectedID.contains(chatID)) return;

    static const QString strLog     = QStringLiteral("[%1] %2");

    static const QString strStart   = QStringLiteral("/старт");
    static const QString strStop    = QStringLiteral("/стоп");
    static const QString strSh      = QStringLiteral("/зоны");
    static const QString strParts   = QStringLiteral("/разделы");
    static const QString strHelp    = QStringLiteral("/справка");
    static const QString strARM     = QStringLiteral("/взять");
    static const QString strDISARM  = QStringLiteral("/снять");
    //static const QString strON      = QStringLiteral("/включить");
    //static const QString strOFF     = QStringLiteral("/выключить");

    if (msg.startsWith(tlgObjectName, Qt::CaseInsensitive)){
        msg = msg.right(tlgObjectName.length()-strARM.length()).simplified();
    }

    controlPP typeControl = SHPP_NO;

    if (msg.startsWith(strHelp, Qt::CaseInsensitive))
    {
        logTLG_in.writeLog( strLog.arg(chatID).arg(msg) );

        telegram.telegramSendMessage(chatID,
                                     QStringLiteral(u"📟 Бот HUB-C2000PP %1 (%2). Доступные команды:\n"
                                                    "🔹/старт - показать список из 10 последних событий;\n"
                                                    "🔹/старт n - показать список из n последних событий;\n"
                                                    "🔹/стоп - отключить обновление списка событий;\n"
                                                    "🔹/зоны - показать состояния зон;\n"
                                                    "🔹/разделы - показать состояния разделов;\n"
                                                    "🔹/взять 1 - взять зону 1 на охрану, можно /взять 1,2,3;\n"
                                                    "🔹/снять 1 - снять зону 1 с охраны, можно /снять 1,2,3;\n"
                                                    "🔹/взять р1 - взять раздел 1 на охрану, можно /взять р1,2,3;\n"
                                                    "🔹/снять р1 - снять раздел 1 с охраны, можно /снять р1,2,3;\n"
                                                    //"🔹/включить 1 - включить реле 1, можно /включить 1,2,3;\n"
                                                    //"🔹/выключить 1 - выключить реле 1, можно /выключить 1,2,3;\n"
                                                    "Регистр не имеет значения. Если объектов несколько, то можно перед командой написать название объекта.").
                                     arg(QCoreApplication::applicationVersion(), tlgObjectName) );

    }
    else if (msg.startsWith(strStart, Qt::CaseInsensitive))
    {
        logTLG_in.writeLog( strLog.arg(chatID).arg(msg) );

        msg = msg.right(msg.length()-strStart.length());
        quint16 d = msg.simplified().toUShort();
        if (d==0) d = 10;
        else if (d>50) d = 50;

        if (startIdCount.value(chatID)!=d){
            startIdCount[chatID] = d;

            const QSet<quint16> zones = mapTlgFilterZ.value(chatID);

            QList<QDate> listDate;
            QStringList  strList1;
            QStringList  strList2;

            quint8 n=0;
            qint32 count = mSimpleFileDB->count();
            if (count>1000) count -= 1000;
            else count = 0;

            for (qint32 i = mSimpleFileDB->count()-1; i>=count; i--)
            {
                EventC2000PP event;
                event.setData( mSimpleFileDB->value(i) );

                if ( !(event.isSh() && zones.contains( event.getSh() )) )
                {
                    listDate.prepend( event.getDatePP().date() );
                    strList1.prepend( eventToString(event, true, true, true, true) );
                    strList2.prepend( eventToString(event, true, false, true, true) );

                    if (++n>=d) break;
                }
            }

            startIdDate [chatID] = listDate;
            startIdList1[chatID] = strList1;
            startIdList2[chatID] = strList2;
        }

        telegramSendEvents(chatID);
    }
    else if (msg.startsWith(strStop, Qt::CaseInsensitive))
    {
        logTLG_in.writeLog( strLog.arg(chatID).arg(msg) );

        startIdCount.remove(chatID);
        startIdDate. remove(chatID);
        startIdList1.remove(chatID);
        startIdList2.remove(chatID);
    }
    else if (msg.startsWith(strSh, Qt::CaseInsensitive))
    {
        logTLG_in.writeLog( strLog.arg(chatID).arg(msg) );

        QString ret;

        auto configC2000PP = mC2000PP->getConfigC2000PP();
        const QSet<quint16> zones = mapTlgFilterZ.value(chatID);
        quint16 maxSh = mC2000PP->getMaxSh();
        for(quint16 nSh=1; nSh<maxSh; nSh++)
        {
            if (!zones.contains(nSh) && configC2000PP->isCorrectSh(nSh)){
                quint8 e = mStateC2000PP->getShState(nSh);
                ret += QStringLiteral("%1[%2] %3 (%4)\n").
                        arg(EventsStatic::getEventMarker(e)).
                        arg(nSh).
                        arg(configC2000PP->getDescriptionSh(nSh-1),
                            EventsStatic::getSrtState(e));
            }
        }

        if (!ret.isEmpty()) telegram.telegramSendMessage(chatID, ret);
    }
    if (msg.startsWith(strParts, Qt::CaseInsensitive))
    {
        logTLG_in.writeLog( strLog.arg(chatID).arg(msg) );

        QString ret;

        auto configC2000PP = mC2000PP->getConfigC2000PP();
        const QSet<quint16> zones = mapTlgFilterZ.value(chatID);
        quint16 maxPart = mC2000PP->getMaxPart();
        for(quint16 nPart=1; nPart<maxPart; nPart++)
        {
            if (configC2000PP->isCorrectPart(nPart))
            {
                bool ok = true;
                const auto shs = configC2000PP->getShInPart(nPart);
                for (const quint16 nSh : shs){
                    if (zones.contains(nSh)){
                        ok = false;
                        break;
                    }
                }

                if (ok){
                    quint8 e = mStateC2000PP->getPartState(nPart);
                    ret += QStringLiteral("%1[%2] %3 (%4)\n").
                            arg(EventsStatic::getEventMarker(e)).
                            arg(nPart).
                            arg(configC2000PP->getDescriptionPart(nPart-1),
                                EventsStatic::getSrtState(e) );
                }
            }
        }

        if (!ret.isEmpty()) telegram.telegramSendMessage(chatID, ret);
    }
    else if (msg.startsWith(strARM, Qt::CaseInsensitive))
    {
        logTLG_in.writeLog( strLog.arg(chatID).arg(msg) );

        typeControl = SHPP_ARM;
        msg = msg.right(msg.length()-strARM.length());
    }
    else if (msg.startsWith(strDISARM, Qt::CaseInsensitive))
    {
        logTLG_in.writeLog( strLog.arg(chatID).arg(msg) );

        typeControl = SHPP_DISARM;
        msg = msg.right(msg.length()-strDISARM.length());
    }/*
    else if (msg.startsWith(strON, Qt::CaseInsensitive))
    {
        logTLG_in.writeLog( strLog.arg(chatID).arg(msg) );

        typeControl = SHPP_CONTROL_ON;
        msg = msg.right(msg.length()-strON.length());
    }
    else if (msg.startsWith(strOFF, Qt::CaseInsensitive))
    {
        logTLG_in.writeLog( strLog.arg(chatID).arg(msg) );

        typeControl = SHPP_CONTROL_OFF;
        msg = msg.right(msg.length()-strOFF.length());
    }*/

    if (typeControl!=SHPP_NO)
    {
        msg=msg.simplified();

        bool isPart = false;
        if ((typeControl==SHPP_ARM || typeControl==SHPP_DISARM) && msg.startsWith(QStringLiteral("р"), Qt::CaseInsensitive)){
            isPart=true;
            msg = msg.right(msg.length()-1);
        }

        QSet<quint16> zones = mapTlgFilterZ.value(chatID);

        const QStringList list = msg.split(QLatin1Char(','));

        for (const QString &str : list)
        {
            quint16 d = str.simplified().toUShort();

            if (d>0){
                if ((typeControl==SHPP_ARM) || (typeControl==SHPP_DISARM))
                {
                    QSet<quint16> shs;

                    if (isPart){
                        auto configC2000PP = mC2000PP->getConfigC2000PP();
                        shs = configC2000PP->getShInPart(d);
                    }
                    else shs.insert(d);

                    for(const quint16 sh : qAsConst(shs)){
                        if (!zones.contains(sh))
                            mC2000PP->controlSh(sh, typeControl);
                    }
                }/*
                else if ((typeControl==SHPP_CONTROL_ON) || (typeControl==SHPP_CONTROL_OFF)){
                    mC2000PP->controlRl(d, typeControl);
                }*/
            }
        }
    }
}
//------------------------------------------------------------------------------
// Telegram. Изменилось состояние подключения
//------------------------------------------------------------------------------
void HubConfig::slotTlgAuthState(TelegramState state)
{
    tlgState = state;
    if ((state==TelegramState::eTerminated) || (state == TelegramState::eEmpty)){
        tlgListID.clear();
        tlgListName.clear();
    }

    QByteArray data;
    QDataStream ds(&data, QIODevice::WriteOnly);
    ds.setVersion(dataStreamVersion);
    ds << static_cast<quint8>(eNetCfg_TLG) << static_cast<quint8>(eNetTLG_AuthState) << state;
    sendAllSockets(data);
}
//------------------------------------------------------------------------------
// Telegram. Ошибка
//------------------------------------------------------------------------------
void HubConfig::slotTlgError(QString errorString)
{
    QByteArray data;
    QDataStream datastream(&data, QIODevice::WriteOnly);
    datastream.setVersion(dataStreamVersion);
    datastream << static_cast<quint8>(eNetCfg_TLG) << static_cast<quint8>(eNetTLG_connect) << false;
    sendAllSockets(data);

    if (!errorString.isEmpty()){
        QByteArray dt;
        QDataStream ds(&dt, QIODevice::WriteOnly);
        ds.setVersion(dataStreamVersion);
        ds << static_cast<quint8>(eNetCfg_TLG) << static_cast<quint8>(eNetTLG_error) << errorString;
        sendAllSockets(dt);
    }
}
//------------------------------------------------------------------------------
// Telegram. Отправить журнал событий
//------------------------------------------------------------------------------
void HubConfig::telegramSendEvents(qint64 chatID)
{
    QString ret = QStringLiteral("📔%1").arg(tlgObjectName);

    const QList<QDate> listDate = startIdDate.value(chatID);
    if (!listDate.isEmpty())
    {
        // Проверим одинаковая ли дата в событиях
        bool bSameDate = true;
        QDate firstDate = listDate.first();
        for(const QDate date: listDate){
            if (firstDate!=date){
                bSameDate = false;
                break;
            }
        }

        if (bSameDate) {
            ret += QStringLiteral(" [%1]").arg(firstDate.toString( EventsStatic::dateFormat ));

            const QStringList strList2 = startIdList2.value(chatID);
            for(const QString &str: strList2){
                ret += QStringLiteral("\n") + str;
            }
        }
        else{
            const QStringList strList1 = startIdList1.value(chatID);
            for(const QString &str: strList1){
                ret += QStringLiteral("\n") + str;
            }
        }
    }
    else ret += QStringLiteral(u" (Нет событий.)");

    telegram.telegramEditMessage(chatID, ret);
}
