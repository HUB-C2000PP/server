//
// Copyright Kuzminov Sergey (kuzminov_sergey@mail.ru) 2021
//
// Distributed under the Unlicense License. (See accompanying
// file LICENSE_1_0.txt or copy at http://unlicense.org/)
//

#ifndef GSMSMS_H
#define GSMSMS_H

// https://wiki.iarduino.ru/page/a6_gprs_at/
// http://hardisoft.ru/soft/samodelkin-soft/otpravka-sms-soobshhenij-v-formate-pdu-teoriya-s-primerami-na-c-chast-1/
// http://hardisoft.ru/soft/samodelkin-soft/poluchenie-i-dekodirovanie-sms-soobshhenij-v-formate-pdu/
// http://smstools3.kekekasvi.com/topic.php?id=288

#include <QDebug>
#include <QQueue>
#include <QTimer>
#include <QDateTime>
#include <QTimeZone>
#include <QTimerEvent>
#include <QSerialPort>
#include <QSerialPortInfo>
#include <QRegularExpression>

#if QT_VERSION < QT_VERSION_CHECK(6, 0, 0)
#include <QTextCodec>
#else
#include <QStringEncoder>
#endif

//------------------------------------------------------------------------------
class GsmSMS : public QObject
{
    Q_OBJECT

private:
    quint8     lastGSMLevel = 0;
    quint8     lastCreg = 0;
    QString    lastCops;
    QString    lastErr;
    QByteArray lastManuf;
    QByteArray lastModel;
    QByteArray lastImei;

    QSerialPort serialPort;

    bool bStart = false;
    bool bAlive = false;
    bool bATI = false;
    bool bSendSMS = false;
    bool bWaitingResponse = false;
    quint8 waitingConfirmSMS = 0;

    QList<QByteArray> buffCMD;
    QList<QByteArray> buffSMS;
    QList<QByteArray> buffPhone;
    QList<QByteArray> buffPhoneSent;
    QByteArray fromModem;

    int timer2s = 0;
    void timerEvent(QTimerEvent *event);
    void startTimerCMD();
    void stopTimerCMD();

    void closePort(void);
    void openPort(void);
    void next(void);

    QByteArray findData(QByteArray data, QByteArray find, bool *ok = nullptr);
    QByteArray findAnswer(QByteArray data, QByteArray *val = nullptr);
    QByteArray encodeToPDU(QByteArray tel, QString msg);

    inline QByteArray byteToHex(quint8 ch);

    QByteArray phoneToPDU(QByteArray data);
    QByteArray phoneFromPDU(QByteArray data);

    QByteArray encodeUCS2(QString str);
    QString    decodeUCS2(QByteArray data);

    QByteArray encodeGSM7bit(QByteArray data);
    QByteArray decodeGSM7bit(QByteArray data);

    QDateTime dateFromPDU(QByteArray data);

    bool decodeUDH(QByteArray udf, quint16 *ied1, quint8 *ied2, quint8 *ied3);
    QMap<quint16, QString> partsMsg;

    bool decodePDU(QByteArray pdu, QByteArray *phone, QDateTime *dateTime, QString *msg);

public:
    explicit GsmSMS(QObject *parent = nullptr);
    ~GsmSMS(void);
    inline bool isStarted(void);
    int countSMSinBuff(void);

    static QString OpsState(quint8 state);
    static bool testPhoneNumber(const QString &phone);

public slots:
    void changePort(QString portName);
    void start(void);
    void stop (void);
    void sendMsg(const QByteArray &phone, QString msg);
    void sendUSSD(QByteArray msg);

private slots:
    void readyRead(void);
    void errorOccurred(QSerialPort::SerialPortError error);

signals:
    void signalSMSsent(QByteArray phone, bool ok);
    void signalSMSreceived(QByteArray phone, QDateTime dateTime, QString msg);
    void signalUSSDreceived(QString msg);
    void signalLevel(quint8 level);
    void signalError(QString err);
    void signalOperator(quint8 state, QString name);
    void signalModel(QByteArray manufacturer, QByteArray model, QByteArray imei);
};
#endif //GSMSMS_H
