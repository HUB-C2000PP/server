//
// Copyright Kuzminov Sergey (kuzminov_sergey@mail.ru) 2021
//
// Distributed under the Unlicense License. (See accompanying
// file LICENSE_1_0.txt or copy at http://unlicense.org/)
//

#include "GsmSMS.h"

//------------------------------------------------------------------------------
bool GsmSMS::testPhoneNumber(const QString &phone)
{
    const static QRegularExpression testPhone( QStringLiteral("^[+]?[0-9]{11,16}$") );
    return testPhone.match(phone).hasMatch();
}
//------------------------------------------------------------------------------
QString GsmSMS::OpsState(quint8 state)
{
    const static QStringList opsState {
        QStringLiteral(u"Не зарегистрирован в сети"),
                QStringLiteral(u"Домашний оператор"),
                QStringLiteral(u"Поиск сети"),
                QStringLiteral(u"Регистрация отклонена"),
                QStringLiteral(u"Статус неизвестен"),
                QStringLiteral(u"Роуминг")};

    return opsState.value(state);
}
//------------------------------------------------------------------------------
GsmSMS::GsmSMS(QObject *parent) : QObject(parent)
{
    QObject::connect(&serialPort, &QSerialPort::readyRead,     this, &GsmSMS::readyRead);
    QObject::connect(&serialPort, &QSerialPort::errorOccurred, this, &GsmSMS::errorOccurred);
}
//------------------------------------------------------------------------------
GsmSMS::~GsmSMS(void)
{
    stop();
}
//------------------------------------------------------------------------------
void GsmSMS::timerEvent(QTimerEvent *event)
{
    if (event->timerId() == timer2s)
    {
        if ( !serialPort.isOpen() ) openPort();
        else {
            if (bAlive && (buffCMD.count()<4)){
                buffCMD.append("AT+CSQ\r\n");    // Качество сигнала
                buffCMD.append("AT+CREG?\r\n");  // Информация о регистрации в сети.
                buffCMD.append("AT+COPS?\r\n");  // Информация об операторе в сети которого зарегистрирован модуль.
                buffCMD.append("AT+CMGL=4\r\n"); // Запросить все СМС сообщения
            }

            bWaitingResponse = false;
            next();
        }
    }
}
//------------------------------------------------------------------------------
void GsmSMS::startTimerCMD()
{
    if (timer2s==0) timer2s = startTimer(2000);
}
//------------------------------------------------------------------------------
void GsmSMS::stopTimerCMD()
{
    if (timer2s!=0){
        killTimer(timer2s);
        timer2s = 0;
    }
}
//------------------------------------------------------------------------------
void GsmSMS::start(void)
{
    if (!bStart) {
        bStart = true;
        openPort();
    }
}
//------------------------------------------------------------------------------
void GsmSMS::stop(void)
{
    if (bStart)
    {
        bStart = false;
        closePort();
    }
}
//------------------------------------------------------------------------------
bool GsmSMS::isStarted(void)
{
    return bStart;
}
//------------------------------------------------------------------------------
int GsmSMS::countSMSinBuff(void)
{
    return buffSMS.count();
}
//------------------------------------------------------------------------------
void GsmSMS::changePort(QString portName)
{
    if ( isStarted() )
    {
        if (serialPort.portName()!=portName)
        {
            stop();
            serialPort.setPortName(portName);
            start();
        }
    }
    else serialPort.setPortName(portName);
}
//------------------------------------------------------------------------------
void GsmSMS::closePort(void)
{
    stopTimerCMD();

    bAlive   = false;
    bATI     = false;
    bSendSMS = false;
    bWaitingResponse = false;
    buffCMD.clear();

    if (serialPort.isOpen()) serialPort.close();

    if (lastGSMLevel) {
        lastGSMLevel = 0;
        emit signalLevel(0);
    }

    if (!lastManuf.isEmpty()){
        lastManuf.clear();
        lastModel.clear();
        lastImei.clear();
        emit signalModel(lastManuf, lastModel, lastImei);
    }

    if (lastCreg!=0 || !lastCops.isEmpty()){
        lastCreg = 0;
        lastCops.clear();
        emit signalOperator(lastCreg, lastCops);
    }
}
//------------------------------------------------------------------------------
void GsmSMS::openPort(void)
{
    if ( !serialPort.portName().isEmpty() )
    {
        closePort();

        QSerialPortInfo serialPortInfo(serialPort);
        if (!serialPortInfo.isNull())
        {
            if (serialPort.open(QIODevice::ReadWrite) )
            {
                if ( serialPort.setBaudRate   (QSerialPort::Baud9600) &&
                     serialPort.setDataBits   (QSerialPort::Data8) &&
                     serialPort.setParity     (QSerialPort::NoParity) &&
                     serialPort.setStopBits   (QSerialPort::OneStop) &&
                     serialPort.setFlowControl(QSerialPort::NoFlowControl) )
                {
                    if (!lastErr.isEmpty()) {
                        lastErr.clear();
                        emit signalError(lastErr);
                    }

                    buffCMD.append("ATE0\r\n");          // Без эха.
                    buffCMD.append("ATV1\r\n");          // Полный текстовый ответ.
                    buffCMD.append("AT+CMEE=2\r\n");     // Максимальный уровень информации об ошибке.

                    buffCMD.append("ATI\r\n");           // Определить модель модема

                    buffCMD.append("AT+CSQ\r\n");        // Качество сигнала
                    buffCMD.append("AT+CREG?\r\n");      // Информация о регистрации в сети.
                    buffCMD.append("AT+COPS=3,1\r\n");   // Установить формат ответа названия оператора.
                    buffCMD.append("AT+COPS?\r\n");      // Информация об операторе в сети которого зарегистрирован модуль.

                    //buffCMD.append("AT+CMGF=0\r\n");     // Режим PDU отправки SMS. Этот режим и так установлен по умолчанию.

                    next();
                }
            }
        }

        startTimerCMD();
    }
}
//------------------------------------------------------------------------------
void GsmSMS::next(void)
{
    if (bAlive)
    {
        if (!bWaitingResponse)
        {
            if (!buffCMD.isEmpty()){  // Команды из очереди инициализации
                QByteArray send = buffCMD.takeFirst();
                if (send=="ATI\r\n") bATI = true;
                serialPort.write(send);
                bWaitingResponse = true;
            }
            else
            { // Команды из очереди SMS
                if (!buffSMS.isEmpty())
                {
                    serialPort.write( "AT+CMGS=" + QByteArray::number((buffSMS.first().count()>>1)-1) +'\r' );
                    bSendSMS = true;
                    bWaitingResponse = true;
                }
            }
        }
    }
    // Пошлем ESC, на случай, если не завершена отправка сообщения на прошлом сеансе.
    else serialPort.write("\x1B\r\nAT\r\n");  // AT - проверить отклик модема.
}
//------------------------------------------------------------------------------
void GsmSMS::readyRead(void)
{
    fromModem += serialPort.readAll();

    QByteArray fromModemTest = fromModem;
    while (fromModemTest.endsWith(' ')) fromModemTest.chop(1);

    if (fromModemTest.endsWith('>'))
    {
        //qDebug() <<"[]"<< fromModem;

        //---------------------------------
        // Второй этап отправки SMS
        //---------------------------------
        stopTimerCMD();

        if (bSendSMS && !buffSMS.isEmpty())
        {
            if (!buffPhone.isEmpty()) buffPhoneSent.append( buffPhone.takeFirst() );
            QByteArray send = buffSMS.takeFirst();
            serialPort.write(send + '\x1A'); // Подтвердить отправку SMS
            ++waitingConfirmSMS;
        }
        else serialPort.write("\x1B"); // Отменить отправку SMS

        bSendSMS = false;
        fromModem.clear();
        startTimerCMD();
        return;
    }
    //else if (fromModemTest.lastIndexOf("OK")<0) return; // Так не очень хорошо. Некоторые ответы сливаются в один.
    else if (!fromModemTest.endsWith("\r\n")) return;

    //---------------------------------
    // Анализ принятых от модема данных
    //---------------------------------
    stopTimerCMD();

    //qDebug() << fromModem;

    bAlive = true;
    bWaitingResponse = false;

    bool ok1;
    QByteArray err = findData(fromModem, "ERROR:", &ok1);
    if (ok1) // Если ошибка
    {
        QString strErr = QString::fromLatin1(err);
        if (lastErr!=strErr){
            lastErr = strErr;
            emit signalError(lastErr);
        }

        if (waitingConfirmSMS && (fromModem.indexOf("+CMS ERROR:")>=0)){
            --waitingConfirmSMS;
            QByteArray phone;
            if (!buffPhoneSent.isEmpty()) phone = buffPhoneSent.takeFirst();
            emit signalSMSsent(phone, false);
        }

        bSendSMS = false;
    }
    else // Если нет ошибок
    {
        if (!lastErr.isEmpty()) {
            lastErr.clear();
            emit signalError(lastErr);
        }

        if (bATI)
        {
            //-----------------------------
            // Модель модема
            //-----------------------------
            bool ok;
            QByteArray manuf = findData(fromModem, "Manufacturer:", &ok);
            if (ok){
                QByteArray model = findData(fromModem, "Model:", &ok);
                if (ok){
                    QByteArray imei  = findData(fromModem, "IMEI:", &ok);
                    if (ok){
                        if (lastManuf!=manuf || lastModel!=model || lastImei!=imei)
                        {
                            lastManuf = manuf;
                            lastModel = model;
                            lastImei  = imei;
                            emit signalModel(lastManuf, lastModel, lastImei);
                        }
                    }
                }
            }

            bATI=false;
        }
        else
        {
            QByteArray val;
            QByteArray answer = findAnswer(fromModem, &val);
            if (!answer.isEmpty())
            {
                //-----------------------------
                // Качество сигнала
                //-----------------------------
                if (answer == "+CSQ:")
                {
                    if (!val.isEmpty())
                    {
                        quint16 level = val.split(',').first().toUShort();
                        if (level>=99) level = 0;
                        else {
                            level = (level+1)*100 >> 5;
                            if (level>100) level =100;
                        }

                        if (lastGSMLevel!=level) {
                            lastGSMLevel = level;
                            emit signalLevel( lastGSMLevel );
                        }
                    }
                    else
                    {
                        if (lastGSMLevel) {
                            lastGSMLevel = 0;
                            emit signalLevel(0);
                        }
                    }
                }
                //-----------------------------
                // Информация о регистрации в сети.
                //-----------------------------
                else if (answer == "+CREG:")
                {
                    if (!val.isEmpty())
                    {
                        bool ok;
                        quint16 iCreg = val.split(',').value(1).toUShort(&ok);
                        if (ok)
                        {
                            if (iCreg>5) iCreg = 0;

                            if (lastCreg != iCreg ) {
                                lastCreg = static_cast<quint8>(iCreg);
                                emit signalOperator(lastCreg, lastCops);
                            }
                        }
                    }
                }
                //-----------------------------
                // Информация об операторе, в сети которого зарегистрирован модуль.
                //-----------------------------
                else if (answer == "+COPS:")
                {
                    if (!val.isEmpty())
                    {
                        QList<QByteArray> baList = val.split(',');

                        // Иногда в процессе передергивания USB модема этот параметр не устанавливается с первого раза.
                        if (baList.value(1) == "2"){
                            buffCMD.append("AT+COPS=3,1\r\n"); // Установить формат ответа названия оператора.
                            buffCMD.append("AT+COPS?\r\n");    // Информация об операторе в сети которого зарегистрирован модуль.
                        }

                        QString strCops = QString::fromLatin1( baList.value(2) );
                        strCops = strCops.remove(QLatin1Char('\"'));
                        if (lastCops != strCops )
                        {
                            lastCops = strCops;
                            emit signalOperator(lastCreg, lastCops);
                        }
                    }
                }
                //-----------------------------
                // Подтверждение отправки СМС
                //-----------------------------
                else if (answer == "+CMGS:")
                {
                    if (waitingConfirmSMS){
                        --waitingConfirmSMS;
                        QByteArray phone;
                        if (!buffPhoneSent.isEmpty()) phone = buffPhoneSent.takeFirst();
                        emit signalSMSsent(phone, true); // СМС сообщение успешно отправлено
                    }
                }
                //-----------------------------
                // Принято СМС
                //-----------------------------
                else if (answer == "+CMGL:")
                {
                    int index1=-1;
                    int index2 = fromModem.indexOf("\r\n+CMGL:");
                    while (index2>=0)
                    {
                        if (index1>=0) {
                            QByteArray pdu = fromModem.mid(index1, index2-index1);
                            int index3 = pdu.indexOf("\r\n", 10);
                            if (index3>=0)
                            {
                                QList<QByteArray> listBA = pdu.mid(8, index3-8).simplified().split(',');
                                if (!listBA.isEmpty()){
                                    bool ok;
                                    quint8 n = listBA.first().toUShort(&ok);
                                    if (ok) buffCMD.prepend("AT+CMGD="+QByteArray::number(n)+",0\r\n"); // удалить одно сообщение
                                }

                                pdu = QByteArray::fromHex( pdu.right(pdu.count()-index3-2) );

                                QByteArray phone;
                                QDateTime dateTime;
                                QString msg;
                                if ( GsmSMS::decodePDU(pdu, &phone, &dateTime, &msg))
                                    emit signalSMSreceived(phone, dateTime, msg);
                            }
                        }

                        index1 = index2;
                        index2 = fromModem.indexOf("\r\n+CMGL:", index1+1);
                        if (index2<0) index2=fromModem.indexOf("\r\n\r\nOK", index1+1);
                    }
                }
                //-----------------------------
                // Получен ответ на USSD запрос
                //-----------------------------
                else if (answer == "+CUSD:")
                {
                    QList <QByteArray> listBA = val.split(',');
                    QByteArray ba = listBA.value(2);

                    if (ba=="15")
                    {
                        ba = listBA.value(1);
                        if (ba.count()>2)
                        {
                            ba = ba.mid(1, ba.count()-2);
                            ba = QByteArray::fromHex( ba );
                            emit signalUSSDreceived( QString::fromLatin1( GsmSMS::decodeGSM7bit( ba ) ) );
                        }
                    }
                    else if (ba=="72")
                    {
                        ba = listBA.value(1);
                        if (ba.count()>2)
                        {
                            ba = ba.mid(1, ba.count()-2);
                            ba = QByteArray::fromHex( ba );
                            emit signalUSSDreceived( GsmSMS::decodeUCS2( ba ) );
                        }
                    }
                }
            }
        }
    }

    fromModem.clear();

    next();

    startTimerCMD();
}
//------------------------------------------------------------------------------
QByteArray GsmSMS::findData(QByteArray data, QByteArray find, bool *ok)
{
    if (ok!=nullptr) *ok = false;
    QByteArray ret;
    int index1 = data.indexOf(find);
    if (index1>=0)
    {
        index1 += find.count();

        int index2 = data.indexOf("\r\n", index1);
        if (index2>=0)
        {
            ret =data.mid(index1, index2-index1).simplified();
            if ( (ok!=nullptr) && !ret.isEmpty() ) *ok = true;
        }
    }

    return ret;
}
//------------------------------------------------------------------------------
QByteArray GsmSMS::findAnswer(QByteArray data, QByteArray *val)
{
    QByteArray ret;
    int index1 = data.indexOf("\r\n+")+2;
    if (index1!=1)
    {
        int index2 = data.indexOf(':', index1+1)+1;
        if (index2!=0)
        {
            ret =data.mid(index1, index2-index1);

            if (val!=nullptr)
            {
                index1 = data.indexOf("\r\n", index2);
                if (index1>=0){
                    *val =data.mid(index2, index1-index2).simplified();
                }
            }
        }
    }

    return ret;
}
//------------------------------------------------------------------------------
void GsmSMS::errorOccurred(QSerialPort::SerialPortError error)
{
    if ( !(error == QSerialPort::NoError || error == QSerialPort::TimeoutError) )
    {
        if (lastErr!=serialPort.errorString())
        {
            lastErr = serialPort.errorString();
            emit signalError(lastErr);
        }

        if (serialPort.isOpen()) {
            closePort();
            startTimerCMD();
        }
    }
}
//------------------------------------------------------------------------------
void GsmSMS::sendMsg(const QByteArray &phone, QString msg)
{
    if (!msg.isEmpty()){
        buffPhone.append(phone);
        buffSMS.append( encodeToPDU(phone, msg) );
        next();
    }
}
//------------------------------------------------------------------------------
void GsmSMS::sendUSSD(QByteArray msg)
{
    if (!msg.isEmpty()){
        buffCMD.append("AT+CUSD=1,\""+encodeGSM7bit(msg).toHex()+"\",15\r\n");
        next();
    }
}
//------------------------------------------------------------------------------
QByteArray GsmSMS::encodeToPDU(QByteArray tel, QString msg)
{
    QByteArray ret;

    if (tel.startsWith('+')) tel=tel.right( tel.count()-1 );

    //if (tel.count()==11) // Для России
    if (tel.count()<17) // Стандарт
    {
        msg.truncate(70);
        QByteArray encodedString = encodeUCS2(msg);

        ret.append( "000100" );
        ret.append( byteToHex(static_cast<quint8>(tel.count())) );
        ret.append( "91");
        ret.append( phoneToPDU(tel) );
        ret.append( "0008" );
        ret.append( byteToHex(static_cast<quint8>(encodedString.count())) );
        ret.append( encodedString.toHex() );
    }

    return ret;
}
//------------------------------------------------------------------------------
QByteArray GsmSMS::byteToHex(quint8 ch)
{
    QByteArray ret;
    ret.append(ch);
    return ret.toHex();
}
//------------------------------------------------------------------------------
QByteArray GsmSMS::phoneToPDU(QByteArray data)
{
    if (data.count()%2 == 1) data.append('F');
    int count = data.count();
    for (int i=0; i<count; i+=2) {
        char ch = data[i];
        data[i] = data[i+1];
        data[i+1] = ch;
    }

    return data;
}
//------------------------------------------------------------------------------
QByteArray GsmSMS::phoneFromPDU(QByteArray data)
{
    data = phoneToPDU(data);
    if (data.endsWith('F') || data.endsWith('f')) data.chop(1);

    return data;
}
//------------------------------------------------------------------------------
QByteArray GsmSMS::encodeUCS2(QString str)
{
#if QT_VERSION < QT_VERSION_CHECK(6, 0, 0)
    QTextCodec *codec = QTextCodec::codecForName("UTF-16BE");
    QTextEncoder *encoder = codec->makeEncoder(QTextCodec::IgnoreHeader);
    QByteArray ret = encoder->fromUnicode(str);
    delete encoder;
#else
    auto encoder = QStringEncoder(QStringEncoder::Utf16BE);
    QByteArray ret = encoder(str);
#endif

    return ret;
}
//------------------------------------------------------------------------------
QString GsmSMS::decodeUCS2(QByteArray data)
{
#if QT_VERSION < QT_VERSION_CHECK(6, 0, 0)
    QTextCodec *codec = QTextCodec::codecForName("UTF-16BE");
    QTextDecoder *deencoder = codec->makeDecoder(QTextCodec::IgnoreHeader);
    QString ret = deencoder->toUnicode(data);
    delete deencoder;
#else
    auto deencoder = QStringDecoder(QStringDecoder::Utf16BE);
    QString ret = deencoder(data);
#endif

    return ret;
}
//------------------------------------------------------------------------------
QByteArray GsmSMS::encodeGSM7bit(QByteArray data)
{
    QByteArray ret;

    if (!data.isEmpty())
    {
        quint8 j = 0;
        quint16 d = 0;
        quint16 count = static_cast<quint16>(data.count());
        for (quint16 i=0; i<count; i++)
        {
            if (j==0){
                j=7;
                d=data[i++];
            }

            quint16 b = (i<data.count()) ? data[i] : 0;
            b<<=j--;
            d|=b;
            ret.append( static_cast<char>(d));
            d>>=8;
        }

        if (d) ret.append( static_cast<char>(d));
    }

    return ret;
}
//------------------------------------------------------------------------------
QByteArray GsmSMS::decodeGSM7bit(QByteArray data)
{
    QByteArray ret;

    if (!data.isEmpty())
    {
        quint8 j=0;
        quint16 d = static_cast<quint8>(data[0]);
        quint16 count = static_cast<quint16>(data.count());
        for (quint16 i=1; i<count; i++)
        {
            ret.append(static_cast<char>(d) & 0x7f);
            d>>=7;

            quint16 b = static_cast<quint8>(data[i]);
            b<<=++j;
            d|=b;

            if (j==7) {
                j=0;
                ret.append(static_cast<char>(d) & 0x7f);
                d>>=7;
            }
        }

        if (d) ret.append(static_cast<char>(d) & 0x7f);
        d>>=7;
        if (d) ret.append(static_cast<char>(d));
    }

    return ret;
}
//------------------------------------------------------------------------------
QDateTime GsmSMS::dateFromPDU(QByteArray data)
{
    data = QByteArray("20") + phoneToPDU(data);

    //----------------------------------------------
    // Timezone
    //----------------------------------------------
    quint8 tz = data.mid(14, 2).toUShort(nullptr, 16);

    bool bNegative = tz & 0x80;
    tz = ((tz>>4) & 7)*10 + (tz & 0x0f);

    int offsetSeconds = ((tz >> 2)*60 + (tz & 3)*15) * 60; // (hour*60 + min)*60
    if (bNegative) offsetSeconds = -offsetSeconds;
    //----------------------------------------------

    data.chop(2);
    QDateTime dateTime = QDateTime::fromString(QString::fromLatin1(data), QStringLiteral("yyyyMMddhhmmss") );
    dateTime.setTimeZone( QTimeZone(offsetSeconds) );

    return dateTime;
}
//------------------------------------------------------------------------------
bool GsmSMS::decodeUDH(QByteArray udf, quint16 *ied1, quint8 *ied2, quint8 *ied3)
{
    if (udf.count()<5) return false;

    quint8 iei = udf[0];

    if (iei==0){
        *ied1 = static_cast<quint8>(udf[2]);
        *ied2 = static_cast<quint8>(udf[3]);
        *ied3 = static_cast<quint8>(udf[4]);
    }
    else if (iei==8){
        if (udf.count()<6) return false;

        *ied1 = static_cast<quint8>(udf[3])<<8 | static_cast<quint8>(udf[2]);
        *ied2 = static_cast<quint8>(udf[4]);
        *ied3 = static_cast<quint8>(udf[5]);
    }
    else return false;

    return true;
}
//------------------------------------------------------------------------------
bool GsmSMS::decodePDU(QByteArray pdu, QByteArray *phone, QDateTime *dateTime, QString *msg)
{
    quint16 index = pdu[0]+1;
    quint8 TP_MTIandCo = pdu[index++]; // TP-MTI & Co
    if ( (TP_MTIandCo & 3) != 0 ) return false; // Раскодируем только сообщения вида SMS-DELIVER

    /*
    switch (TP_MTIandCo & 3){ // Message Type indicator (TP-MTI)
    case 0: qDebug("SMS-DELIVER"); break;
    case 1: qDebug("SMS-SUBMIT REPORT"); break;
    case 2: qDebug("SMS-STATUS REPORT"); break;
    }
    */

    bool TP_UDHI = TP_MTIandCo & 0x40;// User Data header Indicator

    quint8 senderLenght = pdu[index++];
    if (senderLenght & 1) senderLenght++;
    senderLenght>>=1;

    quint8 senderType = (pdu[index++] & 0x7f) >> 4;
    *phone = pdu.mid(index, senderLenght);
    index += senderLenght+1;

    if (senderType==1){ // Международный формат номера
        *phone = phoneFromPDU(phone->toHex());
    }
    else if (senderType==5){ // Буквенно-цифровой, упакованый в 7 бит на символ
        *phone = decodeGSM7bit(*phone);
    };

    quint8 TP_DCS = pdu[index++]; // Data coding scheme

    *dateTime = dateFromPDU( pdu.mid(index, 7).toHex() );
    index+=7;

    quint8 TP_UDL = pdu[index++]; //User Data Length

    quint16 ied1= 0;
    quint8  ied2= 0;
    quint8  ied3= 0;
    bool bDecodeUDH=false;

    if ( (TP_DCS==0) || (TP_DCS==0x10) ) // Сообщение упаковано в 7 бит на символ
    {
        quint8 UDHL = 0;

        if (TP_UDHI) {
            UDHL = pdu[index]; // Длина блока UDH
            bDecodeUDH = decodeUDH(pdu.mid(index+1, UDHL), &ied1, &ied2, &ied3);

            // Ниже эквивалент формулы UDHL = (++UDHL)*8/7 с округлением вверх
            quint16 n = (++UDHL)<<3;
            UDHL = ((n/7) + ((n%7) ? 1:0));
        }

        // Ниже эквивалент формулы TP_UDL = TP_UDL*7/8 с округлением вверх
        quint16 n = TP_UDL * 7;
        TP_UDL = ((n>>3) + ((n%8) ? 1:0));

        QByteArray data = decodeGSM7bit( pdu.mid(index, TP_UDL) );
        if (UDHL) data = data.right(data.count() - UDHL);
        *msg = QString::fromLatin1(data);
    }
    else if ( (TP_DCS==8) || (TP_DCS==0x18) ) // Сообщение упаковано в кодировку UCS2
    {
        if (TP_UDHI) // User Data header Indicator
        {
            quint8 UDHL = pdu[index]; // Длина блока UDH вместе с самим байтом блинны
            bDecodeUDH = decodeUDH(pdu.mid(index+1, UDHL), &ied1, &ied2, &ied3);

            index += ++UDHL;
            TP_UDL -= UDHL;
        }

        *msg = decodeUCS2( pdu.mid(index, TP_UDL) );
    }
    else return false;

    if (bDecodeUDH){
        if (ied3==1) partsMsg[ied1] = *msg;
        else partsMsg[ied1] += *msg;

        if (ied2==ied3){
            *msg = partsMsg[ied1];
            partsMsg.remove(ied1);
        }
        else return false;
    }

    return true;
}
