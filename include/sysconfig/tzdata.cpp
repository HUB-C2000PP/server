//
// Copyright Kuzminov Sergey (kuzminov_sergey@mail.ru) 2021
//
// Distributed under the Unlicense License. (See accompanying
// file LICENSE_1_0.txt or copy at http://unlicense.org/)
//

#include "tzdata.h"

const QString tzdata::dirZone       = QStringLiteral("/usr/share/zoneinfo");
const QString tzdata::fileZone      = QStringLiteral("/etc/localtime");
const QString tzdata::fileZoneInfo  = QStringLiteral("/etc/timezone");

QString tzdata::currentRegion;
QString tzdata::currentTimeZone;

//------------------------------------------------------------------------------
QString tzdata::getTimezone(void)
{
    QString timezone;

    QFile file(fileZoneInfo);
    if ( file.open(QIODevice::ReadOnly | QIODevice::Text) )
    {
        timezone = QString::fromUtf8( file.readLine().simplified() );
        currentRegion   = timezone.section(L'/', 0, 0);
        currentTimeZone = timezone.section(L'/', 1, 1);
    }

    return timezone;
}
//------------------------------------------------------------------------------
QString tzdata::getRegion(void)
{
    if (currentRegion.isEmpty()) getTimezone();
    return currentRegion;
}
//------------------------------------------------------------------------------
QString tzdata::getTZone(void)
{
    if (currentTimeZone.isEmpty()) getTimezone();
    return currentTimeZone;
}
//------------------------------------------------------------------------------
void tzdata::setTimezone(const QString &region, const QString &tzone)
{
    currentRegion.clear();
    currentTimeZone.clear();

    QString timezone = region + L'/' + tzone;

    QFile file(fileZoneInfo);
    if (file.open(QIODevice::WriteOnly | QIODevice::Text | QIODevice::Truncate)){
        file.write( timezone.toUtf8() );
    }

    if (file.error() != QFileDevice::NoError)
        qCritical("[tzdata::setTimezone] %s: %s", qUtf8Printable(file.errorString()), qUtf8Printable(file.fileName()));

    QString command = QStringLiteral("ln -sf %1/%2 %3").arg(dirZone, timezone, fileZone);
    int r = system( command.toLocal8Bit() );
    Q_UNUSED(r)
}
//------------------------------------------------------------------------------
QStringList tzdata::getRegions(void)
{
    static QStringList lastTZones;

    if (lastTZones.isEmpty()){
        QDir dir(dirZone);
        dir.setFilter(QDir::Dirs | QDir::NoDotAndDotDot);
        lastTZones = dir.entryList();

        int index = lastTZones.indexOf( QStringLiteral("posix") );
        if (index>=0) lastTZones.removeAt(index);

        index = lastTZones.indexOf( QStringLiteral("right") );
        if (index>=0) lastTZones.removeAt(index);
    }

    return lastTZones;
}
//------------------------------------------------------------------------------
QStringList tzdata::getTZones(QString region)
{
    static QString lastRegion;
    static QStringList lastTZones;

    if (lastRegion!=region){
        lastRegion=region;
        QDir dir( QStringLiteral("%1/%2/").arg(dirZone, region) );
        dir.setFilter(QDir::Files | QDir::NoDotAndDotDot);
        lastTZones = dir.entryList();
    }

    return lastTZones;
}
