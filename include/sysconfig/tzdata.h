//
// Copyright Kuzminov Sergey (kuzminov_sergey@mail.ru) 2021
//
// Distributed under the Unlicense License. (See accompanying
// file LICENSE_1_0.txt or copy at http://unlicense.org/)
//

#ifndef TZDATA_H
#define TZDATA_H

#include <QtCore>

class tzdata
{
public:
    static QString getTimezone(void);
    static QString getRegion(void);
    static QString getTZone(void);
    static void    setTimezone(const QString &region, const QString &tzone);

    static QStringList getRegions(void);
    static QStringList getTZones (QString region);

private:
    static const QString dirZone;
    static const QString fileZone;
    static const QString fileZoneInfo;

    static QString currentRegion;
    static QString currentTimeZone;
};

#endif // TZDATA_H
