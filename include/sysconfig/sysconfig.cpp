//
// Copyright Kuzminov Sergey (kuzminov_sergey@mail.ru) 2021
//
// Distributed under the Unlicense License. (See accompanying
// file LICENSE_1_0.txt or copy at http://unlicense.org/)
//

#include "sysconfig.h"

const QString ethernet::ethNameEth0 = QStringLiteral("eth0");
const QString ethernet::ethFileName = QStringLiteral("/etc/network/interfaces");
const QString ethernet::dnsFileName = QStringLiteral("/etc/resolv.conf");
const QString ethernet::wscFileName = QStringLiteral("/var/www/config.js");

//------------------------------------------------------------------------------
void ethernet::getConfig(QString *ethAddress, QString *ethNetmask, QString *ethGateway, bool *bDHCP)
{
    ethAddress->clear();
    ethNetmask->clear();
    ethGateway->clear();

    *bDHCP=true;

    QFile ethFile(ethFileName);
    if ( ethFile.open(QIODevice::ReadOnly | QIODevice::Text) )
    {
        QString currentIface;

        while (!ethFile.atEnd())
        {
            QByteArray ba = ethFile.readLine().simplified();
            if (ba.startsWith("iface"))
            {
                QString str = QString::fromLatin1(ba);
                if (str.section(L' ', 2, 2)==QLatin1String("inet"))
                {
                    currentIface = str.section(L' ', 1, 1);
                    if ( (currentIface==ethNameEth0) && (str.section(L' ', 3, 3)==QLatin1String("static")) ) *bDHCP = false;
                }
            }
            else
            {
                if (currentIface==ethNameEth0)
                {
                    if      (ba.startsWith("address")) *ethAddress = QString::fromLatin1(ba).section(L' ', 1, 1);
                    else if (ba.startsWith("netmask")) *ethNetmask = QString::fromLatin1(ba).section(L' ', 1, 1);
                    else if (ba.startsWith("gateway")) *ethGateway = QString::fromLatin1(ba).section(L' ', 1, 1);
                }
            }
        }
    }
}
//------------------------------------------------------------------------------
void ethernet::setConfig(const QString &newData)
{
    QFile file( ethFileName + QStringLiteral(".template") );
    if (file.open(QIODevice::ReadOnly | QIODevice::Text))
    {
        QByteArray ba = file.readAll() + newData.toLatin1();
        file.close();

        file.setFileName(ethFileName);
        if (file.open(QIODevice::WriteOnly | QIODevice::Text | QIODevice::Truncate)){
            file.write(ba);
        }

        if (file.error() != QFileDevice::NoError)
            qCritical("[ethernet::setConfig] %s: %s", qUtf8Printable(file.errorString()), qUtf8Printable(file.fileName()));
    }
}
//------------------------------------------------------------------------------
QString ethernet::strConfigEth0(QString ethAddress, QString ethNetmask, QString ethGateway, bool bDHCP)
{
    QString ret = QStringLiteral("\n");
    ret += QStringLiteral("auto %1\n").arg(ethNameEth0);
    ret += QStringLiteral("iface %1 inet %2\n").arg(ethNameEth0, bDHCP ? QStringLiteral("dhcp") : QStringLiteral("static"));
    if (!bDHCP) {
        if (!ethAddress.startsWith(L'.') && !ethAddress.isEmpty()) ret += QStringLiteral("address %1\n").arg(ethAddress.simplified());
        if (!ethNetmask.startsWith(L'.') && !ethNetmask.isEmpty()) ret += QStringLiteral("netmask %1\n").arg(ethNetmask.simplified());
        if (!ethGateway.startsWith(L'.') && !ethGateway.isEmpty()) ret += QStringLiteral("gateway %1\n").arg(ethGateway.simplified());
    }
    return ret;
}
//------------------------------------------------------------------------------
void ethernet::getConfigDNS(QString *ethDNS1, QString *ethDNS2)
{
    ethDNS1->clear();
    ethDNS2->clear();

    QFile file(dnsFileName);
    if ( file.open(QIODevice::ReadOnly | QIODevice::Text) )
    {
        while (!file.atEnd())
        {
            QByteArray ba = file.readLine().simplified();
            if (ba.startsWith("nameserver"))
            {
                QString str = QString::fromLatin1(ba).section(L' ', 1, 1);
                if ( ethDNS1->isEmpty() )
                    *ethDNS1 = str;
                else if ( ethDNS2->isEmpty() ) {
                    *ethDNS2 = str;
                    break;
                }
            }
        }

        file.close();
    }
}
//------------------------------------------------------------------------------
void ethernet::setConfigDNS(QString ethDNS1, QString ethDNS2)
{
    QFile file(dnsFileName);
    if (file.open(QIODevice::ReadOnly | QIODevice::Text))
    {
        if (ethDNS1.startsWith(L'.')) ethDNS1.clear();
        if (ethDNS2.startsWith(L'.')) ethDNS2.clear();

        if (ethDNS1.isEmpty() && !ethDNS2.isEmpty()){
            ethDNS1 = ethDNS2;
            ethDNS1.clear();
        }

        QByteArray newDNS1;
        QByteArray newDNS2;

        if (!ethDNS1.isEmpty()) newDNS1 = QByteArrayLiteral("nameserver ") + ethDNS1.simplified().toLatin1() + QByteArrayLiteral("\n");
        if (!ethDNS2.isEmpty()) newDNS2 = QByteArrayLiteral("nameserver ") + ethDNS2.simplified().toLatin1() + QByteArrayLiteral("\n");

        quint8 n = 0;
        QByteArray data;
        while (!file.atEnd())
        {
            QByteArray ba = file.readLine();
            if (ba.simplified().startsWith("nameserver")){
                if (n==0){
                    ba = newDNS1;
                    n=1;
                }
                else if (n==1){
                    ba = newDNS2;
                    n=2;
                }
                else ba.clear();
            }

            data += ba;
        }

        if (n==0){ data += QByteArrayLiteral("\n") + newDNS1; n=1;}
        if (n==1)  data += newDNS2;

        file.close();

        if (file.open(QIODevice::WriteOnly | QIODevice::Text | QIODevice::Truncate)){
            file.write(data);
            file.close();
        }
    }
}
//------------------------------------------------------------------------------
void ethernet::setConfigWS(QString ethAddress)
{
    QFile file(wscFileName);
    if (file.open(QIODevice::ReadOnly | QIODevice::Text))
    {
        QByteArray data;

        while (!file.atEnd()) {
            QByteArray ba = file.readLine();
            QByteArray temp = ba.simplified();
            if (temp.startsWith("wsUri")) ba = QByteArrayLiteral("wsUri = \"")+ ethAddress.toLatin1() + QByteArrayLiteral("\";\r\n");
            else if (temp.startsWith("wssUri")) ba = QByteArrayLiteral("wssUri = \"")+ ethAddress.toLatin1() + QByteArrayLiteral("\";\r\n");

            data += ba;
        }

        file.close();

        if (file.open(QIODevice::WriteOnly | QIODevice::Text | QIODevice::Truncate)){
            file.write( data );
            file.close();
        }
    }
}

