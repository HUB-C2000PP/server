//
// Copyright Kuzminov Sergey (kuzminov_sergey@mail.ru) 2021
//
// Distributed under the Unlicense License. (See accompanying
// file LICENSE_1_0.txt or copy at http://unlicense.org/)
//

#ifndef SYSCONFIG_H
#define SYSCONFIG_H

#include <QtCore>

class ethernet
{
public:
    static void setConfig(const QString &newData);
    static void getConfig(QString *ethAddress, QString *ethNetmask, QString *ethGateway, bool *bDHCP);

    static void setConfigDNS(QString  ethDNS1, QString  ethDNS2);
    static void getConfigDNS(QString *ethDNS1, QString *ethDNS2);

    static void setConfigWS(QString ethAddress);

    static QString strConfigEth0(QString ethAddress, QString  ethNetmask, QString  ethGateway, bool bDHCP);

    static const QString ethNameEth0;
    static const QString ethFileName;
    static const QString dnsFileName;
    static const QString wscFileName;
};

#endif // SYSCONFIG_H
