/****************************************************************************
**
** Copyright (C) 2017 The Qt Company Ltd.
** Contact: http://www.qt.io/licensing/
**
** This file is part of the QtSerialBus module of the Qt Toolkit.
**
** $QT_BEGIN_LICENSE:LGPL3$
** Commercial License Usage
** Licensees holding valid commercial Qt licenses may use this file in
** accordance with the commercial license agreement provided with the
** Software or, alternatively, in accordance with the terms contained in
** a written agreement between you and The Qt Company. For licensing terms
** and conditions see http://www.qt.io/terms-conditions. For further
** information use the contact form at http://www.qt.io/contact-us.
**
** GNU Lesser General Public License Usage
** Alternatively, this file may be used under the terms of the GNU Lesser
** General Public License version 3 as published by the Free Software
** Foundation and appearing in the file LICENSE.LGPLv3 included in the
** packaging of this file. Please review the following information to
** ensure the GNU Lesser General Public License version 3 requirements
** will be met: https://www.gnu.org/licenses/lgpl.html.
**
** GNU General Public License Usage
** Alternatively, this file may be used under the terms of the GNU
** General Public License version 2.0 or later as published by the Free
** Software Foundation and appearing in the file LICENSE.GPL included in
** the packaging of this file. Please review the following information to
** ensure the GNU General Public License version 2.0 requirements will be
** met: http://www.gnu.org/licenses/gpl-2.0.html.
**
** $QT_END_LICENSE$
**
****************************************************************************/

#include "ModbusRtuC2000EthernetMaster.h"

//-----------------------------------------------------------------------------
ModbusRtuC2000EthernetMaster::ModbusRtuC2000EthernetMaster(QObject *parent) : QModbusClient(parent)
{
    m_sendTimer.setSingleShot(true);
    QObject::connect(&m_sendTimer, &QTimer::timeout, this, [this]() { processQueue(); });

    m_socket = new QUdpSocket(this);
    QObject::connect(m_socket, &QUdpSocket::readyRead, this, [this]()
    {
        //---------------
        // C2000-Ethernet
        //---------------
        QByteArray datagram = m_socket->readAll();
        if (datagram.count()>5)
            if ( (static_cast<quint8>(datagram[0]) == 0x10) &&
                 (static_cast<quint8>(datagram[1])+5 == datagram.count()) &&
                 ((datagram[4]&0x0f)==0))
                responseBuffer = datagram.right(datagram.count()-5);

        if (responseBuffer.size() < 2) return;

        const QModbusSerialAdu tmpAdu(QModbusSerialAdu::Rtu, responseBuffer);
        int pduSizeWithoutFcode = QModbusResponse::calculateDataSize(tmpAdu.pdu());
        if (pduSizeWithoutFcode < 0) return;

        // server address byte + function code byte + PDU size + 2 bytes CRC
        int aduSize = 2 + pduSizeWithoutFcode + 2;
        if (tmpAdu.rawSize() < aduSize) return;

        // Special case for Diagnostics:ReturnQueryData. The response has no
        // length indicator and is just a simple echo of what we have send.
        if (tmpAdu.pdu().functionCode() == QModbusPdu::Diagnostics) {
            const QModbusResponse response = tmpAdu.pdu();
            if (canMatchRequestAndResponse(response, tmpAdu.serverAddress())) {
                quint16 subCode = 0xffff;
                response.decodeData(&subCode);
                if (subCode == 0) {
                    if (response.data() != m_current.requestPdu.data())
                        return; // echo does not match request yet
                    aduSize = 2 + response.dataSize() + 2;
                    if (tmpAdu.rawSize() < aduSize)
                        return; // echo matches, probably checksum missing
                }
            }
        }

        const QModbusSerialAdu adu(QModbusSerialAdu::Rtu, responseBuffer.left(aduSize));

        // check CRC
        if (!adu.matchingChecksum()) return;

        const QModbusResponse response = adu.pdu();
        if (!canMatchRequestAndResponse(response, adu.serverAddress())) return;

        if (m_state != Receive) return;

        m_sendTimer.stop();
        processQueueElement(response, m_current);

        m_state = Schedule; // reschedule, even if empty
        processQueue();
    });

#if QT_VERSION < QT_VERSION_CHECK(5, 15, 0)
    using TypeId = void (QUdpSocket::*)(QUdpSocket::SocketError);
    QObject::connect(m_socket, static_cast<TypeId>(&QUdpSocket::error), [this](QUdpSocket::SocketError /*error*/)
#else
    QObject::connect(m_socket, &QUdpSocket::errorOccurred, this, [this](QUdpSocket::SocketError /*error*/)
#endif
    {
        if (m_socket->state() == QAbstractSocket::UnconnectedState) setState(QModbusDevice::UnconnectedState);
        setError(QModbusClient::tr("UDP socket error (%1).").arg(m_socket->errorString()), QModbusDevice::ConnectionError);
    });

    QObject::connect(m_socket, &QUdpSocket::bytesWritten, this, [this](qint64 bytes) {
        m_current.bytesWritten += bytes-5; // For C2000-Ethernet
        if (m_state == Send && (m_current.bytesWritten == m_current.adu.size()) && !m_current.reply.isNull()) {
            m_state = Receive;
            m_sendTimer.stop();
        }
    });

    QObject::connect(m_socket, &QUdpSocket::aboutToClose, this, [this]()
    {
        Q_ASSERT(state() == QModbusDevice::ClosingState);
        m_sendTimer.stop();
    });
}
//-----------------------------------------------------------------------------
ModbusRtuC2000EthernetMaster::~ModbusRtuC2000EthernetMaster()
{
    close();
}
//-----------------------------------------------------------------------------
bool ModbusRtuC2000EthernetMaster::open()
{
    if (state() == QModbusDevice::ConnectedState) return true;

    m_state = Idle;

    quint16 port = static_cast<quint16>(connectionParameter(NetworkPortParameter).toInt());
    if ( m_socket->bind(port) )
    {
        m_socket->connectToHost(connectionParameter(NetworkAddressParameter).toString(), port);
        setState(QModbusDevice::ConnectedState);
    }
    else setError(m_socket->errorString(), QModbusDevice::ConnectionError);

    return (state() == QModbusDevice::ConnectedState);
}
//-----------------------------------------------------------------------------
void ModbusRtuC2000EthernetMaster::close()
{
    if (state() == QModbusDevice::UnconnectedState) return;

    setState(QModbusDevice::ClosingState);

    if (m_socket->isOpen()) m_socket->close();

    // enqueue current active request back for abortion
    m_queue.enqueue(m_current);
    m_current = QueueElement();

    int numberOfAborts = 0;
    while (!m_queue.isEmpty()) {
        // Finish each open reply and forget them
        QueueElement elem = m_queue.dequeue();
        if (!elem.reply.isNull()) {
            elem.reply->setError(QModbusDevice::ReplyAbortedError, QModbusClient::tr("Reply aborted due to connection closure."));
            numberOfAborts++;
        }
    }

    setState(QModbusDevice::UnconnectedState);
}
//-----------------------------------------------------------------------------
bool ModbusRtuC2000EthernetMaster::isOpen()
{
    if (m_socket) if (m_socket->isOpen()) return true;
    return false;
}
//-----------------------------------------------------------------------------
bool ModbusRtuC2000EthernetMaster::canMatchRequestAndResponse(const QModbusResponse &response, int sendingServer) const
{
    if (m_current.reply.isNull())
        return false;   // reply deleted
    if (m_current.reply->serverAddress() != sendingServer)
        return false;   // server mismatch
    if (m_current.requestPdu.functionCode() != response.functionCode())
        return false;   // request for different function code
    return true;
}
//-----------------------------------------------------------------------------
void ModbusRtuC2000EthernetMaster::scheduleNextRequest() {
    m_state = Schedule;
    processQueue();
}
//-----------------------------------------------------------------------------
void ModbusRtuC2000EthernetMaster::processQueueElement(const QModbusResponse &pdu, const QueueElement &element)
{
    element.reply->setRawResult(pdu);
    if (pdu.isException()) {
        element.reply->setError(QModbusDevice::ProtocolError, QModbusClient::tr("Modbus Exception Response."));
        return;
    }

    if (element.reply->type() == QModbusReply::Raw) {
        element.reply->setFinished(true);
        return;
    }

    QModbusDataUnit unit = element.unit;
    if (!processResponse(pdu, &unit)) {
        element.reply->setError(QModbusDevice::UnknownError, QModbusClient::tr("An invalid response has been received."));
        return;
    }

    element.reply->setResult(unit);
    element.reply->setFinished(true);
}
//-----------------------------------------------------------------------------
void ModbusRtuC2000EthernetMaster::processQueue()
{
    Q_ASSERT_X(!m_sendTimer.isActive(), "processQueue", "send timer active");

    switch (m_state) {
    case Schedule:
        m_current = QueueElement();
        if (!m_queue.isEmpty()) {
            m_current = m_queue.dequeue();
            if (m_current.reply) {
                m_state = Send;
                writeAdu();
            } else processQueue();
        } else m_state = Idle;
        break;

    case Send:
        // send timeout will always happen unless canceled by very quick bytesWritten
        if (m_current.reply.isNull()) scheduleNextRequest();
        else if (m_current.bytesWritten < m_current.adu.size())
        {
            if (m_current.numberOfRetries <= 0) {
                if (m_current.reply) m_current.reply->setError(QModbusDevice::TimeoutError, QModbusClient::tr("Request timeout."));
                m_current = QueueElement();
                scheduleNextRequest();
            } else writeAdu();
        } else m_state = Receive;
        break;

    case Receive:
        // receive timeout will only happen after successful send
        if (m_current.reply.isNull()) scheduleNextRequest();
        else if (m_current.numberOfRetries <= 0) {
            if (m_current.reply) m_current.reply->setError(QModbusDevice::TimeoutError, QModbusClient::tr("Response timeout."));
            scheduleNextRequest();
        } else {
            m_state = Send;
            writeAdu();
        }
        break;

    case Idle:
    default:
        Q_ASSERT_X(false, "processQueue", QByteArray("unexpected state: ").append(m_state));
        break;
    }
}
//-----------------------------------------------------------------------------
void ModbusRtuC2000EthernetMaster::writeAdu()
{
    m_current.bytesWritten = 0;
    m_current.numberOfRetries--;

    //---------------
    // C2000-Ethernet
    //---------------
    QByteArray sendBuff(5,0);
    sendBuff[0] = 0x10;
    sendBuff[1] = static_cast<char>(m_current.adu.count());
    sendBuff[2] = static_cast<char>(++countNumberUDP >> 8);
    sendBuff[3] = static_cast<char>(countNumberUDP);
    sendBuff[4] = 0x10;
    sendBuff   += m_current.adu;
    //---------------

    m_socket->write(sendBuff);
    m_sendTimer.start(timeout());
}
//-----------------------------------------------------------------------------
QModbusRequest ModbusRtuC2000EthernetMaster::createReadRequest(const QModbusDataUnit &data)
{
    if (!data.isValid()) return QModbusRequest();

    switch (data.registerType()) {
    case QModbusDataUnit::Coils:
        return QModbusRequest(QModbusRequest::ReadCoils, quint16(data.startAddress()),            quint16(data.valueCount()));
    case QModbusDataUnit::DiscreteInputs:
        return QModbusRequest(QModbusRequest::ReadDiscreteInputs, quint16(data.startAddress()),   quint16(data.valueCount()));
    case QModbusDataUnit::InputRegisters:
        return QModbusRequest(QModbusRequest::ReadInputRegisters, quint16(data.startAddress()),   quint16(data.valueCount()));
    case QModbusDataUnit::HoldingRegisters:
        return QModbusRequest(QModbusRequest::ReadHoldingRegisters, quint16(data.startAddress()), quint16(data.valueCount()));
    default:
        break;
    }

    return QModbusRequest();
}
//-----------------------------------------------------------------------------
QModbusRequest ModbusRtuC2000EthernetMaster::createWriteRequest(const QModbusDataUnit &data)
{
    switch (data.registerType()) {
    case QModbusDataUnit::Coils: {
        if (data.valueCount() == 1) {
            return QModbusRequest(QModbusRequest::WriteSingleCoil, quint16(data.startAddress()),
                                  quint16((data.value(0) == 0u) ? Coil::Off : Coil::On));
        }

        quint8 byteCount = static_cast<quint8>(data.valueCount() / 8);
        if ((data.valueCount() % 8) != 0)
            byteCount += 1;

        quint8 address = 0;
        QVector<quint8> bytes;
        for (quint8 i = 0; i < byteCount; ++i) {
            quint8 byte = 0;
            for (int currentBit = 0; currentBit < 8; ++currentBit)
                if (data.value(address++))
                    byte |= (1U << currentBit);
            bytes.append(byte);
        }

        return QModbusRequest(QModbusRequest::WriteMultipleCoils, quint16(data.startAddress()), quint16(data.valueCount()), byteCount, bytes);
    }

    case QModbusDataUnit::HoldingRegisters: {
        if (data.valueCount() == 1) {
            return QModbusRequest(QModbusRequest::WriteSingleRegister, quint16(data.startAddress()),
                                  data.value(0));
        }

        const quint8 byteCount = static_cast<quint8>(data.valueCount() * 2);
        return QModbusRequest(QModbusRequest::WriteMultipleRegisters, quint16(data.startAddress()), quint16(data.valueCount()), byteCount, data.values());
    }

    case QModbusDataUnit::DiscreteInputs:
    case QModbusDataUnit::InputRegisters:
    default:    // fall through on purpose
        break;
    }
    return QModbusRequest();
}
//-----------------------------------------------------------------------------
QModbusRequest ModbusRtuC2000EthernetMaster::createRWRequest(const QModbusDataUnit &read, const QModbusDataUnit &write)
{
    if ((read.registerType() != QModbusDataUnit::HoldingRegisters)
            && (write.registerType() != QModbusDataUnit::HoldingRegisters)) {
        return QModbusRequest();
    }

    const quint8 byteCount = static_cast<quint8>(write.valueCount() * 2);
    return QModbusRequest(QModbusRequest::ReadWriteMultipleRegisters, quint16(read.startAddress()),
                          quint16(read.valueCount()), quint16(write.startAddress()),
                          quint16(write.valueCount()), byteCount, write.values());
}
//-----------------------------------------------------------------------------
QModbusReply *ModbusRtuC2000EthernetMaster::sendReadRequest(const QModbusDataUnit &read, int serverAddress)
{
    return sendRequest(createReadRequest(read), serverAddress, &read);
}
//-----------------------------------------------------------------------------
QModbusReply *ModbusRtuC2000EthernetMaster::sendWriteRequest(const QModbusDataUnit &write, int serverAddress)
{
    return sendRequest(createWriteRequest(write), serverAddress, &write);
}
//-----------------------------------------------------------------------------
QModbusReply *ModbusRtuC2000EthernetMaster::sendReadWriteRequest(const QModbusDataUnit &read, const QModbusDataUnit &write, int serverAddress)
{
    return sendRequest(createRWRequest(read, write), serverAddress, &read);
}
//-----------------------------------------------------------------------------
QModbusReply *ModbusRtuC2000EthernetMaster::sendRawRequest(const QModbusRequest &request, int serverAddress)
{
    return sendRequest(request, serverAddress, nullptr);
}
//-----------------------------------------------------------------------------
QModbusReply *ModbusRtuC2000EthernetMaster::sendRequest(const QModbusRequest &request, int serverAddress, const QModbusDataUnit *const unit)
{
    if (!isOpen() || state() != QModbusDevice::ConnectedState) {
        setError(QModbusClient::tr("Device not connected."), QModbusDevice::ConnectionError);
        return nullptr;
    }

    if (!request.isValid()) {
        setError(QModbusClient::tr("Invalid Modbus request."), QModbusDevice::ProtocolError);
        return nullptr;
    }

    if (unit) return enqueueRequest(request, serverAddress, *unit, QModbusReply::Common);
    return enqueueRequest(request, serverAddress, QModbusDataUnit(), QModbusReply::Raw);
}
//-----------------------------------------------------------------------------
QModbusReply *ModbusRtuC2000EthernetMaster::enqueueRequest(const QModbusRequest &request, int serverAddress, const QModbusDataUnit &unit, QModbusReply::ReplyType type)
{
    auto reply = new QModbusReply(type, serverAddress, this);

    QueueElement element(reply, request, unit, numberOfRetries() + 1);
    element.adu = QModbusSerialAdu::create(QModbusSerialAdu::Rtu, serverAddress, request);
    m_queue.enqueue(element);

    if (m_state == Idle) scheduleNextRequest();

    return reply;
}
