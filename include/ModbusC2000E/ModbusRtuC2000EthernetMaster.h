/****************************************************************************
**
** Copyright (C) 2017 The Qt Company Ltd.
** Contact: http://www.qt.io/licensing/
**
** This file is part of the QtSerialBus module of the Qt Toolkit.
**
** $QT_BEGIN_LICENSE:LGPL3$
** Commercial License Usage
** Licensees holding valid commercial Qt licenses may use this file in
** accordance with the commercial license agreement provided with the
** Software or, alternatively, in accordance with the terms contained in
** a written agreement between you and The Qt Company. For licensing terms
** and conditions see http://www.qt.io/terms-conditions. For further
** information use the contact form at http://www.qt.io/contact-us.
**
** GNU Lesser General Public License Usage
** Alternatively, this file may be used under the terms of the GNU Lesser
** General Public License version 3 as published by the Free Software
** Foundation and appearing in the file LICENSE.LGPLv3 included in the
** packaging of this file. Please review the following information to
** ensure the GNU Lesser General Public License version 3 requirements
** will be met: https://www.gnu.org/licenses/lgpl.html.
**
** GNU General Public License Usage
** Alternatively, this file may be used under the terms of the GNU
** General Public License version 2.0 or later as published by the Free
** Software Foundation and appearing in the file LICENSE.GPL included in
** the packaging of this file. Please review the following information to
** ensure the GNU General Public License version 2.0 requirements will be
** met: http://www.gnu.org/licenses/gpl-2.0.html.
**
** $QT_END_LICENSE$
**
****************************************************************************/

#ifndef QMODBUSRTUC2000ETHERNET_H
#define QMODBUSRTUC2000ETHERNET_H

#include <QUdpSocket>
#include <QModbusClient>
#include <QtCore/qpointer.h>
#include <QtCore/qqueue.h>
#include <QtCore/qvariant.h>
#include <QtCore/qtimer.h>

#include "QModbusSerialAdu.h"

QT_BEGIN_NAMESPACE

class ModbusRtuC2000EthernetMaster : public QModbusClient
{
    enum State {
        Idle,
        Schedule,
        Send,
        Receive,
    } m_state = Idle;

    enum Coil {
        On = 0xff00,
        Off = 0x0000
    };

    struct QueueElement {
        QueueElement() = default;
        QueueElement(QModbusReply *r, const QModbusRequest &req, const QModbusDataUnit &u, int num, int timeout = -1)
            : reply(r), requestPdu(req), unit(u), numberOfRetries(num)
        {
            if (timeout >= 0) { // always the case for TCP
                timer = QSharedPointer<QTimer>::create();
                timer->setSingleShot(true);
                timer->setInterval(timeout);
            }
        }
        bool operator==(const QueueElement &other) const { return reply == other.reply; }

        QPointer<QModbusReply> reply;
        QModbusRequest requestPdu;
        QModbusDataUnit unit;
        int numberOfRetries;
        QSharedPointer<QTimer> timer;
        QByteArray adu;
        qint64 bytesWritten = 0;
    };

private:
    QTimer m_sendTimer;

    QueueElement m_current;
    QByteArray responseBuffer;

    QQueue<QueueElement> m_queue;
    QUdpSocket *m_socket = nullptr;
    quint16 countNumberUDP = 0;

    void writeAdu();
    void processQueue();
    bool canMatchRequestAndResponse(const QModbusResponse &response, int sendingServer) const;
    void processQueueElement(const QModbusResponse &pdu, const QueueElement &element);
    QModbusReply *enqueueRequest(const QModbusRequest &request, int serverAddress, const QModbusDataUnit &unit, QModbusReply::ReplyType type); // override;

    void scheduleNextRequest();

    QModbusRequest createReadRequest(const QModbusDataUnit &data);
    QModbusRequest createWriteRequest(const QModbusDataUnit &data);
    QModbusRequest createRWRequest(const QModbusDataUnit &read, const QModbusDataUnit &write);

public:
    explicit ModbusRtuC2000EthernetMaster(QObject *parent = nullptr);
    ~ModbusRtuC2000EthernetMaster() override;

    bool isOpen(); // override;

    QModbusReply *sendRequest(const QModbusRequest &request, int serverAddress, const QModbusDataUnit *const unit);
    QModbusReply *sendReadRequest (const QModbusDataUnit &read, int serverAddress);
    QModbusReply *sendWriteRequest(const QModbusDataUnit &write, int serverAddress);
    QModbusReply *sendReadWriteRequest(const QModbusDataUnit &read, const QModbusDataUnit &write, int serverAddress);
    QModbusReply *sendRawRequest(const QModbusRequest &request, int serverAddress);

protected:
    void close() override;
    bool open() override;
};

QT_END_NAMESPACE

#endif // QMODBUSRTUC2000ETHERNET_H
