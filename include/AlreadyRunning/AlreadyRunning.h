//
// Copyright Kuzminov Sergey (kuzminov_sergey@mail.ru) 2021
//
// Distributed under the Unlicense License. (See accompanying
// file LICENSE_1_0.txt or copy at http://unlicense.org/)
//

#ifndef ALREADYRUNNING_H
#define ALREADYRUNNING_H

#include <QByteArray>
#include <QSharedMemory>
#include <QSystemSemaphore>
#include <QCoreApplication>
#include <QCryptographicHash>

class AlreadyRunning
{
private:
    bool running=false;
    QSharedMemory shmem;

public:
    AlreadyRunning(void);
    bool isRunning(void) const {return running;}
};

#endif // ALREADYRUNNING_H
