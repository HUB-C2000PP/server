//
// Copyright Kuzminov Sergey (kuzminov_sergey@mail.ru) 2021
//
// Distributed under the Unlicense License. (See accompanying
// file LICENSE_1_0.txt or copy at http://unlicense.org/)
//

#include "AlreadyRunning.h"

AlreadyRunning::AlreadyRunning(void)
{
    QByteArray hashName = QCoreApplication::applicationFilePath().
        #ifdef Q_OS_WIN
            toUpper().
        #endif
            toUtf8();

    hashName = QCryptographicHash::hash(hashName, QCryptographicHash::Sha3_256).toHex();
    QString smemName = QString::fromLatin1(hashName) + QStringLiteral("smem");

    QSystemSemaphore sema(QString::fromLatin1(hashName) + QStringLiteral("sem"), 1);
    sema.acquire();

    // Для linux, чтобы удалить QSharedMemory после неудачного завершения программы.
#ifdef Q_OS_UNIX
    {
        QSharedMemory shmem(smemName);
        if (shmem.attach()) shmem.detach();
    }
#endif

    shmem.setKey(smemName);
    if (shmem.attach()) {
        shmem.detach();
        running = true;
    }
    else {
        shmem.create(1);
        running = false;
    }

    sema.release();
}
