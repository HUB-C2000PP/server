//
// Copyright Kuzminov Sergey (kuzminov_sergey@mail.ru) 2021
//
// Distributed under the Unlicense License. (See accompanying
// file LICENSE_1_0.txt or copy at http://unlicense.org/)
//

#include <QDebug>
#include <QCoreApplication>
#include "include/AlreadyRunning/AlreadyRunning.h"
#include "include/LogFiles/LogFiles.h"
#include "include/Modbus/Modbus.h"
#include "include/DB/SimpleFileBA.h"
#include "include/DB/SimpleFileDB.h"
#include "include/HUB-Config/HubConfig.h"
#include "include/HUB-Monitoring/HubMonitoring.h"

#include "include/Net/LanDef.h"
#include "include/Net/CanalLocal/CanalLocalServer.h"
#include "include/Net/CanalSTcp/CanalSTcpServer.h"

quint16 applicationVersion;
QString mainPath;
const QDataStream::Version dataStreamVersion = QDataStream::Qt_5_15;

int main(int argc, char *argv[])
{
    qRegisterMetaType <qintptr> ("qintptr"); //Удивительно, но в Qt 5.14.1 (по крайней мере) без этого не работает QObject::connect

    //------------------------------------------------------------------
    // Название и версия программы
    //------------------------------------------------------------------
    QCoreApplication a(argc, argv);
    a.setOrganizationName  ( QStringLiteral("www.arm-skif.ru") );
    a.setOrganizationDomain( QStringLiteral("www.arm-skif.ru") );
    a.setApplicationVersion( QStringLiteral("1.0.2") );
    applicationVersion = 100; // Для передачи по сети. 100==1.00

    //------------------------------------------------------------------
    // Путь к программе
    //------------------------------------------------------------------
    mainPath = QCoreApplication::applicationDirPath();

    //------------------------------------------------------------------
    // Настройка кодировки консоли Windows для правильной работы
    // QString::toLocal8Bit() - применяется при выводе в консоль при
    // помощи: qDebug(). qWarning(), qInfo(), qCritical(), qFatal()
    //------------------------------------------------------------------
#if QT_VERSION < QT_VERSION_CHECK(6, 0, 0)
#ifdef Q_OS_WIN
    if (QLocale::system().country() == QLocale::RussianFederation)
        QTextCodec::setCodecForLocale(QTextCodec::codecForName("IBM 866"));
#else
    if (QLocale::system().country() == QLocale::AnyCountry)
        QTextCodec::setCodecForLocale(QTextCodec::codecForName("UTF-8"));
#endif
#endif

    //------------------------------------------------------------------
    // Для работы с файлами лога
    //------------------------------------------------------------------
    logfiles::init();

    // Включение журналов modbus
    //QLoggingCategory::setFilterRules(QStringLiteral("qt.modbus.lowlevel = true"));
    //QLoggingCategory::setFilterRules(QStringLiteral("qt.modbus = true"));
    //QLoggingCategory::setFilterRules(QStringLiteral("qt.modbus* = true"));

    //------------------------------------------------------------------
    // Файл перевода
    //------------------------------------------------------------------
#ifdef Embedded
    const QLocale locale = QLocale(QLocale::Russian, QLocale::Russia);

#if QT_VERSION < QT_VERSION_CHECK(6, 0, 0)
    QString translationsPath = QStringLiteral("/usr/share/qt5/translations");
#else
    QString translationsPath = QStringLiteral("/usr/share/qt6/translations");
#endif

#else//no Embedded
    const QLocale locale = QLocale::system();

#ifdef Q_OS_WIN
    QString translationsPath = QStringLiteral("%1/plugins/translations").arg(mainPath);
#else
    QString translationsPath = QLibraryInfo::location(QLibraryInfo::TranslationsPath);
#endif

#endif

    QLocale::setDefault(locale);

    //Варианты: qtbase, qtserialport, qtscript, qtwebsockets, qtmultimedia
    const QStringList listTranslator = {QStringLiteral("qtbase"), QStringLiteral("qtserialport")};

    for (const QString &strTranslator : listTranslator){
        QTranslator *translator = new QTranslator(qApp);
        if (translator->load(locale, strTranslator, QStringLiteral("_"), translationsPath))
            qApp->installTranslator(translator);
    }

    //------------------------------------------------------------------
    // Определяем запущено ли уже приложение
    //------------------------------------------------------------------
    AlreadyRunning alreadyRunning;
    if (alreadyRunning.isRunning()) {
        qCritical("Попытка запустить вторую копию программы");
        return -1;
    }

    // ----------------------------------------
    // Работа с С2000-ПП
    // ----------------------------------------
    // Состояния шлейфов сигнализации
    StateC2000PP stateC2000PP;

    // База данных событий
    SimpleFileDB simpleFileDB;
    simpleFileDB.openFile(mainPath + constFileNameBD, EventC2000PP::size());

    // Логика работы с прибором С2000-ПП
    C2000PP objC2000PP;
    objC2000PP.loadFile(mainPath + constFileNameConfigPP);  // Конфигурация прибора С2000-ПП
    objC2000PP.loadFileDescription(mainPath + constFileDescriptions); // Описания Шс/Рл/Разделов

    // Особенности работы с разными портами Modbus (serial, С2000-Ethernet, TCP)
    Modbus objModbus;
    objModbus.registration(&objC2000PP);
    objModbus.loadConfig();

    //------------------------------------------------------------------
    // Сервер TCP (QTcpServer).
    // Через одно подключение работают независимо несколько объектов.
    //------------------------------------------------------------------
    CanalSTcpServer canaSlTcpServer;
    canaSlTcpServer.setLogName(QStringLiteral("CanalSTcpServer.log"));

    //------------------------------------------------------------------
    // Сервер локальных подключений (QLocalServer).
    // Через одно подключение работают независимо несколько объектов.
    //------------------------------------------------------------------
    CanalLocalServer canalLocalServer;
    canalLocalServer.setLogName( QStringLiteral("CanalLocalServer.log") );
    canalLocalServer.setAddress( CanalLocalServer::makeCurrentPathAddress("db", "db") );

    //------------------------------------------------------------------
    // Взаимодействие с сетевыми клиентами
    //------------------------------------------------------------------
    // Объект управления
    HubConfig hubConfig;
    hubConfig.setDataObject(&objC2000PP, &objModbus, &simpleFileDB, &stateC2000PP);

    // Объект мониторинка
    HubMonitoring hubMonitoring;
    hubMonitoring.setDataObject(&objC2000PP, &objModbus, &simpleFileDB, &stateC2000PP);

    canaSlTcpServer. registration(hubConfig.objServerLinkTcp(),       1); // Объект управления
    canaSlTcpServer. registration(hubMonitoring.objServerLinkTcp(),   2); // Объект мониторинга
    canalLocalServer.registration(hubConfig.objServerLinkLocal(),     1); // Объект управления
    canalLocalServer.registration(hubMonitoring.objServerLinkLocal(), 2); // Объект мониторинга

    QObject::connect(&objC2000PP,&C2000PP::signalNewConfigC2000PP,   &hubMonitoring, &HubMonitoring::slotNewConfigC2000PP);
    QObject::connect(&objC2000PP,&C2000PP::signalNewConfigC2000PP,   &objModbus, &Modbus::reconnect);

    QObject::connect(&hubConfig, &HubConfig::signalNewConfigC2000PP, &hubMonitoring, &HubMonitoring::slotNewConfigC2000PP);
    QObject::connect(&hubConfig, &HubConfig::signalNewDescriptions,  &hubMonitoring, &HubMonitoring::slotNewDescriptions);

    QObject::connect(&hubConfig, &HubConfig::setAddress, &canaSlTcpServer, &CanalSTcpServer::setAddress);
    QObject::connect(&hubConfig, &HubConfig::addLogin,   &canaSlTcpServer, &CanalSTcpServer::addLogin);
    hubConfig.loadNetConfig();
    //------------------------------------------------------------------

    return a.exec();
}
