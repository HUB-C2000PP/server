VERSION = 1.0.2
#TARGET =

# Расширенные предупреждения компилятора
# https://habr.com/ru/post/490850
CFLAGS   += -Werror -pedantic-errors
CXXFLAGS += -Werror -pedantic-errors
CFLAGS   += -Wall -Wextra -Wpedantic
CXXFLAGS += -Wall -Wextra -Wpedantic
CFLAGS   += -Wcast-align -Wcast-qual -Wconversion -Wctor-dtor-privacy -Wduplicated-branches -Wduplicated-cond -Wextra-semi -Wfloat-equal -Wlogical-op -Wnon-virtual-dtor -Wold-style-cast -Woverloaded-virtual -Wredundant-decls -Wsign-conversion -Wsign-promo
CXXFLAGS += -Wcast-align -Wcast-qual -Wconversion -Wctor-dtor-privacy -Wduplicated-branches -Wduplicated-cond -Wextra-semi -Wfloat-equal -Wlogical-op -Wnon-virtual-dtor -Wold-style-cast -Woverloaded-virtual -Wredundant-decls -Wsign-conversion -Wsign-promo

CONFIG(release, debug|release){

# Если размер сильно критичен, то отключаем оптимизацию O2 и включаем оптимизацию Os. В win несовместимо с flto.
#QMAKE_CFLAGS_RELEASE   -= -O2
#QMAKE_CXXFLAGS_RELEASE -= -O2
#QMAKE_CFLAGS   += -Os
#QMAKE_CXXFLAGS += -Os

unix{
    # Оптимизация всех файлов как единого целого и удаление неиспользуемых функций. Очень долго, но очень эффективно. В win не всегда собирается с этим.
    QMAKE_CFLAGS   += -flto
    QMAKE_CXXFLAGS += -flto
    QMAKE_LFLAGS   += -flto

    # Удаляет символьную информацию. Очень эффективно. В windows аналог "-s -Wl" включен по умолчанию.
    QMAKE_LFLAGS += -Wl,--strip-all
}

win32{
    # Удаляет неиспользуемые секции. Если включено flto, то это можно отключить, т.к нет эффекта. flto эффективнее.
    QMAKE_CFLAGS   += -ffunction-sections -fdata-sections
    QMAKE_CXXFLAGS += -ffunction-sections -fdata-sections
    QMAKE_LFLAGS   += -Wl,--gc-sections
}

    # Отключить qDebug()
    DEFINES += QT_NO_DEBUG_OUTPUT QT_NO_DEBUG_STREAM
    DEFINES += QT_NO_WARNING_OUTPUT QT_NO_WARNING_STREAM
}

#DEFINES += MODBUS_DEBUG

# Полезно для выявления неправильного преобразования строк
DEFINES += QT_NO_CAST_FROM_ASCII
DEFINES += QT_NO_CAST_TO_ASCII

# The following define makes your compiler emit warnings if you use
# any Qt feature that has been marked deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if it uses deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060200    # disables all the APIs deprecated before Qt 6.2.0

#-----------------------------------------
QT -= gui
QT += network serialport serialbus

CONFIG -= console
CONFIG -= app_bundle
CONFIG += c++17
QMAKE_CXXFLAGS += -std=c++17

#-----------------------------------------
SOURCES += \
    include/Broadcast/broadcastlistener.cpp \
    include/C2000PP/EventsC2000PP/EventC2000PP.cpp \
    include/C2000PP/EventsStatic/EventsStatic.cpp \
    include/C2000PP/StateC2000PP/StateC2000PP.cpp \
    include/GOST/GOST14.cpp \
    include/GsmSMS/GsmSMS.cpp \
    include/HUB-Config/HubConfig.cpp \
    include/HUB-Monitoring/HubMonitoring.cpp \
    include/Modbus/Modbus.cpp \
    include/ModbusC2000E/ModbusRtuC2000EthernetMaster.cpp \
    include/DB/SimpleFileBA.cpp \
    include/DB/SimpleFileDB.cpp \
    include/Net/CanalLocal/CanalLocalServer.cpp \
    include/Net/CanalSTcp/CanalSTcpServer.cpp \
    include/C2000PP/c2000pp.cpp \
    include/C2000PP/ConfigC2000PP/ConfigC2000PP.cpp \
    include/AlreadyRunning/AlreadyRunning.cpp \
    include/LogFiles/LogFiles.cpp \
    include/SMTP/simpleSmtp.cpp \
    include/SMTP/smtp.cpp \
    include/Telegram/TlgProcess.cpp \
    main.cpp


HEADERS += \
    include/Broadcast/broadcastlistener.h \
    include/C2000PP/EventsC2000PP/EventC2000PP.h \
    include/C2000PP/EventsStatic/EventsStatic.h \
    include/C2000PP/StateC2000PP/StateC2000PP.h \
    include/GOST/GOST14.h \
    include/GOST/table.h \
    include/GsmSMS/GsmSMS.h \
    include/HUB-Config/HubConfig.h \
    include/HUB-Config/LanDefConfig.h \
    include/HUB-Monitoring/HubMonitoring.h \
    include/HUB-Monitoring/LanDefMonitoring.h \
    include/Modbus/Modbus.h \
    include/ModbusC2000E/ModbusRtuC2000EthernetMaster.h \
    include/ModbusC2000E/QModbusSerialAdu.h \
    include/DB/SimpleFileBA.h \
    include/DB/SimpleFileDB.h \
    include/Net/CanalLink/ServerLink.h \
    include/Net/CanalLocal/CanalLocalServer.h \
    include/Net/CanalSTcp/CanalSTcpServer.h \
    include/Net/LanDef.h \
    include/C2000PP/c2000pp.h \
    include/C2000PP/ConfigC2000PP/ConfigC2000PP.h \
    include/AlreadyRunning/AlreadyRunning.h \
    include/LogFiles/LogFiles.h \
    include/Define.h \
    include/SMTP/simpleSmtp.h \
    include/SMTP/smtp.h \
    include/Telegram/TlgProcess.h \
    include/Telegram/TlgState.h \
    include/Telegram/TlgStr.h

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target

DEFINES += BUILD_FOR_CONSOLE

win32{
  QMAKE_TARGET_PRODUCT     = "HUB-C2000PP"
  QMAKE_TARGET_DESCRIPTION = "Server HUB-C2000PP"
  QMAKE_TARGET_COMPANY     = "www.arm-skif.ru"
  QMAKE_TARGET_COPYRIGHT   = "www.arm-skif.ru"

  RC_LANG     = 0x419
  RC_CODEPAGE = 1251
}

linux:contains(QT_ARCH, arm64|arm) {
    DEFINES += Embedded
    SOURCES += ../src/include/sysconfig/sysconfig.cpp
    HEADERS += ../src/include/sysconfig/sysconfig.h

    SOURCES += ../src/include/sysconfig/tzdata.cpp
    HEADERS += ../src/include/sysconfig/tzdata.h
}
